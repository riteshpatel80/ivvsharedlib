0.2.0 (November, 22, 2019)

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

- new toast view
- chat message icon
# 3.0.4
- participants bug fix
- double help header fixed
# 3.0.3
- changed dependencies to work in kp
# 3.0.2
- downgrade a dependency
# 3.0.1
- leaving participant popup is working.
- added kp_ prefix to all strings to make them unique
- two minute timer in waiting room now implemented
- pop ups are now gone
- added check so popup don't show in survey screen
# 2.2.6
- added endvv callback
- added spanish translations for survey screen
- tabs disappears when in dialout and call ends
# 3.0.0
- fixed endVV to clear everything
- presentation view no longer disappears
- chat crash when moving between fragments fixed
# 2.2.4
- survey fixes
# 2.2.3
- updated to androidx
- added gotham medium text and a few more fixes
- leave appointment bug fixed
- join msg removed from people joining from waiting room
- minor text fix for survey screen
# 2.2.2
- Five star survey feature added
- help security fix made
- join from waiting room works
- changed Yes in spanish to Si
- updated new chat toast message
- added custom banner for participants joining and leaving
## 2.2.1
- Fixed video visit for 4+0 view
- added new message toast for ada talk back
- tabs stay hidden when moving completely to the background and back
- participant update and dialout phone format fixed
- participant delete and update event in now local
- update to internal participants call
- presentation view zoom resets on new presentation
- Added headers and reused leave appointment string.
- Header now displays properly
- orientation can change when presentation goes to full screen
- added ADA scroll talk back text to waiting room
- added confirm message to waiting room leave button and video hang up.
- Fixed the issue of previous shared screen content for a while
- Changed error strings to be more generalized.
- Fixed 4680: Help header is displayed twice for Member
## 2.0.5 was a hotfix
## 2.0.4
- Fixed chat crasher: Attempt to invoke virtual method '...scrollToPosition(int)' on a null object reference
- Fixed NGVV-4420: On hold message is not showing for member participant when they are in the contact picker screen

## 2.0.3
- Fixed chat messages disappearing under input box when dismissing keyboard
- Fixed NGVV-3860: chat messages wouldn't always appear at bottom of the screen
- Fixed NGVV-4160: Accessibility talk-back was speaking through earpiece instead of speakerphone
- Fixed NGVV-4164: Host text in participant list wasn't translated
- Fixed NGVV-3887: Fade out controls disappearing issue
- Added new translation for call in user

## 2.0.2
- 🎉  Bug fixes including envlbl fix for BFFs

## 2.0.1
- 🎉  Bug fixes

## 2.0.0
- no code changes from 1.2.2, just official 2.0.0 version

## 1.2.2
- 🎉  Bug fixes for audio routing issues, notification code moved to tenant app, misc. minor issues

## 1.2.1
- 🎉  Bug fixes for missing provider feed related issues

## 1.2.0
- 🎉  Bug fixes

## 1.1.0
- 🎉  Bug fixes

## 1.0.0

- 🎉 Network issues - changed verbiage for alerts
- 🎉 Removed call-in user numbers in display name
- 🎉 Host ending scenarios
- 🎉 Handle mic and camera permissions 
- 🎉  Bug fixes (including colors, chat, help page, on hold functionality, participant roles, waiting room, etc.)

## 0.2.0
- 🎉 Video Visit call functionality 
- 🎉  Participant list
- 🎉  Audio Management 


## 0.1.0

- 🎉 Initial api #1
