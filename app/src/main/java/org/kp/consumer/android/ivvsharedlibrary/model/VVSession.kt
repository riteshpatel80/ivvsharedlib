package org.kp.consumer.android.ivvsharedlibrary.model

import java.io.Serializable

data class VVSession(
    val token: String?
) : Serializable