package org.kp.consumer.android.ivvsharedlibrary.util

import androidx.core.view.AccessibilityDelegateCompat
import androidx.core.view.ViewCompat
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat
import android.view.View

object AccessibilityUtils {
    fun setFocusToAccessibility(vararg view: View) {
        view.forEach {
            //To stop talkback from saying double tap to activate
            ViewCompat.setAccessibilityDelegate(it, object :
                AccessibilityDelegateCompat() {
                override fun onInitializeAccessibilityNodeInfo(
                    host: View?,
                    info: AccessibilityNodeInfoCompat?
                ) {
                    super.onInitializeAccessibilityNodeInfo(host, info)
                    info?.apply {
                        addAction(AccessibilityNodeInfoCompat.ACTION_FOCUS)
                    }
                }
            })

        }
    }
}