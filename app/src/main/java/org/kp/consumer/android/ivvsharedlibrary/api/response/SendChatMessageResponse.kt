package org.kp.consumer.android.ivvsharedlibrary.api.response

import com.google.gson.annotations.SerializedName

data class SendChatMessageResponse(
    @SerializedName("status")
    val status: String,

    @SerializedName("response")
    val response: String,

    @SerializedName("errors")
    override val errors: List<ResponseError>

) : VVBaseResponse