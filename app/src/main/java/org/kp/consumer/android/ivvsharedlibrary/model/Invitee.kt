package org.kp.consumer.android.ivvsharedlibrary.model

import com.google.gson.annotations.SerializedName


internal data class Invitee(
    @SerializedName("name")
    val name: String,

    @SerializedName("notificationId")
    val notificationId: String,

    @SerializedName("notificationType")
    val notificationType: String
)