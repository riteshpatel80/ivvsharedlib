package org.kp.consumer.android.ivvsharedlibrary.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

internal data class ChatMessageModel(

    @SerializedName("payload")
    val messageBody: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("origin")
    val origin: String,
    @SerializedName("uuid")
    val senderUUID: String,

    var isReceivedMessage: Boolean? = null,
    var timestamp: Long? = null,
    var roleIconResourceId: Int? = null

) : Serializable