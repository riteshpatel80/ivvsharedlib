package org.kp.consumer.android.ivvsharedlibrary.api.task

import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.DialOutRequest
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.DialOutRequestModel
import org.kp.consumer.android.ivvsharedlibrary.api.response.DialOutResponseConverter


internal class DialOutTask(
    dialOutRequestModel: DialOutRequestModel,
    registrationToken: String
) : VVBaseHttpTask(
    DialOutRequest(dialOutRequestModel, registrationToken),
    converter = DialOutResponseConverter()
)