
package org.kp.consumer.android.ivvsharedlibrary.data.source

import com.here.oksse.ServerSentEvent
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.JoinConferenceRequestModel
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.LeaveConferenceRequestModel
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.SurveyRequestModel
import org.kp.consumer.android.ivvsharedlibrary.api.response.*
import org.kp.consumer.android.ivvsharedlibrary.model.DialOut
import org.kp.consumer.android.ivvsharedlibrary.model.Room
import org.webrtc.SessionDescription


class Repository(val dataSource: RemoteDataSource): DataSource {

    override fun setUpConferenceRequest(
        token: String,
        callback: DataSource.GetCallback<SetupConferenceResponse>
    ) {

        dataSource.setUpConferenceRequest(token, callback)

    }


    override fun requestToken(
        room: Room,
        displayName: String,
        callback: DataSource.GetCallback<RequestTokenResponse>, mServerEventListener: ServerSentEvent.Listener
    ) {


        dataSource.requestToken(room, displayName, callback, mServerEventListener);

    }


    override fun refreshToken(
        room: Room,
        token: String,
        callback: DataSource.GetCallback<RefreshTokenResponse>
    ) {

        dataSource.refreshToken(room, token, callback)
    }


    override fun makeCall(
        room: Room,
        participantUUID: String,
        sdp: SessionDescription,
        callback: DataSource.GetCallback<CallResponse>
    ) {
        dataSource.makeCall(room, participantUUID, sdp, callback)
    }


    override fun addDialOutRequest(
        dialOut: DialOut,
        callback: DataSource.GetCallback<DialOutResponse>
    ) {
        dataSource.addDialOutRequest(dialOut, callback)
    }

    override fun doAck(
        room: Room,
        participantUUID: String,
        callUUID: String,
        callback: DataSource.GetCallback<VVBaseResponse>
    ) {
        dataSource.doAck(room, participantUUID, callUUID, callback)
    }

    override fun getParticipantsRequest(
        uuid: String,
        time: String,
        source: String,
        event: String,
        callback: DataSource.GetCallback<GetParticipantsResponse>
    ) {
        dataSource.getParticipantsRequest(uuid, time, source, event, callback)
    }


    override fun joinConference(
        joinConfRequestModel: JoinConferenceRequestModel,
        registrationToken: String,
        callback: DataSource.GetCallback<JoinConferenceResponse>
    ) {
        dataSource.joinConference(joinConfRequestModel, registrationToken, callback)
    }

    override fun leaveConference(
        leaveConferenceRequestModel: LeaveConferenceRequestModel,
        registrationToken: String,
        callback: DataSource.GetCallback<LeaveConferenceResponse>
    ) {
        dataSource.leaveConference(leaveConferenceRequestModel, registrationToken, callback)
    }

    override fun sendChatMessage(message: String, callback: DataSource.GetCallback<VVBaseResponse>) {
        dataSource.sendChatMessage(message, callback)
    }

    override fun ivvRenewToken(token: String, callback: DataSource.GetCallback<VVBaseResponse>) {

        dataSource.ivvRenewToken(token, callback)
    }

    override fun disconnect(
        room: Room,
        participantUUID: String,
        token: String,
        callUUID: String,
        callback: DataSource.GetCallback<VVBaseResponse>
    ) {
        dataSource.disconnect(room, participantUUID, token, callUUID, callback)
    }

    override fun getPexipUpdatedToken(): String {
        return dataSource.getPexipUpdatedToken()
    }

    override fun sendSurvey(
        surveyRequestModel: SurveyRequestModel,
        callback: DataSource.GetCallback<VVBaseResponse>
    ) {
        dataSource.sendSurvey(surveyRequestModel,callback)
    }

    companion object {

        private var INSTANCE: Repository? = null

        /**
         * Returns the single instance of this class, creating it if necessary.

         * @param remoteDataSource the backend data source
         * *
         *
         * *
         * @return the [Repository] instance
         */
        @JvmStatic
        fun getInstance(remoteDataSource: RemoteDataSource) =
                INSTANCE
                        ?: synchronized(Repository::class.java) {
                    INSTANCE
                            ?: Repository(remoteDataSource)
                            .also { INSTANCE = it }
                }


        /**
         * Used to force [getInstance] to create a new instance
         * next time it's called.
         */
        @JvmStatic
        fun destroyInstance() {
            INSTANCE = null
        }
    }
}