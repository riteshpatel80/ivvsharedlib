package org.kp.consumer.android.ivvsharedlibrary.util

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Build
import org.kp.consumer.android.ivvsharedlibrary.util.Log
import org.webrtc.ThreadUtils
import androidx.core.content.ContextCompat.getSystemService

/**
 * AppProximitySensor manages functions related to the proximity sensor in
 * the AppRTC demo.
 * On most device, the proximity sensor is implemented as a boolean-sensor.
 * It returns just two values "NEAR" or "FAR". Thresholding is done on the LUX
 * value i.e. the LUX value of the light sensor is compared with a threshold.
 * A LUX-value more than the threshold means the proximity sensor returns "FAR".
 * Anything less than the threshold value and the sensor  returns "NEAR".
 */
class AppProximitySensor(val context: Context, var onSensorStateListener: Runnable) :
    SensorEventListener {
    val TAG = this.javaClass.simpleName

    val sensorManager: SensorManager =
        context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    var proximitySensor: Sensor? = null
    var lastStateReportIsNear: Boolean = false

    /**
     * Activate the proximity sensor. Also do initialization if called for the
     * first time.
     */
    fun start(): Boolean {
        if (!initDefaultSensor()) {
            return false
        }
        sensorManager.registerListener(this, proximitySensor, SensorManager.SENSOR_DELAY_NORMAL)
        return true;
    }

    /** Deactivate the proximity sensor. */
    fun stop() {
        if (proximitySensor == null) {
            return;
        }
        sensorManager.unregisterListener(this, proximitySensor);
    }

    /** Getter for last reported state. Set to true if "near" is reported. */
    fun sensorReportsNearState(): Boolean {
        return lastStateReportIsNear
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        if (accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE) {
            Log.e(TAG, "The values returned by this sensor cannot be trusted")
        }
    }

    override fun onSensorChanged(event: SensorEvent) {
        val distanceInCentimeters: Float = event?.values[0]
        if (distanceInCentimeters < proximitySensor!!.maximumRange) {
            Log.d(TAG, "Proximity sensor => NEAR state")
            lastStateReportIsNear = true
        } else {
            Log.d(TAG, "Proximity sensor => FAR state")
            lastStateReportIsNear = false
        }

        // Report about new state to listening client. Client can then call
        // sensorReportsNearState() to query the current state (NEAR or FAR).
        if (onSensorStateListener != null) {
            onSensorStateListener.run()
        }
    }

    /**
     * Get default proximity sensor if it exists. Tablet devices (e.g. Nexus 7)
     * does not support this type of sensor and false will be returned in such
     * cases.
     */
    fun initDefaultSensor(): Boolean {
        if (proximitySensor != null) {
            return true
        }
        proximitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        if (proximitySensor == null) {
            return false
        }
        return true
    }
}