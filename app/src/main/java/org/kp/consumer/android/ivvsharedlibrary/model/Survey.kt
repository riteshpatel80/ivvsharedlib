package org.kp.consumer.android.ivvsharedlibrary.model

import com.google.gson.annotations.SerializedName

data class Survey (

    @SerializedName("rating")
    val rating : String?,
    @SerializedName("concerns")
    val concerns : List<String>?,
    @SerializedName("comments")
    val comments : String?,
    @SerializedName("osName")
    val osName : String = "",
    @SerializedName("osVersion")
    val osVersion : String = "",
    @SerializedName("browserName")
    val browserName : String = "",
    @SerializedName("browseVersion")
    val browseVersion : String = ""
    )
