
package org.kp.consumer.android.ivvsharedlibrary.features.waitingRoom
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import android.view.LayoutInflater
import org.kp.consumer.android.ivvsharedlibrary.R
import org.kp.consumer.android.ivvsharedlibrary.databinding.WaitingProviderItemBinding
import org.kp.consumer.android.ivvsharedlibrary.model.Provider


class WaitingRoomProviderAdapter: RecyclerView.Adapter<WaitingRoomProviderAdapter.WaitingRoomProviderViewHolder>() {

    var provider = emptyList<Provider>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WaitingRoomProviderViewHolder {
        val binding: WaitingProviderItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.waiting_provider_item, parent, false)
        return WaitingRoomProviderViewHolder(binding)
    }

    override fun getItemCount() = provider.size

    override fun onBindViewHolder(holder: WaitingRoomProviderViewHolder, position: Int) {
        val item = provider[position]
        holder.bind(item)
    }

    fun setData(items: List<Provider>) {
        provider = items
        notifyDataSetChanged()
    }

    inner class WaitingRoomProviderViewHolder(private val binding: WaitingProviderItemBinding) : RecyclerView.ViewHolder(binding.root)  {
        var itemRowBinding: WaitingProviderItemBinding

        init{

            itemRowBinding = binding

        }

        fun bind(item: Provider) {
            binding.setItem(item)
            binding.executePendingBindings()
        }

    }


}