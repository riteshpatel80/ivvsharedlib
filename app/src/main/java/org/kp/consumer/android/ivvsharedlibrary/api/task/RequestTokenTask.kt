package org.kp.consumer.android.ivvsharedlibrary.api.task

import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.RequestTokenRequest
import org.kp.consumer.android.ivvsharedlibrary.api.response.RequestTokenResponse
import org.kp.consumer.android.ivvsharedlibrary.api.response.RequestTokenResponseConverter

internal class RequestTokenTask(pexipHost:String,roomAlias: String, mDisplayName: String,pin:String) : VVBaseHttpTask(

    RequestTokenRequest(pexipHost,roomAlias,mDisplayName,pin),
    converter = RequestTokenResponseConverter()
)