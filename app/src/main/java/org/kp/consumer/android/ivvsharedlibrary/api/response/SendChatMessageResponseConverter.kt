package org.kp.consumer.android.ivvsharedlibrary.api.response

internal class SendChatMessageResponseConverter : Converter {
    override fun convert(responseJson: String): SendChatMessageResponse {
        return Converter.convertFromJson(responseJson)
    }
}