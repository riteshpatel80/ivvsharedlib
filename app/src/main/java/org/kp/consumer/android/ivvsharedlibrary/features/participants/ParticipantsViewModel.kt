package org.kp.consumer.android.ivvsharedlibrary.features.participants

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.kp.consumer.android.ivvsharedlibrary.util.Log
import kotlinx.coroutines.*
import org.kp.consumer.android.ivvsharedlibrary.Event
import org.kp.consumer.android.ivvsharedlibrary.api.response.GetParticipantsResponse
import org.kp.consumer.android.ivvsharedlibrary.api.response.ResponseResult
import org.kp.consumer.android.ivvsharedlibrary.data.source.DataSource
import org.kp.consumer.android.ivvsharedlibrary.data.source.RemoteDataSource
import org.kp.consumer.android.ivvsharedlibrary.data.source.Repository
import org.kp.consumer.android.ivvsharedlibrary.features.VideoViewModel
import org.kp.consumer.android.ivvsharedlibrary.features.VideoViewModel.Companion.KP_MEMBER_MOBILE_APP_ANDROID
import org.kp.consumer.android.ivvsharedlibrary.model.Participant
import org.kp.consumer.android.ivvsharedlibrary.util.CHAIR

internal class ParticipantsViewModel(private val tasksRepository: Repository) : ViewModel(){

    private val viewModelJob = Job()

    private val viewModelScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private val dispatcher = Dispatchers.IO

    val chatItems = MutableLiveData<Event<MutableList<Participant>>>()
    val participantItems = MutableLiveData<Event<MutableList<Participant>>>()
    val participantsItemsObserbale = MutableLiveData<Event<MutableList<Participant>>>()
    val participantsListWaitingCheck = MutableLiveData<Event<MutableList<Participant>>>()
    var participantList: List<Participant>? = null

    private val _dialogCloseEvent = MutableLiveData<Event<Unit>>()
    val dialogCloseEvent: LiveData<Event<Unit>>
        get() = _dialogCloseEvent


    private val _errorMessage = MutableLiveData<Event<ResponseResult>>()
    val errorMesssage: LiveData<Event<ResponseResult>>
        get() = _errorMessage


    val participantCallback =  object : DataSource.GetCallback<GetParticipantsResponse> {
        override fun onSuccess() {
        }

        override fun onSuccess(value: GetParticipantsResponse) {
            Log.d(ParticipantsViewModel::class.java.simpleName,"List size" + value.count)

            // make the call again if the values returned aren't there
            var participantCallGetsNullValues = false
            for (participant in value.participantList){
                if (participant.region == "null" || participant.channelId == "null" || participant.role == "null"){
                    participantCallGetsNullValues = true
                }
            }
            viewModelScope.launch {
                Log.d(this@ParticipantsViewModel.javaClass.simpleName, value.participantList.toString())
                if (participantCallGetsNullValues) {
                    getParticipants()
                }
                setList(value.participantList)
            }
        }

        override fun onFailure(errrorCode: ResponseResult.Failure) {
            Log.e(ParticipantsViewModel::class.java.simpleName,"ErrorCode: ${errrorCode.errorCode}")
            viewModelScope.launch {
                _dialogCloseEvent.value = Event((Unit))
            }

        }

        override fun onFailure(value: ResponseResult.ServiceError) {
            Log.e(ParticipantsViewModel::class.java.simpleName,"ErrorCode: ${value.error.code} Error: ${value.error.errorDescription}")
            viewModelScope.launch {
                _dialogCloseEvent.value = Event((Unit))
            }
        }
    }


    fun getParticipants(uuid: String = "0", time: String = System.currentTimeMillis().toString(), source: String = KP_MEMBER_MOBILE_APP_ANDROID, event: String = "NONE"){
        viewModelScope.launch(dispatcher) {
            tasksRepository.getParticipantsRequest(uuid,time,source,event, participantCallback)
        }

    }

    fun deleteParticipant(uuid:String) : Boolean{
        if (participantList.isNullOrEmpty()) return true
        val newParticipantList:ArrayList<Participant> = arrayListOf()
        participantList?.let {
            for (participant in it){
                if (participant.uuid != uuid){
                    newParticipantList.add(participant)
                }
            }
        }
        participantList?.let {
            val didListChange = it.size > newParticipantList.size
            if (didListChange) {
                setList(newParticipantList)
                return false
            }
        }
        return true
    }


    fun setList(list : List<Participant>) {
        participantList = list
        participantItems.value = Event(list.toMutableList())
        chatItems.value = Event(list.toMutableList())
        participantsItemsObserbale.value = Event(list.toMutableList())
        participantsListWaitingCheck.value = Event(list.toMutableList())

    }


    fun doesChairExits(it: MutableList<Participant>) :Boolean {
        it.forEach {
            if (it.vmrRole == CHAIR) {
                return true
            }
        }
        return false
    }

    fun updateParticipantMute(uuid: String, ismuted : Boolean) : Boolean {
        if (participantList.isNullOrEmpty()) return true
        val newParticipantList:ArrayList<Participant> = arrayListOf()
        var muteChanged = false
        participantList?.let {
            for (participant in it){
                if (participant.uuid == uuid){
                    participant.isMuted = ismuted
                    muteChanged = true
                }
                newParticipantList.add(participant)
            }
        }
        if (muteChanged) {
            setList(newParticipantList)
            return false
        }
        return true
    }
}