package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig

import com.google.gson.Gson


internal class LeaveConferenceRequest(leaveConfRequestModel: LeaveConferenceRequestModel, registrationToken: String) : VVBaseRequest(REQUEST_TYPE.PUT, JOIN_CONF_URL_PATH) {

    init {
        addHeader(AUTHORIZATION, "$BEARER $registrationToken")
        addParam(TOKEN_KEY, registrationToken)

        body = Gson().toJson(leaveConfRequestModel)
    }

    companion object {
        private const val JOIN_CONF_URL_PATH = "/leave"
    }
}