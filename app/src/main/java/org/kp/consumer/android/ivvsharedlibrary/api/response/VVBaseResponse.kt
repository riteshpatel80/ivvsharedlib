package org.kp.consumer.android.ivvsharedlibrary.api.response

import java.io.Serializable


interface VVBaseResponse : Serializable {
    val errors: List<ResponseError>?

    fun hasErrors() : Boolean {
        return !errors.isNullOrEmpty()
    }
}