package org.kp.consumer.android.ivvsharedlibrary.util

import android.content.Context
import android.net.ConnectivityManager

object NetworkUtils {

    fun isInternetAvailable(context: Context): Boolean? {
        val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val info = manager.activeNetworkInfo
        return info?.isConnected
    }

}