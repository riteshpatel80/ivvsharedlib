package org.kp.consumer.android.ivvsharedlibrary.api.task

import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.DisconnectRequest
import org.kp.consumer.android.ivvsharedlibrary.api.response.GenericConverter

/**
 * Created by venkatakalluri on 2019-12-03.
 * Copyright © 2019 Kaiser Permanente. All rights reserved.
 *
 */

internal class DisconnectTask (pexipHost: String,pin: String, roomAlias: String,participantUUID: String,token:String,callUUID: String): VVBaseHttpTask(
    DisconnectRequest(pexipHost,pin, roomAlias,participantUUID,token,callUUID),
    converter = GenericConverter()
) {
}