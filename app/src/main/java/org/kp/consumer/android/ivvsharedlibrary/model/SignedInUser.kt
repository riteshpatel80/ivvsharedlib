package org.kp.consumer.android.ivvsharedlibrary.model

import com.google.gson.annotations.SerializedName


data class SignedInUser(

    @SerializedName("id")
    val id : String,

    //TODO change back to val when returned by service
    @SerializedName("idType")
    var idType : String,

    @SerializedName("name")
    val name : String,

    @SerializedName("role")
    val role : String,

    @SerializedName("region")
    val region : String,

    @SerializedName("languagePreference")
    val languagePreference : String,

    @SerializedName("applicationId")
    val applicationId : String,

    @SerializedName("channelId")
    val channelId : String,

    @SerializedName("registerId")
    val registerId: String,

    @SerializedName("pexipParticipantUUID")
    var pexipParticipantUUID: String
)