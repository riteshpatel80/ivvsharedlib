package org.kp.consumer.android.ivvsharedlibrary.controller


import android.annotation.SuppressLint
import android.app.Notification
import android.content.Context
import android.os.Handler
import androidx.fragment.app.FragmentActivity
import kotlin.coroutines.Continuation
import kotlin.coroutines.suspendCoroutine
import kotlin.coroutines.resume
import org.kp.consumer.android.ivvsharedlibrary.MainActivity
import org.kp.consumer.android.ivvsharedlibrary.data.source.RemoteDataSource
import org.kp.consumer.android.ivvsharedlibrary.error.VVError
import org.kp.consumer.android.ivvsharedlibrary.features.VideoFragment
import org.kp.consumer.android.ivvsharedlibrary.features.waitingRoom.WaitingRoomFragment
import org.kp.consumer.android.ivvsharedlibrary.model.*
import org.kp.consumer.android.ivvsharedlibrary.util.VVLogLevel
import org.kp.kpnetworking.dispatcher.RequestHandler
import org.kp.kpnetworking.dispatcher.RequestHandlerSingleton


@SuppressLint("StaticFieldLeak")
object VVSessionController : DefaultVVSessionSessionController {

    private lateinit var waitForActivity: Continuation<MainActivity>
    private lateinit var mContext: Context
    private lateinit var activity: MainActivity
    private lateinit var mVVEnvironment: VVEnvironmentConfig
    private lateinit var mVVTenantInfo: VVClientInfo
    private var mVVEventHandler: VVEventHandler? = null
    var vvEventHandler: VVEventHandler? = null
        get() = mVVEventHandler

    private var mVVConferenceToken: String? = null
    private var isVVHost: Boolean = false
    lateinit var mVVNotification: Notification


    fun initialize(
        context: Context,
        vvEnvironmentConfig: VVEnvironmentConfig,
        vvClientInfo: VVClientInfo,
        vvEventHandler: VVEventHandler?
    ): DefaultVVSessionSessionController {
        mContext = context
        mVVEnvironment = vvEnvironmentConfig
        mVVTenantInfo = vvClientInfo
        mVVEventHandler = vvEventHandler


        RemoteDataSource.baseurl = mVVEnvironment.baseURL
        RemoteDataSource.clientId = mVVEnvironment.clientId
        RemoteDataSource.envlbl = mVVEnvironment.envLabel


        val requestHandlerBuilder = RequestHandler.RequestHandlerBuilder()
        requestHandlerBuilder.supportCookies(true)
        RequestHandlerSingleton.init(requestHandlerBuilder)




        return this
    }


    /**
     *  Sets the Logging Level
     *
     * @param logLevel
     */
    override suspend fun setLogLevel(logLevel: VVLogLevel) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     *  Starts the VV
     *
     * @param vvConferenceToken
     * @param isHost
     * @param notification
     * @return the result, containing a VVSession if successful or VVError if unsuccessful
     */
    override suspend fun startVV(
        layoutId: Int,
        vvConferenceToken: String,
        isHost: Boolean,
        notification: Notification
    ): VVResult<VVSession, VVError> {

        RemoteDataSource.vvConferenceToken = vvConferenceToken
        isVVHost = isHost
        mVVNotification = notification

        activity = suspendCoroutine {
            waitForActivity = it
            startFragment(layoutId, vvConferenceToken, isHost)
        }

        mContext = activity

        return suspendCoroutine { cont ->
            startVVWith(vvConferenceToken, isHost, cont)
        }
    }

    /**
     *  Retrieves the Bandwidth Information while in a Video Visit
     *
     * @return the bandwsithd information.  0 if no video visit is running
     */
    override suspend fun getVVBandwidth(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     *  Generic method to Sends a message into the Controller
     *
     * @param message
     */
    override suspend fun sendMessage(message: Any): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     *  Ends the VV
     *
     *
     */
    override suspend fun endVV() {


        if (mVVConferenceToken != null) {

            var videoFragment =
                (mContext as androidx.fragment.app.FragmentActivity).supportFragmentManager.findFragmentByTag(
                    VideoFragment.TAG
                );

            videoFragment?.let {
                (videoFragment as VideoFragment).stopVideoCall(mVVConferenceToken!!)
            } ?: run {

                var waitingRoomFragmentFragment =
                    (mContext as androidx.fragment.app.FragmentActivity).supportFragmentManager.findFragmentByTag(
                        WaitingRoomFragment.TAG
                    );
                waitingRoomFragmentFragment?.let {
                    (waitingRoomFragmentFragment as WaitingRoomFragment).stopVideoCall(
                        mVVConferenceToken!!
                    )

                }


            }


        }
    }

    private fun startFragment(containerId: Int, vvConferenceToken: String, isHost: Boolean) {

        mVVConferenceToken = vvConferenceToken

        (mContext as FragmentActivity).supportFragmentManager.beginTransaction()
            .replace(
                containerId,
                WaitingRoomFragment.newInstance(vvConferenceToken, containerId),
                WaitingRoomFragment.TAG
            )
            .commit();


    }

    private fun startVVWith(
        vvConferenceToken: String,
        isHost: Boolean,
        cont: Continuation<VVResult<VVSession, VVError>>
    ) {

        var vvSession: VVResult<VVSession, VVError> =
            Success(VVSession(vvConferenceToken));

        cont.resume(vvSession)

    }

    private fun activityReady(activity: MainActivity) {
        waitForActivity.resume(activity)
    }


}