
package org.kp.consumer.android.ivvsharedlibrary.features.waitingRoom
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import android.view.LayoutInflater
import org.kp.consumer.android.ivvsharedlibrary.R
import org.kp.consumer.android.ivvsharedlibrary.databinding.WaitingParticipantItemBinding
import org.kp.consumer.android.ivvsharedlibrary.model.Participant


class WaitingRoomPatientAdapter: RecyclerView.Adapter<WaitingRoomPatientAdapter.WaitingRoomPatientViewHolder>() {

    var provider = emptyList<Participant>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WaitingRoomPatientViewHolder {
        val binding: WaitingParticipantItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.waiting_participant_item, parent, false)
        return WaitingRoomPatientViewHolder(binding)
    }

    override fun getItemCount() = provider.size

    override fun onBindViewHolder(holder: WaitingRoomPatientViewHolder, position: Int) {
        val item = provider[position]
        holder.bind(item)
    }

    fun setData(items: List<Participant>) {
        provider = items
        notifyDataSetChanged()
    }

    inner class WaitingRoomPatientViewHolder(private val binding: WaitingParticipantItemBinding) : RecyclerView.ViewHolder(binding.root)  {
        var itemRowBinding: WaitingParticipantItemBinding

        init{

            itemRowBinding = binding

        }

        fun bind(item: Participant) {
            binding.setItem(item)
            binding.executePendingBindings()
        }

    }


}