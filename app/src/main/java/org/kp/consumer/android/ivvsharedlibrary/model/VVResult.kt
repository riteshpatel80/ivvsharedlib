package org.kp.consumer.android.ivvsharedlibrary.model

/**
 * Result class, like an enum but with mapping functions that support railway oriented programming
 *
 * @param A the Success case
 * @param B the Failure case
 */
sealed class VVResult<A, B> {
    abstract fun <C> map(mapping: (A) -> C): VVResult<C, B>
    abstract fun <C> flatMap(mapping: (A) -> VVResult<C, B>): VVResult<C, B>
    abstract fun <C> mapFailure(mapping: (B) -> C): VVResult<A, C>
    abstract fun <C> flatMapFailure(mapping: (B) -> VVResult<A, C>): VVResult<A, C>
    abstract fun orElse(other: A): A
    abstract fun orElse(function: (B) -> A): A
}

data class Success<A, B>(val value: A) : VVResult<A, B>() {
    override fun <C> map(mapping: (A) -> C): VVResult<C, B> = Success(mapping(value))
    override fun <C> flatMap(mapping: (A) -> VVResult<C, B>): VVResult<C, B> = mapping(value)
    override fun <C> mapFailure(mapping: (B) -> C): VVResult<A, C> = Success(value)
    override fun <C> flatMapFailure(mapping: (B) -> VVResult<A, C>): VVResult<A, C> = Success(value)
    override fun orElse(other: A): A = value
    override fun orElse(function: (B) -> A): A = value
}

data class Error<A, B>(val value: B) : VVResult<A, B>() {
    override fun <C> map(mapping: (A) -> C): VVResult<C, B> = Error(value)
    override fun <C> flatMap(mapping: (A) -> VVResult<C, B>): VVResult<C, B> = Error(value)
    override fun <C> mapFailure(mapping: (B) -> C): VVResult<A, C> = Error(mapping(value))
    override fun <C> flatMapFailure(mapping: (B) -> VVResult<A, C>): VVResult<A, C> = mapping(value)
    override fun orElse(other: A): A = other
    override fun orElse(function: (B) -> A): A = function(value)
}