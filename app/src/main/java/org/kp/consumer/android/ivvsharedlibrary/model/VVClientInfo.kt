package org.kp.consumer.android.ivvsharedlibrary.model

import java.io.Serializable

data class VVClientInfo(var appName: String, var appVersion: String) : Serializable {
    val appNameAndVersion: String
        get() = appName + " " + appVersion
}