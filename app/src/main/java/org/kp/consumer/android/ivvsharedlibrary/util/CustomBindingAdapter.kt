package org.kp.consumer.android.ivvsharedlibrary.util

import androidx.databinding.BindingAdapter
import android.os.Build

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import org.kp.consumer.android.ivvsharedlibrary.R
import org.kp.consumer.android.ivvsharedlibrary.model.Participant

import java.text.SimpleDateFormat
import java.util.*

class CustomBindingAdapter {
    companion object {
        @JvmStatic
        @BindingAdapter("bind:roleAvatar")
        fun setRoleAvatar(view: ImageView, item: Participant?) {
            if (item?.host == true){
                setImageAndDescription(
                    view,
                    R.drawable.participants_host,
                    R.string.kp_host_provider
                )
                return
            }
            when (item?.role) {
                Role.PROVIDER.role -> {
                    setImageAndDescription(
                        view,
                        R.drawable.doctor_icon,
                        R.string.kp_provider
                    )
                }
                Role.PATIENT.role -> {
                    setImageAndDescription(view, R.drawable.primary_patient, R.string.kp_patient)
                }
                Role.CONSULTING_CLINICIAN.role -> {
                    setImageAndDescription(view, R.drawable.doctor_icon, R.string.kp_provider)
                }
                Role.DIAL_OUT.role -> {
                    setImageAndDescription(
                        view,
                        R.drawable.call_in_user,
                       null
                    )
                }
                Role.DIAL_IN.role -> {
                    setImageAndDescription(
                        view,
                        R.drawable.call_in_user,
                        null
                    )
                }
                Role.INTERPRETER.role -> {
                    setImageAndDescription(view, R.drawable.interpreter, null)
                }
                Role.PROVIDER_NURSE.role -> {
                    setImageAndDescription(view, R.drawable.doctor_icon, R.string.kp_provider)
                }Role.MEMBER_PROXY.role -> {
                    setImageAndDescription(view, R.drawable.secondary_patient, R.string.kp_member_proxy)
                }
                else -> {
                    setImageAndDescription(view, R.drawable.secondary_patient, R.string.kp_guest)
                }
            }
        }

        private fun setImageAndDescription(view: ImageView, image: Int, description: Int?) {
            view.setImageResource(image)
            // used for caching the image in chat fragment
            view.setTag(R.id.image_resource_id, image)
            if (description != null) view.contentDescription = view.context.resources.getString(description)
            else view.contentDescription = null
        }

        @JvmStatic
        @BindingAdapter("bind:host")
        fun sethost(view: TextView, isHost: Boolean) {
            if (isHost) {
                view.visibility = View.VISIBLE
            } else {
                view.visibility = View.GONE
            }
        }

        @JvmStatic
        @BindingAdapter("bind:datetime")
        fun setDateTime(isMember: Boolean, view: TextView, time: String?) {
            val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US)
            formatter.timeZone = TimeZone.getTimeZone("UTC")
            Log.d(this.javaClass.simpleName,  time)
            val result = formatter.parse(time)
            val dateFormated = SimpleDateFormat("h:mma", Locale.US)
            val dateFormat = dateFormated.format(result)

              var msg = view.context.resources.getString(R.string.kp_your_msg)
              if(!isMember){
                  msg = view.context.resources.getString(R.string.kp_this_msg)
              }

            val timeTextFull = "$msg ${view.context.resources.getString(R.string.kp_appointment_time)} $dateFormat"
            view.text = timeTextFull
        }

        @JvmStatic
        @BindingAdapter("bind:name")
        fun setName(view: TextView, item: Participant?) {
            item?.let {
                if (it.role == Role.DIAL_OUT.role) {
                    view.text = view.resources.getString(R.string.kp_call_in_user)
                } else if (it.name == INTERPRETER) {
                    view.text = view.resources.getString(R.string.kp_interpreter)
                }
                if (it.host) {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                        view.setTextAppearance(view.context, R.style.ParticipantHostsStyle)
                    } else {
                        view.setTextAppearance(R.style.ParticipantHostsStyle)
                    }
                } else {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                        view.setTextAppearance(view.context, R.style.ParticipantsStyle)
                    } else {
                        view.setTextAppearance(R.style.ParticipantsStyle)
                    }
                }
            }
        }
    }
}