package org.kp.consumer.android.ivvsharedlibrary.api.task

import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.RefreshTokenRequest
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.RequestTokenRequest
import org.kp.consumer.android.ivvsharedlibrary.api.response.RefreshTokenResponseConverter
import org.kp.consumer.android.ivvsharedlibrary.api.response.RequestTokenResponseConverter

internal class RefreshTokenTask(pexipHost:String,roomAlias: String,pin:String, token: String): VVBaseHttpTask(
    RefreshTokenRequest(pexipHost,roomAlias,pin, token),
    converter = RefreshTokenResponseConverter()
) {
}