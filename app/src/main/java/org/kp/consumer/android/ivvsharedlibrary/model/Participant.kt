package org.kp.consumer.android.ivvsharedlibrary.model

import com.google.gson.annotations.SerializedName


data class Participant(

    @SerializedName("name")
    val name: String,

    @SerializedName("id")
    val id: String,

    @SerializedName("idType")
    val idType: String,

    @SerializedName("UUID")
    val uuid: String,

    @SerializedName("role")
    val role: String,

    @SerializedName("vmrRole")
    val vmrRole: String,

    @SerializedName("host")
    val host: Boolean,

    @SerializedName("region")
    val region: String,

    @SerializedName("applicationId")
    val applicationId: String,

    @SerializedName("channelId")
    val channelId: String,

    @SerializedName("callDirection")
    val callDirection: String,

    @SerializedName("callQuality")
    val callQuality: String,

    @SerializedName("hasMediatrue")
    val hasMedia: Boolean,

    @SerializedName("isMuted")
    var isMuted: Boolean,

    @SerializedName("isOnHold")
    val isOnHold: Boolean,

    @SerializedName("isStreaming")
    val isStreaming: Boolean
)