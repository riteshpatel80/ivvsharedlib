package org.kp.consumer.android.ivvsharedlibrary.util

import org.kp.consumer.android.ivvsharedlibrary.R

object ErrorMessages {

    fun getErrorMessages(errorCode: Int): Int {


        return when (errorCode) {

            0 -> R.string.kp_drop_unexpectedly

            2000 -> R.string.kp_unable_to_register

            2001 -> R.string.kp_visit_ended

            2002 -> R.string.kp_unable_to_register

            2003 -> R.string.kp_visit_ended

            2004 -> R.string.kp_visit_ended

            2010 -> R.string.kp_unable_to_register

            2011 -> R.string.kp_system_error_2

            2012 -> R.string.kp_system_error_2

            2020 -> R.string.kp_system_error_2

            2021 -> R.string.kp_system_error_2

            2022 -> R.string.kp_system_error_2
            2030 -> R.string.kp_system_error_2

            2040 -> R.string.kp_system_error_2

            2050 -> R.string.kp_unable_to_dialout_the_number

            2060 -> R.string.kp_unable_to_discover_participant

            2070 -> R.string.kp_unable_to_invite_user

            2071 -> R.string.kp_system_error_2

            2080 -> R.string.kp_unable_to_invite_user

            2081 -> R.string.kp_system_error_2

            2082 -> R.string.kp_provider_not_registered_in_region

            2090 -> R.string.kp_unable_to_transfer_host

            2100 -> R.string.kp_unable_to_acquire_host

            2101 -> R.string.kp_role_not_allowed_to_acquire_host

            2110 -> R.string.kp_unable_to_get_clinician_invite_Status

            else -> R.string.kp_system_error_2

        }


    }


}