package org.kp.consumer.android.ivvsharedlibrary.util


import android.content.Context
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import android.view.View
import android.widget.TextView

import org.kp.consumer.android.ivvsharedlibrary.R



object DialogUtils {
    fun createProgressDialog(context: Context, @StringRes message: Int, cancellable: Boolean): AlertDialog {
        val layout = View.inflate(context, R.layout.layout_progress_dialog, null)
        val tvMessage = layout.findViewById<TextView>(R.id.tvMessage)
        tvMessage.setText(message)
        return AlertDialog.Builder(context)
                .setView(layout)
                .setCancelable(cancellable)
                .create()
    }

    fun showOkCancelDialog(context: Context, @StringRes message: Int, cancellable: Boolean, listener: DialogButtonClickListener) {
        AlertDialog.Builder(context)
                .setMessage(message)
                .setCancelable(cancellable)
                .setPositiveButton(R.string.kp_common_yes) { dialog, which -> listener.onYesClicked() }
                .setNegativeButton(R.string.kp_common_cancel) { dialog, which -> }
                .create()
                .show()
    }

    fun showOkCancelDialog(context: Context, message: String, cancellable: Boolean, listener: DialogButtonClickListener) {
        AlertDialog.Builder(context)
            .setMessage(message)
            .setCancelable(cancellable)
            .setPositiveButton(R.string.kp_common_yes) { dialog, which -> listener.onYesClicked() }
            .setNegativeButton(R.string.kp_common_cancel) { dialog, which -> }
            .create()
            .show()
    }
    fun showOkDialog(context: Context, message: String, cancellable: Boolean,listener: DialogButtonClickListener?= null) {
        AlertDialog.Builder(context)
            .setMessage(message)
            .setCancelable(cancellable)
            .setPositiveButton(R.string.kp_ok){ dialog, which -> listener?.onYesClicked() }
            .create()
            .show()
    }
    fun showOkDialog(context: Context, @StringRes message: Int, cancellable: Boolean,listener: DialogButtonClickListener?= null) {
        AlertDialog.Builder(context)
            .setMessage(message)
            .setCancelable(cancellable)
            .setPositiveButton(R.string.kp_ok){ dialog, which -> listener?.onYesClicked() }
            .create()
            .show()
    }

    interface DialogButtonClickListener {
        fun onYesClicked()
    }
}
