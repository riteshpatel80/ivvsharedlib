package org.kp.consumer.android.ivvsharedlibrary.util

const val HTTPS = "https"
const val MUST_BE_HTTPS = "URL must be set with a secure ('https') protocol"
const val GUEST = "Guest"
const val PAUSE_MESSAGE = "KPCustomMessage:EVENT_ON_HOLD"
const val MUTE_MESSAGE = "KPCustomMessage:EVENT_MUTE_AUDIO"
const val EVENT_MODERATOR_CHANGED = "KPCustomMessage: EVENT_MODERATOR_CHANGED"
const val CHAIR="chair"
const val INTERPRETER = "INTERPRETER"
const val YES = "YES"
const val NO = "NO"
const val TEST_SHOULD_HAVE_THROWN_EXEPTION = "Test should have thrown exception"