package org.kp.consumer.android.ivvsharedlibrary.features.dialout

import android.Manifest
import android.app.Activity
import androidx.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.Rect
import android.os.Bundle
import android.provider.ContactsContract
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import android.telephony.PhoneNumberFormattingTextWatcher
import org.kp.consumer.android.ivvsharedlibrary.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_video.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.dialout_fragment.*
import org.kp.consumer.android.ivvsharedlibrary.R
import org.kp.consumer.android.ivvsharedlibrary.ViewModelFactory
import org.kp.consumer.android.ivvsharedlibrary.databinding.DialoutFragmentBinding
import org.kp.consumer.android.ivvsharedlibrary.features.VideoFragment
import org.kp.consumer.android.ivvsharedlibrary.model.DialOut
import org.kp.consumer.android.ivvsharedlibrary.util.AccessibilityUtils
import org.kp.consumer.android.ivvsharedlibrary.util.DialogUtils

class DialoutFragment : Fragment() {

    private var viewModel: DialoutViewModel? = null
    private val REQUEST_PERMISSIONS_CODE_CONTACT_PERMISSIONS = 1

    private var activity: Activity? = null
    override fun onAttach(context: Context) {
        super.onAttach(context)

        activity = context as Activity
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.dialout_fragment, container, false)

        DialoutFragmentBinding.bind(view).apply {
            viewModel = activity?.run {
                ViewModelProvider(
                    requireActivity(),
                    ViewModelFactory.getInstance(this.application)
                )
                    .get(DialoutViewModel::class.java)

            }
        }
        viewModel?.dialoutResult?.observe(viewLifecycleOwner, Observer {
            it?.getContentIfNotHandled()?.let {
                if (it.equals(viewModel?.SUCCESS)) {
                    activity?.let { it ->
                        DialogUtils.showOkDialog(
                            it,
                            resources.getString(R.string.kp_invitation_success),
                            false
                        )
                        resetPhoneText()
                    }

                } else {
                    activity?.let { it ->
                        DialogUtils.showOkDialog(
                            it,
                            resources.getString(R.string.kp_invitation_failed),
                            false
                        )
                    }
                }
            }
        })
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupPermissionsForContacts()
        AccessibilityUtils.setFocusToAccessibility(add_participants_text, phone_number_text)
        add_number.addTextChangedListener(PhoneNumberFormattingTextWatcher())
    }

    private fun hideButtonsWhenKeyboardIsUp() {
        dialout_root_view.viewTreeObserver
            .addOnGlobalLayoutListener(rootViewListener)
    }
    val rootViewListener = ViewTreeObserver.OnGlobalLayoutListener { //when presentation starts while in this fragment, this method to detect the keyboard forces the tabs to stay viisible
        //this conditional makes it so if the phone is in landscape (which means user should be in this tab) then don't run this operation.
        if (activity?.requestedOrientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
            dialout_root_view?.let {
                val r = Rect()
                dialout_root_view.getWindowVisibleDisplayFrame(r)

                val heightDiff = dialout_root_view.rootView.height - (r.bottom - r.top)
                if (heightDiff > dialout_root_view.rootView.height / 4) {
                    invite_btn.visibility = View.GONE
                    parentFragment?.let {
                        it.tabs.visibility = View.GONE
                    }
                } else {
                    invite_btn.visibility = View.VISIBLE
                    parentFragment?.let {
                        it.tabs.visibility = View.VISIBLE
                    }

                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        dialout_root_view.viewTreeObserver.removeOnGlobalLayoutListener(rootViewListener)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            Log.d(this.javaClass.simpleName, data.toString())
            add_number.setText("")
            activity?.apply {
                viewModel?.getContact(contentResolver, data!!, ({
                    Log.d(this@DialoutFragment.javaClass.simpleName, it)
                    add_number.setText(it)
                }))
            }
        }

    }

    override fun onResume() {
        super.onResume()
        parentFragment?.apply {
            back?.apply {
                setOnClickListener {
                    hideKeyboard()
                    switchToParticipants()
                }
            }
            reset?.apply {
                setOnClickListener {
                    resetPhoneText()
                }
            }
        }

        invite_btn.setOnClickListener { v ->
            val phoneNumber = add_number.text.toString().filter { c -> c in '0'..'9' }
            if (phoneNumber.length != 10) {
                add_number_layout.error = resources.getString(R.string.kp_invalid_phone_number)
                inline_error.visibility = View.VISIBLE
                activity?.let {
                    add_number.background = ContextCompat.getDrawable(it, R.drawable.errorborder)
                }
                callToast(resources.getString(R.string.kp_phone_number_needs_to_be_exactly_10_digits))
                return@setOnClickListener
            }

            add_number.error = null
            val dialOut = DialOut(
                phoneNumber,
                resources.getString(R.string.kp_dialout_member),
                resources.getString(R.string.kp_dialout)
            )

            viewModel?.addDialOut(dialOut)
        }

        add_number_directory.setOnClickListener {
            (parentFragment as VideoFragment).setShouldHoldMessageNotDisplay(false)
            checkPermissions()
        }

        hideButtonsWhenKeyboardIsUp()
        hideKeyboard()

    }


    private fun checkPermissions() {
        activity?.let {
            if (ActivityCompat.checkSelfPermission(
                    it,
                    Manifest.permission.READ_CONTACTS
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions( //Method of Fragment
                    arrayOf(Manifest.permission.READ_CONTACTS),
                    REQUEST_PERMISSIONS_CODE_CONTACT_PERMISSIONS
                )
            } else {
                val intent =
                    Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI)
                intent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                startActivityForResult(intent, REQUEST_PERMISSIONS_CODE_CONTACT_PERMISSIONS)
            }
        }
    }

    private fun resetPhoneText() {
        add_number?.setText("")
        errorText()
    }

    private fun errorText() {
        add_number_layout.error = null
        activity?.let {
            add_number.background = ContextCompat.getDrawable(it, R.drawable.border)
            inline_error.visibility = View.GONE
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        activity?.let {
            if (requestCode == REQUEST_PERMISSIONS_CODE_CONTACT_PERMISSIONS) {
                val isContactAllowed =
                    permissions.getOrNull(0).equals(Manifest.permission.READ_CONTACTS)
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED
                if (!isContactAllowed) {
                    val fullDeniedText =
                        getString(R.string.kp_access_denied) + "\n\n" + getString(R.string.kp_contacts_permissions_denied)
                    DialogUtils.showOkDialog(
                        it,
                        fullDeniedText,
                        false
                    )
                }
            }
        }
    }

    private fun setupPermissionsForContacts() {
        activity?.let {

            if (ActivityCompat.checkSelfPermission(
                    it,
                    Manifest.permission.READ_CONTACTS
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions( //Method of Fragment
                    arrayOf(Manifest.permission.READ_CONTACTS),
                    REQUEST_PERMISSIONS_CODE_CONTACT_PERMISSIONS
                )
            }
        }
    }

    fun switchToParticipants() {
        parentFragment?.apply {
            back?.visibility = View.GONE
            fragment_title?.text = resources.getString(R.string.kp_participants)
            reset?.visibility = View.GONE
            add_participants?.visibility = View.VISIBLE
        }
        parentFragment?.childFragmentManager?.popBackStack()
    }

    fun callToast(message: String) {
        activity?.let {
            Toast.makeText(it, message, Toast.LENGTH_LONG).show()
        }
    }

    fun hideKeyboard() {
        activity?.let {
            val inputMethodManager =
                it.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(add_number.windowToken, 0)
        }
    }

    override fun onDetach() {
        super.onDetach()
        activity = null
    }
}