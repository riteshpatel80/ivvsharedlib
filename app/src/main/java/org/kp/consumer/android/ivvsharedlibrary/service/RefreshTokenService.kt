package org.kp.consumer.android.ivvsharedlibrary.service

import android.content.Intent
import android.os.IBinder

import android.app.*

import android.os.Handler
import androidx.annotation.VisibleForTesting
import org.kp.consumer.android.ivvsharedlibrary.util.Log
import kotlinx.coroutines.*
import org.kp.consumer.android.ivvsharedlibrary.Injection
import org.kp.consumer.android.ivvsharedlibrary.api.response.RefreshTokenResponse
import org.kp.consumer.android.ivvsharedlibrary.api.response.ResponseResult
import org.kp.consumer.android.ivvsharedlibrary.api.response.VVBaseResponse
import org.kp.consumer.android.ivvsharedlibrary.data.source.DataSource
import org.kp.consumer.android.ivvsharedlibrary.data.source.RemoteDataSource
import org.kp.consumer.android.ivvsharedlibrary.util.PAUSE_MESSAGE
import kotlin.coroutines.CoroutineContext
import org.kp.consumer.android.ivvsharedlibrary.controller.VVSessionController
import org.kp.consumer.android.ivvsharedlibrary.data.source.Repository
import org.kp.consumer.android.ivvsharedlibrary.util.MUTE_MESSAGE


/**
 *  This service implements refresh token and ivv Renew token call
 *
 *  Created by venkatakalluri on 11/29/19.
 *  Copyright © 2019 Kaiser Permanente. All rights reserved.
 */

class RefreshTokenService : Service(), CoroutineScope {

    var job = Job()
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.IO

    private val TAG = RefreshTokenService::class.java!!.getSimpleName()

    private var stateService = STATE_SERVICE.NOT_CONNECTED

    private val mMainHandler = Handler()
    private val mRenewTokenHandler = Handler()

    var taskRepostiory = Injection.provideTasksRepository(this)

    private var refreshLimit: Long = 0
    private var refreshCount: Int = 1

    override fun onBind(intent: Intent): IBinder? {
        // TODO: Return the communication channel to the service.
        return null
    }

    override fun onCreate() {
        super.onCreate()
        stateService = STATE_SERVICE.NOT_CONNECTED
    }

    override fun onDestroy() {
        stateService = STATE_SERVICE.NOT_CONNECTED
        launch {
            job.cancelAndJoin()
        }
        super.onDestroy()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent == null) {
            stopForeground(true)
            stopSelf()
            return Service.START_NOT_STICKY
        }


        // if user starts the service
        when (intent.action) {
            ACTION.START_ACTION -> {
                Log.d(TAG, "Received user starts foreground intent")

                startForeground(
                    NOTIFICATION_ID_FOREGROUND_SERVICE,
                    VVSessionController.mVVNotification
                )
                connect()
                var expires = intent.getLongExtra(EXPIRES, REFRESH_TOKEN_EXPIRE)
                Log.d("RefreshTokenService", expires.toString())
                reset(expires)
                refresh()

                var renewTokenTime = RemoteDataSource.getInstance()
                    .setupConferenceResponse?.webConfig?.httpConfig?.renewToken?.internval
                Log.d("RefreshTokenService", "renewToken time :${renewTokenTime}")
                renewTokenTime?.let {
                    ivvRenewToken(renewTokenTime)
                }
            }
            ACTION.STOP_ACTION -> {
                Log.d(TAG,"refresh token service stopped")
                stopForeground(true)
                stopSelf()
                removeHandlers()
            }
            ACTION.SEND_PAUSE_ACTION -> {
                var name =
                    RemoteDataSource.getInstance().setupConferenceResponse?.signedInUser?.name
                sendMessage("$PAUSE_MESSAGE:$name")
            }
            ACTION.SEND_MUTE_ACTION -> {
                sendMessage("$MUTE_MESSAGE:false")
            }
            ACTION.SEND_UNMUTE_ACTION -> {
                sendMessage("$MUTE_MESSAGE:true")
            }

            else -> {
                stopForeground(true)
                stopSelf()
                removeHandlers()
            }
        }

        return Service.START_NOT_STICKY
    }

    //it resets the values
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun reset(expires: Long) {
        refreshCount = 1
        refreshLimit = expires / REFRESH_TIME
    }

    private fun sendMessage(eventName: String) {

        Log.d(TAG, "sendMessage: $eventName")

        launch {
            taskRepostiory.sendChatMessage(
                eventName,
                object : DataSource.GetCallback<VVBaseResponse> {
                    override fun onSuccess() {
                    }

                    override fun onSuccess(value: VVBaseResponse) {

                    }

                })
        }
    }

    private fun removeHandlers() {
        mRenewTokenHandler.removeCallbacksAndMessages(null)
        mMainHandler.removeCallbacksAndMessages(null)
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun refreshCall(repository: Repository) {

        launch {
            repository.refreshToken(
                repository.dataSource.setupConferenceResponse?.room!!,
                repository.dataSource._pexipToken!!,
                object : DataSource.GetCallback<RefreshTokenResponse> {
                    override fun onSuccess() {

                    }

                    override fun onSuccess(value: RefreshTokenResponse) {
                        Log.d(TAG, "refreshCall onSuccess :${value.token}")
                        RemoteDataSource.getInstance()._pexipToken = value.token
                        reset(value._expires)
                        refresh()
                    }

                    override fun onFailure(value: ResponseResult.ServiceError) {
                        super.onFailure(value)
                        Log.e(TAG, "refreshToken error : ${value.error.errorDescription}")
                        var intent = Intent(REFRESH_ERROR)
                        intent.putExtra(ERROR_DESC, value.error.errorDescription)
                        androidx.localbroadcastmanager.content.LocalBroadcastManager.getInstance(this@RefreshTokenService)
                            .sendBroadcast(intent)
                    }

                    override fun onFailure(errrorCode: ResponseResult.Failure) {
                        super.onFailure(errrorCode)

                        if (refreshCount < refreshLimit) {
                            refresh()
                            refreshCount += 1
                        }
                        Log.e(TAG, "refreshToken errorCode : ${errrorCode.errorCode}")
                    }


                })
        }
    }

    private fun renewTokenCall() {

        launch {

            taskRepostiory.ivvRenewToken(
                RemoteDataSource.vvConferenceToken!!,
                object : DataSource.GetCallback<VVBaseResponse> {
                    override fun onSuccess() {
                        var renewTokenTime = RemoteDataSource.getInstance()
                            .setupConferenceResponse?.webConfig?.httpConfig?.renewToken?.internval
                        renewTokenTime?.let {
                            ivvRenewToken(renewTokenTime)
                        }
                    }

                    override fun onSuccess(value: VVBaseResponse) {

                    }


                })
        }
    }


    private fun refresh() {

        var nextCallTime = REFRESH_TIME * 1000L
        mMainHandler.postDelayed(mRefreshTokenRunnable, nextCallTime)
    }

    private fun ivvRenewToken(value: Long) {
        var nextCallTime = (value.minus(20))?.times(1000)
        mRenewTokenHandler.postDelayed(mRenewTokenRunnable, nextCallTime)
    }

    private val mRefreshTokenRunnable = {
        Log.d(TAG, "Refreshing User tokens...")
        refreshCall(taskRepostiory)
    }
    private val mRenewTokenRunnable = {
        Log.d(TAG, "Renew token tokens...")
        renewTokenCall()
    }

    // its connected, so change the notification text
    private fun connect() {
        stateService = STATE_SERVICE.CONNECTED
        startForeground(NOTIFICATION_ID_FOREGROUND_SERVICE, VVSessionController.mVVNotification)
    }


    companion object {

        val NOTIFICATION_ID_FOREGROUND_SERVICE = 8466503

        private val REFRESH_TOKEN_EXPIRE = 120L
        private val REFRESH_TIME = 30

        val REFRESH_ERROR = "refresh_error"

        val ERROR_DESC = "error_desc"

        val EXPIRES = "expires"

        val name = "notification"

        object ACTION {
            val MAIN_ACTION = "action.main"
            val START_ACTION = "action.start"
            val STOP_ACTION = "action.stop"
            val SEND_PAUSE_ACTION = "action.pause"
            val SEND_MUTE_ACTION = "action.mute"
            val SEND_UNMUTE_ACTION = "action.unmute"
        }

        object STATE_SERVICE {
            val CONNECTED = 10
            val NOT_CONNECTED = 0
        }


    }
}
