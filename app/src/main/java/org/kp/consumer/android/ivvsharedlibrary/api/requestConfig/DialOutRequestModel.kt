package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig

import com.google.gson.annotations.SerializedName
import org.kp.consumer.android.ivvsharedlibrary.model.Appointment
import org.kp.consumer.android.ivvsharedlibrary.model.DialOut
import org.kp.consumer.android.ivvsharedlibrary.model.SignedInUser
import java.io.Serializable

internal data class DialOutRequestModel(

    @SerializedName("dialOut")
    val dialOut: DialOut

) : Serializable