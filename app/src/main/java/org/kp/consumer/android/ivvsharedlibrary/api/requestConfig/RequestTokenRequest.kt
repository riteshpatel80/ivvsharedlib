package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig

import org.kp.consumer.android.ivvsharedlibrary.util.Log
import org.json.JSONObject
import org.kp.kpnetworking.request.BaseRequestConfig

class RequestTokenRequest( pexipHost:String,roomAlias: String, mDisplayName: String,pin:String):BaseRequestConfig(REQUEST_TYPE.POST, "$pexipHost$API_VERSION$roomAlias$URL_KEY") {

    init{
        Log.d("RequestTokenRequest", "url = $pexipHost$API_VERSION$roomAlias$URL_KEY?display_name=$mDisplayName");
        addHeader(PIN_KEY, pin)
        addHeader(VVBaseRequest.CONTENT_TYPE, VVBaseRequest.CONTENT_TYPE_JSON)

        var jsonObject = JSONObject()
        jsonObject.put(DISPLAY_NAME,mDisplayName)

        body = jsonObject.toString()
    }

    companion object{
        const val PIN_KEY = "pin"
        const val URL_KEY= "/request_token"
        const val API_VERSION = "/api/client/v2/conferences/"
        const val DISPLAY_NAME = "display_name"

    }

}