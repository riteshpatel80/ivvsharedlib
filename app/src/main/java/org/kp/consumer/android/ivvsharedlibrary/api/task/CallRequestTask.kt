package org.kp.consumer.android.ivvsharedlibrary.api.task

import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.CallRequest
import org.kp.consumer.android.ivvsharedlibrary.api.response.CallResponseConverter
import org.webrtc.SessionDescription

internal class CallRequestTask(pexipHost: String, roomAlias: String,participantUUID: String,token:String, sdp: SessionDescription):VVBaseHttpTask(
    CallRequest(pexipHost,roomAlias,participantUUID,token,sdp),
    converter =CallResponseConverter()
) {
}