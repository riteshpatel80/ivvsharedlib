package org.kp.consumer.android.ivvsharedlibrary.api.response

import org.json.JSONObject

internal class RequestTokenResponseConverter: Converter {


    override fun convert(responseJson: String) : RequestTokenResponse {

        var result = JSONObject(responseJson).get("result")

      return Converter.convertFromJson(result.toString())

    }
}