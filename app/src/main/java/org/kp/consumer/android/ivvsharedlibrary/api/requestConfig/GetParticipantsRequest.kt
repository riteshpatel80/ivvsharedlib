package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig

import com.google.gson.Gson


internal class GetParticipantsRequest(getParticipantsRequestModel: GetParticipantsRequestModel, registrationToken: String) :
    VVBaseRequest(REQUEST_TYPE.GET,
        JOIN_CONF_URL_PATH) {

    init {
        addHeader(AUTHORIZATION, "$BEARER $registrationToken")
        addParam(MEETING_ROOM,getParticipantsRequestModel.appointmentMeetingRoomNumber)
        addParam(REGION,getParticipantsRequestModel.appointmentRegion)
        body = Gson().toJson(getParticipantsRequestModel)
     }

    companion object {
        private const val JOIN_CONF_URL_PATH = "/participant"
        private const val MEETING_ROOM = "appointmentMeetingRoomNumber"
        private const val REGION = "appointmentRegion"
    }
}