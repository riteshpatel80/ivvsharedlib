package org.kp.consumer.android.ivvsharedlibrary.features.chat

import android.app.Activity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.inputmethod.InputMethodManager
import android.widget.LinearLayout
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_chat.*

import org.kp.consumer.android.ivvsharedlibrary.R
import org.kp.consumer.android.ivvsharedlibrary.ViewModelFactory
import org.kp.consumer.android.ivvsharedlibrary.databinding.FragmentChatBinding
import org.kp.consumer.android.ivvsharedlibrary.features.VideoFragment
import org.kp.consumer.android.ivvsharedlibrary.features.VideoViewModel
import org.kp.consumer.android.ivvsharedlibrary.features.participants.ParticipantsViewModel
import org.kp.consumer.android.ivvsharedlibrary.model.ChatMessageModel

/**
 * This fragment hosts the participant chat
 */
internal class ChatFragment : Fragment() {
    private lateinit var videoViewModel: VideoViewModel
    private lateinit var chatViewModel: ChatViewModel
    private lateinit var participantViewModel: ParticipantsViewModel

    private lateinit var chatMessagesAdapter: ChatMessagesAdapter
    private lateinit var chatLayoutManager: LinearLayoutManager

    private lateinit var activity: Activity

    private lateinit var keyboardListener : ViewTreeObserver.OnGlobalLayoutListener
    private var lastRecylerViewUpdateTimestamp = System.currentTimeMillis()
    private val handler = Handler()

    var chatMessagesList: List<ChatMessageModel>? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)

        activity = context as Activity
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_chat, container, false)

        FragmentChatBinding.bind(view).apply {
            videoViewModel = activity?.run {
                ViewModelProvider(
                    requireActivity(),
                    ViewModelFactory.getInstance(activity.application)
                ).get(VideoViewModel::class.java)
            }
            chatViewModel = activity?.run {
                ViewModelProvider(
                    requireActivity(),
                    ViewModelFactory.getInstance(activity.application)
                ).get(ChatViewModel::class.java)
            }
            participantViewModel = activity?.run {
                ViewModelProvider(
                    requireActivity(),
                    ViewModelFactory.getInstance(activity.application)
                ).get(ParticipantsViewModel::class.java)
            }
        }
        val chatMessageObserver = Observer<List<ChatMessageModel>> {
            it?.let {
                if (it.isNotEmpty()) {
                    // update messages in view model
                    chatMessagesList = it
                    updateRecyclerView()
                }
            }
        }
        videoViewModel.receivedChatMessagesObservable.observe(
            viewLifecycleOwner,
            chatMessageObserver
        )
        return view;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        chatLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(
            getActivity(),
            RecyclerView.VERTICAL,
            true
        )
        chatMessagesAdapter = ChatMessagesAdapter()

        // listen for participant updates in case new
        // participants join while on the chat screen
        participantViewModel.chatItems.observe(viewLifecycleOwner, Observer { items ->
            // ignore the observable because we can't consume it here,
            // otherwise they won't show up on the participants fragment
            chatMessagesAdapter.updateParticipants(participantViewModel.participantList)
            // check if call-in participant exists and show/hide info banner
            if(chatViewModel.shouldDisplayCallInBanner(participantViewModel.participantList)) {
                call_in_user_info_banner.visibility = View.VISIBLE
            }
            else {
                call_in_user_info_banner.visibility = View.GONE
            }
        })

        // initialize the participants list
        makeParticipantsCall()

        chat_messages_list.apply {
            itemAnimator = null

            layoutManager = chatLayoutManager

            // specify an viewAdapter (see also next example)
            adapter = chatMessagesAdapter
        }

        // watch chat input field and toggle send button color accordingly
        message_input.addTextChangedListener(object :
            TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                toggleSendButtonColor(s.toString())
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                toggleSendButtonColor(s.toString())
            }

            override fun afterTextChanged(s: Editable?) {
                toggleSendButtonColor(s.toString())
            }
        }
        )

        send_button.setOnClickListener {
            videoViewModel.sendChatMessage(message_input.text.toString())

            // clear input field
            message_input.setText("")
            hideKeyboard()
        }
    }

    override fun onResume() {
        super.onResume()

        keyboardListener = object : ViewTreeObserver.OnGlobalLayoutListener {

            private var previousState: Boolean = isKeyboardOpen()

            // a change in the layout occurred
            override fun onGlobalLayout() {

                val isOpen = isKeyboardOpen()

                // keyboard state is the same, don't do anything
                if (isOpen == previousState) {
                    return
                } else {
                    // keyboard was open and now is closed
                    if(!isOpen){
                        scrollToBottomOfMessages()
                    }
                    previousState = isOpen
                }
            }
        }

        chat_messages_list.viewTreeObserver.addOnGlobalLayoutListener(
            keyboardListener
        )
    }

    override fun onPause() {
        super.onPause()
        chat_messages_list.viewTreeObserver.removeOnGlobalLayoutListener(
            keyboardListener
        )
    }

    private fun hideKeyboard() {
        val inputMethodManager =
            activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    private fun makeParticipantsCall() {
        (parentFragment as VideoFragment).getConferenceResponse()?.let {
            participantViewModel.getParticipants()
        }
    }
    
    private fun toggleSendButtonColor(editable: String?) {
        //if no text has been entered or cleared
        if (editable.isNullOrEmpty()) {
            send_button.apply {
                setBackgroundColor(
                    ContextCompat.getColor(
                        activity,
                        R.color.kp_light_gray
                    )
                )
                setTextColor(
                    ContextCompat.getColor(
                        activity,
                        R.color.kp_gray
                    )
                )
            }
        }
        // if text has been entered
        else {
            send_button.apply {
                setBackgroundColor(ContextCompat.getColor(activity, R.color.kpblue))
                setTextColor(
                    ContextCompat.getColor(
                        activity,
                        R.color.white
                    )
                )
            }
        }
    }

    private fun updateRecyclerView() {
        val currentTimestamp = System.currentTimeMillis()
        // only update recycler view if the quiet period has passed
        if ((lastRecylerViewUpdateTimestamp + UPDATE_QUIET_PERIOD) < currentTimestamp) {
            chatMessagesAdapter.updateData(chatMessagesList)
            lastRecylerViewUpdateTimestamp = System.currentTimeMillis()
            scrollToBottomOfMessages()
        }
        else {
            // make sure we update the recyclerview at least once
            handler.postDelayed({
                updateRecyclerView()
            }, 500L)
        }
    }

    private fun scrollToBottomOfMessages() {
        // add delay before scrolling to account for keyboard closing
        chat_messages_list?.postDelayed(
            Runnable {
                chat_messages_list?.scrollToPosition(0)
            }, 300
        )
    }

    fun isKeyboardOpen(): Boolean {
        val visibleBounds = Rect()
        chat_messages_list.getWindowVisibleDisplayFrame(visibleBounds)
        val heightDiff = chat_messages_list.height - visibleBounds.height()
        val marginOfError = Math.round(
            TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                MARGIN_OF_ERROR_HEIGHT,
                this.resources.displayMetrics
            )
        )
        return heightDiff > marginOfError
    }

    companion object {
        const val TAG = "ChatFragment"
        const val MARGIN_OF_ERROR_HEIGHT = 50F
        const val UPDATE_QUIET_PERIOD = 1000L
    }
}
