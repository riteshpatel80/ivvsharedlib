package org.kp.consumer.android.ivvsharedlibrary.util

/**
 *  This class print the logs in various levels
 *
 * Created by venkatakalluri on 2020-02-20.
 * Copyright © 2019 Kaiser Permanente. All rights reserved.
 */
object Log {

    fun d(tag: String, message: String?) {
        android.util.Log.d(tag, message)
    }

    fun e(tag: String, message: String?) {
        android.util.Log.e(tag, message)
    }
    fun e(tag: String, message: String?, tr: Throwable) {
        android.util.Log.e(tag, message, tr)
    }

    fun i(tag: String, message: String?) {
        android.util.Log.i(tag, message)
    }

    fun w(tag: String, message: String?) {
        android.util.Log.w(tag, message)
    }


}