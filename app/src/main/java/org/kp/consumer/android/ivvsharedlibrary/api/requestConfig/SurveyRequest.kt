package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig

import com.google.gson.Gson
import org.kp.consumer.android.ivvsharedlibrary.data.source.RemoteDataSource
import org.kp.consumer.android.ivvsharedlibrary.util.Log

internal class SurveyRequest(surveyModel: SurveyRequestModel) : VVBaseRequest(REQUEST_TYPE.POST, SURVEY_URL_PATH) {

    init {
        Log.d("SurveyRequest", "url =${this.url}");
        //dev url is different natively so I need to have a check for it.
        if (RemoteDataSource.envlbl == "dev")
            this.url = "https://apiservice-bus-dev.kaiserpermanente.org/kp/dit/service/diag_thrpy/virtualcare/ivv/nativeconference/v1/survey"
        else
            this.url = "https://apiservice-bus-qa.kaiserpermanente.org/service/diag_thrpy/virtualcare/ivv/nativeconference/v1/survey"

        body = Gson().toJson(surveyModel)
    }

    companion object {
        private const val SURVEY_URL_PATH = "/survey"
    }
}