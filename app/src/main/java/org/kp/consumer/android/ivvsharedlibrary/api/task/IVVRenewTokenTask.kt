package org.kp.consumer.android.ivvsharedlibrary.api.task

import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.IVVRenewTokenRequest
import org.kp.consumer.android.ivvsharedlibrary.api.response.GenericConverter

internal class IVVRenewTokenTask(registrationToken: String): VVBaseHttpTask(IVVRenewTokenRequest(registrationToken), GenericConverter()) {
}