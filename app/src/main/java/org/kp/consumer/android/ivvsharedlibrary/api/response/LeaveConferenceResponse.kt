package org.kp.consumer.android.ivvsharedlibrary.api.response

import com.google.gson.annotations.SerializedName


data class LeaveConferenceResponse (

    @SerializedName("errors")
    override val errors: List<ResponseError>

) : VVBaseResponse