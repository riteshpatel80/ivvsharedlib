package org.kp.consumer.android.ivvsharedlibrary.features.chat

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import org.kp.consumer.android.ivvsharedlibrary.data.source.Repository
import org.kp.consumer.android.ivvsharedlibrary.model.Participant
import org.kp.consumer.android.ivvsharedlibrary.util.Role

internal class ChatViewModel(private val tasksRepository: Repository) : ViewModel() {

    private val viewModelJob = Job()
    private val viewModelScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    /**
     * This method determines if we have to display the call-in banner
     * Currently, the rule is to display the banner if a dial-in user
     * or dial-out user or interpreter is present in the video visit
     */
    fun shouldDisplayCallInBanner(participants: List<Participant>?) : Boolean {
        var displayCallIn = false
        participants?.let {
            for (participant in it) {
                if (participant.role == Role.DIAL_OUT.role ||
                    participant.role == Role.DIAL_IN.role ||
                    participant.role == Role.INTERPRETER.role){
                    displayCallIn = true
                    break
                }
            }
        }
        return displayCallIn
    }

    companion object {
        const val TAG = "ChatViewModel"
    }
}
