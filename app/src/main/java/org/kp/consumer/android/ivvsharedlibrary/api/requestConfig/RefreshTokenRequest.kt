package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig

import org.kp.consumer.android.ivvsharedlibrary.util.Log
import org.kp.kpnetworking.request.BaseRequestConfig

class RefreshTokenRequest( pexipHost:String,roomAlias: String,pin:String, token: String):
    BaseRequestConfig(REQUEST_TYPE.POST, "$pexipHost$API_VERSION$roomAlias$URL_KEY") {

    init {
        Log.d("RefreshTokenRequest", "url = $pexipHost$API_VERSION$roomAlias$URL_KEY");
        addHeader(PIN_KEY, pin)
        addHeader(TOKEN_KEY, token)
        body = "{}"
    }

    companion object {
        const val PIN_KEY = "pin"
        const val TOKEN_KEY="token"
        const val URL_KEY = "/refresh_token"
        const val API_VERSION = "/api/client/v2/conferences/"
    }
}