package org.kp.consumer.android.ivvsharedlibrary.model

import java.net.URL
import java.io.Serializable
import org.kp.consumer.android.ivvsharedlibrary.util.*

/**
 * Holds URL and environmentLabel. URLs must be set with an 'https' protocol
 * @property mTitle
 * @property mBaseURL
 * @property mEnvLabel
 * @property mClientId
 */
data class VVEnvironmentConfig(
    private var mTitle: String,
    private var mBaseURL: URL,
    private var mEnvLabel: String,
    private var mClientId: String

) : Serializable {

    var baseURL: String
    var envLabel: String
    var clientId: String

    init {
        baseURL = mBaseURL.validate().toString()
        envLabel = mEnvLabel
        clientId = mClientId
    }

    /**
     * Validates contents of a URL. ('requires' throws IllegalArgumentException)
     *
     * @return the validated url
     */
    private fun URL.validate(): URL {
        require(this.protocol == HTTPS) {
            MUST_BE_HTTPS
        }
        return this
    }

}
