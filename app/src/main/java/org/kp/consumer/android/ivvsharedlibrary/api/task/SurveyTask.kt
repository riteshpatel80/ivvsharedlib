package org.kp.consumer.android.ivvsharedlibrary.api.task

import org.json.JSONObject
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.AckRequest
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.SurveyRequest
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.SurveyRequestModel
import org.kp.consumer.android.ivvsharedlibrary.api.response.Converter
import org.kp.consumer.android.ivvsharedlibrary.api.response.GenericConverter
import org.webrtc.SessionDescription
import java.util.*

internal class SurveyTask(surveyRequestModel: SurveyRequestModel): VVBaseHttpTask(
    SurveyRequest(surveyRequestModel),
    converter = GenericConverter()
) {
}