package org.kp.consumer.android.ivvsharedlibrary.features.chat

import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.TextView
import org.kp.consumer.android.ivvsharedlibrary.R
import org.kp.consumer.android.ivvsharedlibrary.model.ChatMessageModel
import org.kp.consumer.android.ivvsharedlibrary.model.Participant
import org.kp.consumer.android.ivvsharedlibrary.util.CustomBindingAdapter
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


internal class ChatMessagesAdapter :
    androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

    var chatMessageList = ArrayList<ChatMessageModel>()
    var participantList = emptyList<Participant>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {

        if(viewType == TYPE_SENT_MESSAGE) {
            val chatMessageRow =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.sent_chat_message_row, parent, false)
            return SentMessageViewHolder(chatMessageRow as ConstraintLayout)
        }
        else {
            val chatMessageRow =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.received_chat_message_row, parent, false)
            return ReceivedMessageViewHolder(chatMessageRow as ConstraintLayout)
        }
    }

    override fun getItemCount() = chatMessageList.size

    override fun getItemViewType(position: Int): Int {
        val isReceivedMessage = chatMessageList[position].isReceivedMessage

        isReceivedMessage?.let {
            if (it) {
                return TYPE_RECEIVED_MESSAGE
            }
        }
        return TYPE_SENT_MESSAGE
    }

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        val item = chatMessageList[position]
        if(getItemViewType(position) == TYPE_SENT_MESSAGE) {
            (holder as SentMessageViewHolder).bindData(item)
        }
        else {
            (holder as ReceivedMessageViewHolder).bindData(item)
        }
    }

    fun updateData(items: List<ChatMessageModel>?) {
        items?.let {
            val diffCallback = ChatMessagesDiffCallback(chatMessageList, it)
            val diffResult = DiffUtil.calculateDiff(diffCallback)
            chatMessageList.clear()
            chatMessageList.addAll(it)
            diffResult.dispatchUpdatesTo(this)
        }
    }

    fun updateParticipants(participants: List<Participant>?) {
        participants?.let {
            // only update if any of the participants have actually changed
            if(!it.containsAll(participantList) || !participantList.containsAll(it)) {
                participantList = it
                notifyDataSetChanged()
            }
        }
    }

    /**
     * ViewHolder for messages received from other participants in the video session
     */
    inner class ReceivedMessageViewHolder(chatMessageRow: ConstraintLayout) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(chatMessageRow) {

        var chatMessageText: TextView? = null
        var senderName: TextView? = null
        var timestamp: TextView? = null
        var roleIcon: ImageView? = null

        init {
            chatMessageText = chatMessageRow.findViewById(R.id.chat_message_text) as TextView?
            senderName = chatMessageRow.findViewById(R.id.sender_name) as TextView?
            timestamp = chatMessageRow.findViewById(R.id.chat_message_timestamp) as TextView?
            roleIcon = chatMessageRow.findViewById(R.id.sender_role_icon) as ImageView?
        }

        fun bindData(chatMessage: ChatMessageModel) {
            chatMessageText?.text = chatMessage.messageBody
            senderName?.text = chatMessage.origin
            chatMessage.timestamp?.let {
                timestamp?.text = formatTimestamp(it)
            }
            // check if role icon resource id has been cached
            chatMessage.roleIconResourceId?.let {
                roleIcon?.setImageResource(it)
            }
            // look up role icon and set on image view
            if(chatMessage.roleIconResourceId == null){
                var foundParticipant: Boolean = false
                for (participant in participantList){
                    if(chatMessage.senderUUID.equals(participant.uuid,ignoreCase = true)) {
                        // get role icon for participant
                        foundParticipant = true
                        roleIcon?.let {
                            CustomBindingAdapter.setRoleAvatar(it, participant)
                            // cache resource id in case a participant leaves during the session
                            // but others still need to see the correct icon - only applies to received messages
                            chatMessage.roleIconResourceId = it.getTag(R.id.image_resource_id) as Int
                        }
                    }
                }
            }
            else {
                chatMessage.roleIconResourceId?.let {
                    roleIcon?.setImageResource(it)
                }
            }

        }
    }

    /**
     * ViewHolder for messages sent by this participant
     */
    inner class SentMessageViewHolder(chatMessageRow: ConstraintLayout) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(chatMessageRow) {

        var chatMessageText: TextView? = null
        var timestamp: TextView? = null

        init {
            chatMessageText = chatMessageRow.findViewById(R.id.chat_message_text) as TextView?
            timestamp = chatMessageRow.findViewById(R.id.chat_message_timestamp) as TextView?
        }

        fun bindData(chatMessage: ChatMessageModel) {
            chatMessageText?.text = chatMessage.messageBody
            chatMessage.timestamp?.let {
                timestamp?.text = formatTimestamp(it)
            }
        }
    }

    private fun formatTimestamp(timestamp: Long) : String {
        var formattedTimestamp = ""
        timestamp?.let {
            val formatter = SimpleDateFormat(TIMESTAMP_PATTERN, Locale.US)
            val date = Date(it)
            formattedTimestamp = formatter.format(date)
        }
        return formattedTimestamp
    }


    companion object {
        // timestamp should read "12:00 pm"
        const val TIMESTAMP_PATTERN = "h:mm a"
        const val TYPE_RECEIVED_MESSAGE = 1
        const val TYPE_SENT_MESSAGE = 2
    }
}