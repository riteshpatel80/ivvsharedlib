package org.kp.consumer.android.ivvsharedlibrary.api.task

import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.SendChatMessageModel
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.SendChatMessageRequest
import org.kp.consumer.android.ivvsharedlibrary.api.response.SendChatMessageResponseConverter

internal class SendChatMessageTask(
    message: SendChatMessageModel,
    pexipHost: String,
    roomAlias: String,
    pin: String,
    registrationToken: String
) :
    VVBaseHttpTask(
        SendChatMessageRequest(message, pexipHost, roomAlias, pin, registrationToken),
        converter = SendChatMessageResponseConverter()
    )