package org.kp.consumer.android.ivvsharedlibrary.features.chat

import androidx.recyclerview.widget.DiffUtil
import org.kp.consumer.android.ivvsharedlibrary.model.ChatMessageModel

internal class ChatMessagesDiffCallback(
    private val oldList: List<ChatMessageModel>,
    private val newList: List<ChatMessageModel>
) : DiffUtil.Callback() {

    override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
        val oldItem = oldList[p0]
        val newItem = newList[p1]

        return oldItem.messageBody == newItem.messageBody
                && oldItem.senderUUID == newItem.senderUUID
                && oldItem.timestamp == newItem.timestamp
    }

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
        val oldItem = oldList[p0]
        val newItem = newList[p1]

        return oldItem.messageBody == newItem.messageBody
                && oldItem.senderUUID == newItem.senderUUID
                && oldItem.timestamp == newItem.timestamp
                && oldItem.isReceivedMessage == newItem.isReceivedMessage
                && oldItem.roleIconResourceId == newItem.roleIconResourceId
                && oldItem.origin == newItem.origin
                && oldItem.type == newItem.type
    }

}