package org.kp.consumer.android.ivvsharedlibrary.api.response

import org.kp.consumer.android.ivvsharedlibrary.model.DialOut

data class DialOutResponse(

    val dialOut: DialOut, override val errors: List<ResponseError>?


) : VVBaseResponse