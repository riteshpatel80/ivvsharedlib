package org.kp.consumer.android.ivvsharedlibrary.api.response

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


internal interface Converter {

    fun convert(responseJson: String) : VVBaseResponse

    companion object {
        inline fun <reified T> convertFromJson(json: String): T {
            return Gson().fromJson(json, object: TypeToken<T>(){}.type)
        }
    }
}