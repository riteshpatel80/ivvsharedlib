package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig

import com.google.gson.annotations.SerializedName
import org.kp.consumer.android.ivvsharedlibrary.model.Appointment
import org.kp.consumer.android.ivvsharedlibrary.model.SignedInUser
import org.kp.consumer.android.ivvsharedlibrary.model.Survey

class SurveyRequestModel (

    @SerializedName("survey")
    val survey: Survey
)
