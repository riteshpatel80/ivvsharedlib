package org.kp.consumer.android.ivvsharedlibrary.api.task

import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.AckRequest
import org.kp.consumer.android.ivvsharedlibrary.api.response.GenericConverter

internal class AckTask(pexipHost: String, roomAlias: String,participantUUID: String,token:String,callUUID: String): VVBaseHttpTask(
    AckRequest(pexipHost, roomAlias,participantUUID,token,callUUID),
    converter = GenericConverter()
) {
}