package org.kp.consumer.android.ivvsharedlibrary.api.response

import com.google.gson.annotations.SerializedName
import org.kp.consumer.android.ivvsharedlibrary.model.Participant


data class GetParticipantsResponse (

    @SerializedName("participantCount")
    val count: Int,

    @SerializedName("participants")
    val participantList: List<Participant>,

    @SerializedName("errors")
    override val errors: List<ResponseError>

) : VVBaseResponse