package org.kp.consumer.android.ivvsharedlibrary

import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {

    //TODO - To convert this app to a Library:
    //Open the module-level build.gradle file.
    //Delete the line for the applicationId <Only an Android app module can define this>
    //At the top of the file, you should see the following: apply plugin: 'com.android.application'
    //Change it to the following: apply plugin: 'com.android.library'
    //
    //Save the file and click File > Sync Project with Gradle Files.
    //When you want to build the AAR file, select the library module in the Project window and then click Build > Build APK

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {

        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)


    }
}
