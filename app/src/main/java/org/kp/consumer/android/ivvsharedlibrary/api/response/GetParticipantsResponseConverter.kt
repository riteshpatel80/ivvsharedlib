package org.kp.consumer.android.ivvsharedlibrary.api.response


internal class GetParticipantsResponseConverter : Converter {
    override fun convert(responseJson: String) : GetParticipantsResponse = Converter.convertFromJson(responseJson)
}