package org.kp.consumer.android.ivvsharedlibrary.model

import com.google.gson.annotations.SerializedName


data class Appointment(

    @SerializedName("appointmentId")
    val id: String,

    @SerializedName("appointmentIdType")
    val idType: String,

    @SerializedName("appointmentDateTime")
    val apptDateTime: String,

    @SerializedName("appointmentVisitType")
    val visitType: String,

    @SerializedName("appointmentDuration")
    val duration: String,

    @SerializedName("appointmentDurationUOM")
    val durationUOM: String,

    @SerializedName("appointmentRegion")
    val region: String,

    @SerializedName("appointmentMeetingRoomNumber")
    val meetingRoomNumber: String,

    @SerializedName("patient")
    val patient: Patient,

    @SerializedName("provider")
    val providerList: List<Provider>,

    @SerializedName("appointmentFacilityId")
    val facilityId: String,

    @SerializedName("appointmentDeptId")
    val departmentId: String,

    @SerializedName("appointmentClinicId")
    val clinicId: String
)