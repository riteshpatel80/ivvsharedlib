package org.kp.consumer.android.ivvsharedlibrary.features.survey


import android.app.Activity
import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.RatingBar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.fragment_survey.*

import org.kp.consumer.android.ivvsharedlibrary.R
import org.kp.consumer.android.ivvsharedlibrary.ViewModelFactory
import org.kp.consumer.android.ivvsharedlibrary.databinding.FragmentSurveyBinding
import org.kp.consumer.android.ivvsharedlibrary.features.VideoViewModel
import org.kp.consumer.android.ivvsharedlibrary.model.Survey

/**
 * A simple [Fragment] subclass.
 */
class SurveyFragment : Fragment() {

    val TAG = "fivestar"
    private var activity: Activity? = null

    private var viewModel: VideoViewModel? = null

    private var concerns: ArrayList<String> = ArrayList()

    var rating: Float = 0f

    override fun onAttach(context: Context) {
        super.onAttach(context)

        activity = context as Activity
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val videoView = inflater.inflate(R.layout.fragment_survey, container, false)

        FragmentSurveyBinding.bind(videoView).apply {

            viewModel = activity?.run {
                ViewModelProvider(
                    requireActivity(),
                    ViewModelFactory.getInstance(activity!!.application)
                )
                    .get(VideoViewModel::class.java)

            }
        }
        return videoView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //Typeface for checkboxes only work programmatically
        audio_quality.typeface = Typeface.create("sans-serif-medium", Typeface.NORMAL)
        video_quality.typeface = Typeface.create("sans-serif-medium", Typeface.NORMAL)
        ease_quality.typeface = Typeface.create("sans-serif-medium", Typeface.NORMAL)
        survey_review_text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.length > 100) {
                    survey_review_layout.error = " "
                    survey_review_layout.boxStrokeColor= ContextCompat.getColor(activity!!, R.color.error_box_selector)
                }
                else {
                    survey_review_layout
                        .error =""
                    survey_review_layout.boxStrokeColor = ContextCompat.getColor(activity!!, R.color.error_box_default)
                }
                when {
                    s.isEmpty() -> {
                        val counter = "+${100 - s.length}"
                        review_counter.text = counter
                        submit_quality.isEnabled = shouldSubmitBeEnabled()
                    }
                    (100 - s.length) >= 0 -> {
                        val counter = "+${100 - s.length}"
                        review_counter.text = counter
                        review_counter.setTextColor(ContextCompat.getColor(activity!!, R.color.black))
                        submit_quality.isEnabled = shouldSubmitBeEnabled()

                    }
                    else -> {
                        review_counter.text ="${100 - s.length}"
                        review_counter.setTextColor(ContextCompat.getColor(activity!!, R.color.kp_red))
                        submit_quality.isEnabled = shouldSubmitBeEnabled()
                    }
                }
            }

        })

        submit_quality.setOnClickListener {

            viewModel?.insideTheSurvey = false
            submit_quality.isEnabled = false

            if (audio_quality.isChecked){
                concerns.add(audio_quality.tag.toString())
            }
            if (video_quality.isChecked){
                concerns.add(video_quality.tag.toString())
            }
            if (ease_quality.isChecked){
                concerns.add(ease_quality.tag.toString())
            }
            viewModel?.surveyRequest(Survey(
                rating = survey_rating.rating.toInt().toString(),
                comments = survey_review_text.text.toString(),
                concerns = concerns
            ))
        }

        survey_rating.onRatingBarChangeListener = RatingBar.OnRatingBarChangeListener { ratingBar, rating, fromUser ->
            Log.e(TAG,"Rating: $rating")
            this.rating = rating
            submit_quality.isEnabled = shouldSubmitBeEnabled()
        }

        skip_quality.setOnClickListener {
            viewModel?.insideTheSurvey = false
            viewModel?.leaveFromSurvey()
        }
        audio_quality.setOnCheckedChangeListener(submitListener)
        video_quality.setOnCheckedChangeListener(submitListener)
        ease_quality.setOnCheckedChangeListener(submitListener)
    }

    override fun onResume() {
        super.onResume()
        viewModel?.hideTabs()
    }

    private val submitListener : CompoundButton.OnCheckedChangeListener =
        CompoundButton.OnCheckedChangeListener {
                buttonView, isChecked -> submit_quality.isEnabled = shouldSubmitBeEnabled()
        }

    fun shouldSubmitBeEnabled() : Boolean{
        if (rating >= 1f && survey_review_text.text!!.length <= 100){
            return true
        }
        return false
    }


}
