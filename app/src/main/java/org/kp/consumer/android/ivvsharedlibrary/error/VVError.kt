package org.kp.consumer.android.ivvsharedlibrary.error

data class VVError(var code: String? = null, var title: String? = null, var description: String? = null)