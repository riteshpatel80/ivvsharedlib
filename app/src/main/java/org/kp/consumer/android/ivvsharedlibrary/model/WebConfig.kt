package org.kp.consumer.android.ivvsharedlibrary.model

import com.google.gson.annotations.SerializedName

/**
 * Created by venkatakalluri on 2019-12-03.
 * Copyright © 2019 Kaiser Permanente. All rights reserved.
 */



data class WebConfig(
    @SerializedName("httpConfig")
    val httpConfig: HttpConfig
)

data class HttpConfig(
    @SerializedName("renew-token")
    val renewToken: RenewToken,
    @SerializedName("webView")
    val webView: WebViewURL
)


data class RenewToken(
    @SerializedName("interval")
    val internval: Long
)
data class WebViewURL(
    @SerializedName("help")
    val help: Help
)
data class Help(
    @SerializedName("baseUrl")
    val baseUrl: String
)