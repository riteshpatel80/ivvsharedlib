package org.kp.consumer.android.ivvsharedlibrary.features

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.annotation.VisibleForTesting
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonParser

import com.here.oksse.ServerSentEvent
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.TickerMode
import kotlinx.coroutines.channels.ticker
import okhttp3.Request
import okhttp3.Response
import org.kp.consumer.android.ivvsharedlibrary.Event
import org.kp.consumer.android.ivvsharedlibrary.R
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.JoinConferenceRequestModel
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.LeaveConferenceRequestModel
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.SurveyRequestModel
import org.kp.consumer.android.ivvsharedlibrary.api.response.*
import org.kp.consumer.android.ivvsharedlibrary.data.source.DataSource
import org.kp.consumer.android.ivvsharedlibrary.data.source.Repository
import org.kp.consumer.android.ivvsharedlibrary.model.*
import org.kp.consumer.android.ivvsharedlibrary.model.ChatMessageModel
import org.kp.consumer.android.ivvsharedlibrary.model.ParticipantsRequestOptions
import org.kp.consumer.android.ivvsharedlibrary.util.*
import org.webrtc.SessionDescription
import kotlin.collections.ArrayList


internal class VideoViewModel(
    private val remoteReposiotry: Repository,
    private val dispatcherIO: CoroutineDispatcher
) : ViewModel() {

    private val SERVER_SENT_EVENT_TAG = "ServerSentEventTag"
    private val viewModelJob = Job()

    private val viewModelScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private var _conferenceResponse: SetupConferenceResponse? = null

    fun getConferenceResponse(): SetupConferenceResponse? {
        return _conferenceResponse
    }

    private val _expiry = MutableLiveData<Event<Long>>()
    val expiry: LiveData<Event<Long>>
        get() = _expiry


    private val _token = MutableLiveData<String>()
    val token: LiveData<String>
        get() = _token


    private val _participantUUID = MutableLiveData<String>()
    val participantUUID: LiveData<String>
        get() = _participantUUID

    private val _sdp = MutableLiveData<Event<SessionDescription>>()
    val sdp: LiveData<Event<SessionDescription>>
        get() = _sdp

    private val _LoadViewEvent = MutableLiveData<Event<Unit>>()
    val loadViewEvent: LiveData<Event<Unit>>
        get() = _LoadViewEvent

    private val _joinConfereceEvent = MutableLiveData<Event<Unit>>()
    val joinConferenceEvent: LiveData<Event<Unit>>
        get() = _joinConfereceEvent

    private val _leaveConfereceEvent = MutableLiveData<Event<Boolean>>()
    val leaveConferenceEvent: LiveData<Event<Boolean>>
        get() = _leaveConfereceEvent

    private val _dialogCloseEvent = MutableLiveData<Event<Unit>>()
    val diloagCloseEvent: LiveData<Event<Unit>>
        get() = _dialogCloseEvent

    private val _callRequestToken = MutableLiveData<Event<Unit>>()
    val setUpConferenceResponse: LiveData<Event<Unit>>
        get() = _callRequestToken

    val participantsEvent: MutableLiveData<Event<ParticipantsRequestOptions>> = MutableLiveData()

    val paticipantJoinEvent: MutableLiveData<Event<ParticipantAnnouncementEvent>> =
        MutableLiveData()

    private val _errorMessage = MutableLiveData<Event<ResponseResult>>()
    val errorMesssage: LiveData<Event<ResponseResult>>
        get() = _errorMessage

    private val _participantDelete = MutableLiveData<Event<Unit>>()
    val participantDelete: LiveData<Event<Unit>>
        get() = _participantDelete

    val chatToastMessage: MutableLiveData<Event<ChatMessageModel>> = MutableLiveData()

    val _leaveFromSurvey: MutableLiveData<Event<Unit>> = MutableLiveData()
    val leaveFromSurvey: LiveData<Event<Unit>>
        get() = _leaveFromSurvey

    val _hideTabs: MutableLiveData<Event<Unit>> = MutableLiveData()
    val hideTabs: LiveData<Event<Unit>>
        get() = _hideTabs

    val _hideTitle: MutableLiveData<Event<Unit>> = MutableLiveData()
    val hideTitle: LiveData<Event<Unit>>
        get() = _hideTitle

    var insideTheSurvey: Boolean = false

    var callUUID: String? = null;

    var clearData: Boolean = false

    // to be used by the chat fragment
    val receivedChatMessagesObservable: MutableLiveData<List<ChatMessageModel>> = MutableLiveData()
    val showUnreadMessagesBadge: MutableLiveData<Boolean> = MutableLiveData()

    // all received chat messages during the current video session
    val sseMessageList: ArrayList<ChatMessageModel> = ArrayList()

    private val _callEnded = MutableLiveData<Event<Unit>>()
    val callEnded: LiveData<Event<Unit>>
        get() = _callEnded


    private val _onHoldObservable = MutableLiveData<Event<String>>()
    val onHoldObservable: LiveData<Event<String>>
        get() = _onHoldObservable

    private val _muteObservable = MutableLiveData<Event<Boolean>>()
    val muteObservable: LiveData<Event<Boolean>>
        get() = _muteObservable
    //presentation image
    private val _onPresentationImageObservable = MutableLiveData<Event<String>>()
    val onPresentationImageObservable: LiveData<Event<String>>
        get() = _onPresentationImageObservable


    private val _onPresentationObservable = MutableLiveData<Event<Boolean>>()
    val onPresentationObservable: LiveData<Event<Boolean>>
        get() = _onPresentationObservable

    private val _updateMessageRole = MutableLiveData<Event<ChatMessageModel>>()
    val updateMessageRole: LiveData<Event<ChatMessageModel>>
        get() = _updateMessageRole


    fun setUpConference(token: String) {
        viewModelScope.launch(dispatcherIO) {
            remoteReposiotry.setUpConferenceRequest(
                token,
                object : DataSource.GetCallback<SetupConferenceResponse> {
                    override fun onSuccess() {
                        _callRequestToken.postValue(Event(Unit))
                    }

                    override fun onSuccess(value: SetupConferenceResponse) {
                        _conferenceResponse = value
                        _callRequestToken.postValue(Event(Unit))
                    }

                    override fun onFailure(value: ResponseResult.ServiceError) {

                        Log.e(TAG, "setUpConference error : ${value.error.errorDescription}")

                        _dialogCloseEvent.postValue(Event(Unit))
                        _errorMessage.postValue(Event(value))

                    }

                    override fun onFailure(failure: ResponseResult.Failure) {
                        Log.e(TAG, "setUpConference error : ${failure.errorCode}")
                        _dialogCloseEvent.postValue(Event(Unit))
                        _errorMessage.postValue(Event(failure))

                    }


                })
        }


    }

    fun pexipRequestToken() {


        viewModelScope.launch(dispatcherIO) {
            _conferenceResponse?.run {

                remoteReposiotry.requestToken(
                    _conferenceResponse!!.room,
                    _conferenceResponse!!.signedInUser.name,
                    object : DataSource.GetCallback<RequestTokenResponse> {
                        override fun onSuccess() {

                        }

                        override fun onSuccess(requestToken: RequestTokenResponse) {

                            Log.d(TAG, "Token requested")

                            _token.postValue(requestToken.token)
                            _participantUUID.postValue(requestToken.participantUUID)
                            _expiry.postValue(Event(requestToken._expires))
                            _LoadViewEvent.postValue(Event(Unit))

                        }

                        override fun onFailure(value: ResponseResult.ServiceError) {

                            Log.e(
                                TAG,
                                "pexipRequestToken service error : ${value.error.errorDescription}"
                            )

                            _dialogCloseEvent.postValue(Event(Unit))
                            _errorMessage.postValue(Event(value))

                        }

                        override fun onFailure(failure: ResponseResult.Failure) {
                            Log.e(TAG, "pexipRequestToken failure : ${failure.errorCode}")
                            _dialogCloseEvent.postValue(Event(Unit))
                            _errorMessage.postValue(Event(failure))

                        }


                    },
                    serverEventListener
                )


            }
        }

    }


    fun callRequest(sessionDescription: SessionDescription) {

        viewModelScope.launch(dispatcherIO) {
            remoteReposiotry.makeCall(
                _conferenceResponse!!.room,
                _participantUUID.value!!,
                sessionDescription,
                object : DataSource.GetCallback<CallResponse> {
                    override fun onSuccess() {

                    }

                    override fun onSuccess(calResponse: CallResponse) {
                        Log.d(TAG, "callRequest onSuccess : ${calResponse.sdp}")

                        callUUID = calResponse.callUUID

                        var sdpString = calResponse.sdp
                        _sdp.postValue(
                            Event(
                                SessionDescription(
                                    SessionDescription.Type.ANSWER,
                                    sdpString
                                )
                            )
                        )


                    }

                    override fun onFailure(value: ResponseResult.ServiceError) {
                        Log.e(TAG, "callRequest service error : ${value.error.errorDescription}")

                        _dialogCloseEvent.postValue(Event(Unit))
                        //_errorMessage.postValue(Event(value))

                    }

                    override fun onFailure(failure: ResponseResult.Failure) {
                        Log.e(TAG, "callRequest onFailure : ${failure.errorCode}")
                        _dialogCloseEvent.postValue(Event(Unit))
                        //_errorMessage.postValue(Event(failure))

                    }


                })
        }


    }


    fun ackRequest() {

        viewModelScope.launch(dispatcherIO) {
            remoteReposiotry.doAck(
                _conferenceResponse!!.room,
                _participantUUID.value!!,
                callUUID!!,
                object : DataSource.GetCallback<VVBaseResponse> {
                    override fun onSuccess() {
                        _joinConfereceEvent.postValue(Event(Unit))
                        _dialogCloseEvent.postValue(Event(Unit))

                    }

                    override fun onSuccess(value: VVBaseResponse) {


                    }

                    override fun onFailure(value: ResponseResult.ServiceError) {
                        Log.e(TAG, "ackRequest error : ${value.error.errorDescription}")

                        _dialogCloseEvent.postValue(Event(Unit))
                    }

                    override fun onFailure(failure: ResponseResult.Failure) {
                        Log.e(TAG, "ackRequest error : ${failure.errorCode}")

                        _dialogCloseEvent.postValue(Event(Unit))

                    }

                })
        }

    }


    fun joinConference(token: String) {

        var joinConferenceRequestModel =
            JoinConferenceRequestModel(participantUUID.value!!)

        viewModelScope.launch(dispatcherIO) {

            remoteReposiotry.joinConference(
                joinConferenceRequestModel,
                token,
                object : DataSource.GetCallback<JoinConferenceResponse> {
                    override fun onSuccess() {

                    }

                    override fun onSuccess(value: JoinConferenceResponse) {
                        val errors = value.errors
                        if (errors != null && !errors!!.isEmpty()) {
                            val error = errors!!.get(0)
                            _errorMessage.postValue(Event(ResponseResult.ServiceError(error)))
                            _dialogCloseEvent.postValue(Event(Unit))
                            Log.d(TAG, "joinConference error : ${error.errorDescription}")
                        } else {
                            participantsEvent.postValue(Event(ParticipantsRequestOptions()))
                        }
                    }

                    override fun onFailure(value: ResponseResult.ServiceError) {
                        Log.e(TAG, "joinConference Service error : ${value.error.errorDescription}")
                        // _errorMessage.postValue(Event(value))
                        participantsEvent.postValue(Event(ParticipantsRequestOptions()))
                        _dialogCloseEvent.postValue(Event(Unit))

                    }

                    override fun onFailure(failure: ResponseResult.Failure) {
                        Log.e(TAG, "joinConference failure : ${failure.errorCode}")
                        participantsEvent.postValue(Event(ParticipantsRequestOptions()))
                        _dialogCloseEvent.postValue(Event(Unit))
                        //_errorMessage.postValue(Event(failure))

                    }


                })
        }

    }

    fun leaveConference(conferenceToken: String, isHost: Boolean) {

        _conferenceResponse?.let {
            var signInUser = _conferenceResponse!!.signedInUser

            var leaveConferenceRequestModel =
                LeaveConferenceRequestModel(true, signInUser.role, isHost)

            viewModelScope.launch(dispatcherIO) {

                remoteReposiotry.leaveConference(
                    leaveConferenceRequestModel,
                    conferenceToken,
                    object : DataSource.GetCallback<LeaveConferenceResponse> {
                        override fun onSuccess() {

                        }

                        override fun onSuccess(value: LeaveConferenceResponse) {
                            val errors = value.errors
                            if (errors != null && !errors!!.isEmpty()) {
                                val error = errors!!.get(0)

                                viewModelScope.launch {
                                    _dialogCloseEvent.value = Event(Unit)
                                    _leaveConfereceEvent.postValue(Event(true))
                                }
                                Log.e(TAG, "disconnect error : ${error.errorDescription}")
                            } else {

                                viewModelScope.launch {
                                    _dialogCloseEvent.value = Event(Unit)
                                    _leaveConfereceEvent.postValue(Event(true))
                                }
                                Log.d(TAG, "disconnect success")
                            }
                            // clear all messages
                            clearChatMessages()
                        }

                        override fun onFailure(value: ResponseResult.ServiceError) {
                            viewModelScope.launch {
                                _dialogCloseEvent.value = Event(Unit)
                                _leaveConfereceEvent.value = Event(true)

                            }
                            Log.e(TAG, "LeaveConference error : ${value.error.errorDescription}")
                        }

                        override fun onFailure(failure: ResponseResult.Failure) {
                            viewModelScope.launch {
                                _dialogCloseEvent.value = Event(Unit)
                                _leaveConfereceEvent.value = Event(true)
                            }
                            Log.e(TAG, "LeaveConference error : ${failure.errorCode}")
                        }


                    })

            }

        }

    }

    private fun clearChatMessages() {
        sseMessageList.clear()
    }

    fun sendChatMessage(message: String) {
        Log.d(TAG, "message to send = $message")
        // TODO: validate input and sanitize (avoid scripting/injection)
        if (message.isNotEmpty()) {
            viewModelScope.launch(dispatcherIO) {
                remoteReposiotry.sendChatMessage(
                    message,
                    object : DataSource.GetCallback<VVBaseResponse> {

                        override fun onSuccess() {
                            viewModelScope.launch {
                                val sentMessage = ChatMessageModel(
                                    message,
                                    "",
                                    "",
                                    "",
                                    false
                                )
                                sentMessage.isReceivedMessage = false

                                // create sent message timestamp locally since Pexip
                                // doesn't create ANY timestamps for received or sent messages
                                sentMessage.timestamp = System.currentTimeMillis()
                                addChatMessageToList(sentMessage)
                            }
                        }

                        override fun onSuccess(value: VVBaseResponse) {
                        }

                        override fun onFailure(value: ResponseResult.ServiceError) {
                            Log.e(TAG, "send message error : ${value.error.errorDescription}")
                        }

                        override fun onFailure(failure: ResponseResult.Failure) {
                            Log.e(TAG, "send message failure : ${failure.errorCode}")

                        }

                    })
            }
        }
    }

    var serverEventListener = object : ServerSentEvent.Listener {
        override fun onOpen(p0: ServerSentEvent?, p1: Response?) {

        }

        override fun onRetryTime(p0: ServerSentEvent?, p1: Long): Boolean {
            Log.i(SERVER_SENT_EVENT_TAG, "onRetryTime::")
            return false
        }

        override fun onComment(p0: ServerSentEvent?, p1: String?) {
            Log.i(SERVER_SENT_EVENT_TAG, "onComment::$p1")
        }

        override fun onRetryError(p0: ServerSentEvent?, p1: Throwable?, p2: Response?): Boolean {
            Log.i(SERVER_SENT_EVENT_TAG, "onRetryError::" + p1?.message)
            return false;
        }

        override fun onPreRetry(p0: ServerSentEvent?, p1: Request?): Request? {

            return null
        }

        override fun onMessage(
            sse: ServerSentEvent?,
            id: String?,
            event: String?,
            message: String?
        ) {
            Log.d(SERVER_SENT_EVENT_TAG, "event: $event")
            if (event.equals(MESSAGE_RECEIVED_EVENT, true)) {
                message?.let {


                    Log.d(SERVER_SENT_EVENT_TAG, "message: $it")
                    // parse the json first into the model, the determine if it's a custom message
                    val model: ChatMessageModel =
                        Gson().fromJson(it, ChatMessageModel::class.java)
                    model.isReceivedMessage = true
                    // Pexip doesn't send a timestamp with chat messages so we have to create one
                    model.timestamp = System.currentTimeMillis()

                    viewModelScope.launch(Dispatchers.Main) {
                        _updateMessageRole.value = Event(model)
                    }
                    // check if this is a custom chat message, which we are using to
                    // send events like muting participants, e.g.: KPCustomMessage:EVENT_MODERATOR_CHANGED
                    if (model.messageBody.startsWith(MESSAGE_CUSTOM, ignoreCase = true)) {
                        handleCustomChatMessage(model)
                    } else {
                        // this is a regular chat message to be displayed on the screen
                        addChatMessageToList(model)
                    }
                }
            } else if (event.equals(MESSAGE_PARTICIPANT_CREATE, true) ||
                event.equals(MESSAGE_PARTICIPANT_UPDATE, true) ||
                event.equals(MESSAGE_PARTICIPANT_DELETE, true)
            ) {

                Log.d(TAG, "message: $message")
                message?.let {
                    setMuteEvent(message)
                    val uuid = getElementFromMessage(message).get(UUID)
                    val isMuted = getElementFromMessage(message).get(IS_MUTE)
                    val displayName = getElementFromMessage(message).get(DISPLAY_NAME)
                    triggerParticipantListRefresh(
                        ParticipantsRequestOptions(
                            uuid = uuid.asString,
                            source = KP_MEMBER_MOBILE_APP_ANDROID,
                            event = event!!,
                            isMuted = isMuted?.asBoolean
                        )
                    )
                    displayName?.let {
                        triggerJoinToast(displayName, uuid, event)
                    }
                }


            } else if (event.equals(PRESENTATION_FRAME, ignoreCase = true)) {


                val host =
                    "https://${_conferenceResponse!!.room.pexipNode}/api/client/v2/conferences/"
                val pexipToken = remoteReposiotry.getPexipUpdatedToken()
                val alias = _conferenceResponse!!.room.alias

                val url = "$host$alias/presentation.jpeg?id=$id&token=$pexipToken"
                Log.d(TAG, "presentation_url = $url")
                _onPresentationImageObservable.postValue(Event(url))


            } else if (event.equals(PRESENTATION_START, ignoreCase = true)) {

                _onPresentationObservable.postValue(Event(true))

            } else if (event.equals(PRESENTATION_STOP, ignoreCase = true)) {
                _onPresentationObservable.postValue(Event(false))
            } else if (event.equals(CALL_DISCONNECTED, true)) {
                Log.d(TAG, "message: $message")

                clearChatMessages()
                viewModelScope.launch {
                    _callEnded.value = Event(Unit)

                }

            }


        }

        override fun onClosed(p0: ServerSentEvent?) {
            Log.i(SERVER_SENT_EVENT_TAG, "onClosed::")
        }
    }

    private fun triggerJoinToast(
        displayName: JsonElement,
        uuid: JsonElement,
        event: String
    ) {
        viewModelScope.launch {
            paticipantJoinEvent.value = Event(
                ParticipantAnnouncementEvent(
                    displayName = displayName.asString,
                    uuid = uuid.asString,
                    event = event
                )
            )
        }
    }


    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun setMuteEvent(message: String) {
        val isMuted = getElementFromMessage(message).get(IS_MUTE)
        val uuid = getElementFromMessage(message).get(UUID)
        uuid?.let { muteUuid ->
            participantUUID.value?.let { participantUuid ->
                if (muteUuid.asString == participantUuid) {
                    isMuted?.let {
                        if (it.asString == YES) {
                            _muteObservable.postValue(Event(false))
                        } else {
                            _muteObservable.postValue(Event(true))
                        }
                        Log.d(TAG, it.asString)
                    }

                }
            }
        }

    }

    fun getElementFromMessage(message: String): JsonObject {
        val parser = JsonParser()
        val element = parser.parse(message)
        return element.asJsonObject
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun addChatMessageToList(model: ChatMessageModel) {
        // add new message at the beginning of the list
        sseMessageList.add(0, model)
        viewModelScope.launch(Dispatchers.Main) {
            // hasActiveObservers() only works if we remove the chat fragment when it's not visible
            showUnreadMessagesBadge.value = !receivedChatMessagesObservable.hasActiveObservers()
            if (!receivedChatMessagesObservable.hasActiveObservers()) chatToastMessage.value =
                Event(model)
            receivedChatMessagesObservable.setValue(sseMessageList)
        }
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun handleCustomChatMessage(message: ChatMessageModel) {
        // TODO: implement custom message handling
        Log.d(TAG, "Custom chat message = ${message.messageBody}")

        if (message.messageBody.contains(PAUSE_MESSAGE)) {
            _onHoldObservable.postValue(Event(message.origin))
        }
        // if the host role was transferred, we have to refresh the participants list
        else if (message.messageBody.contains(EVENT_MODERATOR_CHANGED)) {
            triggerParticipantListRefresh(ParticipantsRequestOptions(event = EVENT_MODERATOR_CHANGED))
        }

    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun triggerParticipantListRefresh(participantsRequestOptions: ParticipantsRequestOptions) {
        viewModelScope.launch {
            participantsEvent.value = Event(participantsRequestOptions)
        }
    }

    /**
     *  it returns the current user is host or not
     */
    fun isHost(partcipants: List<Participant>?): Boolean {

        _conferenceResponse?.let {
            var id = it.signedInUser.id
            partcipants?.forEach {
                it?.let {
                    if (it.id != null && it.vmrRole != null) {
                        if (it.id.equals(id) && it.vmrRole.equals(CHAIR)) {
                            return true
                        }

                    }
                }
            }
        }

        return false
    }

    fun surveyRequest(survey: Survey) {
        Log.d(TAG,survey.toString())
        _conferenceResponse?.let {
            viewModelScope.launch(dispatcherIO) {
                remoteReposiotry.sendSurvey(
                    surveyRequestModel = SurveyRequestModel(
                        survey = survey
                    )
                    , callback = object : DataSource.GetCallback<VVBaseResponse> {
                        override fun onSuccess() {
                            Log.d(TAG,"survey sent successfully")
                            viewModelScope.launch(Dispatchers.Main) {
                                leaveFromSurvey()
                            }
                        }

                        override fun onSuccess(value: VVBaseResponse) {
                            Log.d(TAG,"survey sent successfully")
                            viewModelScope.launch(Dispatchers.Main) {
                                leaveFromSurvey()
                            }
                        }

                        override fun onFailure(value: ResponseResult.ServiceError) {
                            Log.e(TAG, "survey Service error : ${value.error.errorDescription}")
                            viewModelScope.launch(Dispatchers.Main) {
                                leaveFromSurvey()
                            }
                        }

                        override fun onFailure(errrorCode: ResponseResult.Failure) {
                            Log.e(TAG, "errorcode : ${errrorCode.errorCode}")
                            viewModelScope.launch(Dispatchers.Main) {
                                leaveFromSurvey()
                            }
                        }
                    })
            }
        }
    }

    val MINUTES_UNTIL_TIMEOUT: Long = 2
    val default_TimeOut_responce = ResponseError("","","0","","")
    var unepectedHostLeaveTimer : ReceiveChannel<Unit>? = null
    var isTicketRunning : Boolean = false

    @ObsoleteCoroutinesApi
    suspend fun timeoutHandlerHostLeaveUnexpectedly()
    {
        if (isTicketRunning) return
        unepectedHostLeaveTimer = ticker(MINUTES_UNTIL_TIMEOUT * 60000,MINUTES_UNTIL_TIMEOUT * 60000,viewModelJob)
        isTicketRunning = true
        for (event in unepectedHostLeaveTimer!!){
            unepectedHostLeaveTimer?.cancel()
            unepectedHostLeaveTimer = null
            isTicketRunning = false
            _errorMessage.value = Event(ResponseResult.ServiceError(default_TimeOut_responce))
        }
    }
    fun leaveFromSurvey(){
        insideTheSurvey = false
        _leaveFromSurvey.value = Event(Unit)
    }

    fun hideTabs() {
        _hideTabs.value = Event(Unit)
    }

    @ObsoleteCoroutinesApi
    fun startHostLeftTimer() {
        viewModelScope.launch {
            timeoutHandlerHostLeaveUnexpectedly()
        }
    }

    fun cancelHostTimer() {
        viewModelScope.launch {
            unepectedHostLeaveTimer?.cancel()
            unepectedHostLeaveTimer = null
            isTicketRunning = false
        }
    }


    companion object {

        val TAG = VideoViewModel::class.java.simpleName
        const val MESSAGE_RECEIVED_EVENT = "message_received"
        const val MESSAGE_PARTICIPANT_UPDATE = "participant_update"
        const val MESSAGE_PARTICIPANT_DELETE = "participant_delete"
        const val MESSAGE_PARTICIPANT_CREATE = "participant_create"
        const val CALL_DISCONNECTED = "call_disconnected"
        const val MESSAGE_CUSTOM = "KPCustomMessage"
        const val PRESENTATION_FRAME = "presentation_frame"
        const val PRESENTATION_START = "presentation_start"
        const val PRESENTATION_STOP = "presentation_stop"
        const val IS_MUTE = "is_muted"
        const val DISPLAY_NAME = "display_name"
        const val ROLE = "role"
        const val UUID = "uuid"
        const val KP_MEMBER_MOBILE_APP_ANDROID = "KP_MEMBER_MOBILE_APP_ANDROID"

    }

}

