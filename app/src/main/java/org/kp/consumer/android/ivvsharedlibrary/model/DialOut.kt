package org.kp.consumer.android.ivvsharedlibrary.model

import com.google.gson.annotations.SerializedName

data class DialOut (
    @SerializedName("number")
    val number: String,

    @SerializedName("displayName")
    val displayName: String,

    @SerializedName("role")
    val role: String
    )