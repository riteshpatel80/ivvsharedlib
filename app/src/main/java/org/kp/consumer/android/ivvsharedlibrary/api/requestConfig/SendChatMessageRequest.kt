package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig

import org.kp.consumer.android.ivvsharedlibrary.util.Log
import com.google.gson.Gson
import org.kp.kpnetworking.request.BaseRequestConfig

internal class SendChatMessageRequest(
    message: SendChatMessageModel,
    pexipHost: String, roomAlias: String, pin: String, registrationToken: String
) :
    BaseRequestConfig(
        REQUEST_TYPE.POST,
        "$pexipHost${RefreshTokenRequest.API_VERSION}$roomAlias${SEND_MESSAGE_PATH}"
    ) {

    init {
        addParam(RefreshTokenRequest.TOKEN_KEY, registrationToken)
        addParam(RefreshTokenRequest.PIN_KEY, pin)
        body = Gson().toJson(message)
        Log.d("SendChatMessageRequest", "url = $pexipHost${RefreshTokenRequest.API_VERSION}$roomAlias${SEND_MESSAGE_PATH}");
        Log.d(TAG, "request body = $body")
    }

    companion object {
        val TAG = SendChatMessageRequest::class.java.simpleName
        private const val SEND_MESSAGE_PATH = "/message"
    }
}