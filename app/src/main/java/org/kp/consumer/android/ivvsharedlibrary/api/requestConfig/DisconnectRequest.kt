package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig

import org.kp.consumer.android.ivvsharedlibrary.util.Log
import org.kp.kpnetworking.request.BaseRequestConfig

/**
 *
 *
 * Created by venkatakalluri on 2019-12-03.
 * Copyright © 2019 Kaiser Permanente. All rights reserved.
 */
class DisconnectRequest (pexipHost: String,pin: String, roomAlias: String,participantUUID: String,token:String,callUUID: String): BaseRequestConfig(
    REQUEST_TYPE.POST, "$pexipHost$API_VERSION$roomAlias$PARTCIPANTS_KEY$participantUUID$URL_KEY$callUUID$DISCONNECT_KEY") {


    init {
        Log.d("DisconnectRequest", "url = $pexipHost$API_VERSION$roomAlias$PARTCIPANTS_KEY$participantUUID$URL_KEY$callUUID$DISCONNECT_KEY");

        addHeader(PIN_KEY, pin)
        addHeader(TOKEN_KEY, token)

        body = "{}"

    }


    companion object {

        const val PIN_KEY="pin"
        const val TOKEN_KEY="token"
        const val CONTENT_TYPE ="content_type"
        const val PARTCIPANTS_KEY="/participants/"
        const val URL_KEY="/calls/"
        const val DISCONNECT_KEY="/disconnect"
        const val API_VERSION= "/api/client/v2/conferences/"

    }

}