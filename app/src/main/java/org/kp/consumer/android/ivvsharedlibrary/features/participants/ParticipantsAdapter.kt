
package org.kp.consumer.android.ivvsharedlibrary.features.participants
import androidx.databinding.DataBindingUtil
import android.view.ViewGroup
import android.view.LayoutInflater
import androidx.recyclerview.widget.RecyclerView
import org.kp.consumer.android.ivvsharedlibrary.R
import org.kp.consumer.android.ivvsharedlibrary.databinding.ParticipantItemBinding
import org.kp.consumer.android.ivvsharedlibrary.model.Participant


internal class ParticipantsAdapter: RecyclerView.Adapter<ParticipantsAdapter.ParticipantViewHolder>() {

    var participants = emptyList<Participant>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParticipantViewHolder {
        val binding: ParticipantItemBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
            R.layout.participant_item, parent, false)
        return ParticipantViewHolder(binding)
    }

    override fun getItemCount() = participants.size

    override fun onBindViewHolder(holder: ParticipantViewHolder, position: Int) {
        val item = participants[position]
        holder.bind(item)
    }

    fun setData(items: List<Participant>) {
        participants = items
        notifyDataSetChanged()
    }


    inner class ParticipantViewHolder(private val binding: ParticipantItemBinding) : RecyclerView.ViewHolder(binding.root) {

        var itemRowBinding: ParticipantItemBinding

        init{

            itemRowBinding = binding

        }

        fun bind(item: Participant) {
            binding.setItem(item)
            binding.executePendingBindings()
        }
    }


}