package org.kp.consumer.android.ivvsharedlibrary.api.response

import org.json.JSONObject

class CallResponseConverter: Converter {
    override fun convert(responseJson: String): CallResponse {

        var result = JSONObject(responseJson).get("result")

        return Converter.convertFromJson(result.toString())
    }

}
