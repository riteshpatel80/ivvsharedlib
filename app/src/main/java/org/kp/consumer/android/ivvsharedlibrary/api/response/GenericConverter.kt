package org.kp.consumer.android.ivvsharedlibrary.api.response

import org.kp.consumer.android.ivvsharedlibrary.util.Log
import org.json.JSONObject

internal class GenericConverter : Converter{
    override fun convert(responseJson: String): GenericResponse {
        Log.d("Generic Converter","response : "+ responseJson)
        return Converter.convertFromJson(responseJson)

    }
}