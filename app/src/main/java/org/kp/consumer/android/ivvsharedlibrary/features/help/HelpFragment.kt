package org.kp.consumer.android.ivvsharedlibrary.features.help

import android.annotation.SuppressLint
import android.app.Activity
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.Observer
import android.content.Context
import android.graphics.Bitmap
import org.kp.consumer.android.ivvsharedlibrary.R
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.help_fragment.*
import android.net.http.SslError
import android.webkit.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import org.kp.consumer.android.ivvsharedlibrary.ViewModelFactory
import org.kp.consumer.android.ivvsharedlibrary.databinding.HelpFragmentBinding
import org.kp.consumer.android.ivvsharedlibrary.features.VideoViewModel
import org.kp.consumer.android.ivvsharedlibrary.util.Log
import java.util.*

class HelpFragment : Fragment() {
    companion object{
        val MID = "MID"
        val HELP_PROVIDER = "help"
    }

    private lateinit var activity: Activity
    override fun onAttach(context: Context) {
        super.onAttach(context)

        activity = context as Activity
    }

    private lateinit var viewModel: VideoViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.help_fragment, container, false)

        HelpFragmentBinding.bind(view).apply {


            viewModel = activity?.run {
                ViewModelProvider(
                    requireActivity(),
                    ViewModelFactory.getInstance(activity.application)
                )
                    .get(VideoViewModel::class.java)

            }

        }


        return view
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //TODO: change the url
        viewModel.setUpConferenceResponse.observe(viewLifecycleOwner, Observer {
            val baseURL = viewModel.getConferenceResponse()?.webConfig?.httpConfig?.webView?.help?.baseUrl ?: ""
            val userRegion = viewModel.getConferenceResponse()?.signedInUser?.region ?: MID
            val completeURL = "$baseURL/$userRegion/${Locale.getDefault().language}/$HELP_PROVIDER"
            Log.d(this.javaClass.simpleName, completeURL)
            webView.loadUrl(completeURL)

        })
        val settings = webView.settings
        settings.javaScriptEnabled = true
        settings.domStorageEnabled = true
        webView.webViewClient = MyWebViewClient()
    }

    private inner class MyWebViewClient : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            webView.loadUrl(url)
            return true
        }

        override fun onReceivedSslError(
            view: WebView, handler: SslErrorHandler,
            error: SslError
        ) {
            handler.cancel()
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            help_progress.visibility = View.VISIBLE
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            help_progress.visibility = View.GONE
        }

        override fun onReceivedError(
            view: WebView?,
            request: WebResourceRequest?,
            error: WebResourceError?
        ) {
            super.onReceivedError(view, request, error)
            help_progress.visibility = View.GONE

        }
    }

    fun reload(){
        webView.reload()
    }

}
