package org.kp.consumer.android.ivvsharedlibrary.controller

import android.app.Notification
import org.kp.consumer.android.ivvsharedlibrary.model.VVSession
import org.kp.consumer.android.ivvsharedlibrary.model.VVResult
import org.kp.consumer.android.ivvsharedlibrary.error.VVError
import org.kp.consumer.android.ivvsharedlibrary.util.VVLogLevel
import java.io.Serializable

interface DefaultVVSessionSessionController : Serializable {

    /**
     *  Sets the Logging Level
     *
     * @param logLevel
     */
    suspend fun setLogLevel(logLevel: VVLogLevel)

    /**
     *  Starts the VV
     * @param layoutId where the VideoFragment Will be placed
     * @param vvConferenceToken
     * @param isHost
     * @notification to launch the application if app goes to background
     *
     * Hosting Activity launch mode is singleTop
     *
     * @return the result, containing a VVSession if successful or VVError if unsuccessful
     */
    suspend fun startVV(layoutId: Int, vvConferenceToken: String, isHost: Boolean,notification: Notification): VVResult<VVSession, VVError>

    /**
     *  Retrieves the Bandwidth Information while in a Video Visit
     *
     * @return the bandwsithd information.  0 if no video visit is running
     */
    suspend fun getVVBandwidth(): Int

    /**
     *  Generic method to Sends a message into the Controller
     *
     * @param message
     */
    suspend fun sendMessage(message: Any): Int

    /**
     *  Ends the VV
     *
     *
     */
    suspend fun endVV()



}
