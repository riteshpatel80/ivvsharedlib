package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig

import com.google.gson.annotations.SerializedName
import org.kp.consumer.android.ivvsharedlibrary.model.Appointment
import org.kp.consumer.android.ivvsharedlibrary.model.SignedInUser
import java.io.Serializable


data class JoinConferenceRequestModel(
    @SerializedName("pexipParticipantUUID")
    val pexipParticipantUUID: String

) : Serializable