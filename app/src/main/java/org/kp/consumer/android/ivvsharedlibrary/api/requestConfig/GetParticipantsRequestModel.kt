package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig

import com.google.gson.annotations.SerializedName
import java.io.Serializable


internal data class GetParticipantsRequestModel(

    @SerializedName("appointmentMeetingRoomNumber")
    val appointmentMeetingRoomNumber: String,

    @SerializedName("appointmentRegion")
    val appointmentRegion: String,

    @SerializedName("appointmentVisitType")
    val appointmentVisitType: String?,

    @SerializedName("uuid")
    var uuid: String = "0",

    @SerializedName("time")
    val time: String,

    @SerializedName("source")
    val source: String,

    @SerializedName("event")
    val event: String


) : Serializable