package org.kp.consumer.android.ivvsharedlibrary.model

import com.google.gson.annotations.SerializedName


data class Room(

    @SerializedName("room")
    val room: String?=null,

    @SerializedName("alias")
    val alias: String,

    @SerializedName("pin")
    val pin: String,

    @SerializedName("pexipNode")
    val pexipNode: String
)