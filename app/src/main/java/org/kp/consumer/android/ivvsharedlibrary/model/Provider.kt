package org.kp.consumer.android.ivvsharedlibrary.model

import com.google.gson.annotations.SerializedName


data class Provider(

    @SerializedName("id")
    val id: String,

    @SerializedName("idType")
    val idType: String,

    @SerializedName("name")
    val name: String,

    @SerializedName("region")
    val region: String
)