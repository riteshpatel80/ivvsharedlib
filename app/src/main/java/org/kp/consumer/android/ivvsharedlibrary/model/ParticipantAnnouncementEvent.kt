package org.kp.consumer.android.ivvsharedlibrary.model

data class ParticipantAnnouncementEvent(val displayName : String, val uuid: String, val event: String) {
}