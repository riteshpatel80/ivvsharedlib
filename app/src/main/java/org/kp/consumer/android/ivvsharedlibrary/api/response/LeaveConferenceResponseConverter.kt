package org.kp.consumer.android.ivvsharedlibrary.api.response


internal class LeaveConferenceResponseConverter : Converter {
    override fun convert(responseJson: String) : LeaveConferenceResponse = Converter.convertFromJson(responseJson)
}