package org.kp.consumer.android.ivvsharedlibrary.api.task

import org.kp.consumer.android.ivvsharedlibrary.api.response.Converter
import org.kp.consumer.android.ivvsharedlibrary.api.response.ResponseResult
import org.kp.kpnetworking.dispatcher.RequestHandlerSingleton
import org.kp.kpnetworking.request.BaseRequestConfig

internal open class VVBaseHttpTask(private val config: BaseRequestConfig, private val converter: Converter) {

    fun makeRequest() : ResponseResult {
        val responseData =  RequestHandlerSingleton.getInstance().makeRequest(config)
        return if (responseData.wasSuccessful()) {
            val convertedResponse = converter.convert(responseData.response)
            if (convertedResponse.hasErrors()) {
                ResponseResult.ServiceError(convertedResponse.errors!!.first())
            } else {
                ResponseResult.Success(convertedResponse)
            }
        } else {
            ResponseResult.Failure(responseData.httpStatusCode.toString())
        }
    }
}