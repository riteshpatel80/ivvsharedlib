package org.kp.consumer.android.ivvsharedlibrary.api.response


internal class SetupConferenceResponseConverter : Converter {
    override fun convert(responseJson: String) : SetupConferenceResponse = Converter.convertFromJson(responseJson)
}