package org.kp.consumer.android.ivvsharedlibrary.model

interface VVEventHandler {

    fun onError()

    fun onSessionStarted()

    fun willJoinVV()

    fun didJoinVV()

    fun willEndVV()

    fun didEndVV()

    fun lowBandwidth()
}