package org.kp.consumer.android.ivvsharedlibrary.api.task

import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.JoinConferenceRequest
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.JoinConferenceRequestModel
import org.kp.consumer.android.ivvsharedlibrary.api.response.JoinConferenceResponseConverter


internal class JoinConferenceTask(
    joinConferenceRequestModel: JoinConferenceRequestModel,
    registrationToken: String
) : VVBaseHttpTask(
    JoinConferenceRequest(joinConferenceRequestModel, registrationToken),
    converter = JoinConferenceResponseConverter()
)