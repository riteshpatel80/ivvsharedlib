package org.kp.consumer.android.ivvsharedlibrary.webrtc


//import org.webrtc.VideoCapturerAndroid
//import org.webrtc.VideoRenderer
//import org.webrtc.VideoRendererGui
import android.app.Activity
import android.content.Context
import android.view.View
import kotlinx.android.synthetic.main.video_content.view.*
import org.kp.consumer.android.ivvsharedlibrary.util.Log
import org.webrtc.*
import org.webrtc.audio.AudioDeviceModule
import org.webrtc.audio.JavaAudioDeviceModule.*
import java.util.*
import kotlin.concurrent.timerTask

object PeerConnectionProvider {

    private const val TAG = "PeerConnectionProvider"
    private const val VIDEO_TRACK_ID = "PEXIPv0"
    private const val AUDIO_TRACK_ID = "PEXIPa0"
//    private const val VIDEO_CODEC_VP8 = "VP8"
//    private const val VIDEO_CODEC_VP9 = "VP9"
//    private const val VIDEO_CODEC_H264 = "H264"
//    private const val VIDEO_CODEC_H264_BASELINE = "H264 Baseline"
//    private const val VIDEO_CODEC_H264_HIGH = "H264 High"

    var peerConnection: PeerConnection? = null
    var videoCapturerAndroid: VideoCapturer? = null // VideoCapturerAndroid? = null
    var localAudioTrack: AudioTrack? = null
    var localVideoTrack: VideoTrack? = null
    var removeVideoRenderer: SurfaceViewRenderer? = null // VideoRenderer? = null
    var rtcConstraint = MediaConstraints()

    private var peerConnectionFactory: PeerConnectionFactory? = null
    private var localMediaStream: MediaStream? = null
    var localVideoSource: VideoSource? = null
    private var localAudioSource: AudioSource? = null
    private var localVideoRenderer: SurfaceViewRenderer? = null //VideoRenderer? = null

    private var isNetworkVideoLow: Boolean = false
    var isStartedVideo: Boolean = false

    var lastPacketLossTotal = 0
    var lastPacketReceveidTotal = 0
    var lastFiveLossvaluesCounter = 0
    var tempLastPacketLossTotal = 0
    var tempLastPacketReceveidTotal = 0
    var timer: Timer? = null

    var mContext: Context? = null
    var mVideoView: View? = null
    var rootEglBase: EglBase? = null
    var rsink: ArrayList<VideoSink>? = null

    fun init(context: Context, peerConnectionObserver: PeerConnection.Observer, videoView: View, eglbase:EglBase, remotesink: ArrayList<VideoSink> ) {

        mContext = context
        mVideoView = videoView

        rootEglBase = eglbase
        rsink = remotesink
        lastPacketLossTotal = 0
        lastPacketReceveidTotal = 0
        lastFiveLossvaluesCounter = 0

        isNetworkVideoLow = false

        val adm = createJavaAudioDevice()

        val options = PeerConnectionFactory.InitializationOptions.builder(context)
            .setEnableInternalTracer(true)
            .setFieldTrials("WebRTC-H264HighProfile/Enabled/")
            .createInitializationOptions()
        PeerConnectionFactory.initialize(options)

        val encoderFactory = DefaultVideoEncoderFactory(
            rootEglBase!!.eglBaseContext, true /* enableIntelVp8Encoder */, true
        )
        val decoderFactory = DefaultVideoDecoderFactory(rootEglBase!!.eglBaseContext)
        //val encoderFactory: VideoEncoderFactory = SoftwareVideoEncoderFactory()
        //val decoderFactory: VideoDecoderFactory = SoftwareVideoDecoderFactory()

        val option = PeerConnectionFactory.Options()
        peerConnectionFactory = PeerConnectionFactory.builder()
            .setOptions(option)
            .setAudioDeviceModule(adm)
            .setVideoEncoderFactory(encoderFactory)
            .setVideoDecoderFactory(decoderFactory)
            .createPeerConnectionFactory()

        adm?.release()

        videoCapturerAndroid = createCameraCapturer(Camera2Enumerator(context))

        val surfaceTextureHelper = SurfaceTextureHelper.create("CaptureThread", rootEglBase?.getEglBaseContext())
        localVideoSource = peerConnectionFactory!!.createVideoSource(videoCapturerAndroid!!.isScreencast)
        videoCapturerAndroid?.initialize(surfaceTextureHelper, mContext, localVideoSource?.capturerObserver)
        videoCapturerAndroid?.startCapture(1280, 720, 30)

        localVideoTrack = peerConnectionFactory!!.createVideoTrack(VIDEO_TRACK_ID, localVideoSource)
        localVideoTrack!!.setEnabled(true)

        localAudioSource = peerConnectionFactory?.createAudioSource(MediaConstraints())
        localAudioTrack =
            peerConnectionFactory?.createAudioTrack(AUDIO_TRACK_ID, localAudioSource)
        localAudioTrack?.setEnabled(true)

        localMediaStream = peerConnectionFactory!!.createLocalMediaStream("PEXIP")
        localMediaStream?.addTrack(localVideoTrack)
        localMediaStream?.addTrack(localAudioTrack)

        try {
           localVideoTrack?.addSink { rsink }
            //localVideoTrack?.addRenderer(localVideoRenderer)
        } catch (e: Exception) {
            Log.e(TAG, "Unable to created renderer", e)
            TODO("Add error handling to return control to tenant app")
        }

        //val iceServers = ArrayList<PeerConnection.IceServer>()
        //iceServers.add(new PeerConnectionProvider.IceServer("stun:stun.l.google.com:19302"));

        rtcConstraint = MediaConstraints()
        rtcConstraint.mandatory.add(MediaConstraints.KeyValuePair("OfferToReceiveAudio", "true"))
        rtcConstraint.mandatory.add(MediaConstraints.KeyValuePair("OfferToReceiveVideo", "true"))

//        peerConnection = peerConnectionFactory?.createPeerConnection(
//            iceServer,
//            MediaConstraints(),
//            peerConnectionObserver
//        )

        peerConnection = peerConnectionFactory?.createPeerConnection(iceServer,peerConnectionObserver)

        peerConnection?.addStream(localMediaStream)

        startTimer()

        Log.d(TAG, "initPeerConnection complete")
    }

    private val iceServer = listOf(
        PeerConnection.IceServer.builder("stun:stun.l.google.com:19302")
            .createIceServer()
    )

    fun createJavaAudioDevice(): AudioDeviceModule? { // Enable/disable OpenSL ES playback.
//        if (!peerConnectionParameters.useOpenSLES) {
//            android.util.Log.w(
//                TAG,
//                "External OpenSLES ADM not implemented yet."
//            )
//            // TODO(magjed): Add support for external OpenSLES ADM.
//        }
        // Set audio record error callbacks.
        val audioRecordErrorCallback: AudioRecordErrorCallback = object :
            AudioRecordErrorCallback {
            override fun onWebRtcAudioRecordInitError(errorMessage: String) {
                android.util.Log.e(
                    TAG,
                    "onWebRtcAudioRecordInitError: $errorMessage"
                )
            }

            override fun onWebRtcAudioRecordStartError(
                errorCode: AudioRecordStartErrorCode,
                errorMessage: String
            ) {
                android.util.Log.e(
                    TAG,
                    "onWebRtcAudioRecordStartError: $errorCode. $errorMessage"
                )
            }

            override fun onWebRtcAudioRecordError(errorMessage: String) {
                android.util.Log.e(
                    TAG,
                    "onWebRtcAudioRecordError: $errorMessage"
                )
            }
        }
        val audioTrackErrorCallback: AudioTrackErrorCallback = object :
            AudioTrackErrorCallback {
            override fun onWebRtcAudioTrackInitError(errorMessage: String) {
                android.util.Log.e(
                    TAG,
                    "onWebRtcAudioTrackInitError: $errorMessage"
                )
            }

            override fun onWebRtcAudioTrackStartError(
                errorCode: AudioTrackStartErrorCode,
                errorMessage: String
            ) {
                android.util.Log.e(
                    TAG,
                    "onWebRtcAudioTrackStartError: $errorCode. $errorMessage"
                )
            }

            override fun onWebRtcAudioTrackError(errorMessage: String) {
                android.util.Log.e(
                    TAG,
                    "onWebRtcAudioTrackError: $errorMessage"
                )
            }
        }
        // Set audio record state callbacks.
        val audioRecordStateCallback: AudioRecordStateCallback = object :
            AudioRecordStateCallback {
            override fun onWebRtcAudioRecordStart() {
                android.util.Log.i(TAG, "Audio recording starts")
            }

            override fun onWebRtcAudioRecordStop() {
                android.util.Log.i(TAG, "Audio recording stops")
            }
        }
        // Set audio track state callbacks.
        val audioTrackStateCallback: AudioTrackStateCallback = object :
            AudioTrackStateCallback {
            override fun onWebRtcAudioTrackStart() {
                android.util.Log.i(TAG, "Audio playout starts")
            }

            override fun onWebRtcAudioTrackStop() {
                android.util.Log.i(TAG, "Audio playout stops")
            }
        }
        return builder(mContext)
            .setUseHardwareAcousticEchoCanceler(true)
            .setUseHardwareNoiseSuppressor(true)
            .setAudioRecordErrorCallback(audioRecordErrorCallback)
            .setAudioTrackErrorCallback(audioTrackErrorCallback)
            .setAudioRecordStateCallback(audioRecordStateCallback)
            .setAudioTrackStateCallback(audioTrackStateCallback)
            .createAudioDeviceModule()
    }

    private fun createCameraCapturer(enumerator: CameraEnumerator): VideoCapturer? {
        val deviceNames = enumerator.deviceNames
        // First, try to find front facing camera
        Logging.d(
            TAG,
            "Looking for front facing cameras."
        )
        for (deviceName in deviceNames) {
            if (enumerator.isFrontFacing(deviceName)) {
                Logging.d(
                    TAG,
                    "Creating front facing camera capturer."
                )
                val videoCapturer: VideoCapturer? = enumerator.createCapturer(deviceName, null)
                if (videoCapturer != null) {
                    return videoCapturer
                }
            }
        }
        // Front facing camera not found, try something else
        Logging.d(TAG, "Looking for other cameras.")
        for (deviceName in deviceNames) {
            if (!enumerator.isFrontFacing(deviceName)) {
                Logging.d(
                    TAG,
                    "Creating other camera capturer."
                )
                val videoCapturer: VideoCapturer? = enumerator.createCapturer(deviceName, null)
                if (videoCapturer != null) {
                    return videoCapturer
                }
            }
        }
        return null
    }

    //stop the timer
    fun stopTimer() {
        timer?.cancel()
        timer = null
    }

    //timer run for every second to get the stats
    fun startTimer() {
        if (timer == null) {
            timer = Timer();

            timer?.scheduleAtFixedRate(timerTask {
                peerConnection?.getStats(object : StatsObserver {
                    override fun onComplete(reports: Array<out StatsReport>?) {
                        checkAndShowLowNetwork(mContext!!, reports, mVideoView!!)

                    }
                }, null)

            }, 0, 1000)
        }

    }


    fun freeAllResources() {
        if (localVideoTrack != null) {
            localVideoTrack?.setEnabled(false)
            //localVideoTrack?.removeRenderer(localVideoRenderer)
            localVideoTrack?.removeSink { localVideoRenderer }
            localVideoTrack = null
        }

        if (localAudioTrack != null) {
            localAudioTrack?.setEnabled(false)
            localAudioTrack?.dispose()
            localAudioTrack = null
        }
        //localVideoSource?.stop()
        localVideoSource?.capturerObserver?.onCapturerStopped()

        peerConnection?.removeStream(localMediaStream)
        peerConnection?.close()
        peerConnection = null


        videoCapturerAndroid?.dispose()
        videoCapturerAndroid = null

        localMediaStream = null
        localVideoSource = null
        removeVideoRenderer = null
        peerConnectionFactory = null

        stopTimer()


    }


    private fun checkAndShowLowNetwork(
        context: Context,
        reports: Array<out StatsReport>?,
        videoView: View
    ) {

        reports?.let {


            for (report in reports) {


                var isReceiveVideo = false


                var packetsRecievedLoss = 0
                var packetsReceived = 0
//                var bytesReceived = 0


                // considering the report based on  "type" is ssrc and "id" having receive.
                // all meta data exist under values array and need to iterate
                // calculate last 5 seconds data.

                // received reports are two types : audio & video. Considering the video report only.
                // So, holding data into temp variables while iterating data.
                // After iteration, if report type is video type , then will assign this temp Packet varialbes to actual packet variables(lastrecent value)
                // To find difference of how many packets received & lost, will do subtract from this iteration data to last recent value

                // formula : (packetsLost/(packetsLost+packetsReceived))>.03

                if (report.type.equals("ssrc") && report.id.contains("recv")) {
                    // Log.d(TAG, "Stats: " + report.toString());
                    var iterator = report.values.iterator()

                    while (iterator.hasNext()) {
                        var statReportValue = iterator.next()


                        if (statReportValue.name.equals("transportId")) {
                            if (statReportValue.value.contains(
                                    "video"
                                )
                            ) {
                                // it holds the current report as video type and this value determines whether data should be inserted into array or not
                                isReceiveVideo = true
                            } else {

                                break
                            }

                        }


                        if (statReportValue.name.equals("packetsLost")) {

                            packetsRecievedLoss =
                                statReportValue.value.toInt() - lastPacketLossTotal


                            tempLastPacketLossTotal = statReportValue.value.toInt()

                        }

                        if (statReportValue.name.equals("packetsReceived")) {

                            packetsReceived =
                                statReportValue.value.toInt() - lastPacketReceveidTotal


                            tempLastPacketReceveidTotal = statReportValue.value.toInt()

                        }


                    }





                    if (isReceiveVideo) {
                        //storing last 5 seconds data.. it follows first in first out approach if arraylist size reached 5
                        if (packetsReceived > 0) {
                            var packetTotal = packetsReceived + packetsRecievedLoss

                            var videoPlRatio =
                                packetsRecievedLoss.toDouble() / packetTotal.toDouble()


                            if (videoPlRatio > 0.03) {

                                lastFiveLossvaluesCounter++

                                if (lastFiveLossvaluesCounter == 5) {

                                    isNetworkVideoLow = true
                                }


                            } else {
                                lastFiveLossvaluesCounter = 0
                                isNetworkVideoLow = false

                            }
                            Log.d(
                                TAG,
                                "packets received : $packetsReceived   packetsLost : $packetsRecievedLoss   videoPlRatio: $videoPlRatio "
                            )

                        } else {

                            lastFiveLossvaluesCounter++

                            if (lastFiveLossvaluesCounter == 5) {

                                if (isStartedVideo)
                                    isNetworkVideoLow = true
                            }


                        }

                        //assign temp into actual variables
                        lastPacketLossTotal = tempLastPacketLossTotal
                        lastPacketReceveidTotal = tempLastPacketReceveidTotal
                    }

                }

            }



            if (isNetworkVideoLow) {
                (context as Activity).runOnUiThread {
                    videoView.networkLowTv.visibility = View.VISIBLE
                }
            } else {
                (context as Activity).runOnUiThread {
                    videoView.networkLowTv.visibility = View.GONE
                }
            }


        }

    }

}