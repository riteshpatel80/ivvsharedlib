package org.kp.consumer.android.ivvsharedlibrary.api.task

import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.LeaveConferenceRequest
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.LeaveConferenceRequestModel
import org.kp.consumer.android.ivvsharedlibrary.api.response.LeaveConferenceResponseConverter


internal class LeaveConferenceTask(
    leaveConferenceRequestModel: LeaveConferenceRequestModel,
    registrationToken: String
) : VVBaseHttpTask(
    LeaveConferenceRequest(leaveConferenceRequestModel, registrationToken),
    converter = LeaveConferenceResponseConverter()
)