package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig

import org.kp.consumer.android.ivvsharedlibrary.util.Log
import com.google.gson.Gson


internal class JoinConferenceRequest(joinConfRequestModel: JoinConferenceRequestModel, registrationToken: String) : VVBaseRequest(REQUEST_TYPE.PUT, JOIN_CONF_URL_PATH) {

    init {
        addHeader(AUTHORIZATION, "$BEARER $registrationToken")

        body = Gson().toJson(joinConfRequestModel)

    }

    companion object {
        private const val JOIN_CONF_URL_PATH = "/join"
        private val TAG =JoinConferenceRequest::class.java.simpleName
    }
}