/*
 *  Copyright 2017 Google Inc.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.kp.consumer.android.ivvsharedlibrary



import android.annotation.SuppressLint
import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.annotation.VisibleForTesting
import kotlinx.coroutines.Dispatchers
import org.kp.consumer.android.ivvsharedlibrary.data.source.Repository
import org.kp.consumer.android.ivvsharedlibrary.features.dialout.DialoutViewModel
import org.kp.consumer.android.ivvsharedlibrary.features.VideoViewModel
import org.kp.consumer.android.ivvsharedlibrary.features.chat.ChatViewModel
import org.kp.consumer.android.ivvsharedlibrary.features.participants.ParticipantsViewModel
import org.kp.consumer.android.ivvsharedlibrary.features.waitingRoom.WaitingRoomViewModel


/**
 *
 *
 *
 * This creator is to showcase how to inject dependencies into ViewModels. It's not
 * actually necessary in this case, as the product ID can be passed in a public method.
 */
internal class ViewModelFactory private constructor(
        private val tasksRepository: Repository
) : ViewModelProvider.NewInstanceFactory() {

    val dispatcher = Dispatchers.IO

    override fun <T : ViewModel> create(modelClass: Class<T>) =
            with(modelClass) {
                when {
                    isAssignableFrom(VideoViewModel::class.java) ->
                        VideoViewModel(tasksRepository,dispatcher)
                    isAssignableFrom(ParticipantsViewModel::class.java) ->
                        ParticipantsViewModel(tasksRepository)
                    isAssignableFrom(DialoutViewModel::class.java) ->
                        DialoutViewModel(tasksRepository,dispatcher)
                    isAssignableFrom(ChatViewModel::class.java) ->
                        ChatViewModel(tasksRepository)
                    isAssignableFrom(WaitingRoomViewModel::class.java) ->
                        WaitingRoomViewModel(tasksRepository)

                    else ->
                        throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
                }
            } as T

    companion object {

        @SuppressLint("StaticFieldLeak")
        @Volatile private var INSTANCE: ViewModelFactory? = null

        fun getInstance(application: Application) =
                INSTANCE
                        ?: synchronized(ViewModelFactory::class.java) {
                    INSTANCE
                            ?: ViewModelFactory(
                                Injection.provideTasksRepository(application.applicationContext)
                            )
                            .also { INSTANCE = it }
                }


        @VisibleForTesting
        fun destroyInstance() {
            INSTANCE = null
        }
    }
}
