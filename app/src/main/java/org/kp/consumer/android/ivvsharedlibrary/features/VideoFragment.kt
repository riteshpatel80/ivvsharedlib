package org.kp.consumer.android.ivvsharedlibrary.features


//import org.kp.consumer.android.ivvsharedlibrary.webrtc.PeerConnectionProvider
import android.Manifest
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import kotlinx.android.synthetic.main.activity_video.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.video_content.*
import kotlinx.android.synthetic.main.video_content.view.*
import kotlinx.android.synthetic.main.waiting_room_fragment.*
import org.kp.consumer.android.ivvsharedlibrary.Event
import org.kp.consumer.android.ivvsharedlibrary.R
import org.kp.consumer.android.ivvsharedlibrary.ViewModelFactory
import org.kp.consumer.android.ivvsharedlibrary.api.response.ResponseResult
import org.kp.consumer.android.ivvsharedlibrary.api.response.SetupConferenceResponse
import org.kp.consumer.android.ivvsharedlibrary.controller.VVSessionController
import org.kp.consumer.android.ivvsharedlibrary.databinding.ActivityVideoBinding
import org.kp.consumer.android.ivvsharedlibrary.features.VideoViewModel.Companion.MESSAGE_PARTICIPANT_CREATE
import org.kp.consumer.android.ivvsharedlibrary.features.VideoViewModel.Companion.MESSAGE_PARTICIPANT_DELETE
import org.kp.consumer.android.ivvsharedlibrary.features.VideoViewModel.Companion.MESSAGE_PARTICIPANT_UPDATE
import org.kp.consumer.android.ivvsharedlibrary.features.chat.ChatFragment
import org.kp.consumer.android.ivvsharedlibrary.features.dialout.DialoutFragment
import org.kp.consumer.android.ivvsharedlibrary.features.help.HelpFragment
import org.kp.consumer.android.ivvsharedlibrary.features.participants.ParticipantsFragment
import org.kp.consumer.android.ivvsharedlibrary.features.participants.ParticipantsViewModel
import org.kp.consumer.android.ivvsharedlibrary.features.survey.SurveyFragment
import org.kp.consumer.android.ivvsharedlibrary.features.waitingRoom.WaitingRoomFragment
import org.kp.consumer.android.ivvsharedlibrary.service.RefreshTokenService
import org.kp.consumer.android.ivvsharedlibrary.util.*
import org.kp.consumer.android.ivvsharedlibrary.util.DialogUtils.createProgressDialog
import org.kp.consumer.android.ivvsharedlibrary.webrtc.AppRTCClient
import org.kp.consumer.android.ivvsharedlibrary.webrtc.PeerConnectionClient
import org.kp.consumer.android.ivvsharedlibrary.webrtc.PeerConnectionClient.PeerConnectionEvents
import org.webrtc.*


class VideoFragment : Fragment(), PeerConnectionEvents, AppRTCClient.SignalingEvents {

    private val TAG = "VideoFragment"
    private var isManuallyMicMuted = false
    private var isManuallyVideoTurnedOff = false
    private var isMicOn = true
    private var isVideoOn = true
    private var isConnected = false

    private var mProgressDialog: AlertDialog? = null

    private val showExpireMessageObservable = MutableLiveData<Event<Unit>>()


    private lateinit var viewModel: VideoViewModel

    private lateinit var partcipantViewModel: ParticipantsViewModel

    fun getConferenceResponse(): SetupConferenceResponse? {
        return viewModel.getConferenceResponse()
    }

    private lateinit var activity: Activity

    private var token: String? = null


    private var mSDP: SessionDescription? = null

    private var isHost: Boolean = false

    private var isFailed: Boolean = false

    private var expireMessage: String? = null


    private var containerId: Int? = null

    private var initialFadeoutHanlder = Handler(); //60sec

    private var fadeoutHandler = Handler() //10 sec

    private var isThanksAlertShown: Boolean = false

    private var shouldHoldMessgageNotDisplay: Boolean = false

    private var userChangedOrientation: Boolean = false

    private lateinit var appAudioManager: AppAudioManager

    private lateinit var videoView: View

    private var newVideoStarted: Boolean = false

    private var currentBitmap : Bitmap? = null

    private var isHome : Boolean = true

    private var eglbase: EglBase? = null

    private class ProxyVideoSink : VideoSink {
        private var target: VideoSink? = null
        @Synchronized
        override fun onFrame(frame: VideoFrame) {
            if (target == null) {
                Logging.d(
                    TAG,
                    "Dropping frame in proxy because target is null."
                )
                return
            }
            target!!.onFrame(frame)
        }

        @Synchronized
        fun setTarget(target: VideoSink?) {
            this.target = target
        }
    }

    private val remoteProxyRenderer = ProxyVideoSink()
    private val localProxyVideoSink = ProxyVideoSink()
    private val remoteSinks: ArrayList<VideoSink> = ArrayList()
    private var peerConnectionClient: PeerConnectionClient? = null
    var connectionparam: PeerConnectionClient.PeerConnectionParameters? = null

    private val leaveListener = object : DialogUtils.DialogButtonClickListener {
        override fun onYesClicked() {
            shouldHoldMessgageNotDisplay = true

            if (isConnected) {
                showProgress()
                viewModel.leaveConference(
                    token!!,
                    viewModel.isHost(partcipantViewModel.participantList)
                )
                showSurvey()
            } else {

                removeVideoFragment()
            }
        }
    }



    override fun onAttach(context: Context) {
        super.onAttach(context)

        activity = context as Activity
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        token = arguments?.getString(TOKEN_TAG)
        containerId = arguments?.getInt(CONTAINER_ID)

        appAudioManager = AppAudioManager.create(activity)

        //added new code
        eglbase = getEglBaseInstance()
        remoteSinks.add(remoteProxyRenderer)

        appAudioManager.start(object : AppAudioManager.AudioManagerEvents {
            override fun onAudioDeviceChanged(
                selectedAudioDevice: AppAudioManager.AudioDevice?,
                availableAudioDevices: Set<AppAudioManager.AudioDevice>
            ) {
                Log.d(
                    TAG,
                    "Audio Manager Event: Selected Audio Device - ${selectedAudioDevice?.name}"
                )
            }
        })
    }

    fun getEglBaseInstance() : EglBase
    {
        return EglBase.create()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        videoView = inflater.inflate(R.layout.activity_video, container, false)

        //Pip View
        videoView.pip_video_view.init(eglbase?.eglBaseContext, null)
        videoView.pip_video_view.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FIT);
        videoView.pip_video_view.setZOrderMediaOverlay(true);
        videoView.pip_video_view.setEnableHardwareScaler(true /* enabled */);
        videoView.pip_video_view.setMirror(true)
        localProxyVideoSink.setTarget(videoView.pip_video_view)

        //Full Screen View
        videoView.fullscreen_video_view.init(eglbase?.eglBaseContext, null)
        videoView.fullscreen_video_view.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FILL)
        videoView.fullscreen_video_view.setEnableHardwareScaler(false)
        videoView.fullscreen_video_view.setMirror(false)
        remoteProxyRenderer.setTarget(videoView.fullscreen_video_view)


        val binding = ActivityVideoBinding.bind(videoView).apply {

            viewModel = activity?.run {
                ViewModelProvider(
                    requireActivity(),
                    ViewModelFactory.getInstance(activity.application)
                )
                    .get(VideoViewModel::class.java)

            }

            partcipantViewModel = activity?.run {
                ViewModelProvider(
                    requireActivity(),
                    ViewModelFactory.getInstance(activity.application)
                ).get(ParticipantsViewModel::class.java)
            }
        }
        binding.lifecycleOwner = this
        retainInstance = true


        return videoView;
    }

    private fun shouldRequestRationaleShow() {
        if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA) || shouldShowRequestPermissionRationale(
                Manifest.permission.RECORD_AUDIO
            )
        ) {

            val accessDeniedFullText =
                getString(R.string.kp_access_denied) + "\n\n" + getString(R.string.kp_enable_camera_and_or_audio_access)
            DialogUtils.showOkDialog(
                activity,
                accessDeniedFullText,
                false,
                dialogCallback
            )
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        SurfaceViewRenderer.inflate(context, R.layout.video_content, null)
        //VideoRendererGui.setView(gl_surface_view, null)

        AccessibilityUtils.setFocusToAccessibility(logoView, fragment_title, fullscreen_video_view)

        activity.window.statusBarColor = ContextCompat.getColor(activity, R.color.kpblue)

        viewModel.cancelHostTimer()

        add_participants.setOnClickListener { v ->

            var dialoutFragment =
                childFragmentManager.findFragmentByTag(DIALOUT_TAG);

            if (dialoutFragment == null) {
                childFragmentManager.beginTransaction()
                    .replace(
                        R.id.participants_fragment, DialoutFragment(),
                        DIALOUT_TAG
                    ).addToBackStack(DIALOUT_TAG)
                    .commit()
            }
            showViewVisibility(video_topbar_layout, fragment_title, reset, back)
            hideViewVisibility(waiting_topbar_layout, logoView, add_participants)
            fragment_title.text = resources.getString(R.string.kp_add_participant)
            AccessibilityUtils.setFocusToAccessibility(fragment_title)
        }

        callEnd.setOnClickListener {

            DialogUtils.showOkCancelDialog(
                activity,
                resources.getString(R.string.kp_leave_visit_string) + "\n\n" + resources.getString(R.string.kp_leave_visit),
                true,
                leaveListener
            )

        }

        btn_switch_camera.setOnClickListener { v ->
            //            PeerConnectionProvider.videoCapturerAndroid?.switchCamera(
//                mCameraSwitchHandler
//            )
        }

        audio.setOnClickListener {
            if (!isMicOn) {
                sendMessageService(RefreshTokenService.Companion.ACTION.SEND_MUTE_ACTION)
            } else {
                sendMessageService(RefreshTokenService.Companion.ACTION.SEND_UNMUTE_ACTION)
            }
        }

        audio.setOnCheckedChangeListener { compoundButton, b ->
            isMicOn = b

            //PeerConnectionProvider.localAudioTrack?.setEnabled(isMicOn)
            isManuallyMicMuted = !isMicOn
            toggleFadeoutHandler()
        }




        video.setOnCheckedChangeListener { compoundButton, b ->
            isVideoOn = b;
            //PeerConnectionProvider.localVideoTrack?.setEnabled(isVideoOn)
            isManuallyVideoTurnedOff = !isVideoOn
            toggleFadeoutHandler()

        }

        childFragmentManager.beginTransaction()
            .add(
                R.id.participants_fragment,
                ParticipantsFragment(),
                PARTICIPANTS_TAG
            ).commit()


        tabs.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {

            override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
                // find which radio button is selected

                when (checkedId) {
                    R.id.homeTab -> {
                        showViewVisibility(video_topbar_layout)
                        goHome()
                        removeChatFragment()
                        toggleFadeoutHandler()
                    }
                    R.id.participantsTab -> {
                        isHome = false
                        showViewVisibility(
                            video_topbar_layout,
                            participants_fragment
                        )
                        hideViewVisibility(
                            chat_fragment,
                            help_fragment,
                            back,
                            reset
                        )
                        titleVisibilityAndText(R.string.kp_participants)
                        showAddParticipants()
                        setAccessibilityForVideoViews(false)

                        var participantFragment =
                            childFragmentManager.findFragmentByTag(PARTICIPANTS_TAG);

                        if (participantFragment == null) {
                            childFragmentManager.beginTransaction()
                                .add(
                                    R.id.participants_fragment,
                                    ParticipantsFragment(),
                                    PARTICIPANTS_TAG
                                ).commit()
                        } else {
                            partcipantViewModel.getParticipants()
                        }

                        popDialout()

                        stopHandler(initialFadeoutHanlder)
                        toggleVideoControls(View.GONE)
                        removeChatFragment()

                        //replaceFragmentInActivity(ParticipantsFragment(), R.id.tabs_fragment)
                    }
                    R.id.chatTab -> {
                        isHome = false
                        hideViewVisibility(
                            add_participants,
                            participants_fragment,
                            help_fragment,
                            back,
                            reset
                        )
                        showViewVisibility(
                            video_topbar_layout,
                            chat_fragment
                        )
                        titleVisibilityAndText(R.string.kp_chat)
                        setAccessibilityForVideoViews(false)
                        showNewChatMessageBadge(false)

                        var chatFragment =
                            childFragmentManager.findFragmentByTag(CHAT_TAG);

                        if (chatFragment == null) {
                            childFragmentManager.beginTransaction()
                                .add(R.id.chat_fragment, ChatFragment(), CHAT_TAG).commit()

                        }
                        stopHandler(initialFadeoutHanlder)
                        toggleVideoControls(View.GONE)
                    }
                    else -> {
                        isHome = false
                        hideViewVisibility(
                            logoView,
                            video_topbar_layout,
                            participants_fragment,
                            chat_fragment,
                            add_participants,
                            reset
                        )
                        showViewVisibility(help_fragment)
                        setAccessibilityForVideoViews(false)


                        var helpFragment =
                            childFragmentManager.findFragmentByTag(HELP_TAG);
                        if (helpFragment == null) {
                            childFragmentManager.beginTransaction()
                                .add(R.id.help_fragment, HelpFragment(), HELP_TAG).commit()
                        } else {
                            (helpFragment as HelpFragment).reload()
                        }

                        stopHandler(initialFadeoutHanlder)
                        toggleVideoControls(View.GONE)
                        removeChatFragment()
                    }

                }
            }

        })


        fullscreen_video_view.setOnClickListener {
            toggleVideoControls(View.VISIBLE)
            stopHandler(fadeoutHandler)
            waitAndVisiblityGone(FADE_OUT_TIME_IN_MILLI_SECS, fadeoutHandler)
        }

        waitAndVisiblityGone(INITIAL_FADE_OUT_TIME_IN_MILLI_SECS, initialFadeoutHanlder)

        //when presentation is changing landscape we need to update the resource for the orientation toggle
        //And remove the unused views
        presentation_screen_orientation.setOnClickListener {
            if (activity.requestedOrientation == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
                configurePresentation(
                    true,
                    fullScreen = false
                )
            } else {
                configurePresentation(
                    true,
                    fullScreen = true
                )
            }
        }
        presentationView?.setOnClickListener {
            toggleVideoControls(View.VISIBLE)
            stopHandler(fadeoutHandler)
            waitAndVisiblityGone(FADE_OUT_TIME_IN_MILLI_SECS, fadeoutHandler)
        }


        initData()

    }

    private fun popDialout() {
        var addParticipant = childFragmentManager.findFragmentByTag(DIALOUT_TAG)
        if (addParticipant != null) {
            childFragmentManager.popBackStack()
        }
    }

    //steps to change layout for the presentation
    private fun configurePresentation(
        userChangedOrientation: Boolean,
        fullScreen: Boolean
    ) {

        presentationView.visibility = if (isScreenShared) View.VISIBLE else View.GONE
        presentation_screen_orientation.visibility = if (isScreenShared) View.VISIBLE else View.GONE

        //track if the user changed the orientation
        this.userChangedOrientation = userChangedOrientation

        //change the drawable image for fullscreen toggle
        if (fullScreen) {
            presentation_screen_orientation.background =
                ContextCompat.getDrawable(activity, R.drawable.minimize)
            presentation_screen_orientation.contentDescription = getString(R.string.kp_portrait_mode)

            //change tab view visibility in fullscreen
            video_topbar_layout.visibility = View.GONE
            tabs.visibility = View.GONE

            //change orientation
            activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        } else {
            presentation_screen_orientation.background =
                ContextCompat.getDrawable(activity, R.drawable.fullscreen)
            presentation_screen_orientation.contentDescription = getString(R.string.kp_landscape_mode)

            //change tab view visivility in portrait
            video_topbar_layout.visibility = View.VISIBLE
            tabs.visibility = View.VISIBLE

            //change orientation
            activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }
        presentationScreenViews()

    }

    //change view visibility depending on orientation for presentation
    private fun presentationScreenViews() {
        if (activity.requestedOrientation == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
            tabs?.visibility = View.GONE
            video_topbar_layout?.visibility = View.GONE
        } else {
            tabs?.visibility = View.VISIBLE
            video_topbar_layout?.visibility = View.VISIBLE
        }
    }

    private fun waitAndVisiblityGone(timeInMilliSecs: Long, handler: Handler) {

        handler.postDelayed({

            activity.runOnUiThread {
                toggleVideoControls(View.GONE)
            }

        }, timeInMilliSecs)

    }

    private fun toggleFadeoutHandler() {
        if (!audio.isChecked || !video.isChecked) {
            toggleVideoControls(View.VISIBLE)
            fadeoutHandler.removeMessages(0)
            initialFadeoutHanlder.removeMessages(0)
        } else {
            toggleVideoControls(View.VISIBLE)
            fadeoutHandler.removeMessages(0)
            waitAndVisiblityGone(FADE_OUT_TIME_IN_MILLI_SECS, fadeoutHandler)
        }

    }

    private fun stopHandler(handler: Handler) {
        handler.removeMessages(0)
    }

    fun toggleVideoControls(visibility: Int) {
        videoControlsLayout?.visibility = visibility
        if (isScreenShared) presentation_screen_orientation?.visibility = visibility
    }


    fun removeChatFragment() {
        var chatFragment =
            childFragmentManager.findFragmentByTag(CHAT_TAG);

        if (chatFragment != null) {
            childFragmentManager.beginTransaction().remove(chatFragment).commit()
        }
    }

    private fun goHome() {
        isHome = true
        logoView.visibility = View.VISIBLE
        hideViewVisibility(
            participants_fragment,
            chat_fragment,
            help_fragment,
            reset,
            add_participants,
            back,
            fragment_title
        )
        showViewVisibility(logoView)
        setAccessibilityForVideoViews(true)
        tabs.check(R.id.homeTab)
    }

    fun hideViewVisibility(vararg hiddenViews: View?) {
        hiddenViews.forEach {
            it?.visibility = View.GONE
        }
    }

    fun showViewVisibility(vararg shownViews: View?) {
        shownViews.forEach {
            it?.visibility = View.VISIBLE
        }
    }

    fun setAccessibilityForVideoViews(value: Boolean) {
        if (value) {
            btn_switch_camera.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
            video.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
            audio.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
            callEnd.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
            fullscreen_video_view.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
        } else {
            btn_switch_camera.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_NO
            video.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_NO
            audio.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_NO
            callEnd.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_NO
            fullscreen_video_view.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_NO
        }
    }

    fun showAddParticipants() {
        val role = getConferenceResponse()?.signedInUser?.role
        if (role == Role.PATIENT.role ||
            role == Role.MEMBER_PROXY.role
        )
            add_participants.visibility = View.VISIBLE
    }

    private fun titleVisibilityAndText(
        title: Int
    ) {
        logoView.visibility = View.GONE
        fragment_title.visibility = View.VISIBLE
        fragment_title.text = resources.getString(title)
    }

    private fun createPeerConnectionParameters(): PeerConnectionClient.PeerConnectionParameters {

        // Video call enabled flag.
        val videoCallEnabled: Boolean = true
        // Use screencapture option.
        val useScreencapture: Boolean = false
        // Use Camera2 option.
        val useCamera2: Boolean = true
        // Get default codecs.
        val videoCodec: String = "VP8"
        val audioCodec: String = "OPUS"
        // Check HW codec flag.
        val hwCodec: Boolean = true
        // Check Capture to texture.
        val captureToTexture: Boolean = true
        // Check FlexFEC.
        val flexfecEnabled: Boolean = false
        // Check Disable Audio Processing flag.
        val noAudioProcessing: Boolean = false
        val aecDump: Boolean = false
        val saveInputAudioToFile: Boolean = false
        // Check OpenSL ES enabled flag.
        val useOpenSLES: Boolean = false
        // Check Disable built-in AEC flag.
        val disableBuiltInAEC: Boolean = false
        // Check Disable built-in AGC flag.
        val disableBuiltInAGC: Boolean = false
        // Check Disable built-in NS flag.
        val disableBuiltInNS: Boolean = false
        // Check Disable gain control
        val disableWebRtcAGCAndHPF: Boolean = false

        // Get video resolution from settings.
        var videoWidth = 0
        var videoHeight = 0
        if (videoWidth == 0 && videoHeight == 0) {
            val resolution: String = "Default"
            val dimensions = resolution.split("[ x]+").toTypedArray()
            if (dimensions.size == 2) {
                try {
                    videoWidth = dimensions[0].toInt()
                    videoHeight = dimensions[1].toInt()
                } catch (e: NumberFormatException) {
                    videoWidth = 0
                    videoHeight = 0
                    android.util.Log.e(
                        TAG,
                        "Wrong video resolution setting: $resolution"
                    )
                }
            }
        }

        // Get camera fps from settings.
        var cameraFps = 0
        if (cameraFps == 0) {
            val fps: String = "Default"
            val fpsValues = fps.split("[ x]+").toTypedArray()
            if (fpsValues.size == 2) {
                try {
                    cameraFps = fpsValues[0].toInt()
                } catch (e: NumberFormatException) {
                    cameraFps = 0
                    android.util.Log.e(
                        TAG,
                        "Wrong camera fps setting: $fps"
                    )
                }
            }
        }

        // Check capture quality slider flag.
        val captureQualitySlider: Boolean = false

        // Get video and audio start bitrate.
        var videoStartBitrate = 0
        var audioStartBitrate = 0

        // Check statistics display option.
        val displayHud: Boolean = false

        val tracing: Boolean = false

        // Check Enable RtcEventLog.
        val rtcEventLogEnabled: Boolean = false

        val dataChannelEnabled: Boolean = true
        val ordered: Boolean = true
        val negotiated: Boolean = false
        val maxRetrMs: Int = -1
        val maxRetr: Int = -1
        val id: Int = -1
        val protocol: String = ""

        // Get datachannel options\
        var datachannelparam = PeerConnectionClient.DataChannelParameters(
            ordered,
            maxRetrMs,
            maxRetr,
            protocol,
            negotiated,
            id
        )

        return PeerConnectionClient.PeerConnectionParameters(
            videoCallEnabled,
            false,
            tracing,
            videoWidth,
            videoHeight,
            cameraFps,
            videoStartBitrate,
            videoCodec,
            hwCodec,
            flexfecEnabled,
            audioStartBitrate,
            audioCodec,
            noAudioProcessing,
            aecDump,
            saveInputAudioToFile,
            useOpenSLES,
            disableBuiltInAEC,
            disableBuiltInAGC,
            disableBuiltInNS,
            disableWebRtcAGCAndHPF,
            rtcEventLogEnabled,
            datachannelparam
        )
    }

    private fun initData() {
        isConnected = true

        connectionparam = createPeerConnectionParameters()

        peerConnectionClient = PeerConnectionClient(
            activity,
            eglbase,
            connectionparam,
            this
        )
        val options = PeerConnectionFactory.Options()
        peerConnectionClient!!.createPeerConnectionFactory(options)

        //testing
        val videoCapture: VideoCapturer? = createCameraCapturer(Camera2Enumerator(context))
        peerConnectionClient?.createPeerConnection(localProxyVideoSink,remoteSinks,videoCapture,null)

        peerConnectionClient?.createOffer()

        //PeerConnectionProvider.init(activity, peerConnectionObserver, videoView, eglbase!!, remoteSinks)
        //PeerConnectionProvider.peerConnection
        showProgress()
    }

    private fun showProgress() {

        progressCircular?.visibility = View.VISIBLE

    }

    private fun showDialog() {
        mProgressDialog =
            createProgressDialog(activity, R.string.kp_content_loading, false)
        mProgressDialog?.show()
    }

    override fun onResume() {
        super.onResume()

        (activity as AppCompatActivity).supportActionBar?.hide()

        if (isConnected) {
            if (!isMicOn && !isManuallyMicMuted) {
                isMicOn = true
                peerConnectionClient?.setAudioEnabled(true)
                //PeerConnectionProvider.localAudioTrack?.setEnabled(true)
            }

            if (!isVideoOn && !isManuallyVideoTurnedOff) {
                isVideoOn = true
                peerConnectionClient?.setVideoEnabled(true)
                //PeerConnectionProvider.localVideoTrack?.setEnabled(true)
            }
        }

        setupObservers()

        LocalBroadcastManager.getInstance(context!!).registerReceiver(
            receiver, IntentFilter(RefreshTokenService.REFRESH_ERROR)
        );
        setShouldHoldMessageNotDisplay(false)
        shouldRequestRationaleShow()

        if (isScreenShared && activity.requestedOrientation == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE && tabs?.visibility == View.VISIBLE) {
            tabs?.visibility =
                View.GONE
        }

        if (viewModel.insideTheSurvey) tabs?.visibility = View.GONE

        // resource cleared after going into background so we need to save it and place in back when the view resumes
        currentBitmap?.let {
            if (isScreenShared) presentationView?.setImageBitmap(it)
        }

    }

    override fun onStop() {
        super.onStop()

        Log.d("VideoFragment", "onStop")

        if (isConnected) {

            // commented below block for audio should be enable even if app goes background
            /*if (isMicOn) {
                mLocalAudioTrack?.setEnabled(false)
                isMicOn = false
                isManuallyMicMuted = false
            }*/

            if (!shouldHoldMessgageNotDisplay) {
                Log.d(TAG, "hold message is displaying")
                sendPauseMessageService()
            }


            if (isVideoOn) {
                peerConnectionClient?.setVideoEnabled(false)
                //PeerConnectionProvider.localVideoTrack?.setEnabled(false)
                isVideoOn = false
                isManuallyVideoTurnedOff = false
            }


        }
    }


    private fun setupObservers() {

        val connectionLiveData = ConnectionLiveData(context!!)
        connectionLiveData.observe(viewLifecycleOwner, Observer { isConnected ->
            isConnected?.let {
                if (it) {
                    networkBanner.visibility = View.GONE

                } else {
                    networkBanner.visibility = View.VISIBLE
                }
            }
        })





        viewModel.leaveConferenceEvent.observe(viewLifecycleOwner, Observer {

            it?.getContentIfNotHandled()?.let {
                if (it && !endVVRan && isConnected) {
                    isConnected = false
                    showSurvey()
                    hideWaitingFragment()
                } else {

                    removeWaitingRoomFragment()
                    stopRefreshService()
                    clearFragmentAndData()
                    VVSessionController.vvEventHandler?.didEndVV()
                }

            }
        })

        //setting remote descrption
        viewModel.sdp.observe(viewLifecycleOwner, Observer {

            Log.d(TAG, "sdb observer is answering")
            it?.getContentIfNotHandled()?.let {
                val sdb = it
                //PeerConnectionProvider.peerConnection?.setRemoteDescription(sdpObserver, it)
                peerConnectionClient?.setRemoteDescription(sdb)
                peerConnectionClient?.createAnswer()
                peerConnectionClient?.startRemoteTrack()
            }

        });


        viewModel.diloagCloseEvent.observe(viewLifecycleOwner, Observer {


            it?.getContentIfNotHandled()?.let {

                progressCircular?.visibility = View.GONE

                //PeerConnectionProvider.isStartedVideo = true

            }

        })

        partcipantViewModel.dialogCloseEvent.observe(viewLifecycleOwner, Observer {
            it?.getContentIfNotHandled()?.let {

                progressCircular?.visibility = View.GONE
                //PeerConnectionProvider.isStartedVideo = true
            }
        })



        viewModel.loadViewEvent.observe(viewLifecycleOwner, Observer {

            it?.getContentIfNotHandled()?.let {
                //                createPeerConnection(final VideoSink localRender, final List<VideoSink> remoteSinks,
//                    final VideoCapturer videoCapturer, final AppRTCClient.SignalingParameters signalingParameters)

                val videoCapture: VideoCapturer? = createCameraCapturer(Camera2Enumerator(context))
//                val parameters: AppRTCClient.SignalingParameters = AppRTCClient.SignalingParameters(
//
//                )

                peerConnectionClient?.createPeerConnection(localProxyVideoSink,remoteSinks,videoCapture,null)

                peerConnectionClient?.createOffer()
//                PeerConnectionProvider.init(activity, peerConnectionObserver, videoView, eglbase!!, remoteSinks)
//
//                PeerConnectionProvider.peerConnection!!.createOffer(
//                    sdpObserver,
//                    PeerConnectionProvider.rtcConstraint
//                )
            }
        })


        viewModel.errorMesssage.observe(viewLifecycleOwner, Observer {


            it?.getContentIfNotHandled()?.let {
                when (it) {
                    is ResponseResult.ServiceError -> {

                        VVSessionController.vvEventHandler?.onError()

                        DialogUtils.showOkCancelDialog(
                            activity,
                            ErrorMessages.getErrorMessages(it.error.code.toInt()),
                            false,
                            dialogCallback
                        )

                    }
                    is ResponseResult.Failure -> {
                        VVSessionController.vvEventHandler?.onError()

                        DialogUtils.showOkCancelDialog(
                            activity,
                            ErrorMessages.getErrorMessages(it.errorCode.toInt()),
                            false,
                            dialogCallback
                        )

                    }
                    else -> {
                        VVSessionController.vvEventHandler?.onError()

                        DialogUtils.showOkCancelDialog(
                            activity,
                            getString(R.string.kp_system_error_2),
                            false,
                            dialogCallback
                        )

                    }


                }
            }


        })

        showExpireMessageObservable.observe(viewLifecycleOwner, Observer {

            it?.getContentIfNotHandled()?.let {

                if (!isThanksAlertShown && !viewModel.insideTheSurvey) {
                    showExpireDialog()
                }
            }


        })

        viewModel.showUnreadMessagesBadge.observe(viewLifecycleOwner,
            Observer {
                it?.let {
                    showNewChatMessageBadge(it)
                }
            })

        viewModel.callEnded.observe(viewLifecycleOwner, Observer
        {


            it?.getContentIfNotHandled()?.let {
                if (!isThanksAlertShown)
                    showThanksAlert()
            }


        })

        viewModel.onHoldObservable.observe(viewLifecycleOwner, Observer
        {

            it?.getContentIfNotHandled()?.let {
                showToast("$it " + resources.getString(R.string.kp_hold_msg))

            }

        })

        viewModel.muteObservable.observe(viewLifecycleOwner, Observer {
            it?.getContentIfNotHandled()?.let { isMuted ->
                audio.isChecked = isMuted
            }
        })

        viewModel.onPresentationImageObservable.observe(viewLifecycleOwner, Observer {
            it?.getContentIfNotHandled()?.let {
                presentationView.visibility = if (isScreenShared) View.VISIBLE else View.GONE
                presentation_screen_orientation.visibility =
                    if (isScreenShared) View.VISIBLE else View.GONE
                Glide.with(activity)
                    .asBitmap()
                    .load(it).placeholder(presentationView.drawable)
                    .centerCrop()
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onLoadCleared(placeholder: Drawable?) {}

                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: Transition<in Bitmap>?
                        ) {
                            presentationView?.setImageBitmap(resource)
                            // resource cleared after going into background so we need to save it and place in back when the view resumes
                            currentBitmap = resource
                        }
                    })
                if (newVideoStarted) {
                    presentationView.resetZoom()
                    newVideoStarted = false
                }
                if (!userChangedOrientation)
                    configurePresentation(false, fullScreen = true)
                if (tabs?.visibility == View.VISIBLE && activity?.requestedOrientation == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) tabs?.visibility =
                    View.GONE
                if (!isHome && activity?.requestedOrientation == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) goHome()
            }

        })


        viewModel.onPresentationObservable.observe(viewLifecycleOwner, Observer {

            it?.getContentIfNotHandled()?.let {

                if (it) {
                    homeTab?.isChecked = true
                    isScreenShared = true
                    userChangedOrientation = false
                    newVideoStarted = true

                } else {
                    if (isScreenShared) {
                        isScreenShared = false
                        //clearing the image resource if screen share stops
                        Glide.with(activity).load(it).placeholder(0)
                            .into(presentationView)
                        configurePresentation(false, fullScreen = false)
                    }
                }

            }
        })

        viewModel.participantsEvent.observe(viewLifecycleOwner, Observer {
            Log.d(TAG, "participant event called")
            it?.getContentIfNotHandled()?.let {
                var updateParticipants: Boolean = false
                if (it.event == MESSAGE_PARTICIPANT_DELETE) {
                    partcipantViewModel.participantList?.let {list->
                        for (participant in list){
                            if (participant.uuid == it.uuid){
                                showToast(getString(R.string.kp_vv_left,participant.name))
                            }
                        }
                    }
                    updateParticipants = partcipantViewModel.deleteParticipant(it.uuid)
                } else if (it.event == MESSAGE_PARTICIPANT_UPDATE) {
                    updateParticipants =
                        partcipantViewModel.updateParticipantMute(it.uuid, it.isMuted!!)
                } else {
                    updateParticipants = true
                }
                if (updateParticipants) {
                    partcipantViewModel.getParticipants()
                }
                Log.d(TAG, "participant event refreshed")

            }
        })
        // if there has been a new message while the badge is not visible and screen reader is on,
        // send talkback event
        viewModel.chatToastMessage.observe(viewLifecycleOwner, Observer {
            it?.getContentIfNotHandled()?.let { model ->
                var newChatMessage = ""
                partcipantViewModel.participantList?.let { list ->
                    for (participant in list) {
                        if (model.senderUUID.equals(participant.uuid, ignoreCase = true)) {
                            Log.e(TAG, "Toast message for chat should be replaced")
                            newChatMessage = getString(R.string.kp_new_chat_message,participant.name,model.messageBody)

                        }
                    }
                }
                showToast(newChatMessage)
            }
        })
        partcipantViewModel.participantsListWaitingCheck.observe(viewLifecycleOwner, Observer {
            it?.getContentIfNotHandled()?.let { list ->
                if (!partcipantViewModel.doesChairExits(list)) {
                    if(!viewModel.insideTheSurvey) {
                        showWaitingFragment()
                    }
                }
            }
        })

        viewModel.paticipantJoinEvent.observe(viewLifecycleOwner, Observer {

            it?.getContentIfNotHandled()?.let {announcementEvent ->
                partcipantViewModel.participantList?.let {
                    if (announcementEvent.event == MESSAGE_PARTICIPANT_UPDATE || announcementEvent.event == MESSAGE_PARTICIPANT_CREATE){
                        var announce = true

                        for (participant in it){
                            if (participant.uuid == announcementEvent.uuid){
                                announce = false
                            }
                        }
                        if (announce) {
                            showToast(getString(R.string.kp_vv_joined,announcementEvent.displayName))
                        }
                    }
                }
            }
        })

        viewModel.leaveFromSurvey.observe(viewLifecycleOwner, Observer {
            it?.getContentIfNotHandled()?.let {
                Log.d(TAG,"Left from survey")
                leaveConference()
            }

        })
        viewModel.hideTabs.observe(viewLifecycleOwner, Observer {
            it?.getContentIfNotHandled()?.let {
                tabs?.visibility = View.GONE
            }

        })


    }


    private fun createCameraCapturer(enumerator: CameraEnumerator): VideoCapturer? {
        val deviceNames = enumerator.deviceNames
        // First, try to find front facing camera
        Logging.d(
            TAG,
            "Looking for front facing cameras."
        )
        for (deviceName in deviceNames) {
            if (enumerator.isFrontFacing(deviceName)) {
                Logging.d(
                    TAG,
                    "Creating front facing camera capturer."
                )
                val videoCapturer: VideoCapturer? = enumerator.createCapturer(deviceName, null)
                if (videoCapturer != null) {
                    return videoCapturer
                }
            }
        }
        // Front facing camera not found, try something else
        Logging.d(TAG, "Looking for other cameras.")
        for (deviceName in deviceNames) {
            if (!enumerator.isFrontFacing(deviceName)) {
                Logging.d(
                    TAG,
                    "Creating other camera capturer."
                )
                val videoCapturer: VideoCapturer? = enumerator.createCapturer(deviceName, null)
                if (videoCapturer != null) {
                    return videoCapturer
                }
            }
        }
        return null
    }


    private fun showSurvey() {
        showViewVisibility(video_topbar_layout)
        goHome()
        popDialout()
        viewModel.insideTheSurvey = true
        showViewVisibility(help_fragment)
        setAccessibilityForVideoViews(false)

        var fiveStartFragment =
            childFragmentManager.findFragmentByTag(FIVE_STAR_TAG);
        if (fiveStartFragment == null) {
            childFragmentManager.beginTransaction()
                .add(R.id.help_fragment, SurveyFragment(), FIVE_STAR_TAG)
                .addToBackStack(FIVE_STAR_TAG)
                .commit()
        }
        tabs?.visibility = View.GONE
        mProgressDialog?.dismiss()
    }

    private fun removeWaitingRoomFragment() {

        var firstWaitingRoomFragment =
            (activity as FragmentActivity).supportFragmentManager.findFragmentByTag(
                WaitingRoomFragment.TAG
            );

        firstWaitingRoomFragment?.let {
            (activity as FragmentActivity).supportFragmentManager.beginTransaction()
                .remove(firstWaitingRoomFragment).commit();

        }

        var secondWaitingRoomFragment =
            childFragmentManager.findFragmentByTag(
                WaitingRoomFragment.TAG
            );

        secondWaitingRoomFragment?.let {
            childFragmentManager.beginTransaction()
                .remove(secondWaitingRoomFragment).commit();

        }

    }

    var isScreenShared = false

    override fun onDestroyView() {
        super.onDestroyView()
        appAudioManager.stop()
        remoteProxyRenderer.setTarget(null)
        stopRefreshService()
        freeAllResources()

    }

    fun showToast(msg: String) {

        var toast = Toast.makeText(activity, msg, Toast.LENGTH_LONG)
        var view = toast.view
        view.setBackgroundColor(ContextCompat.getColor(activity, R.color.kp_dark_indigo))
        var text = view.findViewById(android.R.id.message) as TextView
        text.setTextColor(ContextCompat.getColor(activity, R.color.white))
        toast.show()

    }

    fun showThanksAlert() {
        if (viewModel.insideTheSurvey) return
        isThanksAlertShown = true
        if (!shouldHoldMessgageNotDisplay && !isFailed) {
            stopSelfView()
            val fullThankMsg =
                resources.getString(R.string.kp_thanks_title) + "\n\n" + resources.getString(R.string.kp_thanks_msg)
            DialogUtils.showOkDialog(context!!, fullThankMsg, false, thanksCallback)
        }
    }

    fun showWaitingFragment() {

        //PeerConnectionProvider.stopTimer()

        appAudioManager.muteAll()

        //PeerConnectionProvider.localVideoSource?.capturerObserver?.onCapturerStopped() //.stop()
        //PeerConnectionProvider.localAudioTrack?.setEnabled(false)
        Log.e(TAG, "showWaitingFragment")
        work_fragment_layout.visibility = View.VISIBLE

        var bundl = Bundle();
        bundl.putString(TOKEN_TAG, token);
        bundl.putInt(CONTAINER_ID, containerId!!);
        bundl.putBoolean(IS_ALREADY_LANDED, true);

        var waitingRoomFragment =
            childFragmentManager.findFragmentByTag(WAITING_ROOM_FRAGMENT);



        if (waitingRoomFragment == null) {
            waitingRoomFragment = WaitingRoomFragment()
            waitingRoomFragment.arguments = bundl
            childFragmentManager.beginTransaction()
                .replace(
                    R.id.work_fragment_layout,
                    waitingRoomFragment,
                    WAITING_ROOM_FRAGMENT
                ).commit()
        } else {
            (waitingRoomFragment as WaitingRoomFragment).hasNavigated = false
            (waitingRoomFragment as WaitingRoomFragment).cameraUtil.openCamera()
        }

        viewModel.startHostLeftTimer()

    }


    fun hideWaitingFragment() {
        viewModel.cancelHostTimer()
        //PeerConnectionProvider.localVideoSource?.capturerObserver?.onCapturerStarted(true) //restart()
        //PeerConnectionProvider.localAudioTrack?.setEnabled(true)
        parentFragment?.childFragmentManager?.popBackStack()
        work_fragment_layout.visibility = View.GONE
        appAudioManager.unMuteAll()
        tabs.check(R.id.homeTab)
        waiting_topbar_layout?.visibility = View.GONE

        //PeerConnectionProvider.startTimer()

    }

    private fun showNewChatMessageBadge(show: Boolean) {
        var drawable: Drawable? = null
        if (show) {
            drawable = context?.getResources()?.getDrawable(R.drawable.ic_chat_notification)
        } else {
            drawable = context?.getResources()?.getDrawable(R.drawable.chat_selector)
        }

        drawable?.let {
            val height = it.getIntrinsicHeight()
            val width = it.getIntrinsicWidth()
            it.setBounds(0, 0, width, height)
            chatTab.setCompoundDrawables(null, it, null, null)
        }
    }

    private fun clearFragmentAndData() {
        //Reset the mode to portrait at the end of every session
        activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        appAudioManager.stop()
        freeAllResources()

        stopRefreshService()

        removeVideoFragment()
    }

    fun setShouldHoldMessageNotDisplay(boolean: Boolean) {
        shouldHoldMessgageNotDisplay = boolean

    }

    /*
     * This methods starts the Refresh token service
     */
    private fun sendPauseMessageService() {
        var startIntent = Intent(activity, RefreshTokenService::class.java);
        startIntent.setAction(RefreshTokenService.Companion.ACTION.SEND_PAUSE_ACTION);
        activity.startService(startIntent)
    }

    /*
     * This methods starts the Refresh token service
     */
    private fun sendMessageService(action: String) {
        var startIntent = Intent(activity, RefreshTokenService::class.java)
        startIntent.setAction(action)
        activity.startService(startIntent)
    }


    /*
      This method stops the refresh token service
     */
    private fun stopRefreshService() {
        var startIntent = Intent(activity, RefreshTokenService::class.java);
        startIntent.setAction(RefreshTokenService.Companion.ACTION.STOP_ACTION);
        activity.startService(startIntent)
    }


    var dialogCallback = object : DialogUtils.DialogButtonClickListener {
        override fun onYesClicked() {
            clearFragmentAndData()
            setShouldHoldMessageNotDisplay(true)
            VVSessionController.vvEventHandler?.onError()
        }


    }


    var thanksCallback = object : DialogUtils.DialogButtonClickListener {
        override fun onYesClicked() {
            shouldHoldMessgageNotDisplay = true
            isThanksAlertShown = false
            isConnected = false

            showSurvey()
        }


    }

    private fun removeVideoFragment() {
        var videoFragment =
            (activity as androidx.fragment.app.FragmentActivity).supportFragmentManager.findFragmentByTag(TAG);

        videoFragment?.let {
            (activity as androidx.fragment.app.FragmentActivity).supportFragmentManager.beginTransaction()
                .remove(videoFragment).commit();

        }
    }


    private val sdpObserver = object : SdpObserver {
        override fun onCreateSuccess(sessionDescription: SessionDescription) {
            mSDP = sessionDescription
            Log.d(TAG, "S Type in observer:" + mSDP?.type.toString())
            //PeerConnectionProvider.peerConnection!!.setLocalDescription(this, sessionDescription)

            viewModel.callRequest(mSDP!!)


        }

        override fun onSetSuccess() {
            Log.d(TAG, "onSetSuccess: ")
        }

        override fun onCreateFailure(s: String) {
            Log.e(TAG, "onCreateFailure: $s")
        }

        override fun onSetFailure(s: String) {
//            PeerConnectionProvider.peerConnection!!.createOffer(
//                this,
//                PeerConnectionProvider.rtcConstraint
//            )
            Log.e(TAG, "onSetFailure: $s")
        }
    }


    private val peerConnectionObserver = object : PeerConnection.Observer {
        override fun onSignalingChange(signalingState: PeerConnection.SignalingState) {
            Log.d(TAG, "onSignalingChange:$signalingState")
        }

        override fun onIceCandidatesRemoved(p0: Array<out IceCandidate>?) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun onIceConnectionChange(iceConnectionState: PeerConnection.IceConnectionState) {
            Log.e(TAG, "onIceConnectionChange:${iceConnectionState.name}")
            if (iceConnectionState == PeerConnection.IceConnectionState.COMPLETED) {
                viewModel.ackRequest()

                isFailed = false
            }
            if (iceConnectionState == PeerConnection.IceConnectionState.FAILED) {

                isFailed = true

                expireMessage = resources.getString(R.string.kp_drop_alert)

                if (!isThanksAlertShown && !viewModel.insideTheSurvey) {
                    showExpireMessageObservable.postValue(Event(Unit))
                }
            }

        }

        override fun onIceConnectionReceivingChange(b: Boolean) {
            Log.d(TAG, "onIceConnectionReceivingChange:$b")
        }

        override fun onIceGatheringChange(iceGatheringState: PeerConnection.IceGatheringState) {
            Log.e(TAG, "onIceGatheringChange:$iceGatheringState")
        }

        override fun onIceCandidate(iceCandidate: IceCandidate) {
            Log.d(TAG, "onIceCandidate:")
            //PeerConnectionProvider.peerConnection?.addIceCandidate(iceCandidate)
        }

        override fun onAddStream(mediaStream: MediaStream) {
            Log.d(TAG, "onAddStream:")
            //mediaStream.videoTracks.get(0).addSink { PeerConnectionProvider.removeVideoRenderer }
            //mediaStream.videoTracks.get(0).addRenderer(PeerConnectionProvider.removeVideoRenderer)
        }

        override fun onRemoveStream(mediaStream: MediaStream) {
            Log.d(TAG, "onRemoveStream:")
            //PeerConnectionProvider.peerConnection?.removeStream(mediaStream);
        }

        override fun onDataChannel(dataChannel: DataChannel) {
            Log.d(TAG, "onDataChannel:")
        }

        override fun onRenegotiationNeeded() {
            Log.d(TAG, "onRenegotiationNeeded:")
        }

        override fun onAddTrack(p0: RtpReceiver?, p1: Array<out MediaStream>?) {
            Log.d(TAG + " RtpReceiver" , p0.toString())
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }
    }


//    private val mCameraSwitchHandler = object : VideoCapturerAndroid.CameraSwitchHandler {
//        override fun onCameraSwitchDone(b: Boolean) {
//
//        }
//
//        override fun onCameraSwitchError(s: String) {
//
//
//        }
//    }


    private fun freeAllResources() {

        //PeerConnectionProvider.freeAllResources()

        mSDP = null
        expireMessage = null
        mProgressDialog?.dismiss()

        LocalBroadcastManager.getInstance(activity).unregisterReceiver(receiver)

    }

    private fun stopSelfView() {

        //PeerConnectionProvider.localVideoSource?.stop()
        //PeerConnectionProvider.localVideoSource?.capturerObserver?.onCapturerStopped()
    }

    private var endVVRan : Boolean = false
    fun stopVideoCall(conferenceToken: String) {
        if(viewModel.insideTheSurvey) endVVRan = true
        viewModel.insideTheSurvey = true
        viewModel.leaveConference(
            conferenceToken,
            viewModel.isHost(partcipantViewModel.participantList)
        )
    }


    private var receiver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, intent: Intent?) {
            expireMessage = intent?.getStringExtra(RefreshTokenService.ERROR_DESC);
            showExpireMessageObservable.postValue(Event(Unit))

        }

    }

    private fun showExpireDialog() {
        stopSelfView()
        DialogUtils.showOkDialog(context!!, expireMessage!!, false, dialogCallback)
    }

    fun leaveConference(){

        var waitingRoomFragment =
            childFragmentManager.findFragmentByTag(WAITING_ROOM_FRAGMENT);

        if (waitingRoomFragment != null) {

            (waitingRoomFragment as WaitingRoomFragment).closeProgressDialog()

        }
        clearFragmentAndData()


        VVSessionController.vvEventHandler?.didEndVV()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        presentationView.resetZoom()
    }

    companion object {

        val TAG = VideoFragment::class.java.simpleName

        val TOKEN_TAG = "token"
        val IS_HOST = "isHost"

        val IS_ALREADY_LANDED = "isAlreadyLanded"

        val INITIAL_FADE_OUT_TIME_IN_MILLI_SECS = 60 * 1000L

        val FADE_OUT_TIME_IN_MILLI_SECS = 10 * 1000L

        val REQUEST_PERMISSIONS_CODE_CAMERA = 1

        val CHAT_TAG = "chat"
        val HELP_TAG = "help"
        val PARTICIPANTS_TAG = "ParticipantsFragment"
        val WAITING_ROOM_FRAGMENT = "WaitingRoomFragment"
        val DIALOUT_TAG = "dialout"
        val FIVE_STAR_TAG = "fivestar"
        val CONTAINER_ID = "containerId"


        fun newInstance(token: String, containterId: Int): VideoFragment {
            val f = VideoFragment()
            // Supply index input as an argument.
            val args = Bundle()
            args.putString(TOKEN_TAG, token)
            args.putInt(CONTAINER_ID, containterId)
            f.arguments = args
            return f
        }


    }

    override fun onPeerConnectionClosed() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onIceConnected() {
        viewModel.ackRequest()

        isFailed = false
    }

    override fun onIceCandidate(candidate: IceCandidate?) {
        peerConnectionClient?.addRemoteIceCandidate(candidate)
    }

    override fun onIceCandidatesRemoved(candidates: Array<out IceCandidate>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onIceDisconnected() {
        isFailed = true

        expireMessage = resources.getString(R.string.kp_drop_alert)

        if (!isThanksAlertShown && !viewModel.insideTheSurvey) {
            showExpireMessageObservable.postValue(Event(Unit))
        }
    }

    override fun onConnected() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onDisconnected() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onLocalDescription(sdp: SessionDescription?) {
        mSDP = sdp
        Log.d(TAG, "S Type in observer:" + mSDP?.type.toString())
        //PeerConnectionProvider.peerConnection!!.setLocalDescription(this, sessionDescription)

        viewModel.callRequest(mSDP!!)
    }

    override fun onPeerConnectionError(description: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRemoteIceCandidatesRemoved(candidates: Array<out IceCandidate>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onConnectedToRoom(params: AppRTCClient.SignalingParameters?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRemoteIceCandidate(candidate: IceCandidate?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onChannelError(description: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onChannelClose() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRemoteDescription(sdp: SessionDescription?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}