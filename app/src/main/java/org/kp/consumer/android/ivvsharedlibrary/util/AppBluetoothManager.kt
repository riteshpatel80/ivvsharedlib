package org.kp.consumer.android.ivvsharedlibrary.util

import android.Manifest
import android.annotation.SuppressLint
import android.bluetooth.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.media.AudioManager
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.os.Process
import org.kp.consumer.android.ivvsharedlibrary.util.Log

/**
 * AppProximitySensor manages functions related to Bluetooth devices.
 */

class AppBluetoothManager(
    private val appContext: Context,
    private val appAudioManager: AppAudioManager
) {

    val TAG = this.javaClass.simpleName

    // Timeout interval for starting or stopping audio to a Bluetooth SCO device.
    private val BLUETOOTH_SCO_TIMEOUT_MS: Long = 4000
    // Maximum number of SCO connection attempts.
    private val MAX_SCO_CONNECTION_ATTEMPTS = 2

    // Bluetooth connection state.
    enum class State {
        // Bluetooth is not available; no adapter or Bluetooth is off.
        UNINITIALIZED,
        // Bluetooth error happened when trying to start Bluetooth.
        ERROR,
        // Bluetooth proxy object for the Headset profile exists, but no connected headset devices,
        // SCO is not started or disconnected.
        HEADSET_UNAVAILABLE,
        // Bluetooth proxy object for the Headset profile connected, connected Bluetooth headset
        // present, but SCO is not started or disconnected.
        HEADSET_AVAILABLE,
        // Bluetooth audio SCO connection with remote device is closing.
        SCO_DISCONNECTING,
        // Bluetooth audio SCO connection with remote device is initiated.
        SCO_CONNECTING,
        // Bluetooth audio SCO connection with remote device is established.
        SCO_CONNECTED
    }

    private val audioManager = appContext.getSystemService(Context.AUDIO_SERVICE) as AudioManager
    private val handler = Handler(Looper.getMainLooper())

    var scoConnectionAttempts: Int = 0
    var bluetoothState: State = State.UNINITIALIZED
    var bluetoothServiceListener: BluetoothProfile.ServiceListener = BluetoothServiceListener()
    var bluetoothAdapter: BluetoothAdapter? = null
    var bluetoothHeadset: BluetoothHeadset? = null
    lateinit var bluetoothDevice: BluetoothDevice
    private var bluetoothHeadsetReceiver: BluetoothHeadsetBroadcastReceiver? = null

    init {
        bluetoothHeadsetReceiver = BluetoothHeadsetBroadcastReceiver()
    }

    // Runs when the Bluetooth timeout expires. We use that timeout after calling
    // startScoAudio() or stopScoAudio() because we're not guaranteed to get a
    // callback after those calls.
    private val bluetoothTimeoutRunnable = Runnable {
        bluetoothTimeout()
    }

    /**
     * Implementation of an interface that notifies BluetoothProfile IPC clients when they have been
     * connected to or disconnected from the service.
     */
    inner class BluetoothServiceListener : BluetoothProfile.ServiceListener {
        // Called to notify the client when the proxy object has been connected to the service.
        // Once we have the profile proxy object, we can use it to monitor the state of the
        // connection and perform other operations that are relevant to the headset profile.
        override fun onServiceConnected(profile: Int, proxy: BluetoothProfile?) {
            if (profile != BluetoothProfile.HEADSET || bluetoothState == State.UNINITIALIZED) {
                return
            }
            // Android only supports one connected Bluetooth Headset at a time.
            Log.d(TAG, "BluetoothServiceListener.onServiceConnected: BT state=$bluetoothState")
            bluetoothHeadset = proxy as BluetoothHeadset
            Log.d(
                TAG,
                "BluetoothServiceListener.onServiceConnected: BT headset=${bluetoothHeadset?.connectedDevices}"
            )
            updateAudioDeviceState()
        }

        /** Notifies the client when the proxy object has been disconnected from the service. */
        override fun onServiceDisconnected(profile: Int) {
            Log.d(TAG, "BluetoothServiceListener.onServiceConnected: disconnected")
            if (profile != BluetoothProfile.HEADSET || bluetoothState == State.UNINITIALIZED) {
                return
            }
            Log.d(TAG, "BluetoothServiceListener.onServiceDisconnected: BT state=$bluetoothState")
            stopScoAudio()
            bluetoothState = State.HEADSET_UNAVAILABLE
            updateAudioDeviceState()
        }
    }

    // Intent broadcast receiver which handles changes in Bluetooth device availability.
    // Detects headset changes and Bluetooth SCO state changes.
    inner class BluetoothHeadsetBroadcastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            Log.d(TAG, "Receiver called")
            if (bluetoothState == State.UNINITIALIZED) {
                return
            }
            val action = intent?.action
            // Change in connection state of the Headset profile. Note that the
            // change does not tell us anything about whether we're streaming
            // audio to BT over SCO. Typically received when user turns on a BT
            // headset while audio is active using another audio device.
            if (action.equals(BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED)) {
                val state =
                    intent?.getIntExtra(
                        BluetoothHeadset.EXTRA_STATE,
                        BluetoothHeadset.STATE_DISCONNECTED
                    )
                if (state == BluetoothHeadset.STATE_CONNECTED) {
                    scoConnectionAttempts = 0
                    updateAudioDeviceState()
                } else if (state == BluetoothHeadset.STATE_CONNECTING) {
                    // No action needed.
                } else if (state == BluetoothHeadset.STATE_DISCONNECTING) {
                    // No action needed.
                } else if (state == BluetoothHeadset.STATE_DISCONNECTED) {
                    // Bluetooth is probably powered off during the call.
                    stopScoAudio()
                    updateAudioDeviceState()
                }
                // Change in the audio (SCO) connection state of the Headset profile.
                // Typically received after call to startScoAudio() has finalized.
            } else if (action.equals(BluetoothHeadset.ACTION_AUDIO_STATE_CHANGED)) {
                val state = intent?.getIntExtra(
                    BluetoothHeadset.EXTRA_STATE, BluetoothHeadset.STATE_AUDIO_DISCONNECTED
                )
                if (state == BluetoothHeadset.STATE_AUDIO_CONNECTED) {
                    cancelTimer()
                    if (bluetoothState == State.SCO_CONNECTING) {
                        bluetoothState = State.SCO_CONNECTED
                        scoConnectionAttempts = 0
                        updateAudioDeviceState()
                    }
                } else if (state == BluetoothHeadset.STATE_AUDIO_CONNECTING) {
                } else if (state == BluetoothHeadset.STATE_AUDIO_DISCONNECTED) {
                    if (isInitialStickyBroadcast) {
                        return
                    }
                    updateAudioDeviceState()
                }
            }
        }
    }

    /** Returns the internal state. */
    fun getState(): State {
        return bluetoothState
    }

    /**
     * Activates components required to detect Bluetooth devices and to enable
     * BT SCO (audio is routed via BT SCO) for the headset profile. The end
     * state will be HEADSET_UNAVAILABLE but a state machine has started which
     * will start a state change sequence where the final outcome depends on
     * if/when the BT headset is enabled.
     * Example of state change sequence when start() is called while BT device
     * is connected and enabled:
     *   UNINITIALIZED --> HEADSET_UNAVAILABLE --> HEADSET_AVAILABLE -->
     *   SCO_CONNECTING --> SCO_CONNECTED <==> audio is now routed via BT SCO.
     * Note that the AppRTCAudioManager is also involved in driving this state
     * change.
     */
    @SuppressLint("ObsoleteSdkInt", "WrongConstant")
    fun start() {
        if (!hasPermission(appContext, Manifest.permission.BLUETOOTH)) {
            return
        }
        if (bluetoothState != State.UNINITIALIZED) {
            return
        }
        scoConnectionAttempts = 0
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            bluetoothAdapter =
                (appContext.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager).adapter
        } else {
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        }
        if (bluetoothAdapter == null) {
            Log.w(TAG, "Device does not support Bluetooth")
            return
        }
        // Ensure that the device supports use of BT SCO audio for off call use cases.
        if (!audioManager.isBluetoothScoAvailableOffCall) {
            Log.e(TAG, "Bluetooth SCO audio is not available off call")
            return;
        }
        if (!getBluetoothProfileProxy(
                appContext, bluetoothServiceListener, BluetoothProfile.HEADSET
            )!!
        ) {
            Log.e(TAG, "BluetoothAdapter.getProfileProxy(HEADSET) failed")
            return
        }
        // Register receivers for BluetoothHeadset change notifications.
        val bluetoothHeadsetFilter = IntentFilter()
        // Register receiver for change in connection state of the Headset profile.
        bluetoothHeadsetFilter.addAction(BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED)
        // Register receiver for change in audio connection state of the Headset profile.
        bluetoothHeadsetFilter.addAction(BluetoothHeadset.ACTION_AUDIO_STATE_CHANGED)
        bluetoothHeadsetReceiver?.let {
            registerReceiver(it, bluetoothHeadsetFilter)
        }
        Log.d(
            TAG, "HEADSET profile state: "
                    + stateToString(bluetoothAdapter?.getProfileConnectionState(BluetoothProfile.HEADSET)!!)
        );
        Log.d(TAG, "Bluetooth proxy for headset profile has started")
        bluetoothState = State.HEADSET_UNAVAILABLE
        Log.d(TAG, "start done: BT state=$bluetoothState");
    }

    /** Stops and closes all components related to Bluetooth audio. */
    fun stop() {
        bluetoothHeadsetReceiver?.let {
            unregisterReceiver(it)
        }
        Log.d(
            TAG,
            "stop: BT state=$bluetoothState"
        )
        if (bluetoothAdapter != null) {
            // Stop BT SCO connection with remote device if needed.
            stopScoAudio()
            // Close down remaining BT resources.
            if (bluetoothState != State.UNINITIALIZED) {
                cancelTimer()
                if (bluetoothHeadset != null) {
                    bluetoothAdapter?.closeProfileProxy(BluetoothProfile.HEADSET, bluetoothHeadset)
                }
                bluetoothState = State.UNINITIALIZED
            }
        }
        Log.d(TAG, "stop done: BT state=$bluetoothState");
    }

    fun startScoAudio(): Boolean {
        if (scoConnectionAttempts >= MAX_SCO_CONNECTION_ATTEMPTS) {
            return false
        }
        if (bluetoothState != State.HEADSET_AVAILABLE) {
            return false
        }
        Log.d(TAG, "Starting Bluetooth SCO and waits for ACTION_AUDIO_STATE_CHANGED...");
        bluetoothState = State.SCO_CONNECTING
        audioManager.startBluetoothSco()
        scoConnectionAttempts++
        startTimer()
        return true
    }

    fun stopScoAudio() {
        if (bluetoothState != State.SCO_CONNECTING && bluetoothState != State.SCO_CONNECTED) {
            return
        }
        cancelTimer()
        audioManager.stopBluetoothSco()
        bluetoothState = State.SCO_DISCONNECTING
    }

    fun updateDevice() {
        if (bluetoothState == State.UNINITIALIZED || bluetoothHeadset == null) {
            Log.d(TAG, "bluetooth headset is null: ${bluetoothHeadset == null}")
            return
        }
        bluetoothHeadset?.let {
            val devices: List<BluetoothDevice> = it.connectedDevices
            if (devices.isEmpty()) {
                bluetoothState = State.HEADSET_UNAVAILABLE
                Log.d(TAG, "No connected bluetooth headset")
            } else {
                // Always use first device is list. Android only supports one device.
                bluetoothDevice = devices[0]
                bluetoothState = State.HEADSET_AVAILABLE
            }
        }

        Log.d(TAG, "updateDevice done: BT state=$bluetoothState")
    }

    private fun registerReceiver(receiver: BroadcastReceiver, filter: IntentFilter) {
        try {
            appContext.registerReceiver(receiver, filter)
        } catch (e: IllegalArgumentException){
            Log.e(TAG, e.message)
        }
    }

    private fun unregisterReceiver(receiver: BroadcastReceiver) {
        try {
            appContext.unregisterReceiver(receiver)
        } catch (e: IllegalArgumentException){
            Log.e(TAG, e.message)
        }
    }

    private fun getBluetoothProfileProxy(
        context: Context,
        listener: BluetoothProfile.ServiceListener,
        profile: Int
    ): Boolean? {
        return bluetoothAdapter?.getProfileProxy(context, listener, profile)
    }

    private fun hasPermission(context: Context, permission: String): Boolean {
        return context.checkPermission(
            permission,
            Process.myPid(),
            Process.myUid()
        ) == PackageManager.PERMISSION_GRANTED
    }

    /** Ensures that the audio manager updates its list of available audio devices. */
    fun updateAudioDeviceState() {
        appAudioManager.updateAudioDeviceState()
    }

    /** Starts timer which times out after BLUETOOTH_SCO_TIMEOUT_MS milliseconds. */
    private fun startTimer() {
        handler.postDelayed(bluetoothTimeoutRunnable, BLUETOOTH_SCO_TIMEOUT_MS)
    }

    /** Cancels any outstanding timer tasks. */
    fun cancelTimer() {
        handler.removeCallbacks(bluetoothTimeoutRunnable)
    }

    /**
     * Called when start of the BT SCO channel takes too long time. Usually
     * happens when the BT device has been turned on during an ongoing call.
     */
    private fun bluetoothTimeout() {
        if (bluetoothState == State.UNINITIALIZED || bluetoothHeadset == null) {
            return
        }
        if (bluetoothState != State.SCO_CONNECTING) {
            return
        }
        // Bluetooth SCO should be connecting; check the latest result.
        var scoConnected = false
        val devices = bluetoothHeadset?.connectedDevices ?: emptyList()
        if (devices.size > 0) {
            bluetoothDevice = devices[0]
            bluetoothHeadset?.let {
                it.isAudioConnected(bluetoothDevice)
                scoConnected = true
            }
        }
        if (scoConnected) {
            bluetoothState = State.SCO_CONNECTED
            scoConnectionAttempts = 0
        } else {
            stopScoAudio()
        }
        updateAudioDeviceState()
    }

    private fun stateToString(state: Int): String {
        when (state) {
            BluetoothAdapter.STATE_DISCONNECTED -> return "DISCONNECTED"
            BluetoothAdapter.STATE_CONNECTED -> return "CONNECTED"
            BluetoothAdapter.STATE_CONNECTING -> return "CONNECTING"
            BluetoothAdapter.STATE_DISCONNECTING -> return "DISCONNECTING"
            BluetoothAdapter.STATE_OFF -> return "OFF"
            BluetoothAdapter.STATE_ON -> return "ON"
            BluetoothAdapter.STATE_TURNING_OFF ->
                // Indicates the local Bluetooth adapter is turning off. Local clients should immediately
                // attempt graceful disconnection of any remote links.
                return "TURNING_OFF"
            BluetoothAdapter.STATE_TURNING_ON ->
                // Indicates the local Bluetooth adapter is turning on. However local clients should wait
                // for STATE_ON before attempting to use the adapter.
                return "TURNING_ON"
            else -> return "INVALID"
        }
    }
}