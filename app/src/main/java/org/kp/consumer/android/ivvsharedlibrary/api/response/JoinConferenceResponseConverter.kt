package org.kp.consumer.android.ivvsharedlibrary.api.response


internal class JoinConferenceResponseConverter : Converter {
    override fun convert(responseJson: String) : JoinConferenceResponse = Converter.convertFromJson(responseJson)
}