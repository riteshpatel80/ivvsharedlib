package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig


import org.kp.consumer.android.ivvsharedlibrary.data.source.RemoteDataSource
import org.kp.consumer.android.ivvsharedlibrary.data.source.RemoteDataSource.Companion.baseurl
import org.kp.consumer.android.ivvsharedlibrary.util.Log
import org.kp.kpnetworking.request.BaseRequestConfig

internal open class VVBaseRequest (requestType: REQUEST_TYPE, featureUrlPath: String) :
    BaseRequestConfig(requestType, "$HOST$featureUrlPath") {

    init {
        addHeader(X_IBM_CLIENT_ID, RemoteDataSource.clientId)
        addHeader(CONTENT_TYPE, CONTENT_TYPE_JSON)
        addHeader(IVVR_EVLBL_TAG, RemoteDataSource.envlbl)
        Log.d("BaseRequest", this.url)
    }

    companion object {
        const val X_IBM_CLIENT_ID = "X-IBM-Client-Id"
        const val TOKEN_KEY = "token"

        const val AUTHORIZATION = "Authorization"
        const val BEARER = "Bearer"
        const val CONTENT_TYPE = "Content-Type"
        const val CONTENT_TYPE_JSON = "application/json"

        val HOST = baseurl
        const val SETUP_API_VERSION = "/service/diag_thrpy/virtualcare/ivv/nativeconference/v1"
        const val IVVR_EVLBL_TAG = "ivvr-envlbl"
        const val HREG1 = "hreg1"

        const val PIN_KEY = "pin"
        const val REGISTERED_API_VERSION =
            "/kp/qa/service/diag_thrpy/virtualcare/ivv/registration/v1/"
    }
}