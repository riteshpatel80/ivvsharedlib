package org.kp.consumer.android.ivvsharedlibrary.features.waitingRoom

import android.Manifest
import android.app.Activity
import androidx.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import org.kp.consumer.android.ivvsharedlibrary.util.Log
import android.view.*
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.waiting_room_fragment.*

import org.kp.consumer.android.ivvsharedlibrary.R
import org.kp.consumer.android.ivvsharedlibrary.ViewModelFactory
import org.kp.consumer.android.ivvsharedlibrary.api.response.ResponseResult
import org.kp.consumer.android.ivvsharedlibrary.api.response.SetupConferenceResponse
import org.kp.consumer.android.ivvsharedlibrary.controller.VVSessionController
import org.kp.consumer.android.ivvsharedlibrary.databinding.WaitingRoomFragmentBinding
import org.kp.consumer.android.ivvsharedlibrary.features.VideoFragment
import org.kp.consumer.android.ivvsharedlibrary.features.VideoViewModel
import org.kp.consumer.android.ivvsharedlibrary.features.participants.ParticipantsViewModel
import org.kp.consumer.android.ivvsharedlibrary.service.RefreshTokenService
import org.kp.consumer.android.ivvsharedlibrary.util.*
import org.kp.consumer.android.ivvsharedlibrary.util.DialogUtils.DialogButtonClickListener
import org.kp.consumer.android.ivvsharedlibrary.webrtc.PeerConnectionProvider

import java.util.*

class WaitingRoomFragment : Fragment() {
    private var token: String? = null
    private var isAlreadyLanded: Boolean = false
    private var containerId: Int? = null
    lateinit var cameraUtil: CameraUtil


    private var isCameraOn: Boolean = false

    private var isAudioOn: Boolean = false

    private val REQUEST_CAMERA_PERMISSION = 1


    private lateinit var videoViewModel: VideoViewModel
    private lateinit var partcipantViewModel: ParticipantsViewModel
    val providerAdapter = WaitingRoomProviderAdapter()
    val patientAdapter = WaitingRoomPatientAdapter()

    /**
     * An additional thread for running tasks that shouldn't block the UI.
     */
    private var backgroundThread: HandlerThread? = null

    /**
     * A [Handler] for running tasks in the background.
     */
    private var backgroundHandler: Handler? = null

    private lateinit var activity: Activity

    private var joinConferenceCallDone: Boolean = false

    var participantsHanlder = Handler()

    private var mProgressDialog: AlertDialog? = null

    private var setupConferenceResponse: SetupConferenceResponse? = null

    var hasNavigated = false

    private val leaveListener = object : DialogButtonClickListener {
        override fun onYesClicked() {

            if (joinConferenceCallDone) {

                if (isAlreadyLanded) {
                    (parentFragment as VideoFragment).setShouldHoldMessageNotDisplay(true)
                }

                videoViewModel.leaveConference(
                    token!!,
                    videoViewModel.isHost(partcipantViewModel.participantList)
                )
            } else {
                removeWaitingRoomFragment()
                stopRefreshService()
                VVSessionController.vvEventHandler?.didEndVV()
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        activity = context as Activity
        activity.window.statusBarColor = ContextCompat.getColor(activity, R.color.kpblue)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            token = it.getString(TOKEN_TAG)
            isAlreadyLanded = it.getBoolean(IS_ALREADY_LANDED)
            containerId = it.getInt(CONTAINER_ID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.waiting_room_fragment, container, false)

        WaitingRoomFragmentBinding.bind(view).apply {

            videoViewModel = activity?.run {
                ViewModelProvider(
                    requireActivity(),
                    ViewModelFactory.getInstance(activity.application)
                ).get(VideoViewModel::class.java)
            }
            partcipantViewModel = activity?.run {
                ViewModelProvider(
                    requireActivity(),
                    ViewModelFactory.getInstance(activity.application)
                ).get(ParticipantsViewModel::class.java)
            }
        }
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (isAlreadyLanded) {
            joinConferenceCallDone = true
        }

        if (ActivityCompat.checkSelfPermission(
                activity,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(
                activity,
                Manifest.permission.RECORD_AUDIO
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            if (ActivityCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.RECORD_AUDIO
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions( //Method of Fragment
                    arrayOf(
                        Manifest.permission.CAMERA,
                        Manifest.permission.RECORD_AUDIO
                    ),
                    VideoFragment.REQUEST_PERMISSIONS_CODE_CAMERA
                )
            } else if (ActivityCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED
            ) {

                isAudioOn = true
                requestPermissions( //Method of Fragment
                    arrayOf(
                        Manifest.permission.CAMERA
                    ),
                    VideoFragment.REQUEST_PERMISSIONS_CODE_CAMERA
                )

            } else {
                isCameraOn = true
                requestPermissions( //Method of Fragment
                    arrayOf(
                        Manifest.permission.RECORD_AUDIO
                    ),
                    VideoFragment.REQUEST_PERMISSIONS_CODE_CAMERA
                )


            }


        } else {

            NetworkUtils.isInternetAvailable(activity)?.let {

                if (!isAlreadyLanded) {
                    token?.let {
                        showDialog()
                        videoViewModel.setUpConference(it)
                    }
                } else {
                    makeUseOfSetup()
                }

            } ?: run {
                DialogUtils.showOkCancelDialog(
                    activity,
                    getString(R.string.kp_check_your_connection),
                    false,
                    dialogCallback
                )
            }
        }

        waiting_provider_list.apply {
            adapter = providerAdapter
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
                activity,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        }
        waiting_patient_list.apply {
            adapter = patientAdapter
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
                activity,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        }
        cameraUtil = CameraUtil(activity, waiting_self_view, dialogCallback)
    }

    override fun onResume() {
        super.onResume()
        startBackgroundThread()
        (activity as AppCompatActivity).supportActionBar?.hide()

        // When the screen is turned off and turned back on, the SurfaceTexture is already
        // available, and "onSurfaceTextureAvailable" will not be called. In that case, we can open
        // a camera and start preview from here (otherwise, we wait until the surface is ready in
        // the SurfaceTextureListener).
        if (waiting_self_view.isAvailable) {
            Log.d(TAG, "open camera called")
            cameraUtil.openCamera()
        } else {
            Log.d(TAG, "surface texture called")
            waiting_self_view.surfaceTextureListener = cameraUtil.surfaceTextureListener
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        videoViewModel.startHostLeftTimer()
        waiting_room_leave.apply {
            setOnClickListener {
                DialogUtils.showOkCancelDialog(
                    activity,
                    resources.getString(R.string.kp_leave_visit_string) + "\n\n" + resources.getString(R.string.kp_leave_visit),
                    true,
                    leaveListener
                )
            }
        }
        partcipantViewModel.participantsItemsObserbale.observe(viewLifecycleOwner, Observer { items ->

            if (joinConferenceCallDone) {
                mProgressDialog?.dismiss()

                items?.getContentIfNotHandled()?.let {
                    var signedInUserExist = false
                    it?.forEach {
                        if (it.vmrRole.equals(CHAIR) && !hasNavigated) {

                            hasNavigated = true
                            navigateToVideoFragment()
                            return@let
                        }
                        if (videoViewModel.getConferenceResponse()?.signedInUser?.id == it.id){
                            signedInUserExist = true
                        }

                    }
                    if (!hasNavigated) {
                        showWaitingRoomLayout()
                        patientAdapter.setData(it)
                        refreshParticipants()
                    }
                    if ((it.isEmpty() || !signedInUserExist) && !videoViewModel.insideTheSurvey){
                        DialogUtils.showOkDialog(
                            activity,
                            resources.getString(R.string.kp_drop_alert),
                            false
                        )
                        leaveListener.onYesClicked()
                    }
                }
            }
        })
        videoViewModel.setUpConferenceResponse.observe(viewLifecycleOwner, Observer {
            it?.getContentIfNotHandled()?.let {
                makeUseOfSetup()
            }
        })
        videoViewModel.expiry.observe(viewLifecycleOwner, Observer {
            if (!isAlreadyLanded) {
                it?.getContentIfNotHandled()?.let {
                    startRefreshService(it)
                    videoViewModel.joinConference(token!!)
                }
            }
        })

        videoViewModel.participantsEvent.observe(viewLifecycleOwner, Observer {
            joinConferenceCallDone = true
            it?.getContentIfNotHandled()?.let {
                makeParticipantsCall()
            }
        })


        videoViewModel.errorMesssage.observe(viewLifecycleOwner, Observer {


            it?.getContentIfNotHandled()?.let {
                if (!videoViewModel.insideTheSurvey) {
                    when (it) {
                        is ResponseResult.ServiceError -> {

                            DialogUtils.showOkDialog(
                                activity as Context,
                                ErrorMessages.getErrorMessages(it.error.code.toInt()),
                                false,
                                dialogCallback
                            )
                        }
                        is ResponseResult.Failure -> {
                            DialogUtils.showOkDialog(
                                activity as Context,
                                ErrorMessages.getErrorMessages(it.errorCode.toInt()),
                                false,
                                dialogCallback
                            )
                        }
                        else -> {
                            DialogUtils.showOkDialog(
                                activity as Context,
                                getString(R.string.kp_system_error_2),
                                false,
                                dialogCallback
                            )
                        }
                    }
                }
            }
        })

        videoViewModel.diloagCloseEvent.observe(viewLifecycleOwner, Observer {

            it?.getContentIfNotHandled()?.let {
                mProgressDialog?.dismiss()
            }

        })
        partcipantViewModel.dialogCloseEvent.observe(viewLifecycleOwner, Observer {

            it?.getContentIfNotHandled()?.let {
                mProgressDialog?.dismiss()
            }

        })

        videoViewModel.leaveConferenceEvent.observe(viewLifecycleOwner, Observer {
            it?.getContentIfNotHandled()?.let {
                removeWaitingRoomFragment()
                stopRefreshService()
                stopBackgroundThread()
                PeerConnectionProvider.freeAllResources()
                VVSessionController.vvEventHandler?.didEndVV()
            }
        })

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            waiting_room_scrollview.setOnScrollChangeListener(scrollChangeListener)
        }
    }

    val scrollChangeListener =
        View.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            if (!v.canScrollVertically(1)) {
                // bottom of scroll view
                waiting_leave_scroll.importantForAccessibility =
                    View.IMPORTANT_FOR_ACCESSIBILITY_YES

            } else {
                waiting_leave_scroll.importantForAccessibility =
                    View.IMPORTANT_FOR_ACCESSIBILITY_NO_HIDE_DESCENDANTS
            }
            if (!v.canScrollVertically(-1)) {
                // top of scroll view
                waiting_top_scroll.importantForAccessibility =
                    View.IMPORTANT_FOR_ACCESSIBILITY_NO_HIDE_DESCENDANTS
            } else {

                waiting_top_scroll.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
            }
        }

    override fun onPause() {
        cameraUtil.closeCamera()
        stopBackgroundThread()
        super.onPause()
    }

    fun closeCamera() {
        cameraUtil.closeCamera()
    }

    /**
     * Starts a background thread and its [Handler].
     */
    private fun startBackgroundThread() {
        backgroundThread = HandlerThread("CameraBackground").also { it.start() }
        backgroundHandler = Handler(backgroundThread?.looper)
    }

    /**
     * Stops the background thread and its [Handler].
     */
    private fun stopBackgroundThread() {
        backgroundThread?.quitSafely()
        try {
            backgroundThread?.join()
            backgroundThread = null
            backgroundHandler = null
        } catch (e: InterruptedException) {
            Log.e(TAG, e.toString())
        }

    }

    private fun makeUseOfSetup() {
        videoViewModel.getConferenceResponse()?.let { setup ->

            setupConferenceResponse = setup
            providerAdapter.setData(setup.appointment.providerList)
            val name = setup.signedInUser.name.split(' ')
                .joinToString(" ") { it.toLowerCase(Locale.getDefault()).capitalize() }
            val title = "${resources.getString(R.string.kp_welcome)} $name!"
            waiting_title.text = title
            CustomBindingAdapter.setDateTime(
                isMember(setup.signedInUser.role),
                waiting_time_text,
                setup.appointment.apptDateTime
            )
            Log.d(TAG, setup.appointment.toString())

            Log.d(TAG, setup.signedInUser.name + "--" + setup.signedInUser.role)

            if (!isAlreadyLanded) {
                videoViewModel.pexipRequestToken()
            } else {
                makeParticipantsCall()
            }

        }
    }

    private fun showWaitingRoomLayout() {
        setupConferenceResponse?.let {
            waitingLayout.visibility = View.VISIBLE
            waiting_topbar_layout.visibility = View.VISIBLE
        }
    }

    private fun isMember(role: String): Boolean {

        return role.equals(Role.PATIENT.role) || role.equals(Role.MEMBER_PROXY.role)
    }


    private fun refreshParticipants() {

        participantsHanlder.removeMessages(0)

        participantsHanlder.postDelayed(mParticpantsRunnable, REFRESH_TIME)
    }

    private val mParticpantsRunnable = {
        makeParticipantsCall()
    }

    private fun makeParticipantsCall() {
        videoViewModel.getConferenceResponse()?.let {
            partcipantViewModel.getParticipants()
        }
    }

    private fun showDialog() {
        mProgressDialog =
            DialogUtils.createProgressDialog(
                activity as Context,
                R.string.kp_content_loading,
                false
            )
        mProgressDialog?.show()
    }

    /*
     * This methods starts the Refresh token service
     */
    private fun startRefreshService(it: Long) {
        var startIntent = Intent(activity, RefreshTokenService::class.java);
        startIntent.setAction(RefreshTokenService.Companion.ACTION.START_ACTION);
        startIntent.putExtra(RefreshTokenService.EXPIRES, it)
        activity?.startService(startIntent)
    }

    /*
      This method stops the refresh token service
     */
    private fun stopRefreshService() {
        var startIntent = Intent(activity, RefreshTokenService::class.java);
        startIntent.setAction(RefreshTokenService.Companion.ACTION.STOP_ACTION);
        activity?.startService(startIntent)
    }


    var dialogCallback = object : DialogButtonClickListener {
        override fun onYesClicked() {
            mProgressDialog?.dismiss()
            removeWaitingRoomFragment()
            stopRefreshService()
            PeerConnectionProvider.freeAllResources()
            VVSessionController.vvEventHandler?.onError()
        }


    }

    private fun removeWaitingRoomFragment() {
        var waitingRoomFragment =
            (activity as FragmentActivity).supportFragmentManager.findFragmentByTag(TAG);

        waitingRoomFragment?.let {
            (activity as FragmentActivity).supportFragmentManager.beginTransaction()
                .remove(waitingRoomFragment).commit();

        }
    }


    private fun navigateToVideoFragment() {
        closeCamera()
        if (!isAlreadyLanded) {
            activity?.run {
                (activity as FragmentActivity).supportFragmentManager.beginTransaction()
                    .replace(
                        containerId!!,
                        VideoFragment.newInstance(token!!, containerId!!),
                        VideoFragment.TAG
                    )
                    .commit();

            }
        } else {
            (parentFragment as VideoFragment).hideWaitingFragment()
        }
    }

    fun closeProgressDialog() {

        mProgressDialog?.dismiss()
    }


    fun stopVideoCall(conferenceToken: String) {
        videoViewModel.insideTheSurvey = true
        videoViewModel.leaveConference(
            conferenceToken,
            videoViewModel.isHost(partcipantViewModel.participantList)
        )
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {


        if (requestCode == VideoFragment.REQUEST_PERMISSIONS_CODE_CAMERA) {


            if (permissions.size == 2) {


                isCameraOn = permissions[0].equals(Manifest.permission.CAMERA)
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                isAudioOn = permissions[1].equals(Manifest.permission.RECORD_AUDIO)
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED

            } else if (permissions.size == 1) {

                if (permissions[0].equals(Manifest.permission.CAMERA))
                    isCameraOn = grantResults[0] == PackageManager.PERMISSION_GRANTED

                if (permissions[0].equals(Manifest.permission.RECORD_AUDIO))
                    isAudioOn = grantResults[0] == PackageManager.PERMISSION_GRANTED

            }

            if (isCameraOn && isAudioOn) {

                NetworkUtils.isInternetAvailable(activity)?.let {

                    if (!isAlreadyLanded) {
                        token?.let {
                            showDialog()
                            videoViewModel.setUpConference(it)
                        }
                    } else {
                        makeUseOfSetup()
                    }

                } ?: run {
                    DialogUtils.showOkDialog(
                        activity,
                        resources.getString(R.string.kp_check_your_connection),
                        false,
                        dialogCallback
                    )
                }

            } else if (!isCameraOn || !isAudioOn) {

                val accessDeniedFullText =
                    getString(R.string.kp_access_denied) + "\n\n" + getString(R.string.kp_enable_camera_and_or_audio_access)

                DialogUtils.showOkDialog(
                    activity,
                    accessDeniedFullText,
                    false,
                    dialogCallback
                )

            }
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d(TAG,"========waiting room onDestroyView Called==========")
        cameraUtil.closeCamera()

        Log.d(TAG,"==========waiting room onDestroyView completed=========")
    }
    private fun clearResourses(){
        Log.d(TAG, "remotedatasource destroyed")
        stopRefreshService()
    }


    companion object {
        val CONTAINER_ID = "containerId"
        val TOKEN_TAG = "token"
        val IS_ALREADY_LANDED = "isAlreadyLanded"

        val REFRESH_TIME = 20 * 1000L //20sec

        val TAG = WaitingRoomFragment::class.java.simpleName

        @JvmStatic
        fun newInstance(token: String, containerId: Int, isAlreadyLanded: Boolean = false) =
            WaitingRoomFragment().apply {
                arguments = Bundle().apply {
                    putString(TOKEN_TAG, token)
                    putInt(CONTAINER_ID, containerId)
                    putBoolean(IS_ALREADY_LANDED, isAlreadyLanded)
                }
            }
    }
}
