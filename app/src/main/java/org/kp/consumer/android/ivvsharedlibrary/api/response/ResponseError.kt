package org.kp.consumer.android.ivvsharedlibrary.api.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class ResponseError(

    @SerializedName("severity")
    val severity: String,

    @SerializedName("type")
    val type: String,

    @SerializedName("code")
    val code: String,

    @SerializedName("error")
    val error: String,

    @SerializedName("errorDescription")
    val errorDescription: String
) : Serializable