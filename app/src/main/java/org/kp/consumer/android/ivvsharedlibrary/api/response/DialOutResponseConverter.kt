package org.kp.consumer.android.ivvsharedlibrary.api.response


internal class DialOutResponseConverter : Converter {
    override fun convert(responseJson: String) : DialOutResponse = Converter.convertFromJson(responseJson)
}