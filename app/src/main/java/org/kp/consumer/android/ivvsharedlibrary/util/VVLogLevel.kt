package org.kp.consumer.android.ivvsharedlibrary.util

enum class VVLogLevel {
    verbose, debug, info
}