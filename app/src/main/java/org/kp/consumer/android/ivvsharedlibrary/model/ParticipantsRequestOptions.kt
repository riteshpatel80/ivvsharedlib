package org.kp.consumer.android.ivvsharedlibrary.model

import org.kp.consumer.android.ivvsharedlibrary.features.VideoViewModel.Companion.KP_MEMBER_MOBILE_APP_ANDROID

internal data class ParticipantsRequestOptions (val uuid : String = "0",
                                                val source : String = KP_MEMBER_MOBILE_APP_ANDROID,
                                                val isMuted : Boolean? = null,
                                                val event : String = "NONE",
                                                val displayName : String? = null
)