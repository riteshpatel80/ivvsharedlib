package org.kp.consumer.android.ivvsharedlibrary.api.task

import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.SetupConferenceRequest
import org.kp.consumer.android.ivvsharedlibrary.api.response.SetupConferenceResponseConverter


internal class SetUpConferenceTask(registrationToken: String) : VVBaseHttpTask(
    SetupConferenceRequest(registrationToken),
    converter = SetupConferenceResponseConverter()
)