package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig

import com.google.gson.annotations.SerializedName
import org.kp.consumer.android.ivvsharedlibrary.model.Appointment
import org.kp.consumer.android.ivvsharedlibrary.model.SignedInUser
import java.io.Serializable


data class LeaveConferenceRequestModel(
    @SerializedName("postConferenceStatus")
    val postConferenceStatus: Boolean,

    @SerializedName("conferenceStatusRole")
    val conferenceStatusRole: String,

    @SerializedName("endConference")
    val endConference: Boolean

) : Serializable