package org.kp.consumer.android.ivvsharedlibrary.api.response

import com.google.gson.annotations.SerializedName


data class JoinConferenceResponse (

    @SerializedName("errors")
    override val errors: List<ResponseError>

) : VVBaseResponse