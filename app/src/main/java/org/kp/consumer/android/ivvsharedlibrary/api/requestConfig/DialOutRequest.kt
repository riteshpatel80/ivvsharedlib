package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig

import com.google.gson.Gson

internal class DialOutRequest(dialOutRequestModel: DialOutRequestModel, registrationToken: String) : VVBaseRequest(REQUEST_TYPE.POST, DIAL_OUT_URL_PATH) {

    init {
        addHeader(AUTHORIZATION, "$BEARER $registrationToken")
        addParam(TOKEN_KEY, registrationToken)

        body = Gson().toJson(dialOutRequestModel)
    }

    companion object {
        private const val DIAL_OUT_URL_PATH = "/dialout"
    }
}