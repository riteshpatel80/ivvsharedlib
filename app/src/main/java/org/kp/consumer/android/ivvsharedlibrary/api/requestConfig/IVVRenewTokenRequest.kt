package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig

internal class IVVRenewTokenRequest(registrationToken: String) : VVBaseRequest(REQUEST_TYPE.GET, RENEW_TOKEN_PATH) {

    init {
        addHeader(AUTHORIZATION, "$BEARER $registrationToken")
    }
    companion object {
        const val RENEW_TOKEN_PATH = "/renewtoken"
        private val TAG =IVVRenewTokenRequest::class.java.simpleName
    }
}