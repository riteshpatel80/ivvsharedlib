package org.kp.consumer.android.ivvsharedlibrary


import android.content.Context
import org.kp.consumer.android.ivvsharedlibrary.data.source.RemoteDataSource
import org.kp.consumer.android.ivvsharedlibrary.data.source.Repository


object Injection {

    fun provideTasksRepository(context: Context): Repository {

        return Repository.getInstance(RemoteDataSource.getInstance())
    }



}