package org.kp.consumer.android.ivvsharedlibrary.features.dialout

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.content.ContentResolver
import android.content.Intent
import org.kp.consumer.android.ivvsharedlibrary.util.Log
import org.kp.consumer.android.ivvsharedlibrary.api.response.DialOutResponse
import org.kp.consumer.android.ivvsharedlibrary.api.response.ResponseResult
import org.kp.consumer.android.ivvsharedlibrary.data.source.DataSource
import org.kp.consumer.android.ivvsharedlibrary.data.source.Repository
import org.kp.consumer.android.ivvsharedlibrary.model.DialOut
import android.provider.ContactsContract.CommonDataKinds.Phone
import androidx.annotation.VisibleForTesting
import kotlinx.coroutines.*
import org.kp.consumer.android.ivvsharedlibrary.Event


class DialoutViewModel(
    private val tasksRepository: Repository,
    private val dispatcherIO: CoroutineDispatcher
) : ViewModel() {

    private val viewModelJob = Job()
    val SUCCESS = "Success"

    private val viewModelScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    val TAG = this@DialoutViewModel.javaClass.simpleName
    val dialoutResult = MutableLiveData<Event<String>>()

    val dialoutCallback = object : DataSource.GetCallback<DialOutResponse> {
        override fun onSuccess() {
        }

        override fun onSuccess(value: DialOutResponse) {
            viewModelScope.launch {
                Log.d(
                    this@DialoutViewModel.javaClass.simpleName,
                    "addDialOut ran $value"
                )
                dialoutResult.postValue(Event(SUCCESS))
            }

        }

        override fun onFailure(value: ResponseResult.ServiceError) {
            viewModelScope.launch {
                Log.e(
                    this@DialoutViewModel.javaClass.simpleName,
                    "addDialOut service error: ${value.error.errorDescription}"
                )
                dialoutResult.postValue(Event(value.error.errorDescription))
            }
        }

        override fun onFailure(errrorCode: ResponseResult.Failure) {
            viewModelScope.launch {
                Log.e(
                    this@DialoutViewModel.javaClass.simpleName,
                    "addDialOut errorcode: ${errrorCode.errorCode}"
                )
                dialoutResult.postValue(Event(errrorCode.errorCode))
            }

        }

    }

    fun addDialOut(
        dialOut: DialOut
    ) {
        viewModelScope.launch(dispatcherIO) {
            tasksRepository.addDialOutRequest(dialOut, dialoutCallback)
        }
    }

    private val phoneProjection = arrayOf(Phone.DATA)

    fun getContact(
        contentResolver: ContentResolver,
        data: Intent?,
        callback: (String) -> Unit
    ) = viewModelScope.launch(dispatcherIO) {
        data?.data?.let { uri ->
            val cursor = contentResolver.query(uri, phoneProjection, null, null, null)
            cursor?.use { cur ->
                while (cur.moveToNext()) {
                    val number = cur.getString(0)
                    viewModelScope.launch {
                        if (number != null) callback(parseNumber(number))
                    }
                }
            }
        }

    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun parseNumber(number: String): String {
        val newNumber = number.filter { c -> c in '0'..'9' }
        if (newNumber.length < 10) return number
        return newNumber.takeLast(10)

    }
}