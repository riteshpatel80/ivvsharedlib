package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig


internal class SetupConferenceRequest(registrationToken: String) : VVBaseRequest(REQUEST_TYPE.GET, SETUP_CONF_URL_PATH) {

    init {
        addHeader(AUTHORIZATION, "$BEARER $registrationToken")
        addParam(TOKEN_KEY, registrationToken)
    }

    companion object {
        private const val SETUP_CONF_URL_PATH = "/setup"
    }
}