package org.kp.consumer.android.ivvsharedlibrary.api.task

import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.GetParticipantsRequest
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.GetParticipantsRequestModel
import org.kp.consumer.android.ivvsharedlibrary.api.response.GetParticipantsResponseConverter


internal class GetParticipantsTask(
    getParticipantsRequestModel: GetParticipantsRequestModel,
    registrationToken: String
) : VVBaseHttpTask(
    GetParticipantsRequest(getParticipantsRequestModel, registrationToken),
    converter = GetParticipantsResponseConverter()
)