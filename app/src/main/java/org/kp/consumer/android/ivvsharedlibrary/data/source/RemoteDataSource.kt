package org.kp.consumer.android.ivvsharedlibrary.data.source

import android.annotation.SuppressLint
import org.kp.consumer.android.ivvsharedlibrary.util.Log
import com.here.oksse.OkSse
import com.here.oksse.ServerSentEvent
import okhttp3.Request
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.GetParticipantsRequestModel
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.RequestTokenRequest
import org.kp.consumer.android.ivvsharedlibrary.api.response.GetParticipantsResponse
import org.kp.consumer.android.ivvsharedlibrary.api.response.RequestTokenResponse
import org.kp.consumer.android.ivvsharedlibrary.api.response.SetupConferenceResponse
import org.kp.consumer.android.ivvsharedlibrary.api.task.GetParticipantsTask
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.*
import org.kp.consumer.android.ivvsharedlibrary.api.response.*
import org.kp.consumer.android.ivvsharedlibrary.api.response.ResponseResult.*
import org.kp.consumer.android.ivvsharedlibrary.api.response.ResponseResult.Success
import org.kp.consumer.android.ivvsharedlibrary.api.task.*
import org.kp.consumer.android.ivvsharedlibrary.api.task.AckTask
import org.kp.consumer.android.ivvsharedlibrary.api.task.CallRequestTask
import org.kp.consumer.android.ivvsharedlibrary.api.task.RefreshTokenTask
import org.kp.consumer.android.ivvsharedlibrary.api.task.RequestTokenTask
import org.kp.consumer.android.ivvsharedlibrary.api.task.SetUpConferenceTask
import org.kp.consumer.android.ivvsharedlibrary.data.source.Repository.Companion.getInstance
import org.kp.consumer.android.ivvsharedlibrary.model.*
import org.webrtc.SessionDescription

/*

  @pexipscheme  set always "https://"
 */

class RemoteDataSource(private val pexipScheme: String = HTTPS) : DataSource {

    var mServerSentEvent: ServerSentEvent? = null
    private var _setupConferenceResponse: SetupConferenceResponse? = null

    var setupConferenceResponse: SetupConferenceResponse? = null
        get() = _setupConferenceResponse

    var _pexipToken: String? = null
    var pexipToken: String? = null
        get() = _pexipToken


    override fun requestToken(
        room: Room,
        displayName: String,
        callback: DataSource.GetCallback<RequestTokenResponse>,
        mServerSentEventListener: ServerSentEvent.Listener
    ) {

        var requestTokenTask = RequestTokenTask("$pexipScheme${room.pexipNode}", room.alias, displayName, room.pin)


        var requestTokenResult = requestTokenTask.makeRequest()



        if (requestTokenResult is Success) {
            val requestTokenResponse: RequestTokenResponse =
                requestTokenResult.response as RequestTokenResponse

            callback.onSuccess(requestTokenResponse);


            var token: String = requestTokenResponse.token
            _pexipToken = requestTokenResponse.token

            val url =
                "https://" + room.pexipNode + RequestTokenRequest.API_VERSION + room.alias + "/events?token=" + token
            val request = Request.Builder().url(url).build()

            val okSse = OkSse()
            mServerSentEvent = okSse.newServerSentEvent(request, mServerSentEventListener)
        } else if (requestTokenResult is ServiceError) {

            callback.onFailure(requestTokenResult)
        } else {
            callback.onFailure(requestTokenResult as Failure)
        }


    }

    override fun refreshToken(
        room: Room,
        token: String,
        callback: DataSource.GetCallback<RefreshTokenResponse>
    ) {
        Log.d(TAG, "token: $token")
        var refreshTokenTask = RefreshTokenTask("$pexipScheme${room.pexipNode}", room.alias, room.pin, token)

        var refreshTokenResult = refreshTokenTask.makeRequest()



        if (refreshTokenResult is Success) {

            var refreshTokenResponse: RefreshTokenResponse =
                refreshTokenResult.response as RefreshTokenResponse

            callback.onSuccess(refreshTokenResponse)


        } else if (refreshTokenResult is ServiceError) {

            callback.onFailure(refreshTokenResult)
        } else {
            callback.onFailure(refreshTokenResult as Failure)
        }


    }

    override fun setUpConferenceRequest(
        token: String,
        callback: DataSource.GetCallback<SetupConferenceResponse>
    ) {

        var setupConferenceRequest = SetUpConferenceTask(token)


        var setupConfResponseResult = setupConferenceRequest.makeRequest()


        if (setupConfResponseResult is Success) {


            val setupConferenceResponse: SetupConferenceResponse =
                setupConfResponseResult.response as SetupConferenceResponse

            Log.d(TAG, "setup: $setupConferenceResponse")
            _setupConferenceResponse = setupConferenceResponse;
            callback.onSuccess(setupConferenceResponse);
        } else if (setupConfResponseResult is ServiceError) {

            callback.onFailure(setupConfResponseResult)
        } else {
            callback.onFailure(setupConfResponseResult as Failure)
        }


    }

    override fun addDialOutRequest(
        dialOut: DialOut,
        callback: DataSource.GetCallback<DialOutResponse>
    ) {

        val TAG = "DialOut response"
        val dialOutRequest = DialOutTask(
            DialOutRequestModel(dialOut), vvConferenceToken!!
        )

        when (val dialOutResponse = dialOutRequest.makeRequest()) {
            is Success -> {
                Log.e(
                    TAG
                    , "dial out success"
                )
                val dialOutResults = dialOutResponse.response as DialOutResponse
                callback.onSuccess(dialOutResults)
                Log.e(TAG, dialOutResults.toString())
            }
            is ServiceError -> {
                Log.e(TAG, "dial out service error")
                callback.onFailure(dialOutResponse)
            }
            is Failure -> {
                val dialOutResults = dialOutResponse.errorCode
                callback.onFailure(dialOutResponse)
                Log.e(TAG, dialOutResults)
            }

        }
    }

    override fun makeCall(
        room: Room,
        participantUUID: String,
        sdp: SessionDescription,
        callback: DataSource.GetCallback<CallResponse>
    ) {

        _pexipToken?.let {
            var callRequestTask =
                CallRequestTask("$pexipScheme${room.pexipNode}", room.alias, participantUUID, it, sdp)

            var callRequestResult = callRequestTask.makeRequest()


            if (callRequestResult is Success) {

                val callResponse: CallResponse = callRequestResult.response as CallResponse

                callback.onSuccess(callResponse)

            } else if (callRequestResult is ServiceError) {

                callback.onFailure(callRequestResult)
            } else {
                callback.onFailure(callRequestResult as Failure)
            }

        }


    }

    override fun doAck(
        room: Room,
        participantUUID: String,
        callUUID: String,
        callback: DataSource.GetCallback<VVBaseResponse>
    ) {

        _pexipToken?.let {
            var ackTask = AckTask("$pexipScheme${room.pexipNode}", room.alias, participantUUID, it, callUUID)

            var ackTaskResult = ackTask.makeRequest()

            if (ackTaskResult is Success) {

                callback.onSuccess()


            } else if (ackTaskResult is ServiceError) {

                callback.onFailure(ackTaskResult)
            } else {
                callback.onFailure(ackTaskResult as Failure)
            }
        }

    }

    override fun disconnect(
        room: Room,
        participantUUID: String,
        token: String,
        callUUID: String,
        callback: DataSource.GetCallback<VVBaseResponse>
    ) {


        var disconnectTask =
            DisconnectTask("$pexipScheme${room.pexipNode}", room.pin, room.alias, participantUUID, token, callUUID)

        var disconnectTaskResult = disconnectTask.makeRequest()

        if (disconnectTaskResult is Success) {

            callback.onSuccess()


        } else if (disconnectTaskResult is ServiceError) {

            callback.onFailure(disconnectTaskResult)
        } else {
            callback.onFailure(disconnectTaskResult as Failure)
        }


    }

    override fun joinConference(
        joinConfRequestModel: JoinConferenceRequestModel,
        registrationToken: String, callback: DataSource.GetCallback<JoinConferenceResponse>
    ) {


        var joinConferenceTask = JoinConferenceTask(joinConfRequestModel, registrationToken)

        var joinConferenceResult = joinConferenceTask.makeRequest()

        if (joinConferenceResult is Success) {
            callback.onSuccess(joinConferenceResult.response as JoinConferenceResponse)
        } else if (joinConferenceResult is ServiceError) {

            callback.onFailure(joinConferenceResult)
        } else {
            callback.onFailure(joinConferenceResult as Failure)
        }


    }


    override fun leaveConference(
        leaveConferenceRequestModel: LeaveConferenceRequestModel,
        registrationToken: String,
        callback: DataSource.GetCallback<LeaveConferenceResponse>
    ) {
        var leaveConferenceTask =
            LeaveConferenceTask(leaveConferenceRequestModel, registrationToken)

        var leaveConferenceResult = leaveConferenceTask.makeRequest()

        if (leaveConferenceResult is Success) {

            callback.onSuccess(leaveConferenceResult.response as LeaveConferenceResponse)

        } else if (leaveConferenceResult is ServiceError) {

            callback.onFailure(leaveConferenceResult)
        } else {
            callback.onFailure(leaveConferenceResult as Failure)
        }
    }


    override fun getParticipantsRequest(
        uuid: String,
        time: String,
        source: String,
        event: String,
        callback: DataSource.GetCallback<GetParticipantsResponse>
    ) {

        var appointment = setupConferenceResponse!!.appointment
        val getParticipantsRequest = GetParticipantsTask(
            GetParticipantsRequestModel(
                appointmentVisitType = appointment.visitType,
                appointmentRegion = appointment.region,
                appointmentMeetingRoomNumber = appointment.meetingRoomNumber,
                uuid = uuid,
                time = time,
                source = source,
                event = event
            ), vvConferenceToken!!
        )

        when (val getParticipantsResponse = getParticipantsRequest.makeRequest()) {
            is Success -> {

                val getParticipantsResults =
                    getParticipantsResponse.response as GetParticipantsResponse
                callback.onSuccess(getParticipantsResults)
            }
            is ServiceError -> callback.onFailure(getParticipantsResponse)
            is Failure -> callback.onFailure(getParticipantsResponse)
        }
    }

    override fun sendChatMessage(
        message: String,
        callback: DataSource.GetCallback<VVBaseResponse>
    ) {

        val messageModel = SendChatMessageModel(message)
        val room = setupConferenceResponse?.room!!

        val sendMessageTask =
            SendChatMessageTask(
                messageModel,
                "$pexipScheme${room.pexipNode}",
                room.alias,
                room.pin,
                pexipToken!!
            )

        var sendMessageResult = sendMessageTask.makeRequest()

        if (sendMessageResult is Success) {
            callback.onSuccess()
        } else if (sendMessageResult is ServiceError) {
            callback.onFailure(sendMessageResult)
        } else {
            callback.onFailure(sendMessageResult as Failure)
        }
    }


    override fun ivvRenewToken(token: String, callback: DataSource.GetCallback<VVBaseResponse>) {

        val ivvRenewTask = IVVRenewTokenTask(vvConferenceToken!!)

        var ivvRenewTaskResult = ivvRenewTask.makeRequest()

        if (ivvRenewTaskResult is Success) {
            callback.onSuccess(ivvRenewTaskResult.response)
        } else if (ivvRenewTaskResult is ServiceError) {

            callback.onFailure(ivvRenewTaskResult)
        } else {
            callback.onFailure(ivvRenewTaskResult as Failure)
        }

    }


    override fun getPexipUpdatedToken(): String {
        return pexipToken!!
    }

    override fun sendSurvey(
        surveyRequestModel: SurveyRequestModel,
        callback: DataSource.GetCallback<VVBaseResponse>
    ) {

        val request = SurveyTask(surveyRequestModel)
        var surveyResult = request.makeRequest()

        when (surveyResult) {
            is Success -> callback.onSuccess(surveyResult.response)
            is ServiceError -> callback.onFailure(surveyResult)
            else -> callback.onFailure(surveyResult as Failure)
        }
    }

    companion object {


        var baseurl: String? = null
        var clientId: String? = null;
        var vvConferenceToken: String? = null
        var envlbl: String? = null
        val TAG = RemoteDataSource::class.java.simpleName
        val HTTPS= "https://"

        @SuppressLint("StaticFieldLeak")
        @Volatile
        private var INSTANCE: RemoteDataSource? = null

        fun getInstance(pexipScheme: String? = HTTPS) =
            RemoteDataSource.INSTANCE
                ?: synchronized(RemoteDataSource::class.java) {
                    INSTANCE
                        ?: RemoteDataSource(pexipScheme!!)
                            .also { INSTANCE = it }
                }
        /**
         * Used to force [getInstance] to create a new instance
         * next time it's called.
         */
        @JvmStatic
        fun destroyInstance() {
            INSTANCE = null
        }


    }


}