package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig

import org.kp.consumer.android.ivvsharedlibrary.util.Log
import org.json.JSONObject
import org.kp.kpnetworking.request.BaseRequestConfig
import org.webrtc.SessionDescription

class CallRequest(pexipHost: String, roomAlias: String,participantUUID: String,token:String, sdp: SessionDescription): BaseRequestConfig(REQUEST_TYPE.POST, "$pexipHost$API_VERSION$roomAlias$PARTCIPANTS_KEY$participantUUID$URL_KEY") {


  init {
    Log.d("CallRequest", "url = $pexipHost$API_VERSION$roomAlias$PARTCIPANTS_KEY$participantUUID$URL_KEY");

    addHeader(TOKEN_KEY, token)
    addHeader(CONTENT_TYPE, "application/json")

    var jsonObject = JSONObject()

      jsonObject.put(CALL_TYPE, "WEBRTC")
      jsonObject.put(SDP, sdp.description)




    body = jsonObject.toString()


  }


 companion object {

   const val TOKEN_KEY="token"
   const val CALL_TYPE="call_type"
   const val SDP = "sdp"
   const val CONTENT_TYPE ="content_type"
   const val PARTCIPANTS_KEY="/participants/"
   const val URL_KEY="/calls"
   const val API_VERSION= "/api/client/v2/conferences/"

 }

}