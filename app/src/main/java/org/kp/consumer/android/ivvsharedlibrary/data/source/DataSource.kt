package org.kp.consumer.android.ivvsharedlibrary.data.source

import com.here.oksse.ServerSentEvent
import org.kp.consumer.android.ivvsharedlibrary.api.response.SetupConferenceResponse
import org.kp.consumer.android.ivvsharedlibrary.api.response.RequestTokenResponse
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.JoinConferenceRequestModel
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.LeaveConferenceRequestModel
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.SurveyRequestModel
import org.kp.consumer.android.ivvsharedlibrary.api.response.*
import org.kp.consumer.android.ivvsharedlibrary.api.response.VVBaseResponse
import org.kp.consumer.android.ivvsharedlibrary.model.*
import org.webrtc.SessionDescription

interface DataSource {


    fun getParticipantsRequest(
        uuid: String,
        time: String,
        source: String,
        event: String,
        callback: GetCallback<GetParticipantsResponse>
    ) {}

    fun requestToken(room: Room,displayName: String, callback: GetCallback<RequestTokenResponse>,serverSentEvent: ServerSentEvent.Listener){
    }
    fun refreshToken(room: Room,token : String, callback: GetCallback<RefreshTokenResponse>){

    }
    fun setUpConferenceRequest(token: String,callback: GetCallback<SetupConferenceResponse>){

    }
    fun addDialOutRequest(dialOut: DialOut, callback: GetCallback<DialOutResponse>) {

    }
    fun makeCall(room:Room,participantUUID: String,sdp:SessionDescription, callback: GetCallback<CallResponse>){

    }


    fun doAck(room: Room,participantUUID: String,callUUID: String,callback: GetCallback<VVBaseResponse>){

    }

    fun joinConference(joinConfRequestModel: JoinConferenceRequestModel, registrationToken: String, callback: GetCallback<JoinConferenceResponse>){
    }

    fun leaveConference(leaveConferenceRequestModel: LeaveConferenceRequestModel, registrationToken: String, callback: GetCallback<LeaveConferenceResponse>){


    }
    fun ivvRenewToken(token: String,callback: GetCallback<VVBaseResponse>){

    }

    fun getPexipUpdatedToken(): String

    fun disconnect(room: Room,
                   participantUUID: String,
                   token: String,
                   callUUID: String,
                   callback: DataSource.GetCallback<VVBaseResponse>) {}

    fun sendChatMessage(message: String, callback: GetCallback<VVBaseResponse>) {}

    fun sendSurvey(surveyRequestModel: SurveyRequestModel, callback: GetCallback<VVBaseResponse>) {}


    interface GetCallback<T>{


        fun onSuccess()

        fun onSuccess(value: T)

        fun onFailure(value: ResponseResult.ServiceError){


        }
        fun onFailure(errrorCode: ResponseResult.Failure){



        }


    }



}