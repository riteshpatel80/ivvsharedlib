package org.kp.consumer.android.ivvsharedlibrary.model


internal object PexipConfiguration {
    lateinit var hostURL: String
    lateinit var room: String
    lateinit var pin: String
}