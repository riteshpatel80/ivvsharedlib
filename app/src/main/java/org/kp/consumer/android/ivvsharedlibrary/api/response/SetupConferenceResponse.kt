package org.kp.consumer.android.ivvsharedlibrary.api.response

import com.google.gson.annotations.SerializedName
import org.kp.consumer.android.ivvsharedlibrary.model.Appointment
import org.kp.consumer.android.ivvsharedlibrary.model.Room
import org.kp.consumer.android.ivvsharedlibrary.model.SignedInUser
import org.kp.consumer.android.ivvsharedlibrary.model.WebConfig


data class SetupConferenceResponse (

    @SerializedName("appointment")
    val appointment: Appointment,

    @SerializedName("signedInUser")
    val signedInUser: SignedInUser,

    @SerializedName("room")
    val room: Room,

    @SerializedName("webConfig")
    val webConfig: WebConfig,

    @SerializedName("errors")
    override val errors: List<ResponseError>

) : VVBaseResponse