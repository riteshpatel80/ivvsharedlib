package org.kp.consumer.android.ivvsharedlibrary.api.response

import org.json.JSONObject

internal class RefreshTokenResponseConverter: Converter {
    override fun convert(responseJson: String): RefreshTokenResponse {

        var result = JSONObject(responseJson).get("result")

        return Converter.convertFromJson(result.toString())
    }
}