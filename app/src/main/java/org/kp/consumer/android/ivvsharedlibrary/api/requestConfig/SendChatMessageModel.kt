package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig

import com.google.gson.annotations.SerializedName
import java.io.Serializable

internal data class SendChatMessageModel(

    @SerializedName("payload")
    val messageBody: String,
    @SerializedName("type")
    val type: String = "text/plain"

) : Serializable