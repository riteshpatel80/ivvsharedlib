package org.kp.consumer.android.ivvsharedlibrary.api.response

import com.google.gson.annotations.SerializedName

data class CallResponse(
    @SerializedName("call_uuid")
    val callUUID: String,

    @SerializedName("sdp")
    val sdp: String?=null,

    @SerializedName("url")
    val url: String?=null,
    @SerializedName("secure_url")
    val secure_url: String,

    override val errors: List<ResponseError>?
) : VVBaseResponse {
}