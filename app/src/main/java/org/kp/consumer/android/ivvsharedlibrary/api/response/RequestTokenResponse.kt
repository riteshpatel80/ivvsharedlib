package org.kp.consumer.android.ivvsharedlibrary.api.response

import com.google.gson.annotations.SerializedName

data class RequestTokenResponse(


    @SerializedName("participant_uuid")
    val participantUUID: String,

    @SerializedName("display_name")
    val displayName: String,

    @SerializedName("token")
    val token: String,

    @SerializedName("expires")
    val expires: String,

    @SerializedName("errors")
    override val errors: List<ResponseError>

):VVBaseResponse {

    val _expires: Long
    get() = expires.toLong()

}
