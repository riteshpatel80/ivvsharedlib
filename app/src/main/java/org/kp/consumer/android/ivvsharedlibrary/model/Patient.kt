package org.kp.consumer.android.ivvsharedlibrary.model

import com.google.gson.annotations.SerializedName


data class Patient(

    @SerializedName("id")
    val id: String,

    @SerializedName("idType")
    val idType: String,

    @SerializedName("name")
    val name: String,

    //TODO make val when service is corrected
    @SerializedName("region")
    var region: String,

    @SerializedName("patientKPHCHomeInstance")
    val patientKPHCHomeInstance: String,

    @SerializedName("spokenLanguage")
    val spokenLanguage: String,

    @SerializedName("interpreterRequired")
    val interpreterRequired: Boolean = false
)