package org.kp.consumer.android.ivvsharedlibrary.api.response


sealed class ResponseResult {
    data class Success(val response: VVBaseResponse) : ResponseResult()

    data class ServiceError(val error: ResponseError) : ResponseResult()

    class Failure(val errorCode: String) : ResponseResult()
}