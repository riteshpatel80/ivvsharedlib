package org.kp.consumer.android.ivvsharedlibrary.features.participants

import android.app.Activity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.kp.consumer.android.ivvsharedlibrary.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_video.*
import kotlinx.android.synthetic.main.participants_fragment.*
import org.kp.consumer.android.ivvsharedlibrary.R
import org.kp.consumer.android.ivvsharedlibrary.ViewModelFactory
import org.kp.consumer.android.ivvsharedlibrary.databinding.ParticipantsFragmentBinding
import org.kp.consumer.android.ivvsharedlibrary.features.VideoFragment
import org.kp.consumer.android.ivvsharedlibrary.features.VideoViewModel


internal class ParticipantsFragment : Fragment() {

    private lateinit var activity: Activity
    private val participantsAdapter = ParticipantsAdapter()
    override fun onAttach(context: Context) {
        super.onAttach(context)

        activity = context as Activity
    }

    private lateinit var participantsViewModel: ParticipantsViewModel
    private lateinit var videoViewModel: VideoViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.participants_fragment, container, false)

        ParticipantsFragmentBinding.bind(view).apply {
            participantsViewModel = activity?.run {
                ViewModelProvider(
                    requireActivity(),
                    ViewModelFactory.getInstance(activity.application)
                )
                    .get(ParticipantsViewModel::class.java)

            }
            videoViewModel = activity?.run {
                ViewModelProvider(
                    requireActivity(),
                    ViewModelFactory.getInstance(activity.application)
                )
                    .get(VideoViewModel::class.java)

            }
        }
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupAdapter()

        participantsViewModel.participantItems.observe(viewLifecycleOwner, Observer { items ->

            items?.getContentIfNotHandled()?.let {participantList ->
                participantsAdapter.setData(participantList)
            }
        })

    }

    private fun setupAdapter() {
        participantListView.apply {
            adapter = participantsAdapter
            activity?.let {
                layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
                    it,
                    androidx.recyclerview.widget.RecyclerView.VERTICAL,
                    false
                )
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if(activity?.requestedOrientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
        activity?.tabs?.visibility = View.VISIBLE
    }

    fun makeParticipantsCall() {
        participantsViewModel.getParticipants()
    }

    companion object {


        val TAG = ParticipantsFragment::class.java.simpleName

    }

}
