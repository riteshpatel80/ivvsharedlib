package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig

import org.kp.consumer.android.ivvsharedlibrary.util.Log
import org.kp.kpnetworking.request.BaseRequestConfig
import org.webrtc.SessionDescription
import java.util.*

class AckRequest(pexipHost: String, roomAlias: String,participantUUID: String,token:String,callUUID: String): BaseRequestConfig(REQUEST_TYPE.POST, "$pexipHost$API_VERSION$roomAlias$PARTCIPANTS_KEY$participantUUID$URL_KEY$callUUID$ACK_KEY") {


    init {
        Log.d("AckRequest", "url = $pexipHost$API_VERSION$roomAlias$PARTCIPANTS_KEY$participantUUID$URL_KEY$callUUID$ACK_KEY");

        addHeader(TOKEN_KEY, token)
        addHeader(CONTENT_TYPE, "application/json")

        body = "{}"

    }


    companion object {

        const val TOKEN_KEY="token"
        const val CALL_TYPE="call_type"
        const val SDP = "sdp"
        const val CONTENT_TYPE ="content_type"
        const val PARTCIPANTS_KEY="/participants/"
        const val URL_KEY="/calls/"
        const val ACK_KEY="/ack"
        const val API_VERSION= "/api/client/v2/conferences/"

    }



}