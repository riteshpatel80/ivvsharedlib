package org.kp.consumer.android.ivvsharedlibrary.util;

import androidx.test.rule.ActivityTestRule;
import androidx.appcompat.app.AppCompatActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.kp.consumer.android.ivvsharedlibrary.MainTestActivity;
import org.kp.consumer.android.ivvsharedlibrary.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

public class DialogUtilsTest{


    @Rule
    public ActivityTestRule<MainTestActivity> activityRule
    = new ActivityTestRule<>(MainTestActivity.class);

    private AppCompatActivity app = null;

    @Before
    public void setUp() {
        app = activityRule.getActivity();
    }

    @Test
    public void okDialogueDisplaysGivenMessage(){
        app.runOnUiThread(() -> {
            DialogUtils.INSTANCE.showOkDialog(app, "message", false, () -> {

            });
        });
        onView(withText("message")).check(matches(isDisplayed()));
        onView(withText("OK")).check(matches(isDisplayed()));

    }


    @Test
    public void okCancelDialogueDisplaysGivenMessage(){
        app.runOnUiThread(() -> {
            DialogUtils.INSTANCE.showOkCancelDialog(app, "message", false, () -> {

            });
        });
        onView(withText("message")).check(matches(isDisplayed()));
        onView(withText("Yes")).check(matches(isDisplayed()));
        onView(withText("Cancel")).check(matches(isDisplayed()));

    }

    @Test
    public void okCancelDialogueDisplaysGivenResString(){
        app.runOnUiThread(() -> {
            DialogUtils.INSTANCE.showOkCancelDialog(app, R.string.app_name, false, () -> {

            });
        });
        onView(withText(app.getResources().getString(R.string.app_name))).check(matches(isDisplayed()));
        onView(withText("Yes")).check(matches(isDisplayed()));
        onView(withText("Cancel")).check(matches(isDisplayed()));

    }

    @Test
    public void okDialogueDisplaysGivenResString(){
        app.runOnUiThread(() -> {
            DialogUtils.INSTANCE.showOkDialog(app, R.string.app_name,  false, () -> {

            });
        });
        onView(withText(app.getResources().getString(R.string.app_name))).check(matches(isDisplayed()));
        onView(withText("OK")).check(matches(isDisplayed()));

    }
}