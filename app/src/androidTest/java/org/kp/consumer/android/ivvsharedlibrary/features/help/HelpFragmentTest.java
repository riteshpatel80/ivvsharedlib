package org.kp.consumer.android.ivvsharedlibrary.features.help;

import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kp.consumer.android.ivvsharedlibrary.MainTestActivity;
import org.kp.consumer.android.ivvsharedlibrary.test.R;

import java.util.concurrent.TimeUnit;

import utils.ViewUtils;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.matcher.ViewMatchers.isRoot;

@RunWith(AndroidJUnit4.class)
public class HelpFragmentTest {

    @Rule
    public ActivityTestRule<MainTestActivity> activityRule
            = new ActivityTestRule<>(MainTestActivity.class);

    @Test
    public void verifyTheHelpFragmentOpens() {
        HelpFragment frag = new HelpFragment();
        activityRule.getActivity().getSupportFragmentManager()
                .beginTransaction().add(R.id.test_fragment, frag, "tag").commit();
        onView(isRoot()).perform(ViewUtils.waitId(R.id.test_fragment, TimeUnit.SECONDS.toMillis(1)));
        Assert.assertNotNull(activityRule.getActivity().getSupportFragmentManager().findFragmentByTag("tag"));

    }
}