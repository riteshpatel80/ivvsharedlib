package org.kp.consumer.android.ivvsharedlibrary.util;

import org.kp.consumer.android.ivvsharedlibrary.model.Participant;

public class CustomObject {
    public static Participant getParticipant(String name, String role, Boolean isHost) {
        return new Participant(name, "id", "idType", "uuid", role, "vrmRole", isHost, "region", "applicationId", "channelId", "callDirection", "callQuality", false, false, false, false);
    }
}