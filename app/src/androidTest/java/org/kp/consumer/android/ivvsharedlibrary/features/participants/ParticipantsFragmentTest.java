package org.kp.consumer.android.ivvsharedlibrary.features.participants;

import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.appcompat.app.AppCompatActivity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kp.consumer.android.ivvsharedlibrary.MainTestActivity;

import java.util.concurrent.TimeUnit;

import utils.ViewUtils;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.matcher.ViewMatchers.isRoot;

@RunWith(AndroidJUnit4.class)
public class ParticipantsFragmentTest{


    @Rule
    public ActivityTestRule<MainTestActivity> activityRule
            = new ActivityTestRule<>(MainTestActivity.class);

    private AppCompatActivity app = null;

    @Before
    public void setUp() {
        app = activityRule.getActivity();
    }

    @Test
    public void verifyTheParticipantFragmentOpens() {
        ParticipantsFragment frag = new ParticipantsFragment();
        Assert.assertNotNull(frag);
        app.getSupportFragmentManager()
                .beginTransaction().add(org.kp.consumer.android.ivvsharedlibrary.test.R.id.test_fragment, frag, "tag").commit();
        onView(isRoot()).perform(ViewUtils.waitId(org.kp.consumer.android.ivvsharedlibrary.test.R.id.test_fragment, TimeUnit.SECONDS.toMillis(1)));
        Assert.assertNotNull(app.getSupportFragmentManager().findFragmentByTag("tag"));
    }
}