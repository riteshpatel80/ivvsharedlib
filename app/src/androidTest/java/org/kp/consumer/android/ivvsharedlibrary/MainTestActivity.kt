package org.kp.consumer.android.ivvsharedlibrary

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import org.kp.consumer.android.ivvsharedlibrary.test.R

class MainTestActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_test)
    }
}
