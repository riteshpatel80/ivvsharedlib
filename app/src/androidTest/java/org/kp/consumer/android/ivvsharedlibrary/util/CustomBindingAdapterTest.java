package org.kp.consumer.android.ivvsharedlibrary.util;

import androidx.test.rule.ActivityTestRule;
import androidx.appcompat.app.AppCompatActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.kp.consumer.android.ivvsharedlibrary.MainTestActivity;
import org.kp.consumer.android.ivvsharedlibrary.R;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class CustomBindingAdapterTest {


    @Rule
    public ActivityTestRule<MainTestActivity> activityRule
            = new ActivityTestRule<>(MainTestActivity.class);

    private AppCompatActivity app = null;

    private View view = null;

    @Before
    public void setUp() {
        app = activityRule.getActivity();
        view = LayoutInflater.from(app).inflate(R.layout.participant_item, null);
    }

    @Test
    public void testView() {
        assertNotNull(view.findViewById(R.id.role_participants));
    }

    @Test
    public void testViewVisibilityIsTrue() {
        CustomBindingAdapter.sethost(view.findViewById(R.id.participantName), true);
        assertEquals(view.findViewById(R.id.participantName).getVisibility(), View.VISIBLE);
    }

    @Test
    public void testViewVisibilityIsFalse() {
        CustomBindingAdapter.sethost(view.findViewById(R.id.participantName), false);
        assertEquals(view.findViewById(R.id.participantName).getVisibility(), View.GONE);
    }

    @Test
    public void testViewVTimeFormattingMember() {
        CustomBindingAdapter.setDateTime(true, view.findViewById(R.id.participantName), "2020-01-06T15:22:00-0800");
        assertEquals(((TextView) view.findViewById(R.id.participantName)).getText(), "Your appointment is today at 3:22PM");
    }

    @Test
    public void testViewVTimeFormattingNonMember() {
        CustomBindingAdapter.setDateTime(false, view.findViewById(R.id.participantName), "2020-01-06T15:22:00-0800");
        assertEquals(((TextView) view.findViewById(R.id.participantName)).getText(), "This appointment is today at 3:22PM");
    }

    @Test
    public void testViewVDescriptionAndImage() {
        CustomBindingAdapter.setRoleAvatar(view.findViewById(R.id.role_participants), CustomObject.getParticipant("", "PROVIDER", true));
        assertEquals((view.findViewById(R.id.role_participants)).getTag(R.id.image_resource_id), R.drawable.participants_host);
        CustomBindingAdapter.setRoleAvatar(view.findViewById(R.id.role_participants), CustomObject.getParticipant("", "PROVIDER", false));
        assertEquals((view.findViewById(R.id.role_participants)).getTag(R.id.image_resource_id), R.drawable.doctor_icon);
        CustomBindingAdapter.setRoleAvatar(view.findViewById(R.id.role_participants), CustomObject.getParticipant("", "PATIENT", false));
        assertEquals((view.findViewById(R.id.role_participants)).getTag(R.id.image_resource_id), R.drawable.primary_patient);
        CustomBindingAdapter.setRoleAvatar(view.findViewById(R.id.role_participants), CustomObject.getParticipant("", "CONSULTING_CLINICIAN", false));
        assertEquals((view.findViewById(R.id.role_participants)).getTag(R.id.image_resource_id), R.drawable.doctor_icon);
        CustomBindingAdapter.setRoleAvatar(view.findViewById(R.id.role_participants), CustomObject.getParticipant("", "DIAL_OUT", false));
        assertEquals((view.findViewById(R.id.role_participants)).getTag(R.id.image_resource_id), R.drawable.call_in_user);
        CustomBindingAdapter.setRoleAvatar(view.findViewById(R.id.role_participants), CustomObject.getParticipant("", "DIAL_IN", false));
        assertEquals((view.findViewById(R.id.role_participants)).getTag(R.id.image_resource_id), R.drawable.call_in_user);
        CustomBindingAdapter.setRoleAvatar(view.findViewById(R.id.role_participants), CustomObject.getParticipant("", "INTERPRETER", false));
        assertEquals((view.findViewById(R.id.role_participants)).getTag(R.id.image_resource_id), R.drawable.interpreter);
        CustomBindingAdapter.setRoleAvatar(view.findViewById(R.id.role_participants), CustomObject.getParticipant("", "PROVIDER_NURSE", false));
        assertEquals((view.findViewById(R.id.role_participants)).getTag(R.id.image_resource_id), R.drawable.doctor_icon);
        CustomBindingAdapter.setRoleAvatar(view.findViewById(R.id.role_participants), CustomObject.getParticipant("", "MEMBER_PROXY", false));
        assertEquals((view.findViewById(R.id.role_participants)).getTag(R.id.image_resource_id), R.drawable.secondary_patient);
        CustomBindingAdapter.setRoleAvatar(view.findViewById(R.id.role_participants), CustomObject.getParticipant("", "", false));
        assertEquals((view.findViewById(R.id.role_participants)).getTag(R.id.image_resource_id), R.drawable.secondary_patient);
    }

    @Test
    public void testViewNameDialOut() {
        CustomBindingAdapter.setName(view.findViewById(R.id.participantName), CustomObject.getParticipant("", Role.DIAL_OUT.getRole(), false));
        assertEquals(((TextView) view.findViewById(R.id.participantName)).getText(), "Call-In User");
    }

    @Test
    public void testViewNameInterpreter() {
        CustomBindingAdapter.setName(view.findViewById(R.id.participantName), CustomObject.getParticipant(Role.INTERPRETER.getRole(), "", false));
        assertEquals(((TextView) view.findViewById(R.id.participantName)).getText(), "Interpreter");
    }
}