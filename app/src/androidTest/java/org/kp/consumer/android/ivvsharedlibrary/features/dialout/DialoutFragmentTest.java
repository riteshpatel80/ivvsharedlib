package org.kp.consumer.android.ivvsharedlibrary.features.dialout;

import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.appcompat.app.AppCompatActivity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kp.consumer.android.ivvsharedlibrary.MainTestActivity;
import org.kp.consumer.android.ivvsharedlibrary.test.R;

import java.util.concurrent.TimeUnit;

import utils.ViewUtils;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isRoot;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class DialoutFragmentTest {

    @Rule
    public ActivityTestRule<MainTestActivity> activityRule
            = new ActivityTestRule<>(MainTestActivity.class);

    private AppCompatActivity app = null;

    @Before
    public void setUp() {
        app = activityRule.getActivity();
    }

    @Test
    public void verifyTheDialoutFragmentOpens() {
        DialoutFragment frag = new DialoutFragment();
        Assert.assertNotNull(frag);
        app.getSupportFragmentManager()
                .beginTransaction().add(R.id.test_fragment, frag, "tag").commit();
        onView(isRoot()).perform(ViewUtils.waitId(R.id.test_fragment, TimeUnit.SECONDS.toMillis(1)));
        Assert.assertNotNull(app.getSupportFragmentManager().findFragmentByTag("tag"));
    }

    @Test
    public void verifyTheEditTextFormatsNumber() {
        DialoutFragment frag = new DialoutFragment();
        Assert.assertNotNull(frag);
        app.getSupportFragmentManager()
                .beginTransaction().add(R.id.test_fragment, frag, "tag").commit();
        onView(isRoot()).perform(ViewUtils.waitId(R.id.test_fragment, TimeUnit.SECONDS.toMillis(1)));
        Assert.assertNotNull(app.getSupportFragmentManager().findFragmentByTag("tag"));
        onView(withId(R.id.add_number)).perform(typeText("5555555555"));
        onView(withText("(555)555-5555")).check(matches(isDisplayed()));
    }

}
