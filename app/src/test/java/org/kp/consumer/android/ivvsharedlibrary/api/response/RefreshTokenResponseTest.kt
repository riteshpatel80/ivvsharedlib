package org.kp.consumer.android.ivvsharedlibrary.api.response

import org.junit.Assert.*
import org.junit.Test

class RefreshTokenResponseTest{

    @Test
    fun `verify values are assigned correctly`(){
        val model = RefreshTokenResponse("displayName","token","111" , listOf())
        assertEquals(model.displayName,"displayName")
        assertEquals(model.token,"token")
        assertEquals(model.expires,"111")
        assertEquals(model._expires,111)
        assertEquals(model.errors.size,0 )
    }
}