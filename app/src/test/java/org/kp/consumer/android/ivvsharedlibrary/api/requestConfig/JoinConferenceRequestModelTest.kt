package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig

import org.junit.Assert.*
import org.junit.Test

class JoinConferenceRequestModelTest{

    @Test
    fun `verify values are assigned correctly`(){
        val model = JoinConferenceRequestModel("pexipParticipantUUID")
        assertEquals(model.pexipParticipantUUID,"pexipParticipantUUID")
    }
}