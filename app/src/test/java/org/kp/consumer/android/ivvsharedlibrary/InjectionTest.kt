package org.kp.consumer.android.ivvsharedlibrary

import android.content.Context
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.kp.consumer.android.ivvsharedlibrary.data.source.Repository
import org.mockito.Mockito.mock

@RunWith(JUnit4::class)
class InjectionTest{

    @Test
    fun `return repo`(){
        var context = mock(Context::class.java)
        assertTrue(Injection.provideTasksRepository(context)::class == Repository::class)
    }
}