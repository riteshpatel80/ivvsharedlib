package org.kp.consumer.android.ivvsharedlibrary.error

import org.junit.Assert.*
import org.junit.Test

class VVErrorTest {
    @Test
    fun `verify values are assigned correctly`() {
        val vvError = VVError("code","title", "description")
        assertEquals(vvError.code,"code")
        assertEquals(vvError.title,"title")
        assertEquals(vvError.description,"description")
    }

}