package org.kp.consumer.android.ivvsharedlibrary.model

import org.junit.Assert.*
import org.junit.Test

class InviteeTest{

    @Test
    fun `verify values are assigned correctly`(){
        val invitee : Invitee = Invitee(
            "name" ,
            "notificationId" ,
            "notificationType")
        assertEquals(invitee.name,"name")
        assertEquals(invitee.notificationId,"notificationId")
        assertEquals(invitee.notificationType,"notificationType")
    }
}