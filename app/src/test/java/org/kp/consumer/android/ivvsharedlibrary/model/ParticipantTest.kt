package org.kp.consumer.android.ivvsharedlibrary.model

import org.junit.Assert.*
import org.junit.Test

class ParticipantTest{

    @Test
    fun `verify values are assigned correctly`(){
        val participant = Participant(
            "name" ,
            "id",
            "idType",
            "uuid",
            "role",
            "chair",
            true,
            "region",
            "applicationId",
            "channelId",
            "callDirection",
            "callQuality",
            false,
            true,
            false,
            true
        )
        assertEquals(participant.name,"name")
        assertEquals(participant.id,"id")
        assertEquals(participant.idType,"idType")
        assertEquals(participant.uuid,"uuid")
        assertEquals(participant.role,"role")
        assertEquals(participant.vmrRole,"chair")
        assertEquals(participant.host,true)
        assertEquals(participant.region,"region")
        assertEquals(participant.applicationId,"applicationId")
        assertEquals(participant.channelId,"channelId")
        assertEquals(participant.callDirection,"callDirection")
        assertEquals(participant.callQuality,"callQuality")
        assertEquals(participant.hasMedia,false)
        assertEquals(participant.isMuted,true)
        assertEquals(participant.isOnHold,false)
        assertEquals(participant.isStreaming,true)
    }
}