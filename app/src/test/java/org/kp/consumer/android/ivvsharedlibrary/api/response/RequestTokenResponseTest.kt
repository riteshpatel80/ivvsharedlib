package org.kp.consumer.android.ivvsharedlibrary.api.response

import org.junit.Assert.*
import org.junit.Test

class RequestTokenResponseTest{

    @Test
    fun `verify values are assigned correctly`(){
        val model = RequestTokenResponse("participantUUID" ,"displayName" ,"token" ,"111" ,listOf())
        assertEquals(model.participantUUID,"participantUUID")
        assertEquals(model.displayName,"displayName")
        assertEquals(model.token,"token")
        assertEquals(model.expires,"111")
        assertEquals(model._expires,111)
        assertEquals(model.errors.size,0)
    }
}