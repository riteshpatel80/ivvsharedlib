package org.kp.consumer.android.ivvsharedlibrary.model

import org.junit.Assert.*
import org.junit.Test

class ChatMessageModelTest{

    @Test
    fun `verify values are assigned correctly`(){
        val chatMessageModel : ChatMessageModel = ChatMessageModel(
             "messageBody" ,
             "type" ,
             "origin" ,
             "senderUUID",
            false  ,
            1111111111111111,
            1 )
        assertEquals(chatMessageModel.messageBody,"messageBody")
        assertEquals(chatMessageModel.type,"type")
        assertEquals(chatMessageModel.origin,"origin")
        assertEquals(chatMessageModel.senderUUID,"senderUUID")
        assertEquals(chatMessageModel.isReceivedMessage,false)
        assertEquals(chatMessageModel.timestamp,1111111111111111)
        assertEquals(chatMessageModel.roleIconResourceId,1)
    }
}