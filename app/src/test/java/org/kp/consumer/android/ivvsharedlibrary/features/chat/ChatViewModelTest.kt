package org.kp.consumer.android.ivvsharedlibrary.features.chat

import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.kp.consumer.android.ivvsharedlibrary.data.source.Repository
import org.kp.consumer.android.ivvsharedlibrary.model.Participant
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class ChatViewModelTest {

    private lateinit var chatViewModel: ChatViewModel

    @Mock
    lateinit var repository: Repository

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        chatViewModel = ChatViewModel(repository)
    }

    @Test
    fun `given a participant list without a dial-in, dial-out, or interpreter role, should return false`() {
        val list = mutableListOf(chairParticipant)
        assertFalse(chatViewModel.shouldDisplayCallInBanner(list))
    }

    @Test
    fun `given a participant list with a dial-in role, should return true`() {
        val list = mutableListOf(dialInParticipant)
        assertTrue(chatViewModel.shouldDisplayCallInBanner(list))
    }

    @Test
    fun `given a participant list with a dial-out role, should return true`() {
        val list = mutableListOf(dialOutParticipant, chairParticipant)
        assertTrue(chatViewModel.shouldDisplayCallInBanner(list))
    }

    @Test
    fun `given a participant list with an interpreter role, should return true`() {
        val list = mutableListOf(interpreterParticipant, chairParticipant)
        assertTrue(chatViewModel.shouldDisplayCallInBanner(list))
    }

    @Test
    fun `given a participant list with an interpreter or dial-in role, should return true`() {
        val list = mutableListOf(interpreterParticipant, dialInParticipant, chairParticipant)
        assertTrue(chatViewModel.shouldDisplayCallInBanner(list))
    }

    val chairParticipant = Participant(
        name = "Sir Chair",
        id = "",
        idType = "",
        uuid = "",
        role = "",
        vmrRole = "chair",
        host = false,
        region = "",
        applicationId = "",
        channelId = "",
        callDirection = "",
        callQuality = "",
        hasMedia = false,
        isMuted = false,
        isOnHold = false,
        isStreaming = false
    )

    val dialInParticipant = Participant(
        name = "Dial in person",
        id = "",
        idType = "",
        uuid = "",
        role = "DIAL_IN",
        vmrRole = "",
        host = false,
        region = "",
        applicationId = "",
        channelId = "",
        callDirection = "",
        callQuality = "",
        hasMedia = false,
        isMuted = false,
        isOnHold = false,
        isStreaming = false
    )

    val dialOutParticipant = Participant(
        name = "Dial out individual",
        id = "",
        idType = "",
        uuid = "",
        role = "DIAL_OUT",
        vmrRole = "",
        host = false,
        region = "",
        applicationId = "",
        channelId = "",
        callDirection = "",
        callQuality = "",
        hasMedia = false,
        isMuted = false,
        isOnHold = false,
        isStreaming = false
    )

    val interpreterParticipant = Participant(
        name = "Great interpreter",
        id = "",
        idType = "",
        uuid = "",
        role = "INTERPRETER",
        vmrRole = "",
        host = false,
        region = "",
        applicationId = "",
        channelId = "",
        callDirection = "",
        callQuality = "",
        hasMedia = false,
        isMuted = false,
        isOnHold = false,
        isStreaming = false
    )
}