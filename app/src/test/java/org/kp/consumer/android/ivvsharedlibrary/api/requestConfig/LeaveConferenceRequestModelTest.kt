package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig

import org.junit.Assert.*
import org.junit.Test

class LeaveConferenceRequestModelTest{

    @Test
    fun `verify values are assigned correctly`(){
        val model = LeaveConferenceRequestModel(true,"conferenceStatusRole" ,false)
        assertEquals(model.postConferenceStatus,true)
        assertEquals(model.conferenceStatusRole,"conferenceStatusRole")
        assertEquals(model.endConference,false)
    }
}