package org.kp.consumer.android.ivvsharedlibrary.model

import org.junit.Assert.*
import org.junit.Test

class VVClientInfoTest{

    val vvClientInfo : VVClientInfo = VVClientInfo(
        "appName" ,
        "appVersion"
    )

    @Test
    fun `verify values are assigned correctly`(){
        assertEquals(vvClientInfo.appName,"appName")
        assertEquals(vvClientInfo.appVersion,"appVersion")
    }

    @Test

    fun `verify return of function concatenates given strings`(){
        assertEquals(vvClientInfo.appNameAndVersion,"appName appVersion" )
    }
}