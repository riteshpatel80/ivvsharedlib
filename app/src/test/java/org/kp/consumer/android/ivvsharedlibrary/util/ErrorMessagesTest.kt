package org.kp.consumer.android.ivvsharedlibrary.util

import org.junit.Assert.*
import org.junit.Test
import org.kp.consumer.android.ivvsharedlibrary.R

class ErrorMessagesTest{

    @Test
    fun `error codes return proper error strings`(){
        assertEquals(ErrorMessages.getErrorMessages(2000), R.string.kp_unable_to_register)
        assertEquals(ErrorMessages.getErrorMessages(2001), R.string.kp_visit_ended)
        assertEquals(ErrorMessages.getErrorMessages(2002), R.string.kp_unable_to_register)
        assertEquals(ErrorMessages.getErrorMessages(2003), R.string.kp_visit_ended)
        assertEquals(ErrorMessages.getErrorMessages(2004), R.string.kp_visit_ended)
        assertEquals(ErrorMessages.getErrorMessages(2010), R.string.kp_unable_to_register)
        assertEquals(ErrorMessages.getErrorMessages(2011), R.string.kp_system_error_2)
        assertEquals(ErrorMessages.getErrorMessages(2012), R.string.kp_system_error_2)
        assertEquals(ErrorMessages.getErrorMessages(2020), R.string.kp_system_error_2)
        assertEquals(ErrorMessages.getErrorMessages(2021), R.string.kp_system_error_2)
        assertEquals(ErrorMessages.getErrorMessages(2022), R.string.kp_system_error_2)
        assertEquals(ErrorMessages.getErrorMessages(2030), R.string.kp_system_error_2)
        assertEquals(ErrorMessages.getErrorMessages(2040), R.string.kp_system_error_2)
        assertEquals(ErrorMessages.getErrorMessages(2050), R.string.kp_unable_to_dialout_the_number)
        assertEquals(ErrorMessages.getErrorMessages(2060), R.string.kp_unable_to_discover_participant)
        assertEquals(ErrorMessages.getErrorMessages(2070), R.string.kp_unable_to_invite_user)
        assertEquals(ErrorMessages.getErrorMessages(2071), R.string.kp_system_error_2)
        assertEquals(ErrorMessages.getErrorMessages(2080), R.string.kp_unable_to_invite_user)
        assertEquals(ErrorMessages.getErrorMessages(2081), R.string.kp_system_error_2)
        assertEquals(ErrorMessages.getErrorMessages(2082), R.string.kp_provider_not_registered_in_region)
        assertEquals(ErrorMessages.getErrorMessages(2090), R.string.kp_unable_to_transfer_host)
        assertEquals(ErrorMessages.getErrorMessages(2100), R.string.kp_unable_to_acquire_host)
        assertEquals(ErrorMessages.getErrorMessages(2101), R.string.kp_role_not_allowed_to_acquire_host)
        assertEquals(ErrorMessages.getErrorMessages(2110), R.string.kp_unable_to_get_clinician_invite_Status)
        assertEquals(ErrorMessages.getErrorMessages(-1), R.string.kp_system_error_2)
    }
}