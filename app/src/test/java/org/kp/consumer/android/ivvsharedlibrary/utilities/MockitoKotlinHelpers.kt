/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.kp.consumer.android.ivvsharedlibrary.utilities

/**
 * Helper functions that are workarounds to Runtime Exceptions when using kotlin.
 */

import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.http.Fault
import org.mockito.ArgumentCaptor
import org.mockito.Mockito

/**
 * Returns Mockito.eq() as nullable type to avoid java.lang.IllegalStateException when
 * null is returned.
 *
 * Generic T is nullable because implicitly bound by `Any?`.
 */
fun <T> eq(obj: T): T = Mockito.eq<T>(obj)


fun <T : Any> safeEq(value: T): T = eq(value) ?: value


/**
 * Returns Mockito.any() as nullable type to avoid java.lang.IllegalStateException when
 * null is returned.
 */
fun <T> any(): T = Mockito.any<T>()

fun <T> any(type: Class<T>): T = Mockito.any<T>(type)
/**
 * Returns ArgumentCaptor.capture() as nullable type to avoid java.lang.IllegalStateException
 * when null is returned.
 */
fun <T> capture(argumentCaptor: ArgumentCaptor<T>): T = argumentCaptor.capture()

/**
 * Helper function for creating an argumentCaptor in kotlin.
 */
inline fun <reified T : Any> argumentCaptor(): ArgumentCaptor<T> =
        ArgumentCaptor.forClass(T::class.java)

inline fun <reified T : Any> mock(): T = Mockito.mock(T::class.java)!!


fun makePostStubFor(urlRegex: String, response:String){

        WireMock.stubFor(
                WireMock.post(WireMock.urlPathMatching(urlRegex))
                        .willReturn(
                                WireMock.aResponse()
                                        .withStatus(200)
                                        .withBody(response)
                        )
        )
}
fun makeGetStubFor(urlRegex: String, response:String){

        WireMock.stubFor(
                WireMock.get(WireMock.urlPathMatching(urlRegex))
                        .willReturn(
                                WireMock.aResponse()
                                        .withStatus(200)
                                        .withBody(response)
                        )
        )
}
fun makePutStubFor(urlRegex: String, response:String){

        WireMock.stubFor(
                WireMock.put(WireMock.urlPathMatching(urlRegex))
                        .willReturn(
                                WireMock.aResponse()
                                        .withStatus(200)
                                        .withBody(response)
                        )
        )
}


fun makeFailPostStubFor( urlRegex: String){

        WireMock.stubFor(
                WireMock.post(WireMock.urlPathMatching(urlRegex))
                        .willReturn(
                                WireMock.aResponse()
                                        .withStatus(200)
                                        .withFault(Fault.MALFORMED_RESPONSE_CHUNK)
                        )
        )
}
fun makeFailGetStubFor( urlRegex: String){

        WireMock.stubFor(
                WireMock.get(WireMock.urlPathMatching(urlRegex))
                        .willReturn(
                                WireMock.aResponse()
                                        .withStatus(200)
                                        .withFault(Fault.MALFORMED_RESPONSE_CHUNK)
                        )
        )
}
fun makeFailPutStubFor( urlRegex: String){

        WireMock.stubFor(
                WireMock.put(WireMock.urlPathMatching(urlRegex))
                        .willReturn(
                                WireMock.aResponse()
                                        .withStatus(200)
                                        .withFault(Fault.MALFORMED_RESPONSE_CHUNK)
                        )
        )
}