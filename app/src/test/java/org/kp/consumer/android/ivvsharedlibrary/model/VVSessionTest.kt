package org.kp.consumer.android.ivvsharedlibrary.model

import org.junit.Assert.*
import org.junit.Test

class VVSessionTest{

    @Test
    fun `verify values are assigned correctly`(){
        val vvSession : VVSession = VVSession("token")
        assertEquals(vvSession.token,"token")
    }
}