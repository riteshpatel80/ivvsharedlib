package org.kp.consumer.android.ivvsharedlibrary.model

import org.junit.Assert.*
import org.junit.Test

class AppointmentTest{
    @Test
    fun `verify values are assigned correctly`(){
        val appointment :Appointment = Appointment(
            "id",
            "idType",
            "apptDateTime",
            "visitType",
            "duration",
            "durationUOM",
            "region",
            "meetingRoomNumber",
            Patient("id",
                "idType" ,
                "name"  ,
                "region" ,
                "patientKPHCHomeInstance",
                "spokenLanguage",
                 false),
            listOf(),
            "facilityId" ,
            "departmentId" ,
            "clinicId")

        assertEquals(appointment.id, "id")
        assertEquals(appointment.idType, "idType")
        assertEquals(appointment.apptDateTime, "apptDateTime")
        assertEquals(appointment.visitType, "visitType")
        assertEquals(appointment.duration, "duration")
        assertEquals(appointment.durationUOM, "durationUOM")
        assertEquals(appointment.region, "region")
        assertEquals(appointment.meetingRoomNumber, "meetingRoomNumber")
        assertEquals(appointment.patient.id, "id")
        assertEquals(appointment.patient.idType, "idType")
        assertEquals(appointment.patient.name, "name")
        assertEquals(appointment.patient.region, "region")
        assertEquals(appointment.patient.patientKPHCHomeInstance, "patientKPHCHomeInstance")
        assertEquals(appointment.patient.spokenLanguage, "spokenLanguage")
        assertEquals(appointment.patient.interpreterRequired, false)
        assertEquals(appointment.providerList.size,0)
        assertEquals(appointment.facilityId, "facilityId")
        assertEquals(appointment.departmentId, "departmentId")
        assertEquals(appointment.clinicId, "clinicId")
    }
}