package org.kp.consumer.android.ivvsharedlibrary.model

import org.junit.Assert.*
import org.junit.Test

class RoomTest{

    @Test
    fun `verify values are assigned correctly`(){
        val room : Room = Room(
            "room" ,
            "alias" ,
            "pin",
            "pexipNode")
        assertEquals(room.room,"room")
        assertEquals(room.alias,"alias")
        assertEquals(room.pin,"pin")
        assertEquals(room.pexipNode,"pexipNode")
    }
}