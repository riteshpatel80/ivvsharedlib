package org.kp.consumer.android.ivvsharedlibrary.util

import org.kp.consumer.android.ivvsharedlibrary.api.response.*
import org.kp.consumer.android.ivvsharedlibrary.model.DialOut
import org.kp.consumer.android.ivvsharedlibrary.utilities.FileUtils

/**
 *   This class contains mock responses
 *
 * Created by venkatakalluri on 2020-02-05.
 * Copyright © 2019 Kaiser Permanente. All rights reserved.
 */
class GetMockResponse {

    fun getSetUpConferenceResponse(fileName: String? = "/SetupConferenceResponse.json"): SetupConferenceResponse {
        val setupConferenceResponseConverter = SetupConferenceResponseConverter()
        return setupConferenceResponseConverter.convert(getSetUpConferenceResponseString(fileName))
    }

    fun getSetUpConferenceResponseString(fileName: String? = "/SetupConferenceResponse.json" ): String {
        return FileUtils.readFromResources(fileName!!)
    }


    fun getPexipRequestTokenResponse(): RequestTokenResponse {

        val pexipRequestTokenConverter = RequestTokenResponseConverter()

        return pexipRequestTokenConverter.convert(getPexipRequestTokenResponeInString())

    }

    fun getPexipRequestTokenResponeInString(): String =
        FileUtils.readFromResources("/PexipRequestTokenResponse.json")


    fun getCallResponse(): CallResponse {
        val callResponseConverter = CallResponseConverter()
        return callResponseConverter.convert(getCallResponseInString())

    }

    fun getCallResponseInString(): String {
        return FileUtils.readFromResources("/CallResponse.json")
    }

    fun getParticipantsResponseString(): String {
       return FileUtils.readFromResources("/ParticipantsResponse.json")

    }
    fun getParticipantsResponse(): GetParticipantsResponse {
        val participantsResponseConverter = GetParticipantsResponseConverter()
       return participantsResponseConverter.convert(FileUtils.readFromResources("/ParticipantsResponse.json"))

    }

    fun getServiceErrorResponseString(): String {
       return FileUtils.readFromResources("/ServiceError.json")

    }
    fun getServiceErrorResultResponseString(): String {
       return FileUtils.readFromResources("/ServiceErrorResult.json")

    }
    fun getDialOutResponse(): DialOutResponse{
        val dialOutResponseConverter = DialOutResponseConverter()
        return dialOutResponseConverter.convert(getDialOutResponseInString())
    }

    fun getDialOutResponseInString(): String = FileUtils.readFromResources("/DialOutResponse.json")

}