package org.kp.consumer.android.ivvsharedlibrary.data.source

import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mockito.*

class RepositoryTest {

    @Test
    fun `repository should make setup request from the datasource`() {
        val repository = Repository(dataSource)
        repository.setUpConferenceRequest("token", setupCallback)
        verify(dataSource, atLeastOnce()).setUpConferenceRequest("token", setupCallback)
    }

    @Test
    fun `repository should make token request from the datasource`() {
        val repository = Repository(dataSource)
        repository.requestToken(room, "displayName", tokenCallback, serverSentLister)
        verify(dataSource, atLeastOnce()).requestToken(
            room,
            "displayName",
            tokenCallback,
            serverSentLister
        )
    }

    @Test
    fun `repository should make refresh token request from the datasource`() {
        val repository = Repository(dataSource)
        repository.refreshToken(room, "token", refreshCallback)
        verify(dataSource, atLeastOnce()).refreshToken(room, "token", refreshCallback)
    }

    @Test
    fun `repository should make call request from the datasource`() {
        val repository = Repository(dataSource)
        repository.makeCall(room, "participantUUID", sessionDescription, callResponseCallback)
        verify(dataSource, atLeastOnce()).makeCall(
            room,
            "participantUUID",
            sessionDescription,
            callResponseCallback
        )
    }

    @Test
    fun `repository should make addDialOutRequest request from the datasource`() {
        val repository = Repository(dataSource)
        repository.addDialOutRequest(dialout, dialoutCallback)
        verify(dataSource, atLeastOnce()).addDialOutRequest(dialout, dialoutCallback)
    }

    @Test
    fun `repository should make doAck request from the datasource`() {
        val repository = Repository(dataSource)
        repository.doAck(
            room, "participantUUID"
            , "callUUID", baseCallBack
        )
        verify(dataSource, atLeastOnce()).doAck(
            room, "participantUUID"
            , "callUUID", baseCallBack
        )
    }

    @Test
    fun `repository should make getParticipantsRequest request from the datasource`() {
        val repository = Repository(dataSource)
        repository.getParticipantsRequest("0","0","0","0", participantsCallback)
        verify(dataSource, atLeastOnce()).getParticipantsRequest("0","0","0","0", participantsCallback)
    }

    @Test
    fun `repository should make joinConference request from the datasource`() {
        val repository = Repository(dataSource)
        repository.joinConference(joinModel, "registrationToken", joinCallback)
        verify(dataSource, atLeastOnce()).joinConference(
            joinModel,
            "registrationToken",
            joinCallback
        )
    }

    @Test
    fun `repository should make leaveConference request from the datasource`() {
        val repository = Repository(dataSource)
        repository.leaveConference(leaveModel, "registrationToken", leaveCallback)
        verify(dataSource, atLeastOnce()).leaveConference(
            leaveModel,
            "registrationToken",
            leaveCallback
        )
    }

    @Test
    fun `repository should make sendChatMessage request from the datasource`() {
        val repository = Repository(dataSource)
        repository.sendChatMessage("message", baseCallBack)
        verify(dataSource, atLeastOnce()).sendChatMessage("message", baseCallBack)
    }

    @Test
    fun `repository should make ivvRenewToken request from the datasource`() {
        val repository = Repository(dataSource)
        repository.ivvRenewToken("token", baseCallBack)
        verify(dataSource, atLeastOnce()).ivvRenewToken("token", baseCallBack)
    }

    @Test
    fun `repository should make disconnect request from the datasource`() {
        val repository = Repository(dataSource)
        repository.disconnect(room, "participantUUID", "token", "callUUID", baseCallBack)
        verify(dataSource, atLeastOnce()).disconnect(
            room,
            "participantUUID",
            "token",
            "callUUID",
            baseCallBack
        )
    }

    @Test
    fun `repository should make pexip token request from the datasource`() {
        val repository = Repository(dataSource)
        `when`(dataSource.getPexipUpdatedToken()).thenReturn("token")
        val token = repository.getPexipUpdatedToken()
        verify(dataSource, atLeastOnce()).getPexipUpdatedToken()
        assertEquals(token, "token")
    }

    @Test
    fun `repository should recreates object itself`(){
        val repo = Repository.getInstance(dataSource)
        assertEquals(Repository.getInstance(dataSource),repo)
        Repository.destroyInstance()
        assertNotEquals(Repository.getInstance(dataSource),repo)
    }
}