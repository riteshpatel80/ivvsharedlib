package org.kp.consumer.android.ivvsharedlibrary.model

import org.junit.Assert.*
import org.junit.Test

class PatientTest{

    @Test
    fun `verify values are assigned correctly`(){
        val patient: Patient = Patient("id",
            "idType" ,
            "name"  ,
            "region" ,
            "patientKPHCHomeInstance",
            "spokenLanguage",
            true)
        assertEquals(patient.id, "id")
        assertEquals(patient.idType, "idType")
        assertEquals(patient.name, "name")
        assertEquals(patient.region, "region")
        assertEquals(patient.patientKPHCHomeInstance, "patientKPHCHomeInstance")
        assertEquals(patient.spokenLanguage, "spokenLanguage")
        assertEquals(patient.interpreterRequired, true)
    }
}