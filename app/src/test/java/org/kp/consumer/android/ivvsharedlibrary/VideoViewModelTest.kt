package org.kp.consumer.android.ivvsharedlibrary

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.here.oksse.ServerSentEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi

import org.junit.*
import org.junit.Assert.*
import org.junit.runners.MethodSorters
import org.kp.consumer.android.ivvsharedlibrary.api.response.*
import org.kp.consumer.android.ivvsharedlibrary.data.source.DataSource
import org.kp.consumer.android.ivvsharedlibrary.data.source.Repository
import org.kp.consumer.android.ivvsharedlibrary.features.VideoViewModel
import org.kp.consumer.android.ivvsharedlibrary.model.ChatMessageModel
import org.kp.consumer.android.ivvsharedlibrary.model.Participant
import org.kp.consumer.android.ivvsharedlibrary.model.Room
import org.kp.consumer.android.ivvsharedlibrary.util.CHAIR
import org.kp.consumer.android.ivvsharedlibrary.util.GUEST
import org.kp.consumer.android.ivvsharedlibrary.util.GetMockResponse
import org.kp.consumer.android.ivvsharedlibrary.utilities.*
import org.kp.consumer.android.ivvsharedlibrary.utilities.any

import org.mockito.*
import org.mockito.Mockito.*
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.webrtc.SessionDescription


/**
 *
 * Unit test for the implementaton of [VideoViewModel]
 *
 * Created by venkatakalluri on 2020-01-16.
 * Copyright © 2019 Kaiser Permanente. All rights reserved.
 */

@ExperimentalCoroutinesApi
@FixMethodOrder(MethodSorters.DEFAULT)
class VideoViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var remoteRepository: Repository

    @Captor
    private lateinit var getSetupConferenceCallbackCaptor: ArgumentCaptor<DataSource.GetCallback<SetupConferenceResponse>>

    @Captor
    private lateinit var getPexipTokenCallbackCaptor: ArgumentCaptor<DataSource.GetCallback<RequestTokenResponse>>

    @Captor
    private lateinit var getCallResponseCaptor: ArgumentCaptor<DataSource.GetCallback<CallResponse>>

    @Captor
    private lateinit var getJoinConferenceResponseCaptor: ArgumentCaptor<DataSource.GetCallback<JoinConferenceResponse>>

    @Captor
    private lateinit var getLeaveConferenceResponseCaptor: ArgumentCaptor<DataSource.GetCallback<LeaveConferenceResponse>>
    @Captor
    private lateinit var getSendChatMessageResponseCaptor: ArgumentCaptor<DataSource.GetCallback<VVBaseResponse>>
    @Captor
    private lateinit var getAckResponseCaptor: ArgumentCaptor<DataSource.GetCallback<VVBaseResponse>>

    @Captor
    private lateinit var serverEventListener: ArgumentCaptor<ServerSentEvent.Listener>

    private lateinit var videoViewModel: VideoViewModel

    private lateinit var conferenceToken: String

    private lateinit var setupConferenceResponse: SetupConferenceResponse
    private lateinit var pexipRequestTokenResponse: RequestTokenResponse
    private lateinit var callResponse: CallResponse

    private lateinit var mockResponse: GetMockResponse
    val responseError = mock(ResponseError("severity","type","code","error","errorDescription")::class.java)

    lateinit var mockerror : ResponseResult.ServiceError

    @Before
    fun setupVideoViewModel() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this)

        videoViewModel = VideoViewModel(remoteRepository,Dispatchers.Unconfined)
        mockerror = mock(ResponseResult.ServiceError(responseError)::class.java)

        conferenceToken = "0007WtVyE8Gk1VQGU8BogiHroPC2"

        mockResponse = GetMockResponse()

        setupConferenceResponse = mockResponse.getSetUpConferenceResponse()
        pexipRequestTokenResponse = mockResponse.getPexipRequestTokenResponse()
        callResponse = mockResponse.getCallResponse()


    }


    @Test
    fun `setUpConference_SuccessResponse`() {

        setUpConferenceCall()

        // Then the event is triggered
        val value = LiveDataTestUtil.getValue(videoViewModel.setUpConferenceResponse)
        assertNotNull(value.getContentIfNotHandled())

        assertEquals(
            setupConferenceResponse.appointment.id,
            videoViewModel.getConferenceResponse()!!.appointment.id
        )


    }

    @Test
    fun `setUpConference_FailedServiceResponse`() {

        setUpConferenceServiceFailCall()

        // Then the event is triggered
        val value = LiveDataTestUtil.getValue(videoViewModel.diloagCloseEvent)
        assertNotNull(value.getContentIfNotHandled())


    }

    @Test
    fun `setUpConference_FailResponse`() {

        videoViewModel.setUpConference(conferenceToken)

        testCoroutineRule.runBlockingTest {

            verify<Repository>(remoteRepository, atLeastOnce()).setUpConferenceRequest(
                safeEq(conferenceToken),
                capture(getSetupConferenceCallbackCaptor)
            )
        }
        //when the repository returns the error
        getSetupConferenceCallbackCaptor.value.onFailure(ResponseResult.Failure("403"))

        // Then the event is triggered
        val value = LiveDataTestUtil.getValue(videoViewModel.diloagCloseEvent)
        assertNotNull(value.getContentIfNotHandled())

    }

    @Test
    fun `pexipRequestToken_SuccessResponse`() {

        setUpConferenceCall()

        pexipTokenCall()

        // Then the event is triggered
        val value = LiveDataTestUtil.getValue(videoViewModel.loadViewEvent)
        assertNotNull(value.getContentIfNotHandled())

        assertTrue(LiveDataTestUtil.getValue(videoViewModel.token).equals(pexipRequestTokenResponse.token))

        assertTrue(
            LiveDataTestUtil.getValue(videoViewModel.participantUUID).equals(
                pexipRequestTokenResponse.participantUUID
            )
        )


    }
    @Test
    fun `pexipRequestToken_FailedServiceResponse`() {

        setUpConferenceCall()

        pexipTokenServiceFailCall()

        // Then the event is triggered
        val value = LiveDataTestUtil.getValue(videoViewModel.diloagCloseEvent)
        assertNotNull(value.getContentIfNotHandled())


    }
    @Test
    fun `pexipRequestToken_FailResponse`() {

        setUpConferenceCall()

        pexipTokenFailCall()

        // Then the event is triggered
        val value = LiveDataTestUtil.getValue(videoViewModel.diloagCloseEvent)
        assertNotNull(value.getContentIfNotHandled())


    }

    @Test
    fun `callRequest_SuccessResponse`() {

        setUpConferenceCall()

        pexipTokenCall()

        callRequestCall()

        // Then the event is triggered
        val value = LiveDataTestUtil.getValue(videoViewModel.sdp)
        assertNotNull(value.getContentIfNotHandled())


    }
    @Test
    fun `callRequest_ServiceFailResponse`() {

        setUpConferenceCall()

        pexipTokenCall()

        callRequestServiceFailCall()

        // Then the event is triggered
        val value = LiveDataTestUtil.getValue(videoViewModel.diloagCloseEvent)
        assertNotNull(value.getContentIfNotHandled())


    }
    @Test
    fun `callRequest_FailResponse`() {

        setUpConferenceCall()

        pexipTokenCall()

        callRequestFailCall()

        // Then the event is triggered
        val value = LiveDataTestUtil.getValue(videoViewModel.diloagCloseEvent)
        assertNotNull(value.getContentIfNotHandled())


    }

    @Test
    fun `joinConference_SuccessResponse`() {

        setUpConferenceCall()

        pexipTokenCall()

        joinConfernceCall()

        // Then the event is triggered
        val value = LiveDataTestUtil.getValue(videoViewModel.participantsEvent)
        assertNotNull(value.getContentIfNotHandled())

    }
    @Test
    fun `joinConference_ServiceFailResponse`() {

        setUpConferenceCall()

        pexipTokenCall()

        joinConfernceServiceFailCall()

        // Then the event is triggered
        val value = LiveDataTestUtil.getValue(videoViewModel.diloagCloseEvent)
        assertNotNull(value.getContentIfNotHandled())

    }
    @Test
    fun `joinConference_FailResponse`() {

        setUpConferenceCall()

        pexipTokenCall()

        joinConfernceFailCall()

        // Then the event is triggered
        val value = LiveDataTestUtil.getValue(videoViewModel.diloagCloseEvent)
        assertNotNull(value.getContentIfNotHandled())

    }

    @Test
    fun `leaveConference_SuccessResponse`() {

        setUpConferenceCall()

        pexipTokenCall()

        joinConfernceCall()

        videoViewModel.leaveConference(conferenceToken, false)

        testCoroutineRule.runBlockingTest {
            verify<Repository>(remoteRepository).leaveConference(
                any(),
                anyString(),
                capture(getLeaveConferenceResponseCaptor)
            )
        }

        getLeaveConferenceResponseCaptor.value.onSuccess(LeaveConferenceResponse(ArrayList()))

        // Then the event is triggered
        val value = LiveDataTestUtil.getValue(videoViewModel.leaveConferenceEvent)
        assertNotNull(value.getContentIfNotHandled())


    }
    @Test
    fun `leaveConference_ServiceFailResponse`() {

        setUpConferenceCall()

        pexipTokenCall()

        joinConfernceCall()

        videoViewModel.leaveConference(conferenceToken, false)

        testCoroutineRule.runBlockingTest {
            verify<Repository>(remoteRepository).leaveConference(
                any(),
                anyString(),
                capture(getLeaveConferenceResponseCaptor)
            )
        }

        getLeaveConferenceResponseCaptor.value.onFailure(ResponseResult.ServiceError(responseError))

        // Then the event is triggered
        val value = LiveDataTestUtil.getValue(videoViewModel.diloagCloseEvent)
        assertNotNull(value.getContentIfNotHandled())


    }
    @Test
    fun `leaveConference_FailResponse`() {

        setUpConferenceCall()

        pexipTokenCall()

        joinConfernceCall()

        videoViewModel.leaveConference(conferenceToken, false)

        testCoroutineRule.runBlockingTest {
            verify<Repository>(remoteRepository).leaveConference(
                any(),
                anyString(),
                capture(getLeaveConferenceResponseCaptor)
            )
        }

        getLeaveConferenceResponseCaptor.value.onFailure(ResponseResult.Failure("errorCode"))

        // Then the event is triggered
        val value = LiveDataTestUtil.getValue(videoViewModel.diloagCloseEvent)
        assertNotNull(value.getContentIfNotHandled())


    }

    @Test
    fun sendAckRequest(){

        setUpConferenceCall()

        pexipTokenCall()
        callRequestCall()
        videoViewModel.ackRequest()

        testCoroutineRule.runBlockingTest {
            verify<Repository>(remoteRepository).doAck(
                any(Room::class.java),
                anyString(),
                anyString(),
                capture(getAckResponseCaptor)
            )
        }
        getAckResponseCaptor.value.onSuccess()
        // Then the event is triggered
        val value = LiveDataTestUtil.getValue(videoViewModel.diloagCloseEvent)
        assertNotNull(value.getContentIfNotHandled())

    }
    @Test
    fun sendAckServiceFailRequest(){

        setUpConferenceCall()

        pexipTokenCall()
        callRequestCall()
        videoViewModel.ackRequest()

        testCoroutineRule.runBlockingTest {
            verify<Repository>(remoteRepository).doAck(
                any(Room::class.java),
                anyString(),
                anyString(),
                capture(getAckResponseCaptor)
            )
        }
        getAckResponseCaptor.value.onFailure(ResponseResult.ServiceError(responseError))
        // Then the event is triggered
        val value = LiveDataTestUtil.getValue(videoViewModel.diloagCloseEvent)
        assertNotNull(value.getContentIfNotHandled())

    }
    @Test
    fun sendAckFailRequest(){

        setUpConferenceCall()

        pexipTokenCall()
        callRequestCall()
        videoViewModel.ackRequest()

        testCoroutineRule.runBlockingTest {
            verify<Repository>(remoteRepository).doAck(
                any(Room::class.java),
                anyString(),
                anyString(),
                capture(getAckResponseCaptor)
            )
        }
        getAckResponseCaptor.value.onFailure(ResponseResult.Failure("errorCode"))
        // Then the event is triggered
        val value = LiveDataTestUtil.getValue(videoViewModel.diloagCloseEvent)
        assertNotNull(value.getContentIfNotHandled())

    }

    @Test
    fun sendChatMessageCall() {

        videoViewModel.sendChatMessage("message")

        testCoroutineRule.runBlockingTest {
            verify<Repository>(remoteRepository).sendChatMessage(
                safeEq("message"),
                capture(getSendChatMessageResponseCaptor)
            )
        }

        getSendChatMessageResponseCaptor.value.onSuccess()
        assertEquals(videoViewModel.sseMessageList[0].messageBody, "message")

    }

    private fun setUpConferenceCall() {
        videoViewModel.setUpConference(conferenceToken)

        testCoroutineRule.runBlockingTest {
            verify<Repository>(remoteRepository).setUpConferenceRequest(
                safeEq(conferenceToken),
                capture(getSetupConferenceCallbackCaptor)
            )
        }

        getSetupConferenceCallbackCaptor.value.onSuccess(setupConferenceResponse)
    }

    private fun setUpConferenceServiceFailCall() {
        videoViewModel.setUpConference(conferenceToken)

        testCoroutineRule.runBlockingTest {
            verify<Repository>(remoteRepository).setUpConferenceRequest(
                safeEq(conferenceToken),
                capture(getSetupConferenceCallbackCaptor)
            )
        }

        getSetupConferenceCallbackCaptor.value.onFailure(ResponseResult.ServiceError(responseError))
    }

    private fun pexipTokenCall() {
        videoViewModel.pexipRequestToken()

        testCoroutineRule.runBlockingTest {
            verify<Repository>(remoteRepository).requestToken(
                safeEq(videoViewModel.getConferenceResponse()!!.room),
                safeEq(videoViewModel.getConferenceResponse()!!.signedInUser.name),
                capture(getPexipTokenCallbackCaptor), capture(serverEventListener)
            )
        }


        getPexipTokenCallbackCaptor.value.onSuccess(pexipRequestTokenResponse)
    }
    private fun pexipTokenServiceFailCall() {
        videoViewModel.pexipRequestToken()

        testCoroutineRule.runBlockingTest {
            verify<Repository>(remoteRepository).requestToken(
                safeEq(videoViewModel.getConferenceResponse()!!.room),
                safeEq(videoViewModel.getConferenceResponse()!!.signedInUser.name),
                capture(getPexipTokenCallbackCaptor), capture(serverEventListener)
            )
        }


        getPexipTokenCallbackCaptor.value.onFailure(ResponseResult.ServiceError(responseError))
    }
    private fun pexipTokenFailCall() {
        videoViewModel.pexipRequestToken()

        testCoroutineRule.runBlockingTest {
            verify<Repository>(remoteRepository).requestToken(
                safeEq(videoViewModel.getConferenceResponse()!!.room),
                safeEq(videoViewModel.getConferenceResponse()!!.signedInUser.name),
                capture(getPexipTokenCallbackCaptor), capture(serverEventListener)
            )
        }

        getPexipTokenCallbackCaptor.value.onFailure(ResponseResult.Failure("errorCode"))
    }

    private fun callRequestCall() {
        val sdp = SessionDescription(SessionDescription.Type.OFFER, "xyz")
        videoViewModel.callRequest(sdp)

        testCoroutineRule.runBlockingTest {
            verify<Repository>(remoteRepository).makeCall(
                safeEq(videoViewModel.getConferenceResponse()!!.room),
                safeEq(videoViewModel.participantUUID.value!!),
                safeEq(sdp),
                capture(getCallResponseCaptor)
            )
        }

        getCallResponseCaptor.value.onSuccess(callResponse)
    }
    private fun callRequestServiceFailCall() {
        val sdp = SessionDescription(SessionDescription.Type.OFFER, "xyz")
        videoViewModel.callRequest(sdp)

        testCoroutineRule.runBlockingTest {
            verify<Repository>(remoteRepository).makeCall(
                safeEq(videoViewModel.getConferenceResponse()!!.room),
                safeEq(videoViewModel.participantUUID.value!!),
                safeEq(sdp),
                capture(getCallResponseCaptor)
            )
        }

        getCallResponseCaptor.value.onFailure(ResponseResult.ServiceError(responseError))
    }
    private fun callRequestFailCall() {
        val sdp = SessionDescription(SessionDescription.Type.OFFER, "xyz")
        videoViewModel.callRequest(sdp)

        testCoroutineRule.runBlockingTest {
            verify<Repository>(remoteRepository).makeCall(
                safeEq(videoViewModel.getConferenceResponse()!!.room),
                safeEq(videoViewModel.participantUUID.value!!),
                safeEq(sdp),
                capture(getCallResponseCaptor)
            )
        }

        getCallResponseCaptor.value.onFailure(ResponseResult.Failure("errorCode"))
    }


    private fun joinConfernceCall() {

        videoViewModel.joinConference("pexipToken")

        testCoroutineRule.runBlockingTest {
            verify<Repository>(remoteRepository).joinConference(
                any(),
                safeEq("pexipToken"),
                capture(getJoinConferenceResponseCaptor)
            )
        }

        getJoinConferenceResponseCaptor.value.onSuccess(JoinConferenceResponse(ArrayList()))

    }
    private fun joinConfernceServiceFailCall() {

        videoViewModel.joinConference("pexipToken")

        testCoroutineRule.runBlockingTest {
            verify<Repository>(remoteRepository).joinConference(
                any(),
                safeEq("pexipToken"),
                capture(getJoinConferenceResponseCaptor)
            )
        }

        getJoinConferenceResponseCaptor.value.onFailure(ResponseResult.ServiceError(responseError))

    }
    private fun joinConfernceFailCall() {

        videoViewModel.joinConference("pexipToken")

        testCoroutineRule.runBlockingTest {
            verify<Repository>(remoteRepository).joinConference(
                any(),
                safeEq("pexipToken"),
                capture(getJoinConferenceResponseCaptor)
            )
        }

        getJoinConferenceResponseCaptor.value.onFailure(ResponseResult.Failure("errorCode"))

    }

    @Test
    fun `when mute message is sent, mute observable is changed`() {
        setUpConferenceCall()
        pexipTokenCall()


        testCoroutineRule.runBlockingTest {
            testCoroutineRule.methodToBeTested {
                videoViewModel.setMuteEvent("{is_muted: YES,uuid: ${pexipRequestTokenResponse.participantUUID}}")
                var value = LiveDataTestUtil.getValue(videoViewModel.muteObservable)
                assertNotNull(value.getContentIfNotHandled())
                videoViewModel.setMuteEvent("{is_muted: NO,uuid: ${pexipRequestTokenResponse.participantUUID}}")
                value = LiveDataTestUtil.getValue(videoViewModel.muteObservable)
                assertNotNull(value.getContentIfNotHandled())
            }
        }
    }

    @Test
    fun `when custom pause message comes, pause observable is updated`() {
        val chatMessageModel = ChatMessageModel(
            "KPCustomMessage:EVENT_ON_HOLD",
            "type",
            "origin",
            "senderUUID",
            false,
            1,
            1
        )
        videoViewModel.handleCustomChatMessage(chatMessageModel)
        val value = LiveDataTestUtil.getValue(videoViewModel.onHoldObservable)
        assertNotNull(value.getContentIfNotHandled())

    }

    @Test
    fun `when custom moderator message comes, participant list is updated`() {
        val chatMessageModel = ChatMessageModel(
            "KPCustomMessage: EVENT_MODERATOR_CHANGED",
            "type",
            "origin",
            "senderUUID",
            false,
            1,
            1
        )
        videoViewModel.handleCustomChatMessage(chatMessageModel)
        val value = LiveDataTestUtil.getValue(videoViewModel.participantsEvent)
        assertNotNull(value.getContentIfNotHandled())
    }

    @Test
    fun `verify chat message is added to first of list`() {
        val chatMessageModel = ChatMessageModel(
            "messageBody",
            "type",
            "origin",
            "senderUUID",
            false,
            1,
            1
        )
        videoViewModel.addChatMessageToList(chatMessageModel)
        assertEquals(videoViewModel.sseMessageList[0], chatMessageModel)
        val value1 = LiveDataTestUtil.getValue(videoViewModel.showUnreadMessagesBadge)
        val value2 = LiveDataTestUtil.getValue(videoViewModel.receivedChatMessagesObservable)
        assertEquals(value1, true)
        assertEquals(value2[0], chatMessageModel)
    }

    @Test
    fun `when a participants list has host, return true`() {
        setUpConferenceCall()

        val test = Participant(
            "",
            setupConferenceResponse.signedInUser.id,
            "",
            "",
            "",
            CHAIR,
            true,
            "",
            "",
            "",
            "",
            "",
            false,
            false,
            false,
            false
        )
        assertEquals(videoViewModel.isHost(listOf(test)), true)
    }

    @Test
    fun `when a participants list has not host, return false`() {
        setUpConferenceCall()

        val test = Participant(
            "",
            setupConferenceResponse.signedInUser.id + 1,
            "",
            "",
            "",
            GUEST,
            false,
            "",
            "",
            "",
            "",
            "",
            false,
            false,
            false,
            false
        )
        assertEquals(videoViewModel.isHost(listOf(test)), false)
    }


    @After
    fun tearDown() {
        reset(remoteRepository,mockerror)
    }


}