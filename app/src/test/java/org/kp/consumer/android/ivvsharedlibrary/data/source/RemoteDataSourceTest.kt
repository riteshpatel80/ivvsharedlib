package org.kp.consumer.android.ivvsharedlibrary.data.source

import com.github.tomakehurst.wiremock.client.WireMock.*
import org.junit.Before
import org.junit.Test
import org.kp.kpnetworking.dispatcher.RequestHandler
import org.kp.kpnetworking.dispatcher.RequestHandlerSingleton
import org.mockito.*
import org.mockito.Mockito.*
import com.github.tomakehurst.wiremock.junit.WireMockRule
import com.here.oksse.ServerSentEvent
import junit.framework.Assert.*
import org.junit.Rule
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.JoinConferenceRequestModel
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.LeaveConferenceRequestModel
import org.kp.consumer.android.ivvsharedlibrary.api.response.*
import org.kp.consumer.android.ivvsharedlibrary.model.DialOut
import org.kp.consumer.android.ivvsharedlibrary.util.GetMockResponse
import org.kp.consumer.android.ivvsharedlibrary.utilities.*
import org.webrtc.SessionDescription


/**
 *  Unit test for the implementation of [RemoteDataSource]
 *
 * Created by venkatakalluri on 2020-01-28.
 * Copyright © 2019 Kaiser Permanente. All rights reserved.
 */

class RemoteDataSourceTest {

    private lateinit var remoteDataSource: RemoteDataSource

    private lateinit var mockResponse: GetMockResponse

    @Rule
    @JvmField
    var wireMockRule = WireMockRule(8081)

    @Mock
    private lateinit var getPexipTokenCallback: DataSource.GetCallback<RequestTokenResponse>

    @Mock
    private lateinit var getSetupConferenceCallback: DataSource.GetCallback<SetupConferenceResponse>

    @Mock
    private lateinit var getPexipRefreshTokenResponseCallback: DataSource.GetCallback<RefreshTokenResponse>

    @Mock
    private lateinit var callResponseCallback: DataSource.GetCallback<CallResponse>

    @Mock
    private lateinit var getAckResponeCallback: DataSource.GetCallback<VVBaseResponse>

    @Mock
    private lateinit var getDailoutResponseCallback: DataSource.GetCallback<DialOutResponse>

    @Mock
    private lateinit var getJoinConferenceCallback: DataSource.GetCallback<JoinConferenceResponse>

    @Mock
    private lateinit var getLeaveConferenceCallback: DataSource.GetCallback<LeaveConferenceResponse>

    @Mock
    private lateinit var getVVBaseResponseCallback1: DataSource.GetCallback<VVBaseResponse>

    @Mock
    private lateinit var getVVBaseResponseCallback2: DataSource.GetCallback<VVBaseResponse>
    @Mock
    private lateinit var getVVBaseResponseCallback3: DataSource.GetCallback<VVBaseResponse>

    @Mock
    private lateinit var getParticipantsResponseCallback: DataSource.GetCallback<GetParticipantsResponse>

    @Mock
    private lateinit var serverEventListener: ServerSentEvent.Listener

    private lateinit var setupResponse: SetupConferenceResponse

    @Before
    fun setUpRemoteDataSourceTest() {
        MockitoAnnotations.initMocks(this)
        RemoteDataSource.destroyInstance()

        RemoteDataSource.clientId = "clientId"
        RemoteDataSource.envlbl = "envlbl"
        RemoteDataSource.baseurl = "http://localhost:8081"
        RemoteDataSource.vvConferenceToken = "vvConferenceToken"
        remoteDataSource = RemoteDataSource.getInstance("http://")
        remoteDataSource._pexipToken = "pexipToken"

        mockResponse = GetMockResponse();
        setupResponse = mockResponse.getSetUpConferenceResponse()

        val requestHandlerBuilder = RequestHandler.RequestHandlerBuilder()
        requestHandlerBuilder.supportCookies(true)
        RequestHandlerSingleton.init(requestHandlerBuilder)

    }

    @Test
    fun executeSetupConferenceRequest() {

        setupConferenceRequestCall()

        verify(getSetupConferenceCallback).onSuccess(mockResponse.getSetUpConferenceResponse("/SetupConferenceResponse_localhost.json"))
        assertNotNull(remoteDataSource.setupConferenceResponse)
    }

    @Test
    fun executeSetupConferenceFailRequest() {
        makeFailGetStubFor(".*/setup")
        remoteDataSource.setUpConferenceRequest("token", getSetupConferenceCallback)

        verify(getSetupConferenceCallback).onFailure(
            org.kp.consumer.android.ivvsharedlibrary.utilities.any(
                ResponseResult.Failure::class.java
            )
        )
    }

    @Test
    fun executeSetupConferenceFailServiceRequest() {
        makeGetStubFor(".*/setup", mockResponse.getServiceErrorResponseString())
        remoteDataSource.setUpConferenceRequest("token", getSetupConferenceCallback)
        verify(getSetupConferenceCallback).onFailure(
            org.kp.consumer.android.ivvsharedlibrary.utilities.any(
                ResponseResult.ServiceError::class.java
            )
        )
    }

    fun setupConferenceRequestCall() {
        stubFor(
            get(urlPathEqualTo("/setup"))
                .willReturn(
                    aResponse()
                        .withStatus(200)
                        .withBody(mockResponse.getSetUpConferenceResponseString("/SetupConferenceResponse_localhost.json"))
                )
        )
        remoteDataSource.setUpConferenceRequest("token", getSetupConferenceCallback)
    }

    @Test
    fun executeAddDialOutRequest() {
        stubFor(
            post(urlPathMatching(".*/dialout"))
                .willReturn(
                    aResponse()
                        .withStatus(200)
                        .withBody(mockResponse.getDialOutResponseInString())
                )
        )

        remoteDataSource.addDialOutRequest(
            DialOut("number", "displayName", "role"),
            getDailoutResponseCallback
        )
        verify(getDailoutResponseCallback).onSuccess(mockResponse.getDialOutResponse())
    }

    @Test
    fun executeDialOutFailRequestCall() {
        makeFailPostStubFor(".*/dialout")
        remoteDataSource.addDialOutRequest(
            DialOut("number", "displayName", "role"),
            getDailoutResponseCallback
        )

        verify(getDailoutResponseCallback).onFailure(
            org.kp.consumer.android.ivvsharedlibrary.utilities.any(
                ResponseResult.Failure::class.java
            )
        )
    }

    @Test
    fun executeDialOutFailServiceRequestCall() {
        makePostStubFor(".*/dialout", mockResponse.getServiceErrorResponseString())
        remoteDataSource.addDialOutRequest(
            DialOut("number", "displayName", "role"),
            getDailoutResponseCallback
        )
        verify(getDailoutResponseCallback).onFailure(
            org.kp.consumer.android.ivvsharedlibrary.utilities.any(
                ResponseResult.ServiceError::class.java
            )
        )
    }

    @Test
    fun executeJoinConferenceRequest() {
        stubFor(
            put(urlPathMatching("/join"))
                .willReturn(
                    aResponse()
                        .withStatus(200)
                        .withBody("{}")
                )
        )

        remoteDataSource.joinConference(
            JoinConferenceRequestModel("pexipPartcipantUUID"),
            "registrationToken",
            getJoinConferenceCallback
        )
        verify(getJoinConferenceCallback).onSuccess(JoinConferenceResponseConverter().convert("{}"))
    }

    @Test
    fun executeJoinConferenceFailRequestCall() {
        makeFailPutStubFor(".*/join")
        remoteDataSource.joinConference(
            JoinConferenceRequestModel("pexipPartcipantUUID"),
            "registrationToken",
            getJoinConferenceCallback
        )

        verify(getJoinConferenceCallback).onFailure(
            org.kp.consumer.android.ivvsharedlibrary.utilities.any(
                ResponseResult.Failure::class.java
            )
        )
    }

    @Test
    fun executeJoinConferenceFailServiceRequestCall() {
        makePutStubFor(".*/join", mockResponse.getServiceErrorResponseString())
        remoteDataSource.joinConference(
            JoinConferenceRequestModel("pexipPartcipantUUID"),
            "registrationToken",
            getJoinConferenceCallback
        )
        verify(getJoinConferenceCallback).onFailure(
            org.kp.consumer.android.ivvsharedlibrary.utilities.any(
                ResponseResult.ServiceError::class.java
            )
        )
    }

    @Test
    fun executeLeaveConferenceRequest() {
        stubFor(
            put(urlPathMatching("/leave"))
                .willReturn(
                    aResponse()
                        .withStatus(200)
                        .withBody("{}")
                )
        )

        remoteDataSource.leaveConference(
            LeaveConferenceRequestModel(true, "Member", true),
            "registrationToken",
            getLeaveConferenceCallback
        )
        verify(getLeaveConferenceCallback).onSuccess(LeaveConferenceResponseConverter().convert("{}"))
    }

    @Test
    fun executeLeaveConferenceFailRequestCall() {
        makeFailPutStubFor(".*/leave")
        remoteDataSource.leaveConference(
            LeaveConferenceRequestModel(true, "Member", true),
            "registrationToken",
            getLeaveConferenceCallback
        )
        verify(getLeaveConferenceCallback).onFailure(
            org.kp.consumer.android.ivvsharedlibrary.utilities.any(
                ResponseResult.Failure::class.java
            )
        )
    }

    @Test
    fun executeLeaveConferenceFailServiceRequestCall() {
        makePutStubFor(".*/leave", mockResponse.getServiceErrorResponseString())
        remoteDataSource.leaveConference(
            LeaveConferenceRequestModel(true, "Member", true),
            "registrationToken",
            getLeaveConferenceCallback
        )
        verify(getLeaveConferenceCallback).onFailure(
            org.kp.consumer.android.ivvsharedlibrary.utilities.any(
                ResponseResult.ServiceError::class.java
            )
        )
    }

    @Test
    fun executeIVVRenewTokenRequest() {
        stubFor(
            get(urlPathMatching("/renewtoken"))
                .willReturn(
                    aResponse()
                        .withStatus(200)
                        .withBody(
                            "{\n" +
                                    "\"data\": \"\"\n" +
                                    "}"
                        )
                )
        )

        remoteDataSource.ivvRenewToken("token", getVVBaseResponseCallback1)
        verify(getVVBaseResponseCallback1).onSuccess(GenericResponse("", null))
    }

    @Test
    fun executeIVVRenewTokenFailRequestCall() {
        makeFailGetStubFor(".*/renewtoken")
        remoteDataSource.ivvRenewToken("token", getVVBaseResponseCallback1)
        verify(getVVBaseResponseCallback1).onFailure(
            org.kp.consumer.android.ivvsharedlibrary.utilities.any(
                ResponseResult.Failure::class.java
            )
        )
    }

    @Test
    fun executeIVVRenewTokenFailServiceRequestCall() {
        makeGetStubFor(".*/renewtoken", mockResponse.getServiceErrorResponseString())
        remoteDataSource.ivvRenewToken("token", getVVBaseResponseCallback1)
        verify(getVVBaseResponseCallback1).onFailure(
            org.kp.consumer.android.ivvsharedlibrary.utilities.any(
                ResponseResult.ServiceError::class.java
            )
        )
    }

    @Test
    fun executeGetParticipantRequest() {
        setupConferenceRequestCall()

        stubFor(
            get(urlPathMatching("/participant"))
                .willReturn(
                    aResponse()
                        .withStatus(200)
                        .withBody(mockResponse.getParticipantsResponseString())
                )
        )

        remoteDataSource.getParticipantsRequest("0","0","0","0", getParticipantsResponseCallback)
        verify(getParticipantsResponseCallback).onSuccess(mockResponse.getParticipantsResponse())
    }

    @Test
    fun executeGetParticipantFailRequestCall() {
        setupConferenceRequestCall()
        makeFailGetStubFor(".*/participant")
        remoteDataSource.getParticipantsRequest("0","0","0","0", getParticipantsResponseCallback)
        verify(getParticipantsResponseCallback).onFailure(
            org.kp.consumer.android.ivvsharedlibrary.utilities.any(
                ResponseResult.Failure::class.java
            )
        )
    }

    @Test
    fun executeGetParticipantFailServiceRequestCall() {
        setupConferenceRequestCall()
        makeGetStubFor(".*/participant", mockResponse.getServiceErrorResponseString())
        remoteDataSource.getParticipantsRequest("0","0","0","0", getParticipantsResponseCallback)
        verify(getParticipantsResponseCallback).onFailure(
            org.kp.consumer.android.ivvsharedlibrary.utilities.any(
                ResponseResult.ServiceError::class.java
            )
        )
    }

    @Test
    fun executeSendChatMessageRequest() {
        setupConferenceRequestCall()

        stubFor(
            post(urlPathMatching(".*/message"))
                .willReturn(
                    aResponse()
                        .withStatus(200)
                        .withBody(
                            "{\n" +
                                    "\"data\": \"\"\n" +
                                    "}"
                        )
                )
        )

        remoteDataSource.sendChatMessage("message", getVVBaseResponseCallback2)
        verify(getVVBaseResponseCallback2).onSuccess()
    }

    @Test
    fun eexecuteSendChatMessageFailRequestCall() {
        setupConferenceRequestCall()
        makeFailPostStubFor(".*/message")
        remoteDataSource.sendChatMessage("message", getVVBaseResponseCallback2)
        verify(getVVBaseResponseCallback2).onFailure(
            org.kp.consumer.android.ivvsharedlibrary.utilities.any(
                ResponseResult.Failure::class.java
            )
        )
    }

    @Test
    fun executeSendChatMessageFailServiceRequestCall() {
        setupConferenceRequestCall()
        makePostStubFor(".*/message", mockResponse.getServiceErrorResponseString())
        remoteDataSource.sendChatMessage("message", getVVBaseResponseCallback2)
        verify(getVVBaseResponseCallback2).onFailure(
            org.kp.consumer.android.ivvsharedlibrary.utilities.any(
                ResponseResult.ServiceError::class.java
            )
        )
    }

    @Test
    fun executeRefreshToken() {
        stubFor(
            post(urlPathMatching(".*/refresh_token"))
                .willReturn(
                    aResponse()
                        .withStatus(200)
                        .withBody(mockResponse.getPexipRequestTokenResponeInString())
                )
        )
        remoteDataSource.refreshToken(
            setupResponse.room,
            "token",
            getPexipRefreshTokenResponseCallback
        )
        verify(postRequestedFor(urlPathMatching(".*/refresh_token")))
    }

    @Test
    fun executeRefreshFailRequestCall() {
        makeFailPostStubFor(".*/refresh_token")
        remoteDataSource.refreshToken(
            setupResponse.room,
            "token",
            getPexipRefreshTokenResponseCallback
        )
        verify(getPexipRefreshTokenResponseCallback).onFailure(
            org.kp.consumer.android.ivvsharedlibrary.utilities.any(
                ResponseResult.Failure::class.java
            )
        )
    }

    @Test
    fun executeRefreshFailServiceRequestCall() {
        makePostStubFor(".*/refresh_token", mockResponse.getServiceErrorResultResponseString())
        remoteDataSource.refreshToken(
            setupResponse.room,
            "token",
            getPexipRefreshTokenResponseCallback
        )
        verify(getPexipRefreshTokenResponseCallback).onFailure(
            org.kp.consumer.android.ivvsharedlibrary.utilities.any(
                ResponseResult.ServiceError::class.java
            )
        )
    }


    @Test
    fun executeRequestToken() {
        stubFor(
            post(urlPathMatching(".*/request_token"))
                .willReturn(
                    aResponse()
                        .withStatus(200)
                        .withBody(mockResponse.getPexipRequestTokenResponeInString())
                )
        )
        remoteDataSource.requestToken(
            setupResponse.room,
            "displayName",
            getPexipTokenCallback,
            serverEventListener
        )
        verify(getPexipTokenCallback).onSuccess(mockResponse.getPexipRequestTokenResponse())
        assertNotNull(remoteDataSource.getPexipUpdatedToken())
    }
    @Test
    fun executeRequestTokenFailRequestCall() {
        makeFailPostStubFor(".*/request_token")
        remoteDataSource.requestToken(
            setupResponse.room,
            "displayName",
            getPexipTokenCallback,
            serverEventListener
        )
        verify(getPexipTokenCallback).onFailure(
            org.kp.consumer.android.ivvsharedlibrary.utilities.any(
                ResponseResult.Failure::class.java
            )
        )
    }

    @Test
    fun executeRequestTokenFailServiceRequestCall() {
        makePostStubFor(".*/request_token", mockResponse.getServiceErrorResultResponseString())
        remoteDataSource.requestToken(
            setupResponse.room,
            "displayName",
            getPexipTokenCallback,
            serverEventListener
        )
        verify(getPexipTokenCallback).onFailure(
            org.kp.consumer.android.ivvsharedlibrary.utilities.any(
                ResponseResult.ServiceError::class.java
            )
        )
    }

    @Test
    fun executeAckRequest() {
        stubFor(
            post(urlPathMatching(".*/ack"))
                .willReturn(
                    aResponse()
                        .withStatus(200)
                        .withBody("{}")
                )
        )


        remoteDataSource.doAck(
            setupResponse.room,
            "participantUUID",
            "callUUID",
            getAckResponeCallback
        )
        verify(postRequestedFor(urlPathMatching(".*/ack")))
    }
    @Test
    fun executeAckFailRequestCall() {
        makeFailPostStubFor(".*/ack")
        remoteDataSource.doAck(
            setupResponse.room,
            "participantUUID",
            "callUUID",
            getAckResponeCallback
        )
        verify(getAckResponeCallback).onFailure(
            org.kp.consumer.android.ivvsharedlibrary.utilities.any(
                ResponseResult.Failure::class.java
            )
        )
    }

    @Test
    fun executeAckFailServiceRequestCall() {
        makePostStubFor(".*/ack", mockResponse.getServiceErrorResponseString())
        remoteDataSource.doAck(
            setupResponse.room,
            "participantUUID",
            "callUUID",
            getAckResponeCallback
        )
        verify(getAckResponeCallback).onFailure(
            org.kp.consumer.android.ivvsharedlibrary.utilities.any(
                ResponseResult.ServiceError::class.java
            )
        )
    }

    @Test
    fun executeDisconnectRequest() {
        stubFor(
            post(urlPathMatching(".*/disconnect"))
                .willReturn(
                    aResponse()
                        .withStatus(200)
                        .withBody("{}")
                )
        )

        remoteDataSource.disconnect(
            setupResponse.room,
            "participantUUID",
            "token",
            "callUUID",
            getVVBaseResponseCallback3
        )
        verify(postRequestedFor(urlPathMatching(".*/disconnect")))
    }
    @Test
    fun executeDisconnectFailRequestCall() {
        makeFailPostStubFor(".*/disconnect")

        remoteDataSource.disconnect(
            setupResponse.room,
            "participantUUID",
            "token",
            "callUUID",
            getVVBaseResponseCallback3
        )
        verify(getVVBaseResponseCallback3).onFailure(
            org.kp.consumer.android.ivvsharedlibrary.utilities.any(
                ResponseResult.Failure::class.java
            )
        )
    }

    @Test
    fun executeDisconnectFailServiceRequestCall() {
        makePostStubFor(".*/disconnect", mockResponse.getServiceErrorResponseString())

        remoteDataSource.disconnect(
            setupResponse.room,
            "participantUUID",
            "token",
            "callUUID",
            getVVBaseResponseCallback3
        )
        verify(getVVBaseResponseCallback3).onFailure(
            org.kp.consumer.android.ivvsharedlibrary.utilities.any(
                ResponseResult.ServiceError::class.java
            )
        )
    }


    @Test
    fun executeCallRequest() {
        stubFor(
            post(urlPathMatching(".*/calls"))
                .willReturn(
                    aResponse()
                        .withStatus(200)
                        .withBody(mockResponse.getCallResponseInString())
                )
        )

        remoteDataSource.makeCall(
            setupResponse.room,
            "participantUUID",
            SessionDescription(SessionDescription.Type.OFFER, "test description"),
            callResponseCallback
        )
        verify(postRequestedFor(urlPathMatching(".*/calls")))
    }
    @Test
    fun executeCallFailRequestCall() {
        makeFailPostStubFor(".*/calls")

        remoteDataSource.makeCall(
            setupResponse.room,
            "participantUUID",
            SessionDescription(SessionDescription.Type.OFFER, "test description"),
            callResponseCallback
        )
        verify(callResponseCallback).onFailure(
            org.kp.consumer.android.ivvsharedlibrary.utilities.any(
                ResponseResult.Failure::class.java
            )
        )
    }

    @Test
    fun executeCallFailServiceRequestCall() {
        makePostStubFor(".*/calls", mockResponse.getServiceErrorResultResponseString())

        remoteDataSource.makeCall(
            setupResponse.room,
            "participantUUID",
            SessionDescription(SessionDescription.Type.OFFER, "test description"),
            callResponseCallback
        )
        verify(callResponseCallback).onFailure(
            org.kp.consumer.android.ivvsharedlibrary.utilities.any(
                ResponseResult.ServiceError::class.java
            )
        )
    }

    @Test
    fun remoteDataSourceDestroysItself(){
        val remote = RemoteDataSource.getInstance()
        assertSame(remote,RemoteDataSource.getInstance())
        RemoteDataSource.destroyInstance()
        //asserting for code coverage
        assertNull(RemoteDataSource.getInstance().mServerSentEvent)
        assertNotSame(remote,RemoteDataSource.getInstance())
    }
}
