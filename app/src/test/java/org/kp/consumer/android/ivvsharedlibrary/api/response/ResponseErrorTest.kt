package org.kp.consumer.android.ivvsharedlibrary.api.response

import org.junit.Assert.*
import org.junit.Test

class ResponseErrorTest{

    @Test
    fun `verify values are assigned correctly`(){
        val model = ResponseError("severity" ,"type" ,"code" ,"error" ,"errorDescription")
        assertEquals(model.severity,"severity")
        assertEquals(model.type,"type")
        assertEquals(model.code,"code")
        assertEquals(model.error,"error")
        assertEquals(model.errorDescription,"errorDescription")
    }
}