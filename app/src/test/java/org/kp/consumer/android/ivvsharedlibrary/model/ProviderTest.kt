package org.kp.consumer.android.ivvsharedlibrary.model

import org.junit.Assert.*
import org.junit.Test

class ProviderTest{

    @Test
    fun `verify values are assigned correctly`(){
        val provider : Provider = Provider(
            "id"  ,
            "idType"  ,
            "name" ,
            "region" )
        assertEquals(provider.id,"id")
        assertEquals(provider.idType,"idType")
        assertEquals(provider.name,"name")
        assertEquals(provider.region,"region")
    }
}