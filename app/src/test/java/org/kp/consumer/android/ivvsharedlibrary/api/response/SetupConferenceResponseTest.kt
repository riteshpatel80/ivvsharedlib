package org.kp.consumer.android.ivvsharedlibrary.api.response

import org.junit.Assert.*
import org.junit.Test
import org.kp.consumer.android.ivvsharedlibrary.util.GetMockResponse

class SetupConferenceResponseTest{

    @Test
    fun `verify values are assigned correctly`(){
        val setup = GetMockResponse().getSetUpConferenceResponse()
        assertNotNull(setup.webConfig)
    }
}