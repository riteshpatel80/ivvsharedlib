package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig

import org.junit.Assert.*
import org.junit.Test

class GetParticipantsRequestModelTest{

    @Test
    fun `verify values are assigned correctly`(){
        val model = GetParticipantsRequestModel("appointmentMeetingRoomNumber" ,"appointmentRegion" ,"appointmentVisitType","uuid","time","source","event")
        assertEquals(model.appointmentMeetingRoomNumber,"appointmentMeetingRoomNumber")
        assertEquals(model.appointmentRegion,"appointmentRegion")
        assertEquals(model.appointmentVisitType,"appointmentVisitType")
        assertEquals(model.uuid,"uuid")
        assertEquals(model.time,"time")
        assertEquals(model.source,"source")
        assertEquals(model.event,"event")
    }
}