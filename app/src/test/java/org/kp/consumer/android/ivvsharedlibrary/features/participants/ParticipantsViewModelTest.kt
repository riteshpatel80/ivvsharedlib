package org.kp.consumer.android.ivvsharedlibrary.features.participants

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import org.kp.consumer.android.ivvsharedlibrary.model.Participant
import org.junit.*
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.kp.consumer.android.ivvsharedlibrary.api.response.GetParticipantsResponse
import org.kp.consumer.android.ivvsharedlibrary.api.response.ResponseError
import org.kp.consumer.android.ivvsharedlibrary.api.response.ResponseResult
import org.kp.consumer.android.ivvsharedlibrary.api.response.SetupConferenceResponse
import org.kp.consumer.android.ivvsharedlibrary.data.source.DataSource
import org.kp.consumer.android.ivvsharedlibrary.data.source.Repository
import org.kp.consumer.android.ivvsharedlibrary.util.GetMockResponse
import org.kp.consumer.android.ivvsharedlibrary.utilities.TestCoroutineRule
import org.mockito.*
import org.mockito.Mockito.*

class ParticipantsViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()
    @Mock
    lateinit var repository: Repository
    @Mock
    private lateinit var viewModel: ParticipantsViewModel

    @Mock
    private lateinit var getParticipantsResponseCallback: DataSource.GetCallback<GetParticipantsResponse>

    val getResponse = GetMockResponse()

    @Mock
    private lateinit var getSetupConferenceCallback: DataSource.GetCallback<SetupConferenceResponse>
    val participant1 = Participant(
        name = "",
        id = "",
        idType = "",
        uuid = "",
        role = "",
        vmrRole = "",
        host = false,
        region = "",
        applicationId = "",
        channelId = "",
        callDirection = "",
        callQuality = "",
        hasMedia = false,
        isMuted = false,
        isOnHold = false,
        isStreaming = false
    )

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        viewModel = ParticipantsViewModel(repository)
    }

    @After
    fun tearDown() {
        reset(getParticipantsResponseCallback, getSetupConferenceCallback)
    }

    @Test
    fun `given a list, then method sets events`() {
        viewModel.setList(listOf())
        assertEquals(viewModel.participantList?.size, 0)
    }

    @Test
    fun `given an empty list, then method returns false`() {
        Assert.assertFalse(viewModel.doesChairExits(mutableListOf()))
    }

    @Test
    fun `given a list of participants without a chair vrmRole, then method returns false`() {
        Assert.assertFalse(viewModel.doesChairExits(mutableListOf(participant1)))
    }

    @Test
    fun `given a list of participants with a guest vrmRole, then method returns false`() {
        val participant = Participant(
            name = "",
            id = "",
            idType = "",
            uuid = "",
            role = "",
            vmrRole = "guest",
            host = false,
            region = "",
            applicationId = "",
            channelId = "",
            callDirection = "",
            callQuality = "",
            hasMedia = false,
            isMuted = false,
            isOnHold = false,
            isStreaming = false
        )
        Assert.assertFalse(viewModel.doesChairExits(mutableListOf(participant)))
    }

    @Test
    fun `given a list of participants with a chair vrmRole, then method returns true`() {
        val participant = Participant(
            name = "",
            id = "",
            idType = "",
            uuid = "",
            role = "",
            vmrRole = "chair",
            host = false,
            region = "",
            applicationId = "",
            channelId = "",
            callDirection = "",
            callQuality = "",
            hasMedia = false,
            isMuted = false,
            isOnHold = false,
            isStreaming = false
        )
        assertTrue(viewModel.doesChairExits(mutableListOf(participant)))
    }

    @Test
    fun `when participants make a call, then repository should make a successful request`()  {
        viewModel.participantList = null
        viewModel.getParticipants(source = "source")
        viewModel.participantCallback.onSuccess(getResponse.getParticipantsResponse())
        assertEquals(viewModel.participantList?.size, 1)
    }
    val responseError = mock(ResponseError("severity","type","code","error","errorDescription")::class.java)
    @Test
    fun `when participants makes a failed service request, mock gets used`(){
        val mock = mock(ResponseResult.ServiceError(responseError)::class.java)
        `when`(mock.error).thenReturn(responseError)
        `when`(responseError.code).thenReturn("code")
        viewModel.participantCallback.onFailure(mock)
        verify(responseError).code

    }
    @Test
    fun `when participants makes a failed request, mock gets used`(){
        val mock = mock(ResponseResult.Failure("errorCode")::class.java)
        viewModel.participantCallback.onFailure(mock)
        verify(mock).errorCode
    }
    @Test
    fun `when a participant is deleted, return list without participant`(){
        viewModel.participantList = arrayListOf(participant1delete,participant2delete,participant3delete)
        assertEquals(viewModel.participantList!!.size, 3)
        viewModel.deleteParticipant(participant1delete.uuid)
        assertEquals(viewModel.participantList!!.size, 2)
    }
    @Test
    fun `when a participant is muted, return list with updated participants`(){
        viewModel.participantList = arrayListOf(participant1delete)
        assertEquals((viewModel.participantList as ArrayList<Participant>)[0].isMuted, false)
        viewModel.updateParticipantMute(participant1delete.uuid,true)
        assertEquals((viewModel.participantList as ArrayList<Participant>)[0].isMuted, true)
    }
    val participant1delete = Participant(
        name = "",
        id = "",
        idType = "",
        uuid = "1",
        role = "",
        vmrRole = "chair",
        host = false,
        region = "",
        applicationId = "",
        channelId = "",
        callDirection = "",
        callQuality = "",
        hasMedia = false,
        isMuted = false,
        isOnHold = false,
        isStreaming = false
    )
    val participant2delete = Participant(
        name = "",
        id = "",
        idType = "",
        uuid = "2",
        role = "",
        vmrRole = "chair",
        host = false,
        region = "",
        applicationId = "",
        channelId = "",
        callDirection = "",
        callQuality = "",
        hasMedia = false,
        isMuted = false,
        isOnHold = false,
        isStreaming = false
    )
    val participant3delete = Participant(
        name = "",
        id = "",
        idType = "",
        uuid = "3",
        role = "",
        vmrRole = "chair",
        host = false,
        region = "",
        applicationId = "",
        channelId = "",
        callDirection = "",
        callQuality = "",
        hasMedia = false,
        isMuted = false,
        isOnHold = false,
        isStreaming = false
    )
}