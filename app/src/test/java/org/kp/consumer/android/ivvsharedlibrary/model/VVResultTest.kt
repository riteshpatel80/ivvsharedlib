package org.kp.consumer.android.ivvsharedlibrary.model

import org.junit.Assert.*
import org.junit.Test

class VVResultTest{

    inner class TestReturn{
        fun returnSuccessIfGreaterThan5(num : Int) : VVResult<Int,String>{
            if (num > 5) return Success(num) // If value is greater than 5, Return a success object
            else return Error("this") // If value if 5 or less, return an Error object
        }
    }

    val ran6 = TestReturn().returnSuccessIfGreaterThan5(6) //creating a success object
    val ran2 = TestReturn().returnSuccessIfGreaterThan5(2) //creating an error object

    @Test
    fun `testing orElse Success return values`(){
        //Success Returns value
        assertEquals(ran6::class.java,Success::class.java) //check to make sure this is a success
        assertEquals(ran6.orElse(5),6) // A Success objects orElse returns given value in Success
        val ran6String = ran6.orElse { //This should just return the value in success because only failures need to be handled
            if (it == "this") 1
            else -1
        }
        assertEquals(ran6String,6)
    }
    @Test
    fun `testing orElse Failure return values`(){
        //Failure returns something you need to handle
        //check this is an Error
        assertEquals(ran2::class.java,Error::class.java)

        //Error object returns other default value
        assertEquals(ran2.orElse(5),5)

        val ran2ErrorHandle = ran2.orElse{//This is where the error is handled
            if (it == "this") 1 //Return 1 if the failed string was "this"
            else -1 //otherwise return 1
        }

        //asserting that the return was 1
        assertEquals(ran2ErrorHandle,1)
    }

    @Test
    fun `testing flatMap and flatMapFailure return values for success`(){
        //FlatMap allows the values to be manipulated and sent through down the pipeline
        val ran6Map  = ran6.flatMap {
            val y = it * 2 //multiply Success values by 2
            Success<Int,String>(y) //return that as a success
        }

        //There is no failure so this returns original value
        val ran6Mapfail  = ran6.flatMapFailure {
            Error<Int,String>("that")
        }

        // Original value multiplied by two
        assertEquals(ran6Map.orElse(5),12)

        //No failure to return, success always returns value
        assertEquals(ran6Mapfail.orElse(5),6)
    }

    @Test
    fun `testing flatMap and flatMapFailure return values for error`(){
        //There is no Success so this doesn't return a value
        val ran2Map  = ran2.flatMap {
            val y = it * 2
            Success<Int,String>(y)
        }

        //This allows the failure too be caught and handled as it goes down the pipeline
        val ran2Mapfail  = ran2.flatMapFailure {
            Error<Int,String>("that")
        }

        //This new error now needs to be handled
        val ran2ErrorHandle= ran2Mapfail.orElse{
            if (it == "this") 1
            else -1
        }

        //Error object returns other default value
        assertEquals(ran2Map.orElse(5),5)
        assertEquals(ran2Mapfail.orElse(5),5)

        //The error string was different
        assertEquals(ran2ErrorHandle,-1)
    }

    @Test
    fun `testing Map and MapFailure return values for success`(){
        //This makes the value into another success object to be sent down a pipeline
        val ran6Map  = ran6.map {
            val y = it * 2
            Success<Int,String>(y)
        }

        //This makes the error into another error object to be sent down the pipeline
        val ran6Mapfail  = ran6.mapFailure {
            Error<Int,String>("that")
        }

        //This is a success so failure doesn't have to be handled
        val ran6MapFailureError= ran6Mapfail.orElse{
            it.orElse {
                if (it == "that") 1
                else -1
            }
        }
        //returns value of success mapped to the first successes value
        assertEquals(ran6Map.orElse(Success(5)).orElse(9), 12)

        //no error so just returns value original
        assertEquals(ran6Mapfail.orElse(5),6)
        assertEquals(ran6MapFailureError,6)
    }
    @Test
    fun `testing Map and MapFailure return values for error`(){
        //This is a failure so this doesn't get called
        val ran2Map  = ran2.map {
            val y = it * 2
            Success<Int,String>(y)
        }

        //This maps the error object to another one to be sent down the pipeline
        val ran2Mapfail  = ran2.mapFailure {
            Error<Int,String>("that")
        }

        //The outer error needs to be handled
        val ran2MapFailureError= ran2Mapfail.orElse{
            //this inner error needs to be handled
            it.orElse {
                if (it == "that") 1
                else -1
            }
        }
        //returns value of success unmapped since original is error and new value is unmapped
        assertEquals(ran2Map.orElse(Success(3)).orElse(4), 3)

        //error returns other value
        assertEquals(ran2Mapfail.orElse(5),5)
        assertEquals(ran2MapFailureError,1)
    }
}