package org.kp.consumer.android.ivvsharedlibrary

import org.junit.Assert.*
import org.junit.Test

class EventTest{
    @Test
    fun `when an event is set, then hasBeenHandled is set to false`(){
        val event : Event<Unit> = Event(Unit)
        assertFalse(event.hasBeenHandled)
    }
    @Test
    fun `when an event is used, then hasBeenHandled is set to true`(){
        val event : Event<Unit> = Event(Unit)
        event.getContentIfNotHandled()
        assertTrue(event.hasBeenHandled)
    }
    @Test
    fun `when an event is set or used, then peekcontent is not null`(){
        val event : Event<Unit> = Event(Unit)
        event.getContentIfNotHandled()
        assertNotNull(event.peekContent())
    }
    @Test
    fun `when an event is not used, then getContent does not return null`(){
        val event : Event<Unit> = Event(Unit)
        assertNotNull(event.getContentIfNotHandled())
    }
    @Test
    fun `when an event is used, then getContent returns null`(){
        val event : Event<Unit> = Event(Unit)
        event.getContentIfNotHandled()
        assertNull(event.getContentIfNotHandled())
    }
}