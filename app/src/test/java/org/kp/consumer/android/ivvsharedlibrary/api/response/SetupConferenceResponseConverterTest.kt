package org.kp.consumer.android.ivvsharedlibrary.api.response

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.kp.consumer.android.ivvsharedlibrary.utilities.FileUtils


class SetupConferenceResponseConverterTest {

    @Test
    fun `given successful json response from setup conference api, when the response is converted, then the appropriate objects will be populated`() {

        val converter = SetupConferenceResponseConverter()

        val response =
            converter.convert(FileUtils.readFromResources("/SetupConferenceResponse.json"))

        val appt = response.appointment
        assertEquals("VV1007597051218", appt.id)
        assertEquals("UCI", appt.idType)
        assertEquals("2019-01-25T23:22:00-07:00", appt.apptDateTime)
    }
}