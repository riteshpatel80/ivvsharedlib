package org.kp.consumer.android.ivvsharedlibrary.service

import android.app.Service
import android.content.Intent
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import junit.framework.Assert.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.kp.consumer.android.ivvsharedlibrary.api.response.RefreshTokenResponse
import org.kp.consumer.android.ivvsharedlibrary.data.source.DataSource
import org.kp.consumer.android.ivvsharedlibrary.data.source.Repository
import org.kp.consumer.android.ivvsharedlibrary.util.GetMockResponse
import org.kp.consumer.android.ivvsharedlibrary.utilities.TestCoroutineRule
import org.kp.consumer.android.ivvsharedlibrary.utilities.any
import org.kp.consumer.android.ivvsharedlibrary.utilities.capture
import org.mockito.*
import org.mockito.Mockito.*
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.android.controller.ServiceController
import org.robolectric.annotation.Config


/**
 *  Unit test for the implementation of [RefreshTokenService]
 *
 * Created by venkatakalluri on 2020-02-13.
 * Copyright © 2019 Kaiser Permanente. All rights reserved.
 */

const val VERSION_CODE_P = 28
@RunWith(RobolectricTestRunner::class)
@Config(sdk = [VERSION_CODE_P])
class RefreshTokenServiceTest {


    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()


    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private lateinit var remoteRepository: Repository

    @Captor
    private lateinit var getRefreshTokenResponseCallback: ArgumentCaptor<DataSource.GetCallback<RefreshTokenResponse>>

    private lateinit var refreshTokenService: RefreshTokenService
    private lateinit var controller :ServiceController<RefreshTokenService>

    private lateinit var mockResponse: GetMockResponse


    @Before
    fun setupRefreshTokenTest(){
        MockitoAnnotations.initMocks(this)
        controller = Robolectric.buildService(RefreshTokenService::class.java)
        refreshTokenService = controller.create().get();

        mockResponse = GetMockResponse();


        `when`(remoteRepository.dataSource.setupConferenceResponse?.room!!).thenReturn(mockResponse.getSetUpConferenceResponse().room)
        `when`(remoteRepository.dataSource._pexipToken).thenReturn("pexipToken")
    }

    @Test
    fun testWithIntent() {
      assertEquals(Service.START_NOT_STICKY , refreshTokenService.onStartCommand(Intent(),0, 0))
    }


    @Test
    fun executeRefreshTokenRequest(){

        refreshTokenService.refreshCall(remoteRepository)

        testCoroutineRule.runBlockingTest {
            verify<Repository>(remoteRepository).refreshToken(any(), anyString(), capture(getRefreshTokenResponseCallback))
        }

        getRefreshTokenResponseCallback.value.onSuccess(RefreshTokenResponse("name","token","100",ArrayList()))


    }

    @After
    fun tearDown() {
        controller.destroy()
    }

}