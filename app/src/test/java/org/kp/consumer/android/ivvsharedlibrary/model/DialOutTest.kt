package org.kp.consumer.android.ivvsharedlibrary.model

import org.junit.Assert.*
import org.junit.Test

class DialOutTest{

    @Test
    fun `verify values are assigned correctly`(){
        val dialOut : DialOut = DialOut(
            "number",
            "displayName" ,
            "role")
        assertEquals(dialOut.number,"number")
        assertEquals(dialOut.displayName,"displayName")
        assertEquals(dialOut.role,"role")
    }
}