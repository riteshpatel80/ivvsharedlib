package org.kp.consumer.android.ivvsharedlibrary.model

import org.junit.Assert.*
import org.junit.Test

class SignedInUserTest{

    @Test
    fun `verify values are assigned correctly`(){
        val signedInUser : SignedInUser = SignedInUser(
            "id" ,
            "idType" ,
            "name" ,
            "role" ,
            "region" ,
            "languagePreference",
            "applicationId",
            "channelId" ,
            "registerId",
            "pexipParticipantUUID")
        assertEquals(signedInUser.id,"id")
        assertEquals(signedInUser.idType,"idType")
        assertEquals(signedInUser.name,"name")
        assertEquals(signedInUser.role,"role")
        assertEquals(signedInUser.region,"region")
        assertEquals(signedInUser.languagePreference,"languagePreference")
        assertEquals(signedInUser.applicationId,"applicationId")
        assertEquals(signedInUser.channelId,"channelId")
        assertEquals(signedInUser.registerId,"registerId")
        assertEquals(signedInUser.pexipParticipantUUID,"pexipParticipantUUID")
    }
}