package org.kp.consumer.android.ivvsharedlibrary.data.source

import com.here.oksse.ServerSentEvent
import okhttp3.Request
import okhttp3.Response
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.JoinConferenceRequestModel
import org.kp.consumer.android.ivvsharedlibrary.api.requestConfig.LeaveConferenceRequestModel
import org.kp.consumer.android.ivvsharedlibrary.api.response.*
import org.kp.consumer.android.ivvsharedlibrary.model.DialOut
import org.kp.consumer.android.ivvsharedlibrary.model.Room
import org.mockito.Mock
import org.mockito.Mockito
import org.webrtc.SessionDescription

val dataSource = Mockito.mock(RemoteDataSource::class.java)

val setupCallback = Mockito.mock((object : DataSource.GetCallback<SetupConferenceResponse> {
    override fun onSuccess() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSuccess(value: SetupConferenceResponse) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
})::class.java)

val tokenCallback = Mockito.mock(object : DataSource.GetCallback<RequestTokenResponse> {
    override fun onSuccess() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSuccess(value: RequestTokenResponse) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}::class.java)

val serverSentLister = Mockito.mock(object : ServerSentEvent.Listener {
    override fun onOpen(sse: ServerSentEvent?, response: Response?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRetryTime(sse: ServerSentEvent?, milliseconds: Long): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onComment(sse: ServerSentEvent?, comment: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRetryError(
        sse: ServerSentEvent?,
        throwable: Throwable?,
        response: Response?
    ): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onPreRetry(sse: ServerSentEvent?, originalRequest: Request?): Request {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onMessage(
        sse: ServerSentEvent?,
        id: String?,
        event: String?,
        message: String?
    ) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onClosed(sse: ServerSentEvent?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}::class.java)

val room = Mockito.mock(Room::class.java)

val refreshCallback = Mockito.mock(object : DataSource.GetCallback<RefreshTokenResponse> {
    override fun onSuccess() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSuccess(value: RefreshTokenResponse) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}::class.java)

val sessionDescription = Mockito.mock(SessionDescription::class.java)

val callResponseCallback = Mockito.mock(object : DataSource.GetCallback<CallResponse> {
    override fun onSuccess() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSuccess(value: CallResponse) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}::class.java)

val dialout = Mockito.mock(DialOut::class.java)

val dialoutCallback = Mockito.mock(object : DataSource.GetCallback<DialOutResponse> {
    override fun onSuccess() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSuccess(value: DialOutResponse) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}::class.java)

val baseCallBack = Mockito.mock(object : DataSource.GetCallback<VVBaseResponse> {
    override fun onSuccess() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSuccess(value: VVBaseResponse) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}::class.java)

val participantsCallback = Mockito.mock(object : DataSource.GetCallback<GetParticipantsResponse> {
    override fun onSuccess() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSuccess(value: GetParticipantsResponse) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}::class.java)

val joinModel = Mockito.mock(JoinConferenceRequestModel::class.java)

val joinCallback = Mockito.mock(object : DataSource.GetCallback<JoinConferenceResponse> {
    override fun onSuccess() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSuccess(value: JoinConferenceResponse) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}::class.java)


val leaveModel = Mockito.mock(LeaveConferenceRequestModel::class.java)

val leaveCallback = Mockito.mock(object : DataSource.GetCallback<LeaveConferenceResponse> {
    override fun onSuccess() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSuccess(value: LeaveConferenceResponse) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}::class.java)
