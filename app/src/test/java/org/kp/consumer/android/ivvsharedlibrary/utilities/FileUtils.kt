package org.kp.consumer.android.ivvsharedlibrary.utilities

class FileUtils {
    companion object {
        fun readFromResources(fileName: String) = this.javaClass::class.java.getResource(fileName)!!.readText(Charsets.UTF_8)
    }
}