package org.kp.consumer.android.ivvsharedlibrary.util

import org.junit.Assert.*
import org.junit.Test

class VVLogLevelTest{
    @Test
    fun `test log levels`(){
        val info = VVLogLevel.info
        val debug = VVLogLevel.debug
        val verbose = VVLogLevel.verbose
        assertEquals(VVLogLevel.valueOf("info"),info)
        assertEquals(VVLogLevel.valueOf("debug"),debug)
        assertEquals(VVLogLevel.valueOf("verbose"),verbose)
    }
}