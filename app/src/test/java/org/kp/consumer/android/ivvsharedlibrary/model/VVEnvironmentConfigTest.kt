package org.kp.consumer.android.ivvsharedlibrary.model

import org.junit.Assert.*
import org.junit.Test
import org.kp.consumer.android.ivvsharedlibrary.util.TEST_SHOULD_HAVE_THROWN_EXEPTION
import java.net.URL

class VVEnvironmentConfigTest{

    @Test
    fun `verify values are assigned correctly`(){
        val vvEnvironmentConfig : VVEnvironmentConfig = VVEnvironmentConfig(
            "mTitle"  ,
            URL("https://www.mBaseURL.com")  ,
            "mEnvLabel"  ,
            "mClientId" )
        assertEquals(vvEnvironmentConfig.baseURL,"https://www.mBaseURL.com")
        assertEquals(vvEnvironmentConfig.envLabel,"mEnvLabel")
        assertEquals(vvEnvironmentConfig.clientId,"mClientId")
    }

    @Test
    fun `verify fails given http`(){
        try {
            val vvEnvironmentConfig: VVEnvironmentConfig = VVEnvironmentConfig(
                "mTitle",
                URL("http://www.mBaseURL.com"),
                "mEnvLabel",
                "mClientId"
            )
            fail(TEST_SHOULD_HAVE_THROWN_EXEPTION)
        } catch (e: IllegalArgumentException){}
    }
}