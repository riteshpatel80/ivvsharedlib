package org.kp.consumer.android.ivvsharedlibrary.model

import org.junit.Assert.*
import org.junit.Test

class WebConfigTest{

    @Test
    fun `verify values are assigned correctly`(){
        val webConfig : WebConfig = WebConfig(
            HttpConfig(
                RenewToken(1),
                WebViewURL(
                    Help("url"))
            ))
        assertEquals(webConfig.httpConfig.renewToken.internval,1)
        assertEquals(webConfig.httpConfig.webView.help.baseUrl,"url")
    }
}