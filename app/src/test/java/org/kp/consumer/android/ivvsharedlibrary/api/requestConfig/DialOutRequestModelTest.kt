package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig

import org.junit.Test

import org.junit.Assert.*
import org.kp.consumer.android.ivvsharedlibrary.data.source.dialout

class DialOutRequestModelTest {

    @Test
    fun getDialOut() {
        val dialOut = dialout
        val model = DialOutRequestModel(dialOut)
        assertEquals(model.dialOut, dialOut)
    }
}