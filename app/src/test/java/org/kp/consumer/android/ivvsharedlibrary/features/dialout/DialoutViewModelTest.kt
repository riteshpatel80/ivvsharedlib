package org.kp.consumer.android.ivvsharedlibrary.features.dialout

import android.content.ContentResolver
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.*
import org.junit.Assert.assertEquals
import org.kp.consumer.android.ivvsharedlibrary.api.response.ResponseError
import org.kp.consumer.android.ivvsharedlibrary.api.response.ResponseResult
import org.kp.consumer.android.ivvsharedlibrary.data.source.Repository
import org.kp.consumer.android.ivvsharedlibrary.data.source.dialout
import org.kp.consumer.android.ivvsharedlibrary.util.GetMockResponse
import org.kp.consumer.android.ivvsharedlibrary.utilities.LiveDataTestUtil
import org.kp.consumer.android.ivvsharedlibrary.utilities.TestCoroutineRule
import org.kp.consumer.android.ivvsharedlibrary.utilities.any
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations


@ExperimentalCoroutinesApi
class DialoutViewModelTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var mockResponse: GetMockResponse
    @Mock
    lateinit var repository: Repository
    @Mock
    private lateinit var viewModel: DialoutViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        viewModel = DialoutViewModel(repository,Dispatchers.Unconfined)
        mockResponse = GetMockResponse()

    }

    @Test
    fun `given a phone number from the content resolver, parse number into last 10 digit`() {
        Assert.assertEquals("9792678509", viewModel.parseNumber("(979) 267-8509"))
    }

    @Test
    fun `given a phone number with +1 from the content resolver, parse number last into 10 digit`() {
        Assert.assertEquals("9792678509", viewModel.parseNumber("+1 (979) 267-8509"))
    }

    @Test
    fun `given a phone number with less than 10 digits, return number`() {
        Assert.assertEquals("+1 (979) 267", viewModel.parseNumber("+1 (979) 267"))
    }

    @Test
    fun `when user make a dialOutCall, then repository should make a successful request`(){
        viewModel.addDialOut(dialout)
        viewModel.dialoutCallback.onSuccess(mockResponse.getDialOutResponse())
        val value = LiveDataTestUtil.getValue(viewModel.dialoutResult)
        Assert.assertNotNull(value.getContentIfNotHandled())
    }
    @Test
    fun `when dialout makes a failed service request, dialout event get updated`(){
        viewModel.addDialOut(dialout)
        viewModel.dialoutCallback.onFailure(ResponseResult.ServiceError(ResponseError("severity","type","code","error","errorDescription")))
        val value = LiveDataTestUtil.getValue(viewModel.dialoutResult)
        Assert.assertNotNull(value.getContentIfNotHandled())
    }
    @Test
    fun `when dialout makes a failed request, dialout event get updated`(){
        viewModel.addDialOut(dialout)
        viewModel.dialoutCallback.onFailure(ResponseResult.Failure("errorCode"))
        val value = LiveDataTestUtil.getValue(viewModel.dialoutResult)
        Assert.assertNotNull(value.getContentIfNotHandled())
    }
    @Test
    fun `when content resolver is received, return number`(){
        val mockContentresolver = mock(ContentResolver::class.java)
        val mockCursor = mock(Cursor::class.java)
        `when`(mockContentresolver.query(any(Uri::class.java), any(Array<String>::class.java),
            eq(null),eq(null),eq(null))).thenReturn(mockCursor)
        `when`(mockCursor.moveToNext()).thenReturn(true).thenReturn(false)
        `when`(mockCursor.getString(0)).thenReturn("1111111111")
        val mockIntent = mock(Intent::class.java)
        val mockUri = mock(Uri::class.java)
        `when`(mockIntent.data).thenReturn(mockUri)
        testCoroutineRule.runBlockingTest{
            viewModel.getContact(mockContentresolver,mockIntent){
                assertEquals(it,"1111111111")
            }
        }

    }
}