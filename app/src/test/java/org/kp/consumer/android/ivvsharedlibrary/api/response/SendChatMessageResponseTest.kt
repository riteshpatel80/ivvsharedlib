package org.kp.consumer.android.ivvsharedlibrary.api.response

import org.junit.Assert.*
import org.junit.Test

class SendChatMessageResponseTest{

    @Test
    fun `verify values are assigned correctly`(){
        val model = SendChatMessageResponse("status" ,"response"  ,listOf())
        assertEquals(model.status,"status")
        assertEquals(model.response,"response")
        assertEquals(model.errors.size,0)
    }
}