package org.kp.consumer.android.ivvsharedlibrary.features.chat

import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.kp.consumer.android.ivvsharedlibrary.model.ChatMessageModel

class ChatMessagesDiffCallbackTest {

    private lateinit var callback : ChatMessagesDiffCallback
    private lateinit var firstList: List<ChatMessageModel>
    private lateinit var secondList: List<ChatMessageModel>

    @Before
    fun setup() {
        firstList = createFirstList()
        secondList = createSecondList()
        callback = ChatMessagesDiffCallback(firstList, secondList)
    }

    @Test
    fun `verify items are compared correctly`() {
        assertTrue(callback.areItemsTheSame(0,0))
        assertFalse(callback.areItemsTheSame(0,1))
    }

    @Test
    fun getOldListSize() {
        val oldSize = firstList.size
        assertTrue(callback.oldListSize > 0)
        assertEquals(oldSize, callback.oldListSize)
    }

    @Test
    fun getNewListSize() {
        val newSize = secondList.size
        assertTrue(callback.newListSize > 0)
        assertEquals(newSize, callback.newListSize)
    }

    @Test
    fun `verify contents are compared correctly`() {
        assertFalse(callback.areContentsTheSame(0,1))
        assertTrue(callback.areContentsTheSame(1,2))
    }


    private fun createFirstList() : List<ChatMessageModel> {
        val list = ArrayList<ChatMessageModel>()

        list.add(createFirstItem())
        list.add(createSecondItem())

        return list
    }

    private fun createSecondList()  : List<ChatMessageModel> {
        val list = ArrayList<ChatMessageModel>()

        list.add(createFirstItem())
        list.add(createThirdItem())
        list.add(createSecondItem())

        return list
    }

    private fun createFirstItem() : ChatMessageModel {
        val item = ChatMessageModel("My first message", "myType", "myOrigin", "senderUUID")
        item.timestamp = System.currentTimeMillis()
        item.isReceivedMessage = true

        return item
    }

    private fun createSecondItem() : ChatMessageModel {
        val item = ChatMessageModel("Another message", "myType", "differentOrigin", "anotherSenderUUID")
        item.timestamp = System.currentTimeMillis()
        item.isReceivedMessage = true

        return item
    }

    private fun createThirdItem() : ChatMessageModel {
        val item = ChatMessageModel("Third message", "differntType", "myOrigin", "senderUUID")
        item.timestamp = System.currentTimeMillis()
        item.isReceivedMessage = false

        return item
    }
}