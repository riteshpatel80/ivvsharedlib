package org.kp.consumer.android.ivvsharedlibrary.api.requestConfig

import org.junit.Assert.*
import org.junit.Test

class SendChatMessageModelTest{

    @Test
    fun `verify values are assigned correctly`(){
        val model = SendChatMessageModel("messageBody","type")
        assertEquals(model.messageBody,"messageBody")
        assertEquals(model.type,"type")
    }
}