package org.kp.ivv.testharness

import com.google.gson.annotations.SerializedName

data class InviteeResponse(

    @SerializedName("invitee")
    val invitee : CompleteInvitee
)