package org.kp.ivv.testharness

import com.google.gson.annotations.SerializedName

data class GuestBody (

    @SerializedName("signedInUser")
    val signedInUser: SignedInUser,

    @SerializedName("applicationId")
    val applicationId: String = "KP_PROVIDER_MOBILE_APP",

    @SerializedName("InviteMeetingURL")
    val InviteMeetingURL: String,

    @SerializedName("channelId")
    val channelId: String = "MOBILE"
) {

}
