package org.kp.ivv.testharness

import com.google.gson.annotations.SerializedName

class RequestTokenBody(

    @SerializedName("status")
    val status: String,

    @SerializedName("result")
    val result: RequestTokenResult

) {

}
