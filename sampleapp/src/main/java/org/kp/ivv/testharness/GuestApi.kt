package org.kp.ivv.testharness

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface GuestApi {

    @POST("/service/diag_thrpy/virtualcare/eCC/ivvtogo/ivvregistration/v1/register/guest")
    fun getGuestToken(
        @Header("Authorization") auth: String,
        @Header("X-IBM-Client-Id") client: String = IVVxIBMClientID,
        @Header("Content-Type") content: String = CONTENT_TYPE,
        @Header("ivvr-envlbl") envlbl: String = ENVLBL,
        @Body guestBody: GuestBody
    ): Call<GuestResponse>

}