package org.kp.ivv.testharness

import com.google.gson.annotations.SerializedName

data class AccessTokenBody(
    @SerializedName("grant_type")
    val grant_type : String = "password",
    @SerializedName("client_id")
    val client_id : String = "cconnect",
    @SerializedName("client_secret")
    val client_secret : String = "026bf54c-3a90-42ee-ac7e-fb10cf98aa4d",
    @SerializedName("scope")
    val scope : String = "openid cconnect ivvtogo_profile ivvtogo",
    @SerializedName("username")
    val username : String = "A816494",
    @SerializedName("password")
    val password : String = "mmQQmm11"
){

}