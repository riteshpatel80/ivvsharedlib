package org.kp.ivv.testharness

import com.google.gson.annotations.SerializedName

class RequestTokenResult(

    @SerializedName("participant_uuid")
    val participant_uuid: String) {

}
