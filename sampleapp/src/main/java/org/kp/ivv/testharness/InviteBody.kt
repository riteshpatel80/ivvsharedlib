package org.kp.ivv.testharness

import com.google.gson.annotations.SerializedName

data class InviteBody (

    @SerializedName("appointment")
    val appointment: Appointment,

    @SerializedName("signedInUser")
    val signedInUser: CompleteSignInUser,

    @SerializedName("invitee")
    val invitee: Invitee = Invitee()

){}
