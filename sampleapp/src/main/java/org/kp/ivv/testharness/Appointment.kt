package org.kp.ivv.testharness

import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat
import java.util.*


data class Appointment(

    @SerializedName("appointmentId")
    val id: String = "abc113021",

    @SerializedName("appointmentIdType")
    val idType: String = "UCI",

    @SerializedName("appointmentDateTime")
    val apptDateTime: String = "${currentDate}T15:22:00-0800",

    @SerializedName("appointmentVisitType")
    val visitType: String = "C2C",

    @SerializedName("appointmentRegion")
    val region: String = "MID",

    @SerializedName("appointmentDuration")
    val appointmentDuration: String = "30",

    @SerializedName("appointmentMeetingRoomNumber")
    val appointmentMeetingRoomNumber: String = "VV1008788051718",


    @SerializedName("provider")
    val providerList: List<Provider> = listOf(Provider())
)
{
    companion object{
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        val currentDate = sdf.format(Date())

    }
}