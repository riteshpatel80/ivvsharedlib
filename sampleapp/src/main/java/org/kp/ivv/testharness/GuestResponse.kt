package org.kp.ivv.testharness

import com.google.gson.annotations.SerializedName

data class GuestResponse(

    @SerializedName("signedInUser")
    val signedInUser: SignedInUser,
    @SerializedName("vvConferenceToken")
    val vvConferenceToken: String,
    @SerializedName("tokenExpirationDateTime")
    val tokenExpirationDateTime: String,
    @SerializedName("vvConferenceURL")
    val vvConferenceURL: String
){
}