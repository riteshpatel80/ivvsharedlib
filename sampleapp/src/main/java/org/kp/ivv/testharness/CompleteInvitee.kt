package org.kp.ivv.testharness

import com.google.gson.annotations.SerializedName

data class CompleteInvitee(

    @SerializedName("name")
    val name: String,

    @SerializedName("notificationId")
    val notificationId: String,

    @SerializedName("inviteLink")
    val inviteLink: String,

    @SerializedName("notificationType")
    val notificationType: String,

    @SerializedName("notificationTrackable")
    val notificationTrackable: String,

    @SerializedName("source")
    val source: String,

    @SerializedName("idType")
    val idType: String,

    @SerializedName("idVal")
    val idVal: String
){}