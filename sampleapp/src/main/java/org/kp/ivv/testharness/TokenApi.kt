package org.kp.ivv.testharness

import retrofit2.Call
import retrofit2.http.*

interface TokenApi {
    @FormUrlEncoded
    @POST("token")
    fun getAccessToken(@Field ("grant_type") grant_type: String = "password",
                       @Field ("client_id") client_id: String = "cconnect",
                       @Field ("client_secret") client_secret: String = "026bf54c-3a90-42ee-ac7e-fb10cf98aa4d",
                       @Field ("scope") scope: String = "openid cconnect ivvtogo_profile ivvtogo",
                       @Field ("username") username: String = "A816494",
                       @Field ("password") password: String = "mmQQmm11") : Call<AccessToken>

}