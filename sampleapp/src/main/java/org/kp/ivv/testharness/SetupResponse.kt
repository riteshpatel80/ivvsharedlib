package org.kp.ivv.testharness

import com.google.gson.annotations.SerializedName

class SetupResponse (

    @SerializedName("appointment")
    val appointment: Appointment,

    @SerializedName("signedInUser")
    val signedInUser: CompleteSignInUser,

    @SerializedName("room")
    val room: Room

    )
