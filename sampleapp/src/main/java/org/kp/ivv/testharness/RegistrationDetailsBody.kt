package org.kp.ivv.testharness

import com.google.gson.annotations.SerializedName


data class RegistrationDetailsBody (

    @SerializedName("appointment")
    val appointment: Appointment = Appointment(),
    @SerializedName("signedInUser")
    val signedInUser: SignedInUser = SignedInUser(),
    @SerializedName("applicationId")
    val applicationId: String = "KP_PROVIDER_MOBILE_APP",
    @SerializedName("channelId")
    val channelId: String = "MOBILE"

){}
