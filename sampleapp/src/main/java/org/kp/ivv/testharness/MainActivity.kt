package org.kp.ivv.testharness


import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

import kotlinx.coroutines.*
import org.kp.consumer.android.ivvsharedlibrary.BuildConfig.VERSION_NAME
import org.kp.consumer.android.ivvsharedlibrary.controller.VVSessionController.initialize
import org.kp.consumer.android.ivvsharedlibrary.controller.VVSessionController.startVV
import org.kp.consumer.android.ivvsharedlibrary.model.VVClientInfo
import org.kp.consumer.android.ivvsharedlibrary.model.VVEnvironmentConfig
import org.kp.consumer.android.ivvsharedlibrary.model.VVEventHandler
import java.net.URL
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import okhttp3.OkHttpClient
import org.kp.kpnetworking.httpclients.okhttp.KPCookieJar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


import okhttp3.logging.HttpLoggingInterceptor
import com.google.gson.JsonObject

import org.kp.consumer.android.ivvsharedlibrary.controller.VVSessionController.endVV
import org.kp.consumer.android.ivvsharedlibrary.service.RefreshTokenService
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity() {

    private val activityJob = Job()

    private val uiScope = CoroutineScope(Dispatchers.Main + activityJob)

    private val guestStringToBeRemoved =
        "https://hint1.kaiserpermanente.org/AgentRedirect?audience=provider&action=joinPexipAppt&amp;"
    private val FOREGROUND_CHANNEL_ID = "foreground_channel_id"
    private var mNotificationManager: NotificationManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        sharedLibVersion.text = VERSION_NAME


        var tenantInfo = VVClientInfo(
            resources.getResourceName(R.string.app_name),
            "" + BuildConfig.VERSION_CODE
        )
//        var envConfig = VVEnvironmentConfig(
//            "dev",
//            URL("https://apiservice-bus-dev.kaiserpermanente.org/service/diag_thrpy/virtualcare/ivv/nativeconference/v1"),
//            "hreg1",
//            "fe45204b-d9b6-4df5-9535-80bfccc2f5f6"
//        )

        var envConfig = VVEnvironmentConfig(
            "qa",
            URL("https://apiservice-bus-qa.kaiserpermanente.org/service/diag_thrpy/virtualcare/ivv/nativeconference/v1"),
            "HPPIDC",
            " 8a300e77-74fb-4522-9831-0c376c18f4c3"
        )


        initialize(this, envConfig, tenantInfo, object : VVEventHandler {
            override fun didEndVV() {
                Log.d(TAG, "event : didEndVV")
                // Show ActionBar
                if (supportActionBar != null) {
                    supportActionBar?.show()
                }
                vmsValue.setText("")
                showAccessibility()

            }

            override fun didJoinVV() {
                Log.d(TAG, "event : didJoinVV")
            }

            override fun lowBandwidth() {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onError() {
                Log.d(TAG, "event : onError")
                // Show ActionBar
                if (supportActionBar != null) {
                    supportActionBar?.show()
                }
                vmsValue.setText("")
                showAccessibility()
            }

            override fun onSessionStarted() {
                Log.d(TAG, "event : onSessionStartee")
            }

            override fun willEndVV() {
                Log.d(TAG, "event : willEndVV")
            }

            override fun willJoinVV() {
                Log.d(TAG, "event : willJoinVV")
            }


        })


        appointmentID.setText(Appointment().id)
        userID.setText(SignedInUser().id)
        host.setOnClickListener {

            getToken()
        }

        guest.setOnClickListener {
            getAccessToken { accessToken ->
                getRegistrationToken(accessToken) { registerBody ->
                    getGuestCode(
                        registerBody.extProviderVidMtgLongURL.replace(
                            guestStringToBeRemoved,
                            ""
                        ), accessToken
                    ) {
                        vmsValue.setText(it)
                    }

                }
            }
        }

        findViewById<Button>(R.id.startSession).setOnClickListener {
            val inputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
            uiScope.launch {
                //delay for keyboard
                delay(1000)
                // Hide ActionBar
                if (supportActionBar != null) {
                    supportActionBar?.hide();
                }

                hideAccessibility()
                var vmsToken = vmsValue.text.trim().toString()
                startVV(R.id.videoLayoutID, vmsToken, false, prepareNotification())


            }

        }

    }

    private fun prepareNotification(): Notification {
        // handle build version above android oreo
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O && mNotificationManager!!.getNotificationChannel(
                FOREGROUND_CHANNEL_ID
            ) == null
        ) {

            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel =
                NotificationChannel(FOREGROUND_CHANNEL_ID, RefreshTokenService.name, importance)
            channel.enableVibration(false)
            mNotificationManager!!.createNotificationChannel(channel)

        }

        var intent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(
            this,
            0,
            intent,
            0
        )

        // notification builder
        val notificationBuilder: NotificationCompat.Builder
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            notificationBuilder = NotificationCompat.Builder(this, FOREGROUND_CHANNEL_ID)
        } else {
            notificationBuilder = NotificationCompat.Builder(this)
        }
        notificationBuilder
            .setOngoing(true)
            .setAutoCancel(false)
            .setContentIntent(pendingIntent)

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            notificationBuilder.setSmallIcon(org.kp.consumer.android.ivvsharedlibrary.R.drawable.ic_stat_notification_icon_above_lolipop)
            notificationBuilder.setColor(
                ContextCompat.getColor(
                    this,
                    org.kp.consumer.android.ivvsharedlibrary.R.color.kpblue
                )
            )
        } else {
            notificationBuilder.setSmallIcon(org.kp.consumer.android.ivvsharedlibrary.R.drawable.notification_icon)
        }

        return notificationBuilder.build()
    }


    private fun hideAccessibility() {
        sharedLibVersionPrompt.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_NO
        sharedLibVersion.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_NO
        vmsValue.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_NO
        startSession.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_NO
        appointmentID.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_NO
        host.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_NO
        guest.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_NO
        spinner.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_NO_HIDE_DESCENDANTS
        userID.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_NO_HIDE_DESCENDANTS
        user_id.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_NO_HIDE_DESCENDANTS
        app_id.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_NO_HIDE_DESCENDANTS
    }

    private fun showAccessibility() {
        sharedLibVersionPrompt.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
        sharedLibVersion.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
        vmsValue.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
        startSession.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
        appointmentID.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
        host.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
        guest.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
        spinner.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
        userID.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
        user_id.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
        app_id.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
    }

    var vmsToken: String = ""

    private fun getToken() {
        Log.d(TAG, "token called")
        getAccessToken { accessToken ->
            getRegistrationToken(accessToken) { registerBody ->
                vmsValue.setText(registerBody.vvConferenceToken)
            }

            hideKeyboard()
        }

    }

    private fun getAccessToken(callback: (String) -> Unit) {

        uiScope.launch(Dispatchers.IO) {
            val tokenAPI = createAccess()
            tokenAPI.getAccessToken().enqueue(object : Callback<AccessToken> {
                override fun onResponse(
                    call: Call<AccessToken>,
                    response: Response<AccessToken>
                ) {
                    if (response.isSuccessful) {
                        uiScope.launch {
                            toastCall("access token call success")
                            callback(response.body()!!.access_token)
                        }


                    } else {
                        toastCall(response.errorBody().toString())
                        Log.e(TAG, "access token call unsuccessful ${response.errorBody()}")
                    }
                }

                override fun onFailure(call: Call<AccessToken>, t: Throwable) {
                    Log.e(TAG, "access token call failed", t)
                    toastCall("access token unsuccessful ${t.localizedMessage}")
                }

            })
        }

    }

    private fun toastCall(response: String) {
        Toast.makeText(
            this,
            response,
            Toast.LENGTH_LONG
        ).show()
    }

    var roomAlias: String? = null
    var pexipNode: String? = null
    var roomPin: String? = null
    var pexipUUID: String? = null

    private fun getPexipRequestToken(callback: () -> Unit) {
        val requestTokenApi = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://$pexipNode/api/client/v2/conferences/$roomAlias/")
            .build().create(RequestTokenApi::class.java)

        uiScope.launch(Dispatchers.IO) {
            requestTokenApi.getRequest(roomPin!!).enqueue(object : Callback<RequestTokenBody> {
                override fun onFailure(call: Call<RequestTokenBody>, t: Throwable) {
                    Log.e(TAG, "requestToken call failed", t)
                    Log.e(TAG, "$t")
                    toastCall("requestToken call failed")
                }

                override fun onResponse(
                    call: Call<RequestTokenBody>,
                    response: Response<RequestTokenBody>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        Log.d(TAG, response.body().toString())
                        uiScope.launch {
                            toastCall("requestToken call succeeded")
                            pexipUUID = response.body()!!.result.participant_uuid
                            Log.d(TAG, pexipUUID)
                            if (pexipUUID != null) callback()
                        }
                    } else {
                        Log.e(TAG, "requestToken unsuccessful ${response.raw()}")
                        toastCall("requestToken unsuccessful ${response.message()}")
                    }


                }

            })
        }

    }

    private fun joinCall(accessToken: String, callback: () -> Unit) {
        uiScope.launch(Dispatchers.IO) {
            val createRegisterApi =
                createRegister("https://apis-qa.kp.org/kp/qa/service/diag_thrpy/virtualcare/")
            Log.d(TAG, "register called")
            createRegisterApi.joinConference(
                "Bearer $accessToken",
                joinBody = getJoinBody()
            ).enqueue(object : Callback<JsonObject> {
                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    Log.e(TAG, "join call failed", t)
                }

                override fun onResponse(
                    call: Call<JsonObject>,
                    response: Response<JsonObject>
                ) {

                    if (response.isSuccessful) {
                        Log.d(TAG, response.body().toString())
                        uiScope.launch {
                            toastCall("join call success")
                            callback()
                        }
                    } else {
                        Log.e(TAG, "join unsuccessful ${response.raw()}")
                        toastCall("join unsuccessful ${response.message()}")
                    }


                }

            })
        }

    }

    private fun getRegistrationToken(accessToken: String, callback: (RegisterBody) -> Unit) {
        uiScope.launch(Dispatchers.IO) {
            val createRegisterApi = createRegister()
            Log.d(TAG, "register called")
            createRegisterApi.getRestigration(
                "Basic VFM4MjEyOk1uZXJfKmVtMDlrdw==",
                registrationDetailsBody = getRegistrationBody().apply {
                    signedInUser.apply {
                        id = userID.text.toString()
                        role = spinner.selectedItem.toString()
                    }
                }
            ).enqueue(object : Callback<RegisterBody> {
                override fun onFailure(call: Call<RegisterBody>, t: Throwable) {
                    Log.e(TAG, "register call failed", t)
                    toastCall("register call failed: ${t.localizedMessage}")
                }

                override fun onResponse(
                    call: Call<RegisterBody>,
                    response: Response<RegisterBody>
                ) {

                    if (response.isSuccessful) {
                        Log.d(TAG, response.body().toString())
                        uiScope.launch {
                            callback(response.body()!!)
                            toastCall("register succeeded")
                        }
                    } else {
                        Log.e(TAG, "register unsuccessful ${response.raw()}")
                        toastCall("register unsuccessful ${response.message()}")
                    }


                }

            })
        }

    }


    private fun getJoinBody(): JoinBody {


        return JoinBody(pexipUUID!!)


    }

    private fun getRegistrationBody(): RegistrationDetailsBody {

        var registrationDetailsBody = RegistrationDetailsBody()
        if (appointmentID.text.toString() != Appointment().id) {
            registrationDetailsBody = RegistrationDetailsBody(
                appointment = Appointment(
                    id = appointmentID.text.toString()
                ),
                signedInUser = SignedInUser(
                    role = spinner.selectedItem.toString()
                )
            )
        }

        return registrationDetailsBody
    }

    private fun getInviteeBody(setupResponse: SetupResponse): InviteBody {
        setupResponse.signedInUser.pexipParticipantUUID = pexipUUID!!
        var inviteBody = InviteBody(
            appointment = setupResponse.appointment,
            signedInUser = setupResponse.signedInUser
        )
        return inviteBody

    }

    fun getSetup(vvConferenceToken: String, callback: (SetupResponse) -> Unit) {
        uiScope.launch(Dispatchers.IO) {
            val createRegisterApi =
                createRegister("https://apis-qa.kp.org/kp/qa/service/diag_thrpy/virtualcare/")
            Log.d(TAG, "setup called")
            createRegisterApi.getSetup("Bearer $vvConferenceToken", token = vvConferenceToken)
                .enqueue(object : Callback<SetupResponse> {
                    override fun onFailure(call: Call<SetupResponse>, t: Throwable) {
                        Log.e(TAG, "setup failed", t)
                    }

                    override fun onResponse(
                        call: Call<SetupResponse>,
                        response: Response<SetupResponse>
                    ) {
                        if (response.isSuccessful && response.body() != null) {

                            uiScope.launch {
                                response.body()?.let {
                                    val setupResponse = it
                                    Log.d(TAG, "Setup: $setupResponse")
                                    roomAlias = it.room.alias
                                    pexipNode = it.room.pexipNode
                                    roomPin = it.room.pin
                                    callback(setupResponse)
                                }
                            }
                        }
                    }

                })
        }
    }


    fun getInviteCode(
        setupResponse: SetupResponse,
        vvConferenceToken: String,
        callback: (String) -> Unit
    ) {
        uiScope.launch(Dispatchers.IO) {
            val createRegisterApi =
                createRegister("https://apis-qa.kp.org/kp/qa/service/diag_thrpy/virtualcare/")
            if (vvConferenceToken != "") {
                Log.d(TAG, "inivite called")
                createRegisterApi.getInvite(
                    "Bearer $vvConferenceToken",
                    inviteBody = getInviteeBody(setupResponse)
                ).enqueue(object : Callback<InviteeResponse> {
                    override fun onFailure(call: Call<InviteeResponse>, t: Throwable) {
                        Log.e(TAG, "invite failed", t)
                        toastCall("invite failed: ${t.localizedMessage}")
                    }

                    override fun onResponse(
                        call: Call<InviteeResponse>,
                        response: Response<InviteeResponse>
                    ) {
                        if (response.isSuccessful && response.body() != null) {
                            uiScope.launch {
                                response.body()?.let {
                                    Log.d(TAG, it.toString())
                                    val eToken =
                                        it.invitee.inviteLink.removePrefix("https://hreg1.kaiserpermanente.org/AgentRedirect?audience=provider&action=joinPexipAppt&amp;")
                                    Log.d(TAG, eToken)
                                    toastCall("Invite succeeded")
                                    callback(eToken)
                                }
                            }
                        } else {
                            Log.e(TAG, "register unsuccessful ${response.raw()}")
                            toastCall("register unsuccessful ${response.raw()}")
                        }
                    }

                })
            }
        }

    }

    fun getGuestBody(eToken: String): GuestBody {
        val guestBody = GuestBody(
            InviteMeetingURL = eToken,
            signedInUser = SignedInUser(
                id = "Y338393",
                region = "MID",
                idType = "NUID",
                name = "radhika",
                role = spinner.selectedItem.toString()
            )
        )
        return guestBody
    }

    fun getGuestCode(eToken: String, accessToken: String, callback: (String) -> Unit) {
        Log.d(TAG, "guest called")
        uiScope.launch(Dispatchers.IO) {

            val guestApi = createGuest("https://apiservice-bus-qa.kp.org/")

            guestApi.getGuestToken("Bearer $accessToken", guestBody = getGuestBody(eToken))
                .enqueue(object : Callback<GuestResponse> {
                    override fun onFailure(call: Call<GuestResponse>, t: Throwable) {
                        Log.e(TAG, "invite failed", t)
                        toastCall(t.localizedMessage)
                    }

                    override fun onResponse(
                        call: Call<GuestResponse>,
                        response: Response<GuestResponse>
                    ) {
                        if (response.isSuccessful && response.body() != null) {
                            uiScope.launch {
                                response.body()?.let {
                                    Log.d(TAG, it.vvConferenceToken)
                                    callback(it.vvConferenceToken)
                                    toastCall("Guest succeeded")
                                }
                            }
                        } else {

                            Log.e(TAG, response.raw().toString())
                            Log.e(TAG, response.message())
                            toastCall(response.message())

                        }
                    }

                })
        }
    }


    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()

        uiScope.cancel()
    }

    fun hideKeyboard() {
        val inputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(startSession.windowToken, 0)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.refresh -> {
                vmsValue.setText("")
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    companion object {


        val TAG = MainActivity::class.java.simpleName
        val accessTokenURL = "https://ema-qa.kp.org/api/oauth/"
        val registerTokenURL = "https://apis-qa.kp.org/kp/qa/service/diag_thrpy/virtualcare/ivv/"
        val logging = HttpLoggingInterceptor().also {
            // set your desired log level
            it.level = HttpLoggingInterceptor.Level.BODY
        }
        var httpClient = OkHttpClient()
            .newBuilder()
            .cookieJar(KPCookieJar.getInstance())
            .addInterceptor(logging)
            .build()

        fun createAccess(): TokenApi {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(accessTokenURL)
                .client(httpClient)
                .build()

            return retrofit.create(TokenApi::class.java)
        }

        fun createRegister(): RegisterApi {

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .baseUrl(registerTokenURL)
                .build()

            return retrofit.create(RegisterApi::class.java)

        }

        fun createRegister(url: String): RegisterApi {

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .baseUrl(url)
                .build()

            return retrofit.create(RegisterApi::class.java)

        }

        fun createGuest(url: String): GuestApi {

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .baseUrl(url)
                .build()

            return retrofit.create(GuestApi::class.java)

        }


    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        Log.d(MainActivity.TAG, "onBackPressed")
        uiScope.launch {

            endVV()

        }

    }

}