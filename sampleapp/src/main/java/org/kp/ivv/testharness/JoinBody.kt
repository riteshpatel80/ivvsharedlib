package org.kp.ivv.testharness

import com.google.gson.annotations.SerializedName

data class JoinBody (

    @SerializedName("pexipParticipantUUID")
    val pexipParticipantUUID: String
)