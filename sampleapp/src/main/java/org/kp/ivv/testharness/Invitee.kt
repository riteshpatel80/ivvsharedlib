package org.kp.ivv.testharness

import com.google.gson.annotations.SerializedName

data class Invitee (

    @SerializedName("name")
    val name: String = "Consulting clinician name",

    @SerializedName("notificationId")
    val notificationId: String = "4042743855"

){}
