package org.kp.ivv.testharness

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.*

interface RegisterApi {

    @POST("registration/v1/register")
    fun getRestigration(@Header("Authorization") auth : String,
                        @Header("X-IBM-Client-Id") client: String = IVVxIBMClientID,
                        @Header("Content-Type") content: String = CONTENT_TYPE,
                        @Body registrationDetailsBody: RegistrationDetailsBody
    ) : Call<RegisterBody>

    @GET("ivv/nativeconference/v1/setup")
    fun getSetup(@Header("Authorization") auth : String,
                 @Header("X-IBM-Client-Id") client: String = IVVxIBMClientID,
                 @Header("Content-Type") content: String = CONTENT_TYPE,
                 @Header("ivvr-envlbl") envlbl: String = ENVLBL,
                  @Query("token") token : String
    ) : Call<SetupResponse>

    @POST("ivv/nativeconference/v1/invite/clinician")
    fun getInvite(@Header("Authorization") auth : String,
                  @Header("X-IBM-Client-Id") client : String = IVVxIBMClientID,
                  @Header("Content-Type") content : String = "application/json",
                  @Header("ivvr-envlbl") envlbl : String = "hreg1",
                  @Body inviteBody: InviteBody
    ) : Call<InviteeResponse>

    @PUT("ivv/nativeconference/v1/join")
    fun joinConference(@Header("Authorization") auth : String,
                       @Header("X-IBM-Client-Id") client: String = IVVxIBMClientID,
                       @Header("Content-Type") content: String = CONTENT_TYPE,
                       @Header("ivvr-envlbl") envlbl: String = ENVLBL,
                  @Body joinBody: JoinBody
    ) : Call<JsonObject>
}