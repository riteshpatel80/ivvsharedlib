package org.kp.ivv.testharness

import com.google.gson.annotations.SerializedName


data class CompleteSignInUser(

    @SerializedName("id")
    val id : String = "I340993",

    @SerializedName("idType")
    var idType : String = "NUID",

    @SerializedName("name")
    val name : String = "David H Lookner",

    @SerializedName("role")
    val role : String = "PROVIDER",

    @SerializedName("region")
    val region : String = "MID",

    @SerializedName("applicationId")
    var applicationId : String,

    @SerializedName("channelId")
    var channelId : String,

    @SerializedName("registerId")
    var registerId : String,

    @SerializedName("pexipParticipantUUID")
    var pexipParticipantUUID : String
)