package org.kp.ivv.testharness

import com.google.gson.annotations.SerializedName

data class RegisterBody (

    @SerializedName("signedInUser")
    val signedInUser: SignedInUser,
    @SerializedName("vvConferenceToken")
    val vvConferenceToken: String,
    @SerializedName("tokenExpirationDateTime")
    val tokenExpirationDateTime: String,
    @SerializedName("vvConferenceURL")
    val vvConferenceURL: String,
    @SerializedName("extProviderVidMtgLongURL")
    val extProviderVidMtgLongURL: String


    ){}


