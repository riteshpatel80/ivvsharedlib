
package org.kp.ivv.testharness

import com.google.gson.annotations.SerializedName


data class Provider(

    @SerializedName("id")
    val id: String = "W494391",

    @SerializedName("idType")
    val idType: String = "NUID",

    @SerializedName("name")
    val name: String = "prvovider Name",

    @SerializedName("region")
    val region: String = "MID"
)