package org.kp.ivv.testharness

import retrofit2.Call
import retrofit2.http.Header
import retrofit2.http.POST

interface RequestTokenApi {
    @POST("request_token")
    fun getRequest(@Header("pin") pin : String
    ) : Call<RequestTokenBody>
}