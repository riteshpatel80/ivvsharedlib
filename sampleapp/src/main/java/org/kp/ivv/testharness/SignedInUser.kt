package org.kp.ivv.testharness

import com.google.gson.annotations.SerializedName


data class SignedInUser(

    @SerializedName("id")
    var id : String = "I340883",

    @SerializedName("idType")
    var idType : String = "NUID",

    @SerializedName("name")
    val name : String = "David H Lookner",

    @SerializedName("role")
    var role : String = "PROVIDER",

    @SerializedName("region")
    val region : String = "MID"
)