# IVVSharedLIb-android
This is the Android Shared Library

## Example

To run the example project, clone the repo, and run the project.

## Requirements

## Integration

Add MCOE repository:

```ruby
allprojects {
    repositories {
        ...
        maven { url 'https://artifactory-fof.appl.kp.org/artifactory/mcoe_release' }
    }
}
```
Add VideoVisit dependancy:

```ruby
dependencies {
    ...
    implementation 'org.kp.cdts:VideoVisit:0.2.0'
}
```

## Author

Niraj Pendal, niraj.pendal@kp.org

## License
